class DeepScrapper

  # def self.weekly_scrapp
  #   categories = ["refrigerators", "hobs", "dishwashers", "microwaves", "ovens", "washers", "integrables"]
  #
  #   pages      = ['eci', 'puntronic', 'latiendadelpinguino', 'worten', 'mediamarkt','electrocosto',
  #                 'clickelectrodomesticos', 'funnatic', 'carrefour', 'tiendaazul']
  #
  #   date        = Date.today.to_s
  #   categories.each do |category|
  #     pages.each do |page|
  #       headless   = Headless.new
  #       headless.start
  #       browser = Watir::Browser.new :firefox
  #       list    = Scrapper.parse(browser, category, page)
  #       DeepScrapper.scrapp(list, category, date, page, browser)
  #       browser.close
  #       headless.stop
  #     end
  #   end
  # end
  #
  # def self.deactivate_products(date)
  #   ProductPage.where('last_aparition < ?', date).active.all.each do |product|
  #     unless product.out_of_stock
  #       product.out_of_stock = true
  #       product.save!
  #     end
  #   end
  # end

  # def self.update_products_file(date)
  #   products = Product.all
  #   sheets = {}
  #   Axlsx::Package.new do |p|
  #     products.each do |product|
  #       sheet_name = product.category ? product.category.to_sym : :"_"
  #
  #       if (sheet = sheets[sheet_name]).nil?
  #         #puts "sheet_name: [#{sheet_name}]: [#{Product::SPECS[sheet_name]}]"
  #         sheet = p.workbook.add_worksheet(name: sheet_name.to_s)
  #         sheet.add_row (["id", "brand", "name", "ean", "category", "subcategory", "description", "usable", "msrp", "specs"] + (Product::SPECS[sheet_name].nil? ? [] : Product::SPECS[sheet_name]))
  #         sheets[sheet_name] = sheet
  #       end
  #
  #       specs = []
  #       (Product::SPECS[sheet_name].nil? ? [] : Product::SPECS[sheet_name]).each { |spec| specs << product.specs[spec]}
  #
  #       sheet.add_row [product.id, product.brand, product.name, product.ean, product.category, product.subcategory, product.description, product.usable, product.msrp, product.specs.to_json] + specs,
  #                     :types => [:integer, :string, :string, :string, :string, :string, :string, :boolean] # :date, time, :float, :integer, :string, :boolean
  #     end
  #     p.serialize("products_table_#{date}.xlsx")
  #   end
  #   system 'mv', "products_table_#{date}.xlsx", Rails.configuration.route_to_xlsx_files
  # end
  #
  # def self.update_lost_products_file(date)
  #   product_pages = ProductPage.without_product.usable.all
  #   Axlsx::Package.new do |p|
  #     p.workbook.add_worksheet(:name => 'Products not found') do |sheet|
  #       sheet.add_row (["id", "page", "image_url", "url", "shipment", "date", "ean", "product_id", "category", "subcategory", "name",
  #                       "specs", "out_of_stock", "last_aparition", "foul_name", "brand", "usable", "msrp"])
  #       product_pages.each do |product_page|
  #         sheet.add_row [product_page.id, product_page.page, product_page.image_url, product_page.url, product_page.shipment, product_page.date, product_page.ean, product_page.product_id, product_page.category, product_page.product.try(:subcategory),
  #                        product_page.name, product_page.specs, product_page.out_of_stock, product_page.last_aparition, product_page.foul_name],
  #                       :types => [:integer, :string, :string, :string, :string, :date, :string, :integer] # :date, time, :float, :integer, :string, :boolean
  #       end
  #       p.serialize("lost_products_#{date}.xlsx")
  #     end
  #   end
  #   system 'mv', "lost_products_#{date}.xlsx", Rails.configuration.route_to_xlsx_files
  # end

  # def self.create_lost_products(file)
  #     spreadsheet = Roo::Excelx.new(file, packed: false, file_warning: :ignore)
  #     spreadsheet.each do |row|
  #       p =  {ean:      row[6],
  #             name:     row[8],
  #             brand:    row[11],
  #             category: row[7],
  #             usable:   row[12],
  #             msrp:     row[14]
  #       }
  #       begin
  #         prod             =  Product.create!{p}
  #         product_page     = ProductPage.find row[0]
  #         product_page.ean = row[6]
  #         product_page.product = prod
  #         product_page.save!
  #       rescue => ex
  #         message = ex.message
  #         CsvError.create({source_line: row.to_s, error: message, mode: 'Deep',
  #                          parser: row[1], category: row[7]})
  #       end
  #     end
  # end

  # def self.scrapp(list, category, date, page, browser)
  #   if    page == 'eci'
  #     DeepScrapper.eci(list, category, date, page, browser)
  #   elsif page == 'puntronic'
  #     DeepScrapper.puntronic(list, category, date, page, browser)
  #   elsif page == 'latiendadelpinguino'
  #     DeepScrapper.latiendadelpinguino(list, category, date, page, browser)
  #   elsif page == 'worten'
  #     DeepScrapper.worten(list, category, date, page, browser)
  #   elsif page == 'mediamarkt'
  #     DeepScrapper.mediamarkt(list, category, date, page, browser)
  #   elsif page == 'electrocosto'
  #     DeepScrapper.electrocosto(list, category, date, page, browser)
  #   elsif page == 'clickelectrodomesticos'
  #     DeepScrapper.clickelectrodomesticos(list, category, date, page, browser)
  #   elsif page == 'carrefour'
  #     DeepScrapper.carrefour(list, category, date, page, browser)
  #   elsif page == 'funnatic'
  #     DeepScrapper.funnatic(list, category, date, page, browser)
  #   elsif page == 'tiendaazul'
  #     DeepScrapper.tiendaazul(list, category, date, page, browser)
  #   end
  # end

  # def self.tiendaazul(list, category, date, page, browser)
  #   list.map{|itm| itm[:url] }.each do |prod_url|
  #     browser.goto   prod_url
  #     sleep 3
  #     product_page  = ProductPage.by_url(prod_url).last
  #     doc           = Nokogiri::HTML(browser.html)
  #     price         = doc.css('p#precioconiva').map{|q| q.text}.last.gsub('€','').strip
  #     if product_page
  #       product_page.last_aparition = date
  #       product_page.save
  #     else
  #       brand     = doc.css('div#pb-left-column form#buy_block').text.split('Fabricante:')[1].split('<p')[0]
  #       ean       = doc.text.split('EAN: ')[1].split('<')[0].gsub('<strong>','') if doc.text.include? 'EAN:'
  #       name      = doc.css('div#center_column div#primary_block.clearfix h1').text.strip
  #       image_url = doc.css('div#image-block img#bigpic').map{|q| q['src'] }.first
  #       ProductPage.create({brand: brand, image_url: image_url, date: date, url: prod_url, page: page, ean: ean,
  #                           category: category, name: name})
  #     end
  #   end
  # end

  # def self.funnatic(list, category, date, page, browser)
  #   list.map{|itm| itm[:url] }.each do |prod_url|
  #     browser.goto   prod_url
  #     sleep 3
  #     product_page  = ProductPage.by_url(prod_url).last
  #     doc           = Nokogiri::HTML(browser.html)
  #     price         = doc.css('div.final_price span.value').map{|q| q.text}.last.gsub('€','').strip
  #     specs         = {}
  #     spec_names    = doc.css('div#long_desc.tab-pane.fade.in p').map{|q| q.text}
  #     if product_page
  #       product_page.specs          = specs.to_json
  #       product_page.last_aparition = date
  #       product_page.save
  #     else
  #       brand     = doc.css('div.brand img.img-responsive').map{|q| q['alt'] }.last
  #       ean       = doc.css('div.ean13 span.value').text
  #       name      = doc.css('div.product_page_header div.page-header h1').text.strip
  #       image_url = doc.css('div#product_detailed div.row div.photos.col-sm-4.col-md-5 div.main.thumbnail a img.img-responsive').map{|q| q['src'] }.last
  #       ProductPage.create({brand: brand, image_url: image_url, date: date, url: prod_url, page: page, ean: ean,
  #                           category: category, specs: specs.to_json, name: name})
  #     end
  #   end
  # end

  # def self.carrefour(list, category, date, page, browser)
  #   list.map{|itm| itm[:url] }.each do |prod_url|
  #     browser.goto   prod_url
  #     sleep 3
  #     product_page  = ProductPage.by_url(prod_url).last
  #     doc           = Nokogiri::HTML(browser.html)
  #     price         = doc.css('span.new-price').map{|q| q.text}.last.gsub('€','').strip
  #     specs         = {}
  #     spec_names    = doc.css('div#masinfo.tab-pane.fade.active.in div.row.no-margin.tab_content_inner div.info_table ul.striped li.col-sm-5').map{|q| q.text}
  #     spec_data     = doc.css('div#masinfo.tab-pane.fade.active.in div.row.no-margin.tab_content_inner div.info_table ul.striped li.col-sm-7').map{|q| q.text}
  #     spec_names.each_with_index do |name, i|
  #       specs[name] = spec_data[i]
  #     end
  #     if product_page
  #       product_page.specs          = specs.to_json
  #       product_page.last_aparition = date
  #       product_page.save
  #     else
  #       brand     = doc.text.split('"productBrand":"')[1].split('"')[0]
  #       ean       = doc.text.split('"productEAN":"')[1].split('"')[0]
  #       name      = doc.css('div#mainContent div.row div.col-xs-12.col-sm-9 h1.product_name.filter').text.strip
  #       image_url = doc.css('div.imagenPrincipal img.hidden-sm').map{|q| q['src'] }.last.gsub('//','')
  #       ProductPage.create({brand: brand, image_url: image_url, date: date, url: prod_url, page: page, ean: ean,
  #                           category: category, specs: specs.to_json, name: name})
  #     end
  #   end
  # end

  # def self.clickelectrodomesticos(list, category, date, page, browser)
  #   list.map{|itm| itm[:url] }.each do |prod_url|
  #     browser.goto   prod_url
  #     sleep 3
  #     product_page  = ProductPage.by_url(prod_url).last
  #     doc           = Nokogiri::HTML(browser.html)
  #     price         = doc.css('div.availability-price div#price-img span').map{|q| q.text}
  #     price         = doc.css('div.availability-price div#price-img span').map{|q| q.text}.join.gsub('€', '')
  #     specs         = {}
  #     spec_names    =  doc.css('div.std ul').map{|q| q.text}
  #     spec_names.each do |spec|
  #       spec = spec.split(': ')
  #       specs[spec[0]] = spec[1]
  #     end
  #     if product_page
  #       product_page.specs          = specs.to_json
  #       product_page.last_aparition = date
  #       product_page.save
  #     else
  #       brand     = doc.css('form#product_addtocart_form div.span12 div.row-fluid div.product-shop.span7 span.item h4 a img').map{|q| q['alt']}.last
  #       ean       = doc.css('html body.catalog-product-view.product-calentador-cointra-clasic-cm-5-n div.ma-wrapper div.ma-page div.container div.container-home div.ma-main-container.col2-left-layout div.main div.main-inner div.row-fluid.show-grid div.col-main.span9 div.product-view div.product-collateral.row-fluid table tbody tr td div img').map{|q| q['alt']}.last
  #       ean       = ean.split(' ')[1] if ean
  #       name      = doc.css('form#product_addtocart_form div.span12 div.row-fluid div.product-shop.span7 span.item div.product-name h1 span.fn').text.gsub("\n","").strip
  #       image_url = doc.css('a#ma-zoom1.cloud-zoom img').map{|q| q['src'] }.last
  #       ProductPage.create({brand: brand, image_url: image_url, date: date, url: prod_url, page: page,
  #                           ean: ean, category: category, specs: specs.to_json, name: name})
  #     end
  #   end
  # end


  # def self.electrocosto(list, category, date, page, browser)
  #   list.map{|itm| itm[:url] }.each do |prod_url|
  #     browser.goto   prod_url
  #     sleep 3
  #     product_page  = ProductPage.by_url(prod_url).last
  #     doc           = Nokogiri::HTML(browser.html)
  #     price         = doc.css('span#our_price_display').text.gsub('€','')
  #     specs         = {}
  #     spec_names    = doc.css('tbody#cp-1 tr td.tdi').map{|q| q.text}
  #     spec_data     = doc.css('tbody#cp-1 tr td.tdd').map{|q| q.text}
  #     spec_names.each_with_index do |name, i|
  #       specs[name] = spec_data[i]
  #     end
  #     if product_page
  #       product_page.specs          = specs.to_json
  #       product_page.last_aparition = date
  #       product_page.save
  #     else
  #       brand     = doc.css('div#center_column.center_column.col-xs-12.col-sm-9 div div.primary_block.row div.pb-left-column.col-xs-12.col-sm-4.col-md-5 div.content_manufacture div.manu a.image img').map{|q| q['alt']}.last
  #       ean       = doc.css('p#product_reference span').text
  #       name = doc.css('div#center_column.center_column.col-xs-12.col-sm-9 div div.primary_block.row div.pb-center-column.col-xs-12.col-sm-7 h1').text
  #       image_url = doc.css('span#view_full_size a.jqzoom div.zoomPad img').map{|q| q['src']}.last
  #       ProductPage.create({brand: brand, image_url: image_url, date: date, url: prod_url, page: page,
  #                           ean: ean, category: category, specs: specs.to_json, name: name})
  #     end
  #   end
  # end

  # def self.eci(list, category, date, page, browser)
  #   list.map{|itm| itm[:url] }.each do |prod_url|
  #     browser.goto   prod_url
  #     sleep 3
  #     product_page  = ProductPage.by_url(prod_url).last
  #     doc           = Nokogiri::HTML(browser.html)
  #     price         = doc.css("div.product-price span.current").text.split('€')[0]
  #     specs         = {}
  #     spec_names    = doc.css('div#media-info div.product-features.c12 dl.cb dt').map{|q| q.text}
  #     spec_data     = doc.css('div#media-info div.product-features.c12 dl.cb dd').map{|q| q.text}
  #     spec_names.each_with_index do |name, i|
  #       specs[name] = spec_data[i]
  #     end
  #     if product_page
  #       product_page.specs          = specs.to_json
  #       product_page.last_aparition = date
  #       product_page.save
  #     else
  #       brand     = doc.css('h2.brand a').map{|q| q['title']}.first
  #       ean       = doc.css('span#ean-ref').text
  #       name      = doc.css('div.product-name').text.strip.split('  ')[0]
  #       image_url = doc.css('img#product-image-placer').map{|q| q['src']}.last.gsub('//','')
  #       ProductPage.create({brand: brand, image_url: image_url, date: date, url: prod_url, page: page, ean: ean,
  #                           category: category, specs: specs.to_json, name: name})
  #     end
  #   end
  # end

  # def self.puntronic(list, category, date, page, browser)
  #   list.map{|itm| itm[:url] }.each do |prod_url|
  #     browser.goto   prod_url
  #     sleep 3
  #     product_page  = ProductPage.by_url(prod_url).last
  #     doc           = Nokogiri::HTML(browser.html)
  #     price         = doc.css('form#product_addtocart_form div.product-shop.grid12-5 div.product-type-data div.price-stock div.price-box span.regular-price span.price').text.gsub('€','')
  #     specs         = {}
  #     specs_data    = doc.css('div.box_caracteristicas table tbody tr td').map{|s| s.text}
  #     specs_data.values_at(* specs_data.each_index.select {|i| i.even?}).each_with_index do |name, i|
  #       specs[name] = specs_data.values_at(* specs_data.each_index.select {|i| i.odd?})[i]
  #     end
  #     availability  = doc.css('html').text.include? 'Producto no disponible en nuestro almacen'
  #     if product_page
  #       product_page.specs          = specs.to_json
  #       product_page.out_of_stock   = availability
  #       product_page.last_aparition = date
  #       product_page.save
  #     else
  #       brand     = doc.css('div.box-brand.feature-wrapper.bottom-border a img').map{|q|q['src']}.last.split('/').last.split('.').first
  #       name      = doc.css('div.breadcrumbs div.container ul.grid-full li.product strong').map{|q| q.text}.last
  #       image_url = doc.css('div.zoomContainer div.zoomWindowContainer div.zoomWindow').map{|q| q['style']}[0]
  #       image_url = image_url.split('url').last.gsub("\"","").gsub(')','').gsub('(','').gsub(';','') if image_url
  #       ProductPage.create({brand: brand, image_url: image_url,date: date, url: prod_url, page: page,
  #                           category: category, specs: specs.to_json, name: name, out_of_stock: availability})
  #     end
  #   end
  # end

  # def self.latiendadelpinguino(list, category, date, page, browser)
  #   list.map{|itm| itm[:url] }.each do |prod_url|
  #     browser.goto  prod_url
  #     sleep 3
  #     product_page  = ProductPage.by_url(prod_url).last
  #     doc           = Nokogiri::HTML(browser.html)
  #     price         = doc.css('span#our_price_display').text.gsub('€','').strip.gsub(',','.')
  #     availability  = doc.css('html').text.include? 'Agotado en este momento, consulte disponibilidad enviando un mail a'
  #     if product_page
  #       product_page.out_of_stock   = availability
  #       product_page.last_aparition = date
  #       product_page.save
  #     else
  #       brand     = doc.css('img.nombremarcaimg').map{|q| q['alt']}.last
  #       name      = doc.css('html body#product div.body div#content.set-size div.page-title div div.breadcrumbs').text.split('>').last
  #       image_url = doc.css('img#bigpic').map{|q| q['src']}.last
  #       ProductPage.create({brand: brand, image_url: image_url, date: date, url: prod_url,
  #                           page: page, category: category, name: name, out_of_stock: availability})
  #     end
  #   end
  # end

  # def self.worten(list, category, date, page, browser)
  #   list.map{|itm| itm[:url] }.each do |prod_url|
  #     browser.goto  prod_url
  #     sleep 3
  #     product_page  = ProductPage.by_url(prod_url).last
  #     doc           = Nokogiri::HTML(browser.html)
  #     price         = doc.css('div.price span.price_current').first.text.split('€')[0]
  #     specs         = {}
  #     specs_data    = doc.css('div.tech_details_row div.wrapper div.m_tech_details div.cols div dl dt').map{|w| w.text}
  #     specs_value   = doc.css('div.tech_details_row div.wrapper div.m_tech_details div.cols div dl dd').map{|w| w.text}
  #     specs_data.each_with_index do |data, i|
  #       specs[data] = specs_value[i]
  #     end
  #     if product_page
  #       product_page.specs          = specs.to_json
  #       product_page.last_aparition = date
  #       product_page.save
  #     else
  #       brand     = specs['Marca']
  #       ean       = specs['EAN']
  #       name = doc.css('div.banner_sticky_toolbar_content div.banner_sticky_toolbar_nav span.product_name_sticky').text
  #       image_url = doc.css('div.m_show_case_images div.m_show_case_main_image span.m_show_case_main_image_content img').map{|q| q['src']}.first
  #       ProductPage.create({brand: brand, ean: ean, image_url: image_url, date: date, url: prod_url, page: page,
  #                           category: category, name: name, specs: specs.to_json})
  #     end
  #   end
  # end

  # def self.mediamarkt(list, category, date, page, browser)
  #   list.map{|itm| itm[:url] }.each do |prod_url|
  #     browser.goto  prod_url
  #     sleep 3
  #     product_page  = ProductPage.by_url(prod_url).last
  #     doc           = Nokogiri::HTML(browser.html)
  #     price         = doc.css('div.productInfoPackBuyPrice div.mm-price.media__price.bigprices.active div div[1]').text
  #     specs         = {}
  #     specs_data    = doc.css('div.customTagName').map{|w| w.text.gsub("\n",'').strip}
  #     specs_value   = doc.css('div.customTagValue').map{|w| w.text.gsub("\n",'').strip}
  #     specs_data.each_with_index do |data, i|
  #       specs[data] = specs_value[i]
  #     end
  #     if product_page
  #       product_page.specs          = specs.to_json
  #       product_page.last_aparition = date
  #       product_page.save
  #     else
  #       brand     = doc.css('div.productDetailBrand').text
  #       name = doc.css('div.productDetailInfoTopTitle h1.title.productTitle').text
  #       image_url = doc.css('img.productLargeImage.productDetailImage').map{|q| q['src']}.first
  #       ProductPage.create({brand: brand, image_url: image_url, date: date, url: prod_url, page: page,
  #                           category: category, name: name, specs: specs.to_json})
  #     end
  #   end
  # end

end