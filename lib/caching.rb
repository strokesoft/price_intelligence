# Caching system
module Caching
  def self.flush
    $redis.del($redis.keys('*'))
  end

  # def self.flushdb
  #   $redis.flushdb
  # end
  #
  # def self.flushall
  #   $redis.flushall
  # end

  def get_caching_of(hash, field, type: nil)
    parse_data($redis.hget(hash, field), type)
  end

  def fetch_caching_of(hash, field, force: false, type: nil, expire_at_end_of_day: false)
    if Settings.env.app.caching.enabled
      data = $redis.hget(hash, field)
      if data.nil? || force
        data = yield.to_json
        $redis.hset(hash, field, data)
        $redis.expire(hash, DateTime.current().seconds_until_end_of_day) if expire_at_end_of_day
      end
      parse_data(data, type)
    else
      parse_data(yield.to_json, type)
    end
  end

  def fetch_mcaching_of(hash, field, force: false, expire_at_end_of_day: false)
    if Settings.env.app.caching.enabled
      if !$redis.exists(hash) || force
        $redis.mapped_hmset(hash, yield)
        $redis.expire(hash, DateTime.current().seconds_until_end_of_day) if expire_at_end_of_day
      end
      $redis.hget(hash, field)
    else
      yield[field]
    end
  end

  def parse_data(data, type)
    case type
      when 'Date', 'Time' then
        data.present? ? type.constantize.parse(data) : nil
      when 'Integer' then
        data.to_i
      when 'Float' then
        data.to_f
      else
        JSON.load(data)
    end
  end

  def clear_caching_of(key)
    time = Time.now
    $redis.del(key)
    case key
      when 'calculations'
        set_time_cached("last_clear_caching_of_#{key}", time)
        del_time_cached('last_caching_of_calculations')
        $redis.del('end_of_day')
        $redis.del('map_product_image_url')
    end
    Utils.nlogs("Clear caching of #{key} at #{time}")
  end

  def set_time_cached(key, time = Time.now)
    $redis.set("time__#{key}", time)
  end

  def get_time_cached(key)
    time = $redis.get("time__#{key}")
    Time.parse(time) if time
  end

  def del_time_cached(key)
    $redis.del("time__#{key}")
  end

  def caching_now?(last_caching_of_calculations, previous_updated_at, last_updated_at, interval = 5.minutes)
    return  previous_updated_at &&
      (last_caching_of_calculations.nil? || last_updated_at > last_caching_of_calculations) &&
      (previous_updated_at.to_i == last_updated_at.to_i) &&
      (Time.now - last_updated_at) >= interval
  end

  def caching_calculations(force: false)
    Utils.nlogs("Check caching at #{Time.now} >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    if Settings.app.caching.enabled_calculations
      if force || get_time_cached('last_clear_caching_of_calculations').nil?
        # Forced execution or nonexistent previous caching of the calculations
        Utils.nlogs("Forced caching")
        clear_caching_of('calculations')
        caching_calculations_variations
      else
        # We obtain the highest updated_at of the tables involved
        last_caching_of_calculations = get_time_cached('last_caching_of_calculations')
        last_updated_at = get_last_updated_at
        previous_updated_at = get_time_cached('previous_updated_at')

        # If there is a previous caching date and more than 5 minutes (by default) have passed since
        # the last check with modified data then we cache calculations
        if caching_now?(last_caching_of_calculations, previous_updated_at, last_updated_at, 5.minutes)
          # Caching calculations
          caching_calculations_variations(last_updated_at)
        else
          Utils.nlogs("Do not caching yet, wait...")
          # We save current 'last updated_at of products, product_pages and product_prices'
          set_time_cached('previous_updated_at', last_updated_at)
        end
      end
    else
      Utils.nlogs("The programmed caching of calculations is disabled")
    end
  end

  def get_last_updated_at
    [Product.maximum(:updated_at).to_time,
     ProductPrice.maximum(:updated_at).to_time,
     ProductPage.maximum(:updated_at).to_time].compact.max
  end

  def stop_caching?
    get_time_cached('last_caching_of_calculations').nil? || get_time_cached('stop_caching_of_calculations')
  end

  def caching_calculations_variations(last_updated_at = nil)
    Utils.nlogs("Start caching calculations variations >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    start_at = Time.now
    Utils.nlogs("Start at #{start_at}. Keys in calculations: #{$redis.hkeys('calculations').size}")
    if last_updated_at && get_time_cached('last_clear_caching_of_calculations') < last_updated_at
      clear_caching_of('calculations')
    end

    set_time_cached('last_caching_of_calculations') # We avoid the beginnings of new caching processes

    # Variations
    catch :stop_caching do
      Society.all.each do |society|
        markets = society.reference_ecommerces_slug_and_name.keys
        markets = nil if markets.blank?
        brands = Brand.reference_names(society)
        brands = nil if brands.blank?

        ['available', 'listed'].each do |filter|
          DataGroupedCalculation.date_ranges.each_with_index do |range, idx|
            throw :stop_caching if stop_caching?

            if idx == 0 # Only today
              # Overview without filters
              DataGroupedCalculation.new(range[0], range[1], society, {listed: filter}).fetch_dashboard
              # MAP
              # society.available_ecommerces.each do |ecommerce|
              #   throw :stop_caching if stop_caching?
              #   DataGroupedCalculation.new(range[0], range[1], society, {
              #     page: ecommerce[0], listed: filter
              #   }).fetch_minimum_advertised_price(pages_to_show: markets, brands_to_show: brands)
              # end
            end

            # Overview
            DataGroupedCalculation.new(range[0], range[1], society, {
              listed: filter, page: markets, brand: brands
            }).fetch_dashboard

            society.available_root_categories.each do |category|
              throw :stop_caching if stop_caching?
              # Benchmark and Price Evolution
              DataGroupedCalculation.new(range[0], range[1], society, {
                category: category, page: markets, brand: brands, listed: filter
              }).fetch_benchmark
            end
          end
        end
      end

      # Admin Dashboard
      ['today', 'yesterday'].each do |day|
        range = Date.send(day).beginning_of_day..Date.send(day).end_of_day
        fetch_caching_of('end_of_day', "products_count_#{day}", type: 'Integer', expire_at_end_of_day: true) do
          Product.where(created_at: range).count
        end
        fetch_caching_of('end_of_day', "product_prices_count_#{day}", type: 'Integer', expire_at_end_of_day: true) do
          ProductPrice.where(created_at: range).count
        end
        fetch_caching_of('end_of_day', "product_pages_count_#{day}", type: 'Integer', expire_at_end_of_day: true) do
          ProductPage.where(created_at: range).count
        end
      end
    end

    del_time_cached('previous_updated_at') # We enable the next waiting time interval

    if stop_caching? #Stopped caching
      Utils.nlogs("STOPPED caching dashboard variations >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    else
      set_time_cached('last_caching_of_calculations')
      end_at = Time.now
      Utils.nlogs("End at #{end_at}. Duration: #{end_at - start_at} seconds. Keys in calculations: #{$redis.hkeys('calculations').size}")
      Utils.nlogs("End caching dashboard variations >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    end
  end
end