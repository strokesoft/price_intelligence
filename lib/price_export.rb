class PriceExport


  def self.report(first_date = Date.parse('2017-07-03'), last_date = Date.today)
    sheet_name = "#{first_date}_#{last_date}"

    Axlsx::Package.new do |p|
      p.workbook.add_worksheet(:name => sheet_name) do |sheet|
        head_lefth = ['Brand', 'Name', 'EAN', 'Category', 'Subcategory', 'Msrp']
        head = head_lefth +
            Utils.page_names +
            ['Year', 'Month', 'Day']
        sheet.add_row (head)

        first_date.upto(last_date) { |date|
          lines         = PriceExport.find_data(date, true)
          unified_lines = PriceExport.merge_duplications(lines)
          unified_lines.each do |line|
            row = sheet.add_row line.first(head.size)
          end
        }
      end
      p.serialize("#{sheet_name}_price_benchmark.xlsx")
      system 'mv',"#{sheet_name}_price_benchmark.xlsx", Rails.configuration.route_to_xlsx_files
    end
    "#{Rails.configuration.route_to_xlsx_files}/#{sheet_name}_price_benchmark.xlsx"
  end


  def self.daily_report(day, month, year, advanced = nil)
    date         = [year, month, day].join('-')
    file_date    = [year[2..3], month, day].join()
    Axlsx::Package.new do |p|
      p.workbook.add_worksheet(:name => date) do |sheet|
        head_lefth = ['Brand', 'Name'] + (advanced ? ['EAN'] : []) + ['Category', 'Subcategory', 'Msrp']
        head = head_lefth +
                Utils.page_names +
                ['Year', 'Month', 'Day']
        sheet.add_row (head)
        lines         = PriceExport.find_data(Date.parse(date), advanced)
        unified_lines = PriceExport.merge_duplications(lines)
        unified_lines.each do |line|
          row = sheet.add_row line.first(head.size)
          if advanced
            cell_num = 'A'.ord + head_lefth.size
            (0..PageBase::PAGES.count-1).each { |index| sheet.add_hyperlink :location => line[head.size+index], :ref => "#{(cell_num+index).chr}#{row.index + 1}" if line[head.size+index] }
          end
        end
      end
      p.serialize("#{file_date}_price_benchmark#{advanced ? "_extended" : ""}.xlsx")
      system 'mv',"#{file_date}_price_benchmark#{advanced ? "_extended" : ""}.xlsx", Rails.configuration.route_to_xlsx_files
    end
    "#{Rails.configuration.route_to_xlsx_files}/#{file_date}_price_benchmark#{advanced ? "_extended" : ""}.xlsx"
  end

  def self.find_data(date, advanced = nil, valid_prices_from_date = Date.parse('2017-10-20'))
    lines = []
#      Product.usable.by_category('washers').each do |product|
      Product.usable.each do |product|
        unless product.category.blank?
          #prices        = Hash[*(ProductPage.with_stock.by_product(product.id).all.map{|w| [w.page, w.product_prices.last.price]}).flatten]
          prices        = Hash[*(ProductPage.usable.by_product(product.id).all.map{|w| [w.page, (w.available_price_by_date(date, (date < valid_prices_from_date ? false : true)).nil? ? nil : Utils.clear_price(w.available_price_by_date(date,  (date < valid_prices_from_date ? false : true)).price))]}).flatten]
          needed_prices = PageBase::PAGES.map{|q| prices[q] if prices[q] && (prices[q] != '')}
          unless needed_prices.compact.empty?
            links         = Hash[*(ProductPage.usable.by_product(product.id).all.map{|w| [w.page, (w.available_price_by_date(date, (date < valid_prices_from_date ? false : true)).nil? ? nil : w.url)]}).flatten] if advanced
            needed_links  = PageBase::PAGES.map{|q| links[q] if links[q] && (links[q] != '')} if advanced

            lines << [product.brand, product.name] + (advanced ? [product.ean] : []) + [product.category, product.subcategory, product.msrp] +
                      needed_prices +
                     [date.year, date.month, date.day] +
                      (advanced ? needed_links : [])
            # return lines
          end
        end
      end
    lines
  end

  # Junta resultados para "productos duplicados": productos con el mismo EAN
  def self.merge_duplications(lines)
    page_count       = PageBase::PAGES.count
    names            = lines.map{|l| l[1]}
    duplicated_names = names.select{ |e| names.count(e) > 1 }.uniq
    duplicated_lines = {}
    duplicated_names.map{|name| duplicated_lines[name]  = lines.select{|line| name == line[1]}}
    duplicated_lines.each do |name, dlines|
      joint_lines    = dlines[0].dup
      (4..(page_count + 3)).each do |n|
        joint_lines[n] = dlines.map{|e| e[n] unless e[n].nil?}.compact.max.to_s
      end
      positions = lines.each_with_index.select{|l, i| dlines.include? l}.map{|q| q[1]}
      positions.each do |p|
        lines[p] = joint_lines
      end
    end
    lines.uniq
  end
end