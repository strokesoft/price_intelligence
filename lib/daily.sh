#!/usr/bin/env bash

cd /home/straminaprice/app/current
bundle exec rails runner "DailyScrapJob.perform_later" -e production
