#!/usr/bin/env bash

# load rvm ruby
source /usr/local/rvm/environments/ruby-2.4.1

cd /home/rails/price_intelligence/current
bundle exec rails runner "eval(DeepScrapJob.perform_later)" -e production