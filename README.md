# README

Stramina is a rescued project that makes use of continuous scraping.

The best option to have an updated database for local development is to make a dump of the BD in production.

# STRAMINA (PRICE INTELLIGENCE)

* Ruby version: 2.4.4

* Rails version: 5.1.4

* PostgreSQL 9.6.6 (9.6.8)

* Redis 3.0.5

* This proyect uses [Active Admin](https://activeadmin.info/) gem to manage the backend

* We use bootstrap too for the frontend with the template 'Metronic' (Responsive Admin Dashboard Template)

# Raw Setup
* Set `config/database.yml` and `config/settings/app.yml` up properly from samples.
NOTE: You must configure a .env.local in the root of the application with the Scrapinghub certificate path. You can use as example .env.local.sample. This file in production is .env.production
* Run `bundle install`
* Run `bundle exec rake db:create:all` (creates the databases)

# Docker Setup

You can run `setup_docker.sh` to set up and launch full docker environment (see steeps below). Then you can download production database and restore it on docker environment running `restore_db.sh` script.

1. Copy docker-compose.yml.sample to docker-compose.yml. Ensure that default values of postgresql, redis and rails server ports are free or set custom values.
1. Copy Dockerfile.sample to Dockerfile
1. Run `docker-compose build`
1. Copy config/settings/app.yml.sample to config/settings/app.yml
1. Copy config/database.yml.sample.docker to config/database.yml
1. Setup database, you have 2 options:
   * Use database populated with seeds:
     1. Run `docker-compose run web rake db:create`
     1. Run `docker-compose run web rake db:migrate`
     1. Run `docker-compose run web rake db:seed`
     1. Run `docker-compose up`
   * Use existent database in `backups/lastest.sql`:
     1. Run script `./restore_db.sh`
1. Last, visit localhost:3000 (or your configured value)

# Database
If you make any changes in database you must annotate the changes in the model.

    bundle exec annotate -s -i -p before --exclude tests,fixtures

## Preview emails

* Add new mailers in test/mailers/previews
* You can show previews from: http://localhost:3000/rails/mailers

# Deploy
  This proyect is deployed using the `straminaprice` user.
  
  Set your id_rsa ssh public key on /home/straminaprice/.ssh/authorized_keys
  
  Use ssh-agent to autenticate on Git repository with your local keys (you should have your public key on gitlab already):
  
    ssh-add -L

  or

    ssh-add ~/.ssh/id_rsa
  
  If you get an error like: *Could not open a connection to your authentication agent* run before: `eval "$(ssh-agent)"` (this happends on docker dev envs)

## Deploy sequence
  Additionally to master, there is a specific branch for each stage and you must merge to these branch the desired changes you want to deploy.
  
  Ensure branch to deploy is updated to the commit you want to deploy (Note: this don't merge last updates on master, only update your local branch with remote repository).

    cap 'stage' deploy:prepare

  Stage can be any of the defined ones (currently they are: `production`, `staging`).

  Then, deploy.

    cap 'stage' deploy

  When the deploy finishes a tag will be set in the repo branch.
  
  Then, if there are new e-commerces, it's needed to create them on Ecommerces table. Go to server and run:

    bundle exec rails runner script/stramina/permissions.rb

  Then, if there are new seed data (as page_deparment url_templates), it's needed to populate database. Go to server and run:

    rake db:seed

  WARNING! If you have uncommited changes in your working copy the tag creation will fail and the whole deploy will be
  considered failed. If you just did a prepare you are safe, otherwise, check your working copy before deploying.

## Maintenance

    cap 'stage' deploy:maintenance_on
    cap 'stage' deploy:maintenance_off

## Delayed Jobs

    cap 'stage' delayed_job:start
    cap 'stage' delayed_job:stop
    cap 'stage' delayed_job:restart

  **NOTE:** If you get an error like:

```
bundler: failed to load command: bin/delayed_job (bin/delayed_job)
Errno::EPERM: Operation not permitted
  /home/straminaprice/app/shared/bundle/ruby/2.4.0/gems/daemons-1.2.6/lib/daemons/application.rb:378:in `kill'
  /home/straminaprice/app/shared/bundle/ruby/2.4.0/gems/daemons-1.2.6/lib/daemons/application.rb:378:in `stop'
  /home/straminaprice/app/shared/bundle/ruby/2.4.0/gems/daemons-1.2.6/lib/daemons/application_group.rb:178:in `block (2 levels) in stop_all'
```

  It's probably that `delayed_job` process has been launched as `root`, then `straminaprice` user hasn't permisions to stop or restart it. To solve it, login into services server as root (use `sudo su -` from any sudoer user) and stop de process:

```
# cd /home/straminaprice/app/current
# bundle exec bin/delayed_job stop
```

## Environment URLs

  The URLs for the different environments are:

  * Production Frontend: [https://priceintelligence.stramina.com](priceintelligence.stramina.com)
      * User: straminaprice
      * Server: priceintelligence.stramina.com (stramina.aspgems.com)
      * Domain: stramina.com

  * Production Services (delayed jobs and cron tasks defined on `config/schedule.rb`):
      * User: straminaprice
      * Server: services.priceintelligence.stramina.com (stramina-services.aspgems.com)

  * Staging: [https://staging.priceintelligence.stramina.com](https://staging.priceintelligence.stramina.com) ([https://stramina-price.dev.aspgems.com](https://stramina-price.dev.aspgems.com))
      * User: straminaprice
      * Server: staging.priceintelligence.stramina.com (stramina-int.aspgems.com)
      * Domain: staging.priceintelligence.stramina.com (stramina-price.dev.aspgems.com)
      * Administration access: [https://staging.priceintelligence.stramina.com/admin](https://staging.priceintelligence.stramina.com/admin) ([https://stramina-price.dev.aspgems.com/admin](https://stramina-price.dev.aspgems.com/admin))

      * Admin credentials:
        * user: rfuentes@zoomlabs.es
        * pass: rfuentes
        
      * Basic Auth: see nginx configuration (`/etc/nginx/sites-enabled/*`), and password file (`/etc/nginx/password`). See for [create new users/passwords](https://www.digitalocean.com/community/tutorials/how-to-set-up-password-authentication-with-nginx-on-ubuntu-14-04)

## Monitoring Servers

  Servers are monitorized using *monit* service. Specific configuration can be found on /etc/monit and /etc/monit/conf.d. There are a configuration file for each service to monitorize.

  Monit send e-mail notifications to accounts configured on /root/.forward
  
  
## Backups

  Cron job on /var/spool/cron/crontabs/root
  
  It call /usr/local/sbin/backups all days at 6:00. This script make BBDD backups and store them on Amazon S3 who has a rotation 
  
## Gestión de Secretos

  La gestión de secretos está implementada con la gema [chamber](https://github.com/thekompanee/chamber) que permite encryptar y gestionar propiedades encriptadas.
  
  Para encryptar una propiedad (ver por ejemplo config/settings/proxy.yml) es necesario:
  * Anteponer `_secure_` al nombre de las propiedades. Por ejemplo `proxy_api_key` queda `_secure_proxy_api_key`.
  * Después hay que ejecutar la tarea rake que las encripta: `rake chamber:secure_all` (si estamos usando el entorno Docker, debemos entrar en el contenedor de la applicacion: `docker-compose exec web bash`).
  
  Podremos recuperar el valor real de las propiedades directamente accediendo a ellas desde la consola. En nuestor ejemplo `Settings.env.proxy.proxy_api_key`.
  
  Algunos ficheros implicados:
  * `config/chamber.pem`: clave de desencriptado
  * `config/chamber.pem.pub`: clave de encriptado
  * `config/initializers/0_settings.rm`:  creo que este es el que automatiza el desencriptado al acceder a las propiedades
  * `lib/tasks/chamber.rake`: este vale para encriptar las propiedades de los ficheros