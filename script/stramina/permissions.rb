#bundle exec rails runner script/stramina/permissions.rb
puts("Actions in permissions")

include Caching
clear_caching_of('calculations')

stramina = Society.find_or_create_by(name: 'Stramina', has_all_ecommerces: true, has_all_brands: true, has_all_categories: true, has_filter_by_specs: true)
whirlpool = Society.find_or_create_by(name: 'Whirlpool')
User.where("email like '%zoomlabs.es'").update_all(society_id: stramina.id) if stramina.id
User.where("email like '%whirlpool.com'").update_all(society_id: whirlpool.id) if whirlpool.id

puts("Update brands and categories from products")
Product.all.each{|p| p.check_brand_and_categories} && nil

# Logos brands
puts("Update brand images")
REFERENCE_BRANDS = {'AEG'         => 'brand1.jpg', 'Balay'      => 'brand2.jpg',
                    'Bosch'       => 'brand3.jpg', 'Candy'      => 'brand4.jpg',
                    'Electrolux'  => 'brand5.jpg', 'Hotpoint'   => 'brand6.jpg',
                    'Indesit'     => 'brand7.jpg', 'Siemens'    => 'brand8.jpg',
                    'Teka'        => 'brand9.jpg'}
REFERENCE_BRANDS.each do |key, value|
  brand = Brand.find_or_create_by(name: key)
  if brand
    brand.is_reference = true
    brand.picture = Rails.root.join("app/assets/images/#{value}").open
    brand.save!
  end
end

puts("Update ecommerces")
pages_names = Utils.available_pages.map{|market| [market, "#{PageBase.get_class(market)::PAGE_NAME}"]}.to_h
pages_names.each do |key, value|
  Ecommerce.find_or_create_by(name: value, slug: key)
end

