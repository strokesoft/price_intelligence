# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20210228044324) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"
  enable_extension "unaccent"

  create_table "active_admin_comments", id: :serial, force: :cascade do |t|
    t.string "namespace"
    t.text "body"
    t.integer "resource_id"
    t.string "resource_type"
    t.integer "author_id"
    t.string "author_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace"
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"
  end

  create_table "admin_users", id: :serial, force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admin_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true
  end

  create_table "brands", force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.string "picture"
    t.boolean "is_reference", default: false
    t.string "color"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_brands_on_name"
    t.index ["slug"], name: "index_brands_on_slug"
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.string "ancestry"
    t.string "picture"
    t.integer "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "usable", default: true
    t.index ["ancestry"], name: "index_categories_on_ancestry"
    t.index ["name"], name: "index_categories_on_name"
    t.index ["slug"], name: "index_categories_on_slug"
  end

  create_table "category_brands", force: :cascade do |t|
    t.bigint "category_id"
    t.bigint "brand_id"
    t.index ["brand_id"], name: "index_category_brands_on_brand_id"
    t.index ["category_id", "brand_id"], name: "index_category_brands_on_category_id_and_brand_id", unique: true
    t.index ["category_id"], name: "index_category_brands_on_category_id"
  end

  create_table "csv_errors", id: :serial, force: :cascade do |t|
    t.text "source_line"
    t.string "error"
    t.string "mode"
    t.string "parser"
    t.string "category"
    t.boolean "warn"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "n"
    t.date "date"
  end

  create_table "delayed_jobs", id: :serial, force: :cascade do |t|
    t.integer "priority", default: 0, null: false
    t.integer "attempts", default: 0, null: false
    t.text "handler", null: false
    t.text "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string "locked_by"
    t.string "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["priority", "run_at"], name: "delayed_jobs_priority"
  end

  create_table "ecommerces", force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.string "picture"
    t.boolean "is_reference", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "has_proxy", default: false
    t.string "color"
    t.string "proxy_address"
    t.string "proxy_user"
    t.string "proxy_password"
    t.boolean "selenium_proxy"
    t.index ["name"], name: "index_ecommerces_on_name"
    t.index ["slug"], name: "index_ecommerces_on_slug"
  end

  create_table "hoy", id: false, force: :cascade do |t|
    t.date "date"
  end

  create_table "import_specs", force: :cascade do |t|
    t.string "category"
    t.string "subcategory"
    t.string "spec"
    t.integer "order"
    t.string "page"
    t.string "origin_spec"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["category", "subcategory", "spec", "order"], name: "import_specs_index", unique: true
  end

  create_table "msrps", force: :cascade do |t|
    t.bigint "society_id"
    t.bigint "trademark_id"
    t.bigint "product_id"
    t.decimal "price"
    t.date "on_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_id"], name: "index_msrps_on_product_id"
    t.index ["society_id", "trademark_id", "product_id"], name: "index_msrps_on_society_id_and_trademark_id_and_product_id", unique: true
    t.index ["society_id"], name: "index_msrps_on_society_id"
    t.index ["trademark_id"], name: "index_msrps_on_trademark_id"
  end

  create_table "page_departments", force: :cascade do |t|
    t.string "page", null: false
    t.string "department"
    t.string "category", null: false
    t.string "subcategory"
    t.string "url_template", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "order", default: 1, null: false
    t.integer "pages_max", default: 0, null: false
    t.integer "pages_last", default: 0, null: false
    t.integer "products_listed_max", default: 0, null: false
    t.integer "products_listed_last", default: 0, null: false
    t.integer "products_valid_last", default: 0, null: false
  end

  create_table "product_comments", id: :serial, force: :cascade do |t|
    t.integer "product_id"
    t.integer "product_page_id"
    t.text "comment"
    t.date "date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_id"], name: "index_product_comments_on_product_id"
    t.index ["product_page_id"], name: "index_product_comments_on_product_page_id"
  end

  create_table "product_lexicons", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "ean"
    t.integer "product_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "aproximated"
    t.index ["product_id"], name: "index_product_lexicons_on_product_id"
  end

  create_table "product_offers", id: :serial, force: :cascade do |t|
    t.integer "product_id"
    t.integer "product_page_id"
    t.text "offer"
    t.date "date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_id"], name: "index_product_offers_on_product_id"
    t.index ["product_page_id"], name: "index_product_offers_on_product_page_id"
  end

  create_table "product_pages", id: :serial, force: :cascade do |t|
    t.integer "product_id"
    t.string "page"
    t.string "image_url"
    t.string "url"
    t.string "shipment"
    t.date "date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "ean"
    t.string "category"
    t.string "foul_name"
    t.json "specs"
    t.boolean "out_of_stock"
    t.datetime "last_aparition"
    t.boolean "usable", default: true
    t.string "asin"
    t.string "vendor_part_number"
    t.integer "mediamarkt_ref"
    t.string "foul_brand"
    t.boolean "variant", default: false
    t.string "foul_description"
    t.integer "images_counter"
    t.string "notes"
    t.float "customers_vote_average"
    t.integer "customers_comments_count"
    t.index ["category"], name: "index_product_pages_on_category"
    t.index ["out_of_stock"], name: "index_product_pages_on_out_of_stock"
    t.index ["page"], name: "index_product_pages_on_page"
    t.index ["product_id"], name: "index_product_pages_on_product_id"
    t.index ["usable"], name: "index_product_pages_on_usable"
  end

  create_table "product_pages_2017", id: false, force: :cascade do |t|
    t.integer "id"
    t.integer "product_id"
    t.string "page"
    t.string "image_url"
    t.string "url"
    t.string "shipment"
    t.date "date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "ean"
    t.string "category"
    t.string "foul_name"
    t.json "specs"
    t.boolean "out_of_stock"
    t.datetime "last_aparition"
    t.boolean "usable"
    t.string "asin"
    t.string "vendor_part_number"
    t.integer "mediamarkt_ref"
    t.string "foul_brand"
    t.boolean "variant"
    t.string "foul_description"
    t.integer "images_counter"
    t.string "notes"
    t.float "customers_vote_average"
    t.integer "customers_comments_count"
  end

  create_table "product_pages_2018", id: false, force: :cascade do |t|
    t.integer "id"
    t.integer "product_id"
    t.string "page"
    t.string "image_url"
    t.string "url"
    t.string "shipment"
    t.date "date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "ean"
    t.string "category"
    t.string "foul_name"
    t.json "specs"
    t.boolean "out_of_stock"
    t.datetime "last_aparition"
    t.boolean "usable"
    t.string "asin"
    t.string "vendor_part_number"
    t.integer "mediamarkt_ref"
    t.string "foul_brand"
    t.boolean "variant"
    t.string "foul_description"
    t.integer "images_counter"
    t.string "notes"
    t.float "customers_vote_average"
    t.integer "customers_comments_count"
  end

  create_table "product_prices", id: :serial, force: :cascade do |t|
    t.integer "product_page_id"
    t.integer "order"
    t.float "price", null: false
    t.date "date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "out_of_stock"
    t.index ["date"], name: "index_product_prices_on_date"
    t.index ["id", "date"], name: "index_product_prices_on_id_and_date"
    t.index ["out_of_stock", "date"], name: "index_product_prices_on_out_of_stock_and_date"
    t.index ["out_of_stock"], name: "index_product_prices_on_out_of_stock"
    t.index ["product_page_id"], name: "index_product_prices_on_product_page_id"
  end

  create_table "product_prices_2017", id: false, force: :cascade do |t|
    t.integer "id"
    t.integer "product_page_id"
    t.integer "order"
    t.float "price"
    t.date "date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean "out_of_stock"
  end

  create_table "product_prices_2018", id: false, force: :cascade do |t|
    t.integer "id"
    t.integer "product_page_id"
    t.integer "order"
    t.float "price"
    t.date "date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean "out_of_stock"
  end

  create_table "products", id: :serial, force: :cascade do |t|
    t.string "brand"
    t.string "name"
    t.string "ean"
    t.string "category"
    t.string "subcategory"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "usable", default: true
    t.string "msrp"
    t.json "specs", default: {}
    t.boolean "fixed_specs", default: false
    t.string "slug"
    t.string "asin"
    t.string "vendor_part_number"
    t.integer "mediamarkt_ref"
    t.tsvector "tsv"
    t.index ["brand"], name: "index_products_on_brand"
    t.index ["category"], name: "index_products_on_category"
    t.index ["ean"], name: "index_products_on_ean"
    t.index ["id", "category"], name: "index_products_on_id_and_category"
    t.index ["subcategory"], name: "index_products_on_subcategory"
    t.index ["tsv"], name: "index_products_on_tsv", using: :gin
    t.index ["usable"], name: "index_products_on_usable"
  end

  create_table "sessions", force: :cascade do |t|
    t.string "session_id", null: false
    t.text "data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["session_id"], name: "index_sessions_on_session_id", unique: true
    t.index ["updated_at"], name: "index_sessions_on_updated_at"
  end

  create_table "societies", force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.string "picture"
    t.boolean "has_all_ecommerces", default: false
    t.boolean "has_all_brands", default: false
    t.boolean "has_all_categories", default: false
    t.boolean "has_filter_by_specs", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "min_date", default: "1970-01-01"
    t.string "acronym"
    t.index ["name"], name: "index_societies_on_name"
    t.index ["slug"], name: "index_societies_on_slug"
  end

  create_table "society_brands", force: :cascade do |t|
    t.bigint "society_id"
    t.bigint "brand_id"
    t.boolean "is_reference", default: false
    t.index ["brand_id"], name: "index_society_brands_on_brand_id"
    t.index ["society_id", "brand_id"], name: "index_society_brands_on_society_id_and_brand_id", unique: true
    t.index ["society_id"], name: "index_society_brands_on_society_id"
  end

  create_table "society_categories", force: :cascade do |t|
    t.bigint "society_id"
    t.bigint "category_id"
    t.index ["category_id"], name: "index_society_categories_on_category_id"
    t.index ["society_id", "category_id"], name: "index_society_categories_on_society_id_and_category_id", unique: true
    t.index ["society_id"], name: "index_society_categories_on_society_id"
  end

  create_table "society_ecommerces", force: :cascade do |t|
    t.bigint "society_id"
    t.bigint "ecommerce_id"
    t.boolean "is_reference", default: false
    t.index ["ecommerce_id"], name: "index_society_ecommerces_on_ecommerce_id"
    t.index ["society_id", "ecommerce_id"], name: "index_society_ecommerces_on_society_id_and_ecommerce_id", unique: true
    t.index ["society_id"], name: "index_society_ecommerces_on_society_id"
  end

  create_table "society_trademarks", force: :cascade do |t|
    t.bigint "society_id"
    t.bigint "trademark_id"
    t.index ["society_id", "trademark_id"], name: "index_society_trademarks_on_society_id_and_trademark_id", unique: true
    t.index ["society_id"], name: "index_society_trademarks_on_society_id"
    t.index ["trademark_id"], name: "index_society_trademarks_on_trademark_id"
  end

  create_table "top_sellers", force: :cascade do |t|
    t.bigint "product_page_id"
    t.date "date", null: false
    t.integer "topseller_order", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["date"], name: "index_top_sellers_on_date"
    t.index ["id", "date"], name: "index_top_sellers_on_id_and_date"
    t.index ["product_page_id"], name: "index_top_sellers_on_product_page_id"
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "email"
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "admin", null: false
    t.string "first_name"
    t.string "last_name"
    t.bigint "society_id"
    t.boolean "has_daily_email", default: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["society_id"], name: "index_users_on_society_id"
  end

  add_foreign_key "category_brands", "brands"
  add_foreign_key "category_brands", "categories"
  add_foreign_key "product_comments", "product_pages"
  add_foreign_key "product_comments", "products"
  add_foreign_key "product_lexicons", "products"
  add_foreign_key "product_offers", "product_pages"
  add_foreign_key "product_offers", "products"
  add_foreign_key "product_pages", "products"
  add_foreign_key "product_prices", "product_pages"
  add_foreign_key "society_brands", "brands"
  add_foreign_key "society_brands", "societies"
  add_foreign_key "society_categories", "categories"
  add_foreign_key "society_categories", "societies"
  add_foreign_key "society_ecommerces", "ecommerces"
  add_foreign_key "society_ecommerces", "societies"
  add_foreign_key "society_trademarks", "societies"
  add_foreign_key "top_sellers", "product_pages"
end
