society1 = Society.create(name: "Stramina", slug: "stramina", has_all_ecommerces: true,
                          has_all_brands: true, has_all_categories: true, has_filter_by_specs: true,
                          min_date: "1970-01-01", acronym: "STR")

society2 = Society.create(name: "Whirlpool", slug: "whirlpool", picture: "Whirlpool.jpg",
                           has_all_ecommerces: false, has_all_brands: false, has_all_categories: false,
                           has_filter_by_specs: true, min_date: "2017-07-01", acronym: "WH")

users = [{password: "rfuentes", password_confirmation: "rfuentes", email: "rfuentes@zoomlabs.es",
          admin: true, first_name: 'Raul',  last_name: 'Fuentes', society: society1},
         {password: "Cfavo9sQ", password_confirmation: "Cfavo9sQ", email: "dplanelles@zoomlabs.es",
          admin: true, first_name: 'David', last_name: 'Planelles', society: society1},
         {password: "stramina@18", password_confirmation: "stramina@18", email: "carolina@zoomlabs.es",
          admin: true, first_name: 'Carolina', last_name: 'Igartua', society: society1}]
users.each do |user|
  User.create(user)
end


users = [{ email: "lluis_diaz@whirlpool.com",password: "lluidiaz", password_confirmation: "lluidiaz", society: society2, admin: false},
{ email: "carlos_utrillas@whirlpool.com",password: "c_utrillas", password_confirmation: "c_utrillas", society: society2, admin: false},
{ email: "noha_scrivante@whirlpool.com",password: "nscrivante", password_confirmation: "nscrivante", society: society2, admin: false},
{ email: "maria_c_burbano@whirlpool.com",password: "acburbano", password_confirmation: "acburbano", society: society2, admin: false},
{ email: "agustin_pina@whirlpool.com",password: "stinpina", password_confirmation: "stinpina", society: society2, admin: false},
{ email: "nelson_ungredda_intalentia@whirlpool.com",password: "aintalentia", password_confirmation: "aintalentia", society: society2, admin: false},
{ email: "niccolo_pietrucci@whirlpool.com",password: "opietrucci", password_confirmation: "opietrucci", society: society2, admin: false},
{ email: "david_desongles@whirlpool.com",password: "ddesongles", password_confirmation: "ddesongles", society: society2, admin: false},
{ email: "dayana_tovar@whirlpool.com",password: "anatovar", password_confirmation: "anatovar", society: society2, admin: false},
{ email: "mariano_venero@whirlpool.com",password: "novenero", password_confirmation: "novenero", society: society2, admin: false},
{ email: "armando_anjos@whirlpool.com",password: "ndoanjos", password_confirmation: "ndoanjos", society: society2, admin: false},
{ email: "juan_martinez@whirlpool.com",password: "jmartinez", password_confirmation: "jmartinez", society: society2, admin: false},
{ email: "serafin_perez_santos@whirlpool.com",password: "perezsantos", password_confirmation: "perezsantos", society: society2, admin: false},
{ email: "alberto_carrillo_vicens@whirlpool.com",password: "lovicens", password_confirmation: "lovicens", society: society2, admin: false},
{ email: "antonio_cristobal_luque@whirlpool.com",password: "balluque", password_confirmation: "balluque", society: society2, admin: false}]
users.each do |user|
  User.create(user)
end

page_departments = [
    # { page: "amazontopselleres",   department: "", category: "SSD",  subcategory: "",  url_template: '"https://www.amazon.es/gp/bestsellers/computers/937918031?pg=#{page_index}"'},
    # { page: "amazontopselleres",   department: "", category: "RAM", subcategory: "",  url_template: '"https://www.amazon.es/gp/bestsellers/computers/937922031?pg=#{page_index}"'},
    # { page: "amazontopsellercom",  department: "", category: "SSD",  subcategory: "",  url_template: '"https://www.amazon.com/Best-Sellers-Computers-Accessories-Internal-Solid-State-Drives/zgbs/pc/1292116011?pg=#{page_index}"'},
    # { page: "amazontopsellercom",  department: "", category: "RAM", subcategory: "",  url_template: '"https://www.amazon.com/Best-Sellers-Computers-Accessories-Computer-Memory/zgbs/pc/172500?pg=#{page_index}"'},
    # { page: "amazontopsellerfr",   department: "", category: "SSD",  subcategory: "",  url_template: '"https://www.amazon.fr/gp/bestsellers/computers/430351031?pg=#{page_index}"'},
    # { page: "amazontopsellerfr",   department: "", category: "RAM", subcategory: "",  url_template: '"https://www.amazon.fr/gp/bestsellers/computers/430354031?pg=#{page_index}"'},
    # { page: "amazontopsellerde",   department: "", category: "SSD",  subcategory: "",  url_template: '"https://www.amazon.de/gp/bestsellers/computers/430168031?pg=#{page_index}"'},
    # { page: "amazontopsellerde",   department: "", category: "RAM", subcategory: "",  url_template: '"https://www.amazon.de/gp/bestsellers/computers/430178031?pg=#{page_index}"'},
    # { page: "amazontopsellerit",   department: "", category: "SSD",  subcategory: "",  url_template: '"https://www.amazon.it/gp/bestsellers/pc/460114031?pg=#{page_index}"'},
    # { page: "amazontopsellerit",   department: "", category: "RAM", subcategory: "",  url_template: '"https://www.amazon.it/gp/bestsellers/pc/460119031?pg=#{page_index}"'},
    # { page: "amazontopsellercouk", department: "", category: "SSD",  subcategory: "",  url_template: '"https://www.amazon.co.uk/Best-Sellers-Computers-Accessories-Internal-Solid-State-Drives/zgbs/computers/430505031?pg=#{page_index}"'},
    # { page: "amazontopsellercouk", department: "", category: "RAM", subcategory: "",  url_template: '"https://www.amazon.co.uk/Best-Sellers-Computers-Accessories-RAM/zgbs/computers/430511031?pg=#{page_index}"'},
    # { page: "amazontopsellercojp", department: "", category: "SSD",  subcategory: "",  url_template: '"https://www.amazon.co.jp/gp/bestsellers/computers/2151936051?pg=#{page_index}"'},
    # { page: "amazontopsellercojp", department: "", category: "RAM", subcategory: "",  url_template: '"https://www.amazon.co.jp/gp/bestsellers/computers/2151941051?pg=#{page_index}"'}
    # { page: "xtralife", department: "Videogames", category: "Videogames PS4",   subcategory: "",  url_template: '"https://www.xtralife.es/playstation-4/juegos/todos/0/todos/cant/#{self::PRODUCTSxPAGE}"'},
    # { page: "xtralife", department: "Videogames", category: "Videogames PC",    subcategory: "",  url_template: '"https://www.xtralife.es/pc-cd-rom/juegos/todos/0/todos/cant/#{self::PRODUCTSxPAGE}"'},
    # { page: "xtralife", department: "Videogames", category: "Videogames XBO",   subcategory: "",  url_template: '"https://www.xtralife.es/xbox-one/juegos/todos/0/todos/cant/#{self::PRODUCTSxPAGE}"'},
    # { page: "xtralife", department: "Videogames", category: "Videogames SWC",   subcategory: "",  url_template: '"https://www.xtralife.es/switch/juegos/todos/0/todos/cant/#{self::PRODUCTSxPAGE}"'}

    # { page: "game", department: "Videogames", category: "Videogames PS4",   subcategory: "",  url_template: '"https://www.game.es/buscar/juegos%20ps4%20nuevo/#{page_index-1}"'},
    # { page: "game", department: "Videogames", category: "Videogames PC",    subcategory: "",  url_template: '"https://www.game.es/buscar/juegos-pc-nuevos/#{page_index-1}"'},
    # { page: "game", department: "Videogames", category: "Videogames XBO",   subcategory: "",  url_template: '"https://www.game.es/buscar/juegos-xone/#{page_index-1}"'},
    # { page: "game", department: "Videogames", category: "Videogames SWC",   subcategory: "",  url_template: '"https://www.game.es/buscar/juego%20switch%20nuevo/#{page_index-1}"'}

#     PS4:      https://www.game.es/buscar/juegos%20ps4%20nuevo
#     PC:       https://www.game.es/buscar/juegos-pc-nuevos
#     Xbox one: https://www.game.es/buscar/juegos-xone
#     Switch:   https://www.game.es/buscar/juego%20switch%20nuevo
#
#
    # { page: "playstationstore", department: "Videogames", category: "Videogames PS4",   subcategory: "",  url_template: '"https://store.playstation.com/es-es/grid/STORE-MSF75508-PS4CAT/#{page_index}"'}

    # { page: "mediamarkt", department: "Dishwashers Integrated"   , category: "Dishwashers"      , subcategory: ""               , url_template: '"https://www.mediamarkt.es/es/category/_lavavajillas-integrables-702185.html?page=#{page_index}"'},
    # { page: "mediamarkt", department: "Diswashers Freestanding"  , category: "Dishwashers"      , subcategory: ""               , url_template: '"https://www.mediamarkt.es/es/category/_lavavajillas-libre-instalaci%C3%B3n-702184.html?page=#{page_index}"'},
    # { page: "mediamarkt", department: "Cooking"                  , category: "Hobs"             , subcategory: "Induction"      , url_template: '"https://www.mediamarkt.es/es/category/_encimeras-inducci%C3%B3n-702619.html?page=#{page_index}"'},
    # { page: "mediamarkt", department: "Cooking"                  , category: "Hobs"             , subcategory: "Gas"            , url_template: '"https://www.mediamarkt.es/es/category/_encimeras-de-gas-702620.html?page=#{page_index}"'},
    # { page: "mediamarkt", department: "Cooking"                  , category: "Hobs"             , subcategory: "Ceramic"        , url_template: '"https://www.mediamarkt.es/es/category/_encimeras-vitrocer%C3%A1micas-702621.html?page=#{page_index}"'},
    # { page: "mediamarkt", department: "Computers"                , category: "Laptops"          , subcategory: ""               , url_template: '"https://www.mediamarkt.es/es/category/_port%C3%A1tiles-de-menos-de-14-701421.html?page=#{page_index}"'},
    # { page: "mediamarkt", department: "Computers"                , category: "Laptops"          , subcategory: ""               , url_template: '"https://www.mediamarkt.es/es/category/_port%C3%A1tiles-de-14-a-16-9-701422.html?page=#{page_index}"'},
    # { page: "mediamarkt", department: "Computers"                , category: "Laptops"          , subcategory: ""               , url_template: '"https://www.mediamarkt.es/es/category/_port%C3%A1tiles-desde-17-701423.html?page=#{page_index}"'},
    # { page: "mediamarkt", department: "Computers"                , category: "Laptops"          , subcategory: "Gaming"         , url_template: '"https://www.mediamarkt.es/es/category/_port%C3%A1tiles-gaming-701424.html?page=#{page_index}"'},
    # { page: "mediamarkt", department: "Computers"                , category: "Laptops"          , subcategory: "Convertible"    , url_template: '"https://www.mediamarkt.es/es/category/_convertibles-2-en-1-701426.html?page=#{page_index}"'},
    # { page: "mediamarkt", department: "Cooking"                  , category: "Microwaves"       , subcategory: "Solo"           , url_template: '"https://www.mediamarkt.es/es/category/_microondas-sin-grill-702642.html?page=#{page_index}"'},
    # { page: "mediamarkt", department: "Cooking"                  , category: "Microwaves"       , subcategory: ""               , url_template: '"https://www.mediamarkt.es/es/category/_microondas-integrables-702645.html?page=#{page_index}"'},
    # { page: "mediamarkt", department: "Laundry"                  , category: "Microwaves"       , subcategory: "Grill"          , url_template: '"https://www.mediamarkt.es/es/category/_microondas-con-grill-702643.html?page=#{page_index}"'},
    # { page: "mediamarkt", department: "Cooking"                  , category: "Ovens"            , subcategory: "Multifunction"  , url_template: '"https://www.mediamarkt.es/es/category/_hornos-multifunci%C3%B3n-702630.html?page=#{page_index}"'},
    # { page: "mediamarkt", department: "Cooking"                  , category: "Ovens"            , subcategory: "Multifunction"  , url_template: '"https://www.mediamarkt.es/es/category/_hornos-con-microondas-702633.html?page=#{page_index}"'},
    # { page: "mediamarkt", department: "Refrigerators"            , category: "Refrigerators"    , subcategory: "Bottom-Freezer" , url_template: '"https://www.mediamarkt.es/es/category/_frigor%C3%ADficos-integrables-702145.html?page=#{page_index}"'},
    # { page: "mediamarkt", department: "Refrigerators"            , category: "Refrigerators"    , subcategory: "Bottom-Freezer" , url_template: '"https://www.mediamarkt.es/es/category/_frigor%C3%ADficos-combinados-702140.html?page=#{page_index}"'},
    # { page: "mediamarkt", department: "Mobile"                   , category: "Smartphones"      , subcategory: ""               , url_template: '"https://www.mediamarkt.es/es/category/_smartphones-701189.html?page=#{page_index}"'},
    # { page: "mediamarkt", department: "Videogames"               , category: "Videogames PC"    , subcategory: ""               , url_template: '"https://www.mediamarkt.es/es/category/_juegos-pc-702351.html?page=#{page_index}"'},
    # { page: "mediamarkt", department: "Videogames"               , category: "Videogames PS4"   , subcategory: ""               , url_template: '"https://www.mediamarkt.es/es/category/_sony-ps4-702297.html?page=#{page_index}"'},
    # { page: "mediamarkt", department: "Videogames"               , category: "Videogames SWC"   , subcategory: ""               , url_template: '"https://www.mediamarkt.es/es/category/_nintendo-switch-702299.html?page=#{page_index}"'},
    # { page: "mediamarkt", department: "Videogames"               , category: "Videogames XBO"   , subcategory: ""               , url_template: '"https://www.mediamarkt.es/es/category/_xbox-one-702301.html?page=#{page_index}"'},
    # { page: "mediamarkt", department: "Laundry"                  , category: "Washers"          , subcategory: "Front Loading"  , url_template: '"https://www.mediamarkt.es/es/category/_lavadoras-carga-frontal-702191.html?page=#{page_index}"'},
    # { page: "mediamarkt", department: "Laundry"                  , category: "Washers"          , subcategory: "Top Loading"    , url_template: '"https://www.mediamarkt.es/es/category/_lavadoras-carga-superior-702192.html?page=#{page_index}"'},
    # { page: "mediamarkt", department: "Laundry"                  , category: "Washers"          , subcategory: "Front Loading"  , url_template: '"https://www.mediamarkt.es/es/category/_lavadoras-integrables-702194.html?page=#{page_index}"'}

    # { page: "fnac", department: "Videogames", category: "Videogames PS4",   subcategory: "",  url_template: '"https://www.fnac.es/n50050/Todos-los-videojuegos/Juegos-PS4/Todos-los-juegos-PS4-A-Z?ItemPerPage=20&SDM=list&PageIndex=#{page_index}&SFilt=1!206"'},
    # { page: "fnac", department: "Videogames", category: "Videogames XBO",   subcategory: "",  url_template: '"https://www.fnac.es/n50052/Todos-los-videojuegos/Juegos-Xbox-One/Todos-los-juegos-Xbox-One-A-Z?ItemPerPage=20&SDM=list&PageIndex=#{page_index}&SFilt=1!206&sl"'},
    # { page: "fnac", department: "Videogames", category: "Videogames PC",    subcategory: "",  url_template: '"https://www.fnac.es/n51811/Todos-los-videojuegos/Juegos-PC-y-Mac/Todos-los-juegos-PC-A-Z?ItemPerPage=20&SDM=list&PageIndex=#{page_index}&SFilt=1!206&sl"'},
    # { page: "fnac", department: "Videogames", category: "Videogames SWC",   subcategory: "",  url_template: '"https://www.fnac.es/n94849/Todo-Nintendo-Switch/Todos-los-juegos?ItemPerPage=20&SDM=list&PageIndex=#{page_index}&SFilt=1!206&sl"'},
    #
    # { page: "amazon", department: "Videogames", category: "Videogames PS4",   subcategory: "",  url_template: '"https://www.amazon.es/s/s/ref=sr_nr_p_6_0?fst=as%3Aoff&rh=n%3A599382031%2Cn%3A%21599383031%2Cn%3A2581783031%2Cn%3A2581786031%2Cp_85%3A831314031%2Cp_6%3AA1AT7YVPFBWXBL&bbn=2581786031&ie=UTF8&qid=1538060971&rnid=831275031&page=#{page_index}"'},
    # { page: "amazon", department: "Videogames", category: "Videogames XBO",   subcategory: "",  url_template: '"https://www.amazon.es/s/ref=sr_pg_1?fst=as%3Aoff&rh=n%3A599382031%2Cn%3A%21599383031%2Cn%3A2785651031%2Cn%3A2785654031%2Cp_85%3A831314031%2Cp_6%3AA1AT7YVPFBWXBL&bbn=2785654031&ie=UTF8&qid=1538061452&page=#{page_index}"'},
    # { page: "amazon", department: "Videogames", category: "Videogames PC",    subcategory: "",  url_template: '"https://www.amazon.es/s/ref=sr_pg_1?fst=as%3Aoff&rh=n%3A599382031%2Cp_85%3A831314031%2Cp_6%3AA1AT7YVPFBWXBL%2Cn%3A%21599383031%2Cn%3A665498031%2Cn%3A665499031&bbn=665498031&ie=UTF8&qid=1538061350&page=#{page_index}"'},
    # { page: "amazon", department: "Videogames", category: "Videogames SWC",   subcategory: "",  url_template: '"https://www.amazon.es/s/s/ref=sr_nr_p_6_0?fst=as%3Aoff&rh=n%3A599382031%2Cn%3A%21599383031%2Cn%3A12366198031%2Cn%3A12366200031%2Cp_85%3A831314031%2Cp_6%3AA1AT7YVPFBWXBL&bbn=12366200031&ie=UTF8&qid=1538061403&rnid=831275031&page=#{page_index}"'}

    # { page: "carrefour", category: "Hobs",          subcategory: "",              url_template: '"https://www.carrefour.es/placas/cat5980052/c?No=#{(page_index-1)*self::PRODUCTSxPAGE}"'},
    # { page: "carrefour", category: "Dishwashers",   subcategory: "",              url_template: '"https://www.carrefour.es/lavavajillas/cat5980006/c?No=#{(page_index-1)*self::PRODUCTSxPAGE}"'},
    # { page: "carrefour", category: "Microwaves",    subcategory: "",              url_template: '"https://www.carrefour.es/microondas/cat5980056/c?No=#{(page_index-1)*self::PRODUCTSxPAGE}"'},
    # { page: "carrefour", category: "Ovens",         subcategory: "",              url_template: '"https://www.carrefour.es/hornos/cat5980054/c?No=#{(page_index-1)*self::PRODUCTSxPAGE}"'},
    # { page: "carrefour", category: "Refrigerators", subcategory: "",              url_template: '"https://www.carrefour.es/frigorificos/cat5980036/c?No=#{(page_index-1)*self::PRODUCTSxPAGE}"'},
    # { page: "carrefour", category: "Washers",       subcategory: "Front Loading", url_template: '"https://www.carrefour.es/lavadoras/cat5980022/c?No=#{(page_index-1)*self::PRODUCTSxPAGE}"'},
    # { page: "carrefour", category: "Laptops",       subcategory: "",              url_template: '"https://www.carrefour.es/portatiles/cat410364/c?No=#{(page_index-1)*self::PRODUCTSxPAGE}"'},
    # # { page: "carrefour", category: "Integrables",   subcategory: "",              url_template: },
    #
    # { page: "clickelectrodomesticos", category: "Hobs",          subcategory: "",              url_template: '"http://www.clickelectrodomesticos.com/placas-de-coccion.html?limit=#{self::PRODUCTSxPAGE}&p=#{page_index}"'},
    # { page: "clickelectrodomesticos", category: "Dishwashers",   subcategory: "",              url_template: '"http://www.clickelectrodomesticos.com/lavavajillas.html?limit=#{self::PRODUCTSxPAGE}&p=#{page_index}"'},
    # { page: "clickelectrodomesticos", category: "Microwaves",    subcategory: "",              url_template: '"http://www.clickelectrodomesticos.com/microondas.html?limit=#{self::PRODUCTSxPAGE}&p=#{page_index}"'},
    # { page: "clickelectrodomesticos", category: "Ovens",         subcategory: "",              url_template: '"http://www.clickelectrodomesticos.com/hornos-de-cocina.html?limit=#{self::PRODUCTSxPAGE}&p=#{page_index}"'},
    # { page: "clickelectrodomesticos", category: "Refrigerators", subcategory: "",              url_template: '"http://www.clickelectrodomesticos.com/frigorificos.html?limit=#{self::PRODUCTSxPAGE}&p=#{page_index}"'},
    # { page: "clickelectrodomesticos", category: "Washers",       subcategory: "Front Loading", url_template: '"http://www.clickelectrodomesticos.com/lavadoras-y-secadoras/de-carga-frontal.html?limit=#{self::PRODUCTSxPAGE}&p=#{page_index}"'},
    # { page: "clickelectrodomesticos", category: "Washers",       subcategory: "Top Loading",   url_template: '"http://www.clickelectrodomesticos.com/lavadoras-y-secadoras/de-carga-superior.html?limit=#{self::PRODUCTSxPAGE}&p=#{page_index}"'},
    # # { page: "clickelectrodomesticos", category: "Laptops",       subcategory: "",              url_template: },
    # { page: "clickelectrodomesticos", category: "Integrables",   subcategory: "",              url_template: '"http://www.clickelectrodomesticos.com/lavadoras-y-secadoras/integrables.html?limit=#{self::PRODUCTSxPAGE}&p=#{page_index}"'},
    #
    # { page: "eci", category: "Hobs",          subcategory: "",              url_template: '"http://www.elcorteingles.es/electrodomesticos/hornos-placas-y-campanas/placas-de-cocina/#{page_index}?itemsPerPage=#{self::PRODUCTSxPAGE}"'},
    # { page: "eci", category: "Dishwashers",   subcategory: "",              url_template: '"http://www.elcorteingles.es/electrodomesticos/lavavajillas/lavavajillas-60-cm/#{page_index}?itemsPerPage=#{self::PRODUCTSxPAGE}"'},
    # { page: "eci", category: "Microwaves",    subcategory: "",              url_template: '"http://www.elcorteingles.es/electrodomesticos/hornos-placas-y-campanas/hornos-de-cocina/microondas/#{page_index}?itemsPerPage=#{self::PRODUCTSxPAGE}"'},
    # { page: "eci", category: "Ovens",         subcategory: "",              url_template: '"http://www.elcorteingles.es/electrodomesticos/hornos-placas-y-campanas/hornos-de-cocina/#{page_index}?itemsPerPage=#{self::PRODUCTSxPAGE}"'},
    # { page: "eci", category: "Refrigerators", subcategory: "",              url_template: '"http://www.elcorteingles.es/electrodomesticos/frigorificos-y-congeladores/frigorificos-combi/#{page_index}?itemsPerPage=#{self::PRODUCTSxPAGE}"'},
    # { page: "eci", category: "Washers",       subcategory: "Front Loading", url_template: '"http://www.elcorteingles.es/electrodomesticos/lavado-y-secado/lavadora-de-carga-frontal/#{page_index}?itemsPerPage=#{self::PRODUCTSxPAGE}"'},
    # { page: "eci", category: "Washers",       subcategory: "Top Loading",   url_template: '"http://www.elcorteingles.es/electrodomesticos/lavadoras/lavadora-de-carga-superior#{page_index}?itemsPerPage=#{self::PRODUCTSxPAGE}"'},
    # { page: "eci", category: "Laptops",       subcategory: "",              url_template: '"https://www.elcorteingles.es/electronica/ordenadores/portatiles/#{page_index}?itemsPerPage=#{self::PRODUCTSxPAGE}"'},
    # # { page: "eci", category: "Integrables",   subcategory: "",              url_template: },
    #
    # { page: "electrocosto", category: "Hobs",          subcategory: "",              url_template: '"https://www.electrocosto.com/encimeras/?p=#{page_index}&n=#{self::PRODUCTSxPAGE}"'},
    # { page: "electrocosto", category: "Dishwashers",   subcategory: "",              url_template: '"https://www.electrocosto.com/lavavajillas/?p=#{page_index}&n=#{self::PRODUCTSxPAGE}"'},
    # { page: "electrocosto", category: "Microwaves",    subcategory: "",              url_template: '"https://www.electrocosto.com/microondas/?p=#{page_index}&n=#{self::PRODUCTSxPAGE}"'},
    # { page: "electrocosto", category: "Ovens",         subcategory: "",              url_template: '"https://www.electrocosto.com/hornos/?p=#{page_index}&n=#{self::PRODUCTSxPAGE}"'},
    # { page: "electrocosto", category: "Refrigerators", subcategory: "",              url_template: '"https://www.electrocosto.com/frigorificos-combis/?p=#{page_index}&n=#{self::PRODUCTSxPAGE}"'},
    # { page: "electrocosto", category: "Washers",       subcategory: "Front Loading", url_template: '"https://www.electrocosto.com/lavadoras/?p=#{page_index}&n=#{self::PRODUCTSxPAGE}"'},
    # # { page: "electrocosto", category: "Laptops",       subcategory: "",              url_template: },
    # # { page: "electrocosto", category: "Integrables",   subcategory: "",              url_template: },
    #
    # { page: "funnatic", category: "Hobs",          subcategory: "",              url_template: '"https://funnatic.es/es/ct/induccion-gas-vitro-74?page=#{page_index}"'},
    # { page: "funnatic", category: "Dishwashers",   subcategory: "",              url_template: '"https://funnatic.es/es/ct/comprar-lavavajillas-inox-blanco-instalado-precios-73?page=#{page_index}"'},
    # { page: "funnatic", category: "Microwaves",    subcategory: "",              url_template: '"https://funnatic.es/es/ct/comprar-microondas-integrable-retro-diseno-madrid-barcelona-77?page=#{page_index}"'},
    # { page: "funnatic", category: "Ovens",         subcategory: "",              url_template: '"https://funnatic.es/es/ct/horno-standar-83?page=#{page_index}"'},
    # { page: "funnatic", category: "Refrigerators", subcategory: "",              url_template: '"https://funnatic.es/es/ct/combi-22?page=#{page_index}"'},
    # { page: "funnatic", category: "Washers",       subcategory: "Front Loading", url_template: '"https://funnatic.es/es/ct/lavadora-carga-frontal-34?page=#{page_index}"'},
    # { page: "funnatic", category: "Washers",       subcategory: "Top Loading",   url_template: '"https://funnatic.es/es/ct/lavadora-carga-superior-33?page=#{page_index}"'},
    # # { page: "funnatic", category: "Laptops",       subcategory: "",              url_template: },
    # # { page: "funnatic", category: "Integrables",   subcategory: "",              url_template: },
    #
    # { page: "latiendadelpinguino", category: "Hobs",          subcategory: "",              url_template: '"http://www.latiendadelpinguino.com/placas?page=#{page_index}"'},
    # { page: "latiendadelpinguino", category: "Dishwashers",   subcategory: "",              url_template: '"http://www.latiendadelpinguino.com/lavavajillas?page=#{page_index}"'},
    # { page: "latiendadelpinguino", category: "Microwaves",    subcategory: "",              url_template: '"http://www.latiendadelpinguino.com/microondas?page=#{page_index}"'},
    # { page: "latiendadelpinguino", category: "Ovens",         subcategory: "",              url_template: '"http://www.latiendadelpinguino.com/hornos?page=#{page_index}"'},
    # { page: "latiendadelpinguino", category: "Refrigerators", subcategory: "",              url_template: '"http://www.latiendadelpinguino.com/233-frigorificos/s-2/clase-combinado?page=#{page_index}"'},
    # { page: "latiendadelpinguino", category: "Washers",       subcategory: "Front Loading", url_template: '"http://www.latiendadelpinguino.com/136-lavadoras/s-4/tipo_de_carga-frontal?page=#{page_index}"'},
    # { page: "latiendadelpinguino", category: "Washers",       subcategory: "Top Loading",   url_template: '"http://www.latiendadelpinguino.com/136-lavadoras/s-4/tipo_de_carga-superior?page=#{page_index}"'},
    # # { page: "latiendadelpinguino", category: "Laptops",       subcategory: "",              url_template: },
    # # { page: "latiendadelpinguino", category: "Integrables",   subcategory: "",              url_template: },
    #
    # { page: "mediamarkt", category: "Hobs",          subcategory: "",              url_template: '"https://tiendas.mediamarkt.es/encimeras/pagina#{page_index}?perPage=#{self::PRODUCTSxPAGE}"'},
    # { page: "mediamarkt", category: "Dishwashers",   subcategory: "",              url_template: '"https://tiendas.mediamarkt.es/lavavajillas-libre-instalacion/pagina#{page_index}?perPage=#{self::PRODUCTSxPAGE}"'},
    # { page: "mediamarkt", category: "Microwaves",    subcategory: "",              url_template: '"https://tiendas.mediamarkt.es/microondas/pagina#{page_index}?perPage=#{self::PRODUCTSxPAGE}"'},
    # { page: "mediamarkt", category: "Ovens",         subcategory: "",              url_template: '"https://tiendas.mediamarkt.es/coccion-encimeras-hornos/pagina#{page_index}?perPage=#{self::PRODUCTSxPAGE}"'},
    # { page: "mediamarkt", category: "Refrigerators", subcategory: "",              url_template: '"https://tiendas.mediamarkt.es/frigorifico-combinado/pagina#{page_index}?perPage=#{self::PRODUCTSxPAGE}"'},
    # { page: "mediamarkt", category: "Washers",       subcategory: "Front Loading", url_template: '"https://tiendas.mediamarkt.es/lavadoras-carga-frontal/pagina#{page_index}?perPage=#{self::PRODUCTSxPAGE}"'},
    # { page: "mediamarkt", category: "Washers",       subcategory: "Top Loading",   url_template: '"https://tiendas.mediamarkt.es/lavadoras-carga-superior/pagina#{page_index}?perPage=#{self::PRODUCTSxPAGE}"'},
    # { page: "mediamarkt", category: "Laptops",       subcategory: "",              url_template: '"https://tiendas.mediamarkt.es/portatiles/pagina#{page_index}?perPage=#{self::PRODUCTSxPAGE}"'},
    # { page: "mediamarkt", category: "Integrables",   subcategory: "",              url_template: '"https://tiendas.mediamarkt.es/lavadoras-integrables/pagina#{page_index}?perPage=#{self::PRODUCTSxPAGE}"'},
    #
    # { page: "puntronic", category: "Hobs",          subcategory: "",              url_template: '"http://www.puntronic.com/electrodomesticos-cocina/vitroceramicas?limit=#{self::PRODUCTSxPAGE}&mode=grid&p=#{page_index}"'},
    # { page: "puntronic", category: "Dishwashers",   subcategory: "",              url_template: '"http://www.puntronic.com/gran-electrodomestico/lavavajillas-online/lavavajillas-grandes-60cm?limit=#{self::PRODUCTSxPAGE}&mode=grid&p=#{page_index}"'},
    # { page: "puntronic", category: "Microwaves",    subcategory: "",              url_template: '"http://www.puntronic.com/electrodomesticos-cocina/hornos-microondas?limit=#{self::PRODUCTSxPAGE}&mode=grid&p=#{page_index}"'},
    # { page: "puntronic", category: "Ovens",         subcategory: "",              url_template: '"http://www.puntronic.com/electrodomesticos-cocina/hornos?limit=#{self::PRODUCTSxPAGE}&mode=grid&p=#{page_index}"'},
    # { page: "puntronic", category: "Refrigerators", subcategory: "",              url_template: '"http://www.puntronic.com/gran-electrodomestico/frigorificos-y-neveras/frigorificos-combis?limit=#{self::PRODUCTSxPAGE}&mode=grid&p=#{page_index}"'},
    # { page: "puntronic", category: "Washers",       subcategory: "Front Loading", url_template: '"http://www.puntronic.com/gran-electrodomestico/lavadoras/lavadoras-carga-frontal?limit=#{self::PRODUCTSxPAGE}&mode=grid&p=#{page_index}"'},
    # { page: "puntronic", category: "Washers",       subcategory: "Top Loading",   url_template: '"http://www.puntronic.com/gran-electrodomestico/lavadoras/lavadoras-carga-superior?limit=#{self::PRODUCTSxPAGE}&mode=grid&p=#{page_index}"'},
    # # { page: "puntronic", category: "Laptops",       subcategory: "",              url_template: },
    # # { page: "puntronic", category: "Integrables",   subcategory: "",              url_template: },
    #
    # { page: "tiendaazul", category: "Hobs",          subcategory: "",              url_template: '"http://www.tiendaazul.com/63-placas-encimeras-baratas?n=#{self::PRODUCTSxPAGE}"'},
    # { page: "tiendaazul", category: "Dishwashers",   subcategory: "",              url_template: '"http://www.tiendaazul.com/23-lavavajillas-baratos?n=#{self::PRODUCTSxPAGE}"'},
    # { page: "tiendaazul", category: "Microwaves",    subcategory: "",              url_template: '"http://www.tiendaazul.com/24-microondas-baratos?n=#{self::PRODUCTSxPAGE}"'},
    # { page: "tiendaazul", category: "Ovens",         subcategory: "",              url_template: '"http://www.tiendaazul.com/21-hornos-baratos?n=#{self::PRODUCTSxPAGE}"'},
    # { page: "tiendaazul", category: "Refrigerators", subcategory: "",              url_template: '"http://www.tiendaazul.com/68-frigorificos-combi-baratos?n=#{self::PRODUCTSxPAGE}"'},
    # { page: "tiendaazul", category: "Washers",       subcategory: "Front Loading", url_template: '"http://www.tiendaazul.com/22-lavadoras-baratas?n=#{self::PRODUCTSxPAGE}"'},
    # # { page: "tiendaazul", category: "Laptops",       subcategory: "",              url_template: },
    # # { page: "tiendaazul", category: "Integrables",   subcategory: "",              url_template: },
    #
    # { page: "worten", category: "Hobs",          subcategory: "",              url_template: '"https://www.worten.es/productos/electrodomesticos/placas-y-vitroceramicas?p=#{page_index}"'},
    # { page: "worten", category: "Dishwashers",   subcategory: "",              url_template: '"https://www.worten.es/productos/electrodomesticos/lavavajillas/lavavajillas-60cm?p=#{page_index}"'},
    # { page: "worten", category: "Microwaves",    subcategory: "",              url_template: '"https://www.worten.es/productos/pequenos-electrodomesticos/microondas?p=#{page_index}"'},
    # { page: "worten", category: "Ovens",         subcategory: "",              url_template: '"https://www.worten.es/productos/electrodomesticos/hornos?p=#{page_index}"'},
    # { page: "worten", category: "Refrigerators", subcategory: "",              url_template: '"https://www.worten.es/productos/electrodomesticos/frigorificos/combis?p=#{page_index}"'},
    # { page: "worten", category: "Washers",       subcategory: "Front Loading", url_template: '"https://www.worten.es/productos/electrodomesticos/lavadoras/carga-frontal?p=#{page_index}"'},
    # { page: "worten", category: "Washers",       subcategory: "Top Loading",   url_template: '"https://www.worten.es/productos/electrodomesticos/lavadoras/carga-superior?p=#{page_index}"'},
    # { page: "worten", category: "Laptops",       subcategory: "",              url_template: '"https://www.worten.es/productos/informatica/portatiles?p=#{page_index}"'},
    # { page: "worten", category: "Integrables",   subcategory: "",              url_template: '"https://www.worten.es/productos/electrodomesticos/lavadoras/encastre?p=#{page_index}"'}
]
page_departments.each do |page_department|
  PageDepartment.create(page_department)
end


AdminUser.create(email: 'admin@example.com', password: 'password', password_confirmation: 'password') if Rails.env.development?
AdminUser.create(email: 'rfuentes@zoomlabs.es', password: 'rfuentes', password_confirmation: 'rfuentes')


puts("Update ecommerces")
pages_names = Utils.available_pages.map{|market| [market, "#{PageBase.get_class(market)::PAGE_NAME}"]}.to_h
pages_names.each do |key, value|
  Ecommerce.find_or_create_by(name: value, slug: key)
end
