#!/bin/sh

# VARIABLES DE CONFIGURACION
PG_HOST="localhost"
PG_PORT="5432"
PG_DATABASE="price_intelligence"
PG_USER="price_intelligence"
PG_PASSWORD="zoomlab"

BACKUP_DIR="/Users/juanjo/Desktop"
BACKUP_FILE="backup_20170905.zip"
LOG_DIR="/Users/juanjo/Desktop"

# BACKUP
log_file=restore.log

cd $BACKUP_DIR
rm -rf $log_file
echo "$(date +%Y-%m-%d\ %H:%m:%S) - Restoring database backup $BACKUP_FILE..." >> $log_file
if [ -s "$BACKUP_FILE" ]
then
	unzip -f $BACKUP_FILE
	dump_file=dump_${BACKUP_FILE:7:8}.sql
	psql --dbname=postgresql://$PG_USER:$PG_PASSWORD@$PG_HOST:$PG_PORT/$PG_DATABASE -f $dump_file 2>>$log_file
	echo "$(date +%Y-%m-%d\ %H:%m:%S) - Restore completed" >> $log_file
else
	echo "$(date +%Y-%m-%d\ %H:%m:%S) - File not found or empty" >> $log_file
fi