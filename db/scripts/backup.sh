#!/bin/sh

# VARIABLES DE CONFIGURACION
PG_HOST="localhost"
PG_PORT="5432"
PG_DATABASE="price_intelligence"
PG_USER="price_intelligence"
PG_PASSWORD="zoomlab"

BACKUP_DIR="/Users/juanjo/Desktop"
LOG_DIR="/Users/juanjo/Desktop"

# BACKUP
date_info=$(date +%Y%m%d)
dump_file=dump_$date_info.sql
backup_file=backup_$date_info.zip
log_file=backup_$date_info.log

cd $BACKUP_DIR
rm -f $dump_file
rm -f $backup_file
rm -f $log_file
echo "$(date +%Y-%m-%d\ %H:%m:%S) - Starting backup of database..." >> $log_file
pg_dump --dbname=postgresql://$PG_USER:$PG_PASSWORD@$PG_HOST:$PG_PORT/$PG_DATABASE -c >> $dump_file 2>>$log_file
if [ -s "$dump_file" ] 
then 
	zip backup_$date_info.zip $dump_file
	echo "$(date +%Y-%m-%d\ %H:%m:%S) - Generated ZIP file: $backup_file" >> $log_file
	echo "$(date +%Y-%m-%d\ %H:%m:%S) - Backup completed" >> $log_file
fi
rm -f $dump_file