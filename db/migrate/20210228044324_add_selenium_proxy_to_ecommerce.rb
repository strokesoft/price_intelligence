class AddSeleniumProxyToEcommerce < ActiveRecord::Migration[5.1]
  def change
    add_column :ecommerces, :selenium_proxy, :boolean
  end
end
