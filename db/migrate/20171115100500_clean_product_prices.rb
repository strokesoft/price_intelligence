class CleanProductPrices < ActiveRecord::Migration[5.1]
  def self.up
    sql = "DELETE FROM product_prices WHERE id IN (SELECT id FROM (SELECT id, ROW_NUMBER() OVER (partition BY product_page_id, date ORDER BY id desc) AS rnum FROM product_prices) t WHERE t.rnum > 1)"
    result = ActiveRecord::Base.connection.execute(sql)

    # "select count(*) from product_prices where price ~ '[\u00a0]'"
    # "select count(*) from product_prices where price ~ '[\n]'"
    # "select count(*) from product_prices where price ~ '[\r]'"
    # change '\n', '\r', &nbsp; (\u00a0) and € by ' ' then trim. After that change ' Ahorras ' and groups of blank spaces by - for easy find them. Then remove single blank spaces (for cases as '1 234,56')
    sql = "UPDATE product_prices SET price = replace(replace(replace(trim(replace(replace(replace(replace(price, E'\n', ' '), E'\r', ' '), E'\u00a0', ' '), '€', ' ')), ' Ahorras ', '-'), '  ', '-'), ' ', '')"
    result = ActiveRecord::Base.connection.execute(sql)
  end

  def self.down
  end
end