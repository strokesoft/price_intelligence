class AddOutOfStockToProductPrice < ActiveRecord::Migration[5.1]
  def change
    add_column :product_prices, :out_of_stock, :boolean
  end
end