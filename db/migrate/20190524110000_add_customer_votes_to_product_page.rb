class AddCustomerVotesToProductPage < ActiveRecord::Migration[5.1]
  def change
    add_column :product_pages, :customers_vote_average, :float
    add_column :product_pages, :customers_comments_count, :integer
  end
end
