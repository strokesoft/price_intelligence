class CreateProductPages < ActiveRecord::Migration[5.1]
  def change
    create_table :product_pages do |t|
      t.references :product, index: true, foreign_key: true
      # t.references :product_lexicon, index: true, foreign_key: true
      t.string     :page
      t.string     :image_url
      t.string     :url
      t.string     :shipment
      t.string     :ean
      t.string     :category
      t.string     :foul_name
      t.date       :date

      t.timestamps null: false
    end
  end
end
