class AddHasDailyEmailAndAcronymToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users,     :has_daily_email, :boolean, default: false
    add_column :societies, :acronym, :string
  end
end
