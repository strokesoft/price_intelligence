class AddHasProxyToEcommerces < ActiveRecord::Migration[5.1]
  def change
    add_column :ecommerces, :has_proxy, :boolean, :default => false
  end
end
