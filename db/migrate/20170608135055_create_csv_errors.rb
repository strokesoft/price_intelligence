class CreateCsvErrors < ActiveRecord::Migration[5.1]
  def change
    create_table :csv_errors do |t|
      t.text     :source_line
      t.string   :error
      t.string   :mode
      t.string   :parser
      t.string   :category
      t.integer  :n
      t.datetime :date
      t.boolean  :warn

      t.timestamps null: false
    end
  end
end
