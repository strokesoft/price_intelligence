class AddIndexToProductPrices < ActiveRecord::Migration[5.1]
  def change
    add_index :product_prices, [:out_of_stock, :date]
    add_index :product_prices, [:id, :date]
    add_index :products, [:id, :category]
  end
end
