class RemoveInvalidFields < ActiveRecord::Migration[5.1]
  def change
    remove_column :product_prices, :product_id
    remove_column :product_prices, :page
  end
end