class ChangePriceColumn < ActiveRecord::Migration[5.1]
  def self.up
    sql = "UPDATE product_prices SET price = replace(replace(replace(trim(replace(replace(replace(replace(price, E'\n', ' '), E'\r', ' '), E'\u00a0', ' '), '€', ' ')), ' Ahorras ', '-'), '  ', '-'), ' ', '')"
    result = ActiveRecord::Base.connection.execute(sql)

    change_column :product_prices, :price, 'float USING price::double precision', null: false
  end

  def self.down
    change_column :product_prices, :price, :string, null: false
  end
end