class ChangePageDepartmentColumns < ActiveRecord::Migration[5.1]
  def self.up
    rename_column :page_departments, :page_department, :department
    rename_column :page_departments, :default_category, :category
    rename_column :page_departments, :default_subcategory, :subcategory
  end

  def self.down
    rename_column :page_departments, :department, :page_department
    rename_column :page_departments, :category, :default_category
    rename_column :page_departments, :subcategory, :default_subcategory
  end
end
