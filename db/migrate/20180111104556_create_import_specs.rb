class CreateImportSpecs < ActiveRecord::Migration[5.1]
  def change
    create_table :import_specs do |t|
      t.string  :category
      t.string  :subcategory
      t.string  :spec
      t.integer :order
      t.string  :page
      t.string  :origin_spec

      t.timestamps
    end

    add_index :import_specs, [:category, :subcategory, :spec, :order], unique: true, name: "import_specs_index"

    Utils.clean_specs
  end
end
