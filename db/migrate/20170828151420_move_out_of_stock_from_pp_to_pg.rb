class MoveOutOfStockFromPpToPg < ActiveRecord::Migration[5.1]
  def change
    add_column :product_pages,  :out_of_stock,   :boolean
    add_column :product_pages,  :last_aparition, :datetime
  end
end
