class AddPostgresExtensions < ActiveRecord::Migration[5.0]
  def change
    enable_extension 'hstore' unless extension_enabled?('hstore')
    enable_extension 'unaccent' unless extension_enabled?('unaccent')
  end
end
