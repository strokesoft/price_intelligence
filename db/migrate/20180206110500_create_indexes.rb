class CreateIndexes < ActiveRecord::Migration[5.1]
  def change
#    add_index :import_specs, [:category, :subcategory, :spec, :order], unique: true, name: "import_specs_index"

    add_index :products, :brand
    add_index :products, :category
    add_index :products, :usable

    add_index :product_pages, :category
    add_index :product_pages, :usable

    add_index :product_prices, :date
    add_index :product_prices, :out_of_stock
  end
end
