class AddCountersPageDepartments < ActiveRecord::Migration[5.1]
  def change
    add_column :page_departments, :pages_max,             :integer, default: 0, null: false
    add_column :page_departments, :pages_last,            :integer, default: 0, null: false
    add_column :page_departments, :products_listed_max,   :integer, default: 0, null: false
    add_column :page_departments, :products_listed_last,  :integer, default: 0, null: false
    add_column :page_departments, :products_valid_last,   :integer, default: 0, null: false
  end
end
