class AddOrderToPageDepartments < ActiveRecord::Migration[5.1]
  def change
    add_column :page_departments, :order, :integer, default: 1, null: false
  end
end
