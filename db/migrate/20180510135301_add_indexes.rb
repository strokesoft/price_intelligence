class AddIndexes < ActiveRecord::Migration[5.1]
  def change
    add_index :products, :subcategory
    add_index :products, :ean

    add_index :product_pages, :page
    add_index :product_pages, :out_of_stock
  end
end
