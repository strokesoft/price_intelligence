class AddSpecsToProductPages < ActiveRecord::Migration[5.1]
  def change
    add_column :product_pages, :specs, :json
  end
end
