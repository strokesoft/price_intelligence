class CreatePageDepartments < ActiveRecord::Migration[5.1]
  def change
    create_table :page_departments do |t|
      t.string :page,                null: false
      t.string :page_department
      t.string :default_category,    null: false
      t.string :default_subcategory#, null: false, default: ""
      t.string :url_template,        null: false

      t.timestamps
    end
  end
end
