class CleanUsable < ActiveRecord::Migration[5.1]
  def self.up
    change_column :products, :usable, :boolean, :default => true

    Product.where(usable: nil).update_all(usable: true)
    ProductPage.where(usable: nil).update_all(usable: true)
  end

  def self.down
    raise ActiveRecord::IrreversibleMigration, "Can't undo the migration with guarantees"
  end
end
