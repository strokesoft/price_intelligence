class AddFixedSpecsToProducts < ActiveRecord::Migration[5.1]
  def change
    add_column :products, :fixed_specs, :boolean, default: false

    Product.where(category: ['Dishwashers', 'Microwaves', 'Refrigerators', 'Washers']).where("specs::text != '{}' and specs IS NOT NULL").update_all(fixed_specs: true)
  end
end
