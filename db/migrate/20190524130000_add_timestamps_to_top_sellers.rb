class AddTimestampsToTopSellers < ActiveRecord::Migration[5.1]
  def change
    add_timestamps :top_sellers, null: false
  end
end
