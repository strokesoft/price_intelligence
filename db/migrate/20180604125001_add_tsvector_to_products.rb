class AddTsvectorToProducts < ActiveRecord::Migration[5.0]
  def up
    add_column :products, :tsv, :tsvector
    add_index :products, :tsv, using: 'gin'
  end

  def down
    remove_index :products, :tsv
    remove_column :products, :tsv
  end
end
