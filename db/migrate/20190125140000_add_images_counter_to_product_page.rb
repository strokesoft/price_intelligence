class AddImagesCounterToProductPage < ActiveRecord::Migration[5.1]
  def change
    add_column :product_pages, :images_counter, :integer
  end
end
