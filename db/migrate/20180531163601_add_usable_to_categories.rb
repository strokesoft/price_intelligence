class AddUsableToCategories < ActiveRecord::Migration[5.1]
  def change
    add_column :categories, :usable, :boolean, :default => true
  end
end
