class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string   :brand
      t.string   :name
      t.string   :ean
      t.string   :category
      t.string   :subcategory
      t.text     :description
      t.boolean  :usable
      t.datetime :date

      t.timestamps null: false
    end
  end
end