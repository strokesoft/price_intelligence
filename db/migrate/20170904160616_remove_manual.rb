class RemoveManual < ActiveRecord::Migration[5.1]
  def change
    # remove_column :products,      :manual
    remove_column :product_pages, :action

    change_column :csv_errors, :date, :date
  end
end