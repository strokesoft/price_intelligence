class AddIsReferenceToTables < ActiveRecord::Migration[5.1]
  def change
    add_column :society_brands,     :is_reference, :boolean, default: false
    add_column :society_ecommerces, :is_reference, :boolean, default: false
  end
end
