class AddDescriptionToProductPage < ActiveRecord::Migration[5.1]
  def change
    add_column :product_pages, :foul_description, :string
  end
end
