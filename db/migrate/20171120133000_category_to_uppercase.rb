class CategoryToUppercase < ActiveRecord::Migration[5.1]
  def up
    if %w[MySQL PostgreSQL].include? ActiveRecord::Base.connection.adapter_name
      execute "UPDATE products SET category = INITCAP(category)"
      execute "UPDATE product_pages SET category = INITCAP(category)"
    else
      Product.all.each do |product|
        product.update_attributes category: product.category.downcase.titleize
      end
      ProductPage.all.each do |ppage|
        ppage.update_attributes category: ppage.category.downcase.titleize
      end
    end
  end

  def down
    if %w[MySQL PostgreSQL].include? ActiveRecord::Base.connection.adapter_name
      execute "UPDATE products SET category = LOWER(category)"
      execute "UPDATE product_pages SET category = LOWER(category)"
    else
      Product.all.each do |product|
        product.update_attributes category: product.category.downcase
      end
      ProductPage.all.each do |ppage|
        ppage.update_attributes category: ppage.category.downcase
      end
    end
  end
end