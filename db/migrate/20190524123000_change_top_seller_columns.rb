class ChangeTopSellerColumns < ActiveRecord::Migration[5.1]
  def self.up
    rename_column :top_sellers, :order, :topseller_order
  end

  def self.down
    rename_column :top_sellers, :topseller_order, :order
  end
end
