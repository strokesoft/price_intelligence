class CreateProductPrices < ActiveRecord::Migration[5.1]
  def change
    create_table :product_prices do |t|
      t.references :product, index: true, foreign_key: true
      t.references :product_page, index: true, foreign_key: true
      t.integer    :order
      t.string     :page
      t.boolean    :active
      t.string     :price
      t.date       :date

      t.timestamps null: false
    end
  end
end
