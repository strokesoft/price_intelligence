class RemoveProductPricesActive < ActiveRecord::Migration[5.1]
  def change
    remove_column :product_prices, :active
  end
end