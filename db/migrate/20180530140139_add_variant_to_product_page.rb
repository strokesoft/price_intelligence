class AddVariantToProductPage < ActiveRecord::Migration[5.1]
  def change
    add_column :product_pages, :variant, :boolean, default: false
  end
end
