class AddNewTablesToPermissions < ActiveRecord::Migration[5.1]
  def change
    add_column :products, :slug, :string, index: true
    change_column_null :users, :admin, false, false

    create_table :societies do |t|
      t.string  :name, index: true
      t.string  :slug, index: true
      t.string  :picture
      t.boolean :has_all_ecommerces, default: false
      t.boolean :has_all_brands, default: false
      t.boolean :has_all_categories, default: false
      t.boolean :has_filter_by_specs, default: false

      t.timestamps
    end
    add_reference :users, :society, index: true

    create_table :categories do |t|
      t.string  :name, index: true
      t.string  :slug, index: true
      t.string  :ancestry, index: true
      t.string  :picture
      t.integer :position

      t.timestamps
    end

    create_table :ecommerces do |t|
      t.string :name, index: true
      t.string :slug, index: true
      t.string :picture
      t.boolean :is_reference, default: false

      t.timestamps
    end

    create_table :brands do |t|
      t.string  :name, index: true
      t.string  :slug, index: true
      t.string  :picture
      t.boolean :is_reference, default: false
      t.string  :color

      t.timestamps
    end

    create_table :msrps do |t|
      t.references  :society
      t.references  :trademark, references: :brands
      t.references  :product
      t.decimal     :price
      t.date        :on_date

      t.timestamps
    end
    add_index :msrps, [:society_id, :trademark_id, :product_id], unique: true

    # Permissions
    create_table :society_categories do |t|
      t.references :society, foreign_key: true
      t.references :category, foreign_key: true
    end
    add_index :society_categories, [:society_id, :category_id], unique: true

    create_table :society_ecommerces do |t|
      t.references :society, foreign_key: true
      t.references :ecommerce, foreign_key: true
    end
    add_index :society_ecommerces, [:society_id, :ecommerce_id], unique: true

    create_table :society_brands do |t|
      t.references :society, foreign_key: true
      t.references :brand, foreign_key: true
    end
    add_index :society_brands, [:society_id, :brand_id], unique: true

    # Associated to MSRPs
    create_table :society_trademarks do |t|
      t.references :society, foreign_key: true
      t.references :trademark, references: :brands
    end
    add_index :society_trademarks, [:society_id, :trademark_id], unique: true

    # Category and reference brands
    create_table :category_brands do |t|
      t.references :category, foreign_key: true
      t.references :brand, foreign_key: true
    end
    add_index :category_brands, [:category_id, :brand_id], unique: true
  end
end
