class CreateTopSellers < ActiveRecord::Migration[5.1]
  def change
    create_table :top_sellers do |t|
      t.references :product_page, index: true, foreign_key: true
      t.date       :date,  null: false, index: true
      t.integer    :order, null: false
    end

    add_index :top_sellers, [:id, :date]
  end
end
