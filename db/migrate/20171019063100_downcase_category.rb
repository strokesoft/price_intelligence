class DowncaseCategory < ActiveRecord::Migration[5.1]
  def up
    if %w[MySQL PostgreSQL].include? ActiveRecord::Base.connection.adapter_name
      execute "UPDATE product_pages SET category = LOWER(category)"
      execute "UPDATE products SET category = LOWER(category)"
    else
      ProductPage.all.each do |product_page|
        product_page.update_attributes category: product_page.category.downcase
      end
      Product.all.each do |product|
        product.update_attributes category: product.category.downcase
      end
    end
  end
end
