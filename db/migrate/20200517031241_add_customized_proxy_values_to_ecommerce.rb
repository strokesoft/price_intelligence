class AddCustomizedProxyValuesToEcommerce < ActiveRecord::Migration[5.1]
  def change
    add_column :ecommerces, :proxy_address, :string
    add_column :ecommerces, :proxy_user, :string
    add_column :ecommerces, :proxy_password, :string
  end
end
