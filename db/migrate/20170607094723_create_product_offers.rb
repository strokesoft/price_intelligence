class CreateProductOffers < ActiveRecord::Migration[5.1]
  def change
    create_table :product_offers do |t|
      t.references :product, index: true, foreign_key: true
      t.references :product_page, index: true, foreign_key: true
      t.text       :offer
      t.date       :date

      t.timestamps null: false
    end
  end
end
