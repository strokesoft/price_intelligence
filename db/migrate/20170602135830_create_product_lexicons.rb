class CreateProductLexicons < ActiveRecord::Migration[5.1]
  def change
    create_table :product_lexicons do |t|
      t.string     :name
      t.string     :ean
      t.boolean    :aproximated
      t.references :product, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
