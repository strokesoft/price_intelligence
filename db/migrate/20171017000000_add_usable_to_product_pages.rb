class AddUsableToProductPages < ActiveRecord::Migration[5.1]
  def change
    add_column :product_pages, :usable, :boolean, :default => true
  end
end
