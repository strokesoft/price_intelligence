class AddSpecsToProducts < ActiveRecord::Migration[5.1]
  def change
    add_column :products, :specs, :json, :default => {}
  end
end
