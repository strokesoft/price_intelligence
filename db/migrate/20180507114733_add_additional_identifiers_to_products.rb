class AddAdditionalIdentifiersToProducts < ActiveRecord::Migration[5.1]
  def change
    add_column :products, :asin, :string
    add_column :products, :vendor_part_number, :string
    add_column :products, :mediamarkt_ref, :integer

    add_column :product_pages, :asin, :string
    add_column :product_pages, :vendor_part_number, :string
    add_column :product_pages, :mediamarkt_ref, :integer
  end
end
