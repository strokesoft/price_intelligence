source 'https://rubygems.org'
ruby '~> 2.4'

gem 'rails', '5.1.4'

gem 'activerecord-session_store'

gem 'sass-rails'                            # Use SCSS for stylesheets

gem 'uglifier'
gem 'therubyracer', platforms: :ruby
gem 'bcrypt'

gem 'jquery-rails'
gem 'turbolinks'
gem 'jbuilder'
gem 'sdoc', group: :doc

gem 'unicorn'

gem 'activeadmin'#, '1.0.0' # 1.1.0 don't works with activeadmin_addons: ActionView::Template::Error (File to import not found or unreadable: select2.
gem 'activeadmin_addons' # To have select2, DatePickers and others. It don't works with activeadmin 1.1.0: ActionView::Template::Error (File to import not found or unreadable: select2.
gem 'active_admin-sortable_tree'

gem 'jsoneditor-rails'

gem 'devise', '~> 4.3.0'
gem 'devise-bootstrap-views'
gem 'devise-async'
gem 'draper'
gem 'pundit'

gem 'pg'
gem 'pg_search'

# Charts
gem 'groupdate'
gem 'chartkick'
gem 'alertifyjs-rails'

# Redis
gem 'hiredis'
gem 'redis'
gem 'redis-namespace'
gem 'redis-rails'

gem 'kaminari'
gem 'handles_sortable_columns'

gem 'haml-rails'

gem 'daemons'
gem 'delayed_job'
gem 'delayed_job_active_record'
gem 'delayed_job_web'

gem 'jquery-ui-rails', '~> 6.0.1'
gem 'switchery-rails'

gem 'font-awesome-rails'

gem 'gon'

gem 'csv_builder'

gem 'sshkit-sudo'

gem 'net-sftp'

gem 'axlsx'
gem 'roo'

gem 'dotenv-rails'

# File uploader
gem 'carrierwave'
gem 'friendly_id'
gem 'ancestry'
gem 'acts_as_list'

# https://github.com/mozilla/geckodriver/releases
# chmod 775
# sudo mv geckodriver /usr/local/bin/
# source
gem 'selenium-webdriver'
gem 'headless'

#aptitude install xvfb
# gem 'rest-client'
# gem 'mechanize'

gem 'bookland' # for EAN, ISBN, and ASIN management https://github.com/hakanensari/bookland

# Configuration
gem 'chamber', '2.10.1'

# exception notifications
gem 'rollbar'
gem 'oj'

gem 'whenever', require: false

gem 'active_model_serializers'

# Html mails
gem 'roadie-rails'

gem 'active_record_union'

gem 'open_uri_redirections' # This gem applies a patch to OpenURI to optionally allow redirections from HTTP to HTTPS, or from HTTPS to HTTP.

gem 'newrelic_rpm' # Performance measures

gem 'watir'

gem 'nikkou'

group :test do
  # database_cleaner is not required, but highly recommended
  gem 'database_cleaner'
  gem 'factory_girl'
  gem 'factory_girl_rails'
  gem 'faker'
  gem 'rspec'
  gem 'rspec-rails'
  gem 'webrat'
  gem 'cucumber'
end

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'

  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'
  gem 'bootstrap-generators'

  gem 'letter_opener'
  gem 'thin'

  gem 'traceroute' # usar de vez en cuando, analiza rutas accesibles
  gem 'bullet' # usar de vez en cuando

  gem 'rails_best_practices', require: false# usar de vez en cuando, analiza muchas cosas
  gem 'rubycritic', :require => false# usar de vez en cuando, analiza
  gem 'sql_queries_count'

  # Pages speed
  gem 'rack-mini-profiler'
  gem 'meta_request'
end

group :development do
  # # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'better_errors'

  # SMTP server (https://mailcatcher.me/)
  # gem 'mailcatcher'

  # Capistrano
  gem 'capistrano', '3.8.0', require: false
  gem 'capistrano-rvm'
  gem 'cap-ec2', require: false
  gem 'capistrano-bundler', require: false
  gem 'capistrano-rails', require: false
  gem 'capistrano-db-tasks', require: false
  gem 'capistrano-touch-linked-files', require: false
  gem 'capistrano-lazy-templates' , require: false
  gem 'capistrano-safe-deploy-to', require: false
  gem 'capistrano-delayed-job', '~> 1.0'
  gem 'capistrano-unicorn-nginx', '~> 3.2.0'

  gem 'airbrussh', require: false # Airbrussh pretties up your SSHKit and Capistrano output

#  gem 'slackistrano'
#  gem 'slack-notifier'

  # Static code analyzer
  gem 'rubocop', '0.48.1', require: false

  # Vulnerabilities
  gem 'brakeman'

  # Audit of vulnerabilities on gems
  gem 'bundler-audit'

  # Notifications
  # gem 'hipchat'

  # Annotate models
  gem 'annotate'
end





