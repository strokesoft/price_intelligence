Rails.application.routes.draw do
  ActiveAdmin.routes(self)

  authenticated :user, -> user { user.admin? }  do
    mount DelayedJobWeb, at: "/delayed_job"
  end

  match "/admin/import_specs/change_subcategories" => 'admin/import_specs#change_subcategories',  via: :post
  match "/admin/import_specs/change_specs"         => 'admin/import_specs#change_specs',          via: :post
  match "/admin/import_specs/change_origin_specs"  => 'admin/import_specs#change_origin_specs',   via: :post
  match "/admin/import_specs/change_spec_examples" => 'admin/import_specs#change_spec_examples',  via: :post

  match "/admin/import_specs/:id/change_subcategories" => 'admin/import_specs#change_subcategories',  via: :post
  match "/admin/import_specs/:id/change_specs"         => 'admin/import_specs#change_specs',          via: :post
  match "/admin/import_specs/:id/change_origin_specs"  => 'admin/import_specs#change_origin_specs',   via: :post
  match "/admin/import_specs/:id/change_spec_examples" => 'admin/import_specs#change_spec_examples',  via: :post

  match "/admin/page_departments/url_example" => 'admin/page_departments#url_example',  via: :post
  match "/admin/page_departments/:id/url_example" => 'admin/page_departments#url_example',  via: :post

  devise_for :users

  resources :public do
    get 'products', on: :collection
  end
  resources :price_categories
  resources :csv_errors
  resources :products
  resources :files do
    post :upload, on: :collection
    get :send_daily_reports, on: :collection
    get :send_top_seller_report, on: :collection
  end

  match "benchmark"          => "benchmark#index", via: [:get, :post]
  match "benchmark/products" => "benchmark#products", via: :get
  match "map"                => "minimum_advertised_price#index", via: :get
  match "price_evolution"    => "price_evolution#index", via: [:get, :post]

  match "product_evolution"     => "product_evolution#index", via: :get
  match "product_evolution/:id" => "product_evolution#show", via: [:get, :post], as: :show_product_evolution

  root to: 'public#index'
end
