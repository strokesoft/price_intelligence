# config valid only for current version of Capistrano
lock "3.8.0"

Airbrussh.configure do |config|
  config.command_output = true
end

set :client, 'stramina'
set :application, 'price_intelligence'

set :repo_url, "git@gitlab.com:#{fetch(:client)}/#{fetch(:application)}.git"

set :user, 'straminaprice'
set :deploy_to, -> { "/home/#{fetch(:user)}/app" }
set :keep_releases, 5

set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/chamber.pem')
set :linked_dirs,  fetch(:linked_dirs,  []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets',
                                                 'vendor/bundle', 'public/system', 'private/xlsx_files',
                                                 'public/uploads')

# Files that won't be copied from script/deploy/{branch}/ into the root directory
set :exclude_deployment_files, []

set :ssh_options, -> {
  {
      user: fetch(:user),
      forward_agent: true,
      compression: 'none',
  }
}

set :rails_env, 'production'
set :log_level, :info
set :local_user, -> { user_name }

# capistrano-db-tasks related
# if you want to remove the dump file after loading
set :db_local_clean, true
set :db_remote_clean, true

# If you want to import assets, you can change default asset dir (default = system)
# This directory must be in your shared directory on the server
set :assets_dir, %w(public/assets)
# if you want to work on a specific local environment (default = ENV['RAILS_ENV'] || 'development')
set :locals_rails_env, 'development'

# Rollbar deployment notification.
set :rollbar_role,  -> { :app }
set :rollbar_env,   -> { fetch(:app_env) }
set :rollbar_user,  -> { user_name }
set :rollbar_token, -> {
  file = File.expand_path("../script/deploy/#{fetch(:instance)}/config/settings/rollbar.yml", File.dirname(__FILE__))
  YAML.load_file(file)[fetch(:rails_env)]['rollbar']['access_token']
}
set :rollbar_revision, Proc.new { `git log -n 1 --pretty=format:"%H"` }

# Hipchat notification
set :hipchat_token,     '11aIJajW3H8mjyOayHEDBDq6WHYzINZtNYI000oQ'
set :hipchat_room_name, '4500020'
set :hipchat_announce,  true # notify users?
set :hipchat_env,       -> { fetch(:app_env) }
set :hipchat_human,     -> { user_name }

# Whenever
set :whenever_roles,  -> { :batch }
set :whenever_identifier, -> { "#{fetch(:client)}_#{fetch(:application)}_#{fetch(:app_env)}" }

# Name of the deploying user that will be reported to third party services
def user_name
  require 'etc'
  if (u = %x{git config user.name}.strip) != ''
    u
  elsif (u = ENV['USER'].to_s) != ''
    u
  elsif (u = Etc.getlogin.to_s) != ''
    u
  else
    'Someone'
  end
end