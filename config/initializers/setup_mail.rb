# Action mailer configuration
ActionMailer::Base.delivery_method = Settings.env.action_mailer.delivery_method.to_sym
ActionMailer::Base.perform_deliveries = Settings.env.action_mailer.perform_deliveries
ActionMailer::Base.raise_delivery_errors = Settings.env.action_mailer.raise_delivery_errors
ActionMailer::Base.asset_host = Settings.env.action_mailer.asset_host

Settings.env.action_mailer.default_url_options.each do |name, value|
  ActionMailer::Base.default_url_options[name.to_sym] = value
end

Settings.env.action_mailer.smtp_settings.each do |name, value|
  ActionMailer::Base.smtp_settings[name.to_sym] = value
end