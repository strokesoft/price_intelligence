# Supported options: :resque, :sidekiq, :delayed_job, :queue_classic, :torquebox, :backburner, :que, :sucker_punch
# https://github.com/mhfs/devise-async/issues/105
# Devise::Async.backend = :delayed_job