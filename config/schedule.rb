# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever

env :PATH, ENV['PATH']
set :path, '/home/straminaprice/app/current'
set :environment, :production
set :output, {:standard => '/home/straminaprice/app/shared/log/cron.log', :error => '/home/straminaprice/app/shared/log/cron_error.log' }

every 1.day, at: '1:00 am', roles: [:batch] do
  runner "DailyScrapJob.perform_later"
end

every 10.minutes, roles: [:batch] do
  runner "CachingDashboardJob.perform_later if Delayed::Job.where(attempts: 0).where(\"handler LIKE '% job_class: CachingDashboardJob%'\").count == 0"
end

# every :saturday, at: '9:30 am', roles: [:batch] do
#   runner "WeeklyScrapSpecsJob.perform_later"
# end
