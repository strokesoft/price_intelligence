# Simple Role Syntax
# ==================
# Supports bulk-adding hosts to roles, the primary
# server in each group is considered to be the first
# unless any hosts have the primary property set.
# Don't declare `role :all`, it's a meta role
# role :app,          %w(stramina-int.aspgems.com)
# role :web,          %w(stramina-int.aspgems.com)
# role :db,           %w(stramina-int.aspgems.com)
# role :batch,        %w(stramina-int.aspgems.com)
role :app,          %w(staging.priceintelligence.stramina.com)
role :web,          %w(staging.priceintelligence.stramina.com)
role :db,           %w(staging.priceintelligence.stramina.com)
role :batch,        %w(staging.priceintelligence.stramina.com)

set :app_env, 'staging'         # Name used in rollbar/hipchat, etc
set :branch,   fetch(:app_env)  # Git branch deployed
set :instance, fetch(:branch)   # Deploy extra files from script/deploy/<instance>