# Simple Role Syntax
# ==================
# Supports bulk-adding hosts to roles, the primary
# server in each group is considered to be the first
# unless any hosts have the primary property set.
# Don't declare `role :all`, it's a meta role
# role :app,          %w(stramina.aspgems.com stramina-services.aspgems.com)
# role :web,          %w(stramina.aspgems.com)
# role :db,           %w(stramina.aspgems.com)
# role :batch,        %w(stramina-services.aspgems.com)
role :app,          %w(priceintelligence.stramina.com services.priceintelligence.stramina.com)
role :web,          %w(priceintelligence.stramina.com)
role :db,           %w(priceintelligence.stramina.com)
role :batch,        %w(services.priceintelligence.stramina.com)


set :app_env, 'production'      # Name used in rollbar/hipchat, etc
set :branch,   fetch(:app_env)  # Git branch deployed
set :instance, fetch(:branch)   # Deploy extra files from script/deploy/<instance>