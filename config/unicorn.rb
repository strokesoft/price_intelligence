# set path to application
app_dir = File.expand_path("../..", __FILE__)


# if app_dir.include? 'staging'
#   listen 3000, :tcp_nopush => true
#   shared_dir = "/home/rfuentes/staging_stramina/shared"
#   listen "#{shared_dir}/tmp/sockets/staging_unicorn.sock", :backlog => 64
#   pid "#{shared_dir}/tmp/pids/staging_unicorn.pid"
# elsif app_dir.include? 'demo'
  listen 3002, :tcp_nopush => true
  shared_dir = "/home/rails/price_intelligence/shared"
  listen "#{shared_dir}/tmp/sockets/unicorn.sock", :backlog => 64
  pid "#{shared_dir}/tmp/pids/unicorn.pid"
# else
#   listen 8080, :tcp_nopush => true
#   shared_dir = "/home/rfuentes/stramina/shared"
#   listen "#{shared_dir}/tmp/sockets/unicorn.sock", :backlog => 64
#   pid "#{shared_dir}/tmp/pids/unicorn.pid"
# end

working_directory app_dir

# Set unicorn options
worker_processes 4
preload_app true
timeout 3600

# Logging
stderr_path "#{shared_dir}/log/unicorn.stderr.log"
stdout_path "#{shared_dir}/log/unicorn.stdout.log"
