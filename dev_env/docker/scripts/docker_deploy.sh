if [ -f /proc/1/cgroup ] && grep -qa :/docker /proc/1/cgroup ;then
    echo "Añadimos la clave al agente ssh"
    eval "$(ssh-agent -s)"
    ssh-add /root/.ssh/id_rsa
    echo ""

    stage=$1
    while [ "$stage" != "staging" ] && [ "$stage" != "production" ] ; do
        read -p "Indique el entorno a desplegar (staging/production): " stage
    done
    echo ""
    cap "$stage" deploy:prepare
    echo ""

    while [ "$check_deploy" != "si" ] && [ "$check_deploy" != "no" ] ; do
        read -p "¿Está seguro de que quiere desplegar al entorno: $stage? (si/no) : " check_deploy
    done
    echo ""
    if [ "$check_deploy" == "si" ] ;then
        echo "Desplegando entorno: $stage"
    echo ""
        cap "$stage" deploy
    else
        echo "Finalmente no se ha desplegado."
    fi
    echo ""
else
#    if [[ `docker ps -q -f name=price_intelligence_web_deploy` != "" ]] ;then
#        echo "Ya existe un contenedor auxiliar para desplegar arrancado, lo paramos"
#        docker stop price_intelligence_web_deploy
#        echo ""
#    fi
#
#    if [[ `docker ps -a -q -f name=price_intelligence_web_deploy` != "" ]] ;then
#        echo "Ya existe un contenedor auxiliar para desplegar, lo borramos"
#        docker rm price_intelligence_web_deploy
#        echo ""
#    fi
#
#    echo "Lanzamos un contenedor auxiliar para desplegar"
#        docker run --name price_intelligence_web_deploy price_intelligence_web sleep 600 &
#    echo ""
#
#    while [[ `docker ps -q -f name=price_intelligence_web_deploy` == "" ]] ;do
#        echo "Esperando que el contenedor arranque"
#        sleep 2
#    done
#    echo ""
#
#docker ps -a
    ./docker_start.sh

    CONTAINER=`docker-compose ps -q web`

    while [[ "$CONTAINER" == "" || `docker ps -q -f id=$CONTAINER` == "" ]] ;do
        echo "Esperando que el contenedor web arranque"
        sleep 1
        CONTAINER=`docker-compose ps -q web`
    done
    echo ""

    echo "Copiamos nuestra configuración de git al contenedor (correo y usuario)"
    docker exec $CONTAINER git config --global user.email $(eval "git config --global user.email")
    docker exec $CONTAINER git config --global user.name $(eval "git config --global user.name")
    echo ""

    echo "Copiamos la clave privada al contenedor (~/.ssh/id_rsa)"
    docker cp ~/.ssh/id_rsa $CONTAINER:/root/.ssh/id_rsa
    echo ""

    echo "Lanzamos este mismo script dentro de docker"
    docker exec -ti $CONTAINER bash -c "./docker_deploy.sh $1"
    echo ""

#    echo "Paramos el contenedor auxiliar y lo borramos"
#    docker stop price_intelligence_web_deploy
#    docker rm price_intelligence_web_deploy
fi
