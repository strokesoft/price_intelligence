CONTAINER=`docker-compose ps -q web`
if [[ "$CONTAINER" == "" ]]; then
    docker-compose up &
else
    if [[ `docker ps -q -f id=$CONTAINER` == "" ]] ;then
        docker-compose up web &
    else
        echo "Service already running\n\n"
    fi
fi
