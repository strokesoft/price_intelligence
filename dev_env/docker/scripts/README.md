This forlder contains a basic script set.

Probably will be usefull for you to lauch other commands and set as:

=============

> docker-compose exec web bash

To get a shell in launched web container

=============

> docker-compose stop web

for stop only web container (the you can run docker_start.sh again)

=============

> docker-compose stop web
> docker-compose up &

To restart web server.

Note that it:
* launch build install before to try to install new required gems)
* launch pending migrations
* launch seed to populate database with needed information

=============


> docker-compose stop web
> docker-compose run web bash -c "rake db:drop; rake db:create; rake db:migrate; rake db:fixtures:load; rake db:seed" 
> docker-compose up &

To reset dev database

=============

> docker-compose run web bash -c "ruby -I test test/../FILE.rb"

To launch specific test file

=============

> docker-compose run web bash -c "ruby -I test test/../FILE.rb -n test_NAME"

To launch specific test on especific file

=============
