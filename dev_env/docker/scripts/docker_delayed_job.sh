if [ -f /proc/1/cgroup ] && grep -qa :/docker /proc/1/cgroup ;then
    echo "Añadimos la clave al agente ssh"
    eval "$(ssh-agent -s)"
    ssh-add /root/.ssh/id_rsa
    echo ""

    stage=$1
    while [ "$stage" != "staging" ] && [ "$stage" != "production" ] ; do
        read -p "Indique el entorno (staging/production): " stage
    done
    echo ""

    action=$2
    while [ "$action" != "start" ] && [ "$action" != "stop" ] && [ "$action" != "restart" ]; do
        read -p "Indique la accion a realizar en $stage (start/stop/restart): " action
    done
    echo ""

    cap "$stage" delayed_job:"$action"
    echo ""
else
    ./docker_start.sh

    CONTAINER=`docker-compose ps -q web`
    while [[ "$CONTAINER" == "" || `docker ps -q -f id=$CONTAINER` == "" ]] ;do
        echo "Esperando que el contenedor web arranque"
        sleep 1
        CONTAINER=`docker-compose ps -q web`
    done
    echo ""

    echo "Copiamos nuestra configuración de git al contenedor (correo y usuario)"
    docker exec $CONTAINER git config --global user.email $(eval "git config --global user.email")
    docker exec $CONTAINER git config --global user.name $(eval "git config --global user.name")
    echo ""

    echo "Copiamos la clave privada al contenedor (~/.ssh/id_rsa)"
    docker cp ~/.ssh/id_rsa $CONTAINER:/root/.ssh/id_rsa
    echo ""

    echo "Lanzamos este mismo script dentro de docker"
    docker exec -ti $CONTAINER bash -c "./docker_delayed_job.sh $1 $2"
    echo ""
fi
