./docker_start.sh

CONTAINER=`docker-compose ps -q web`

while [[ "$CONTAINER" == "" || `docker ps -q -f id=$CONTAINER` == "" ]] ;do
    echo "Esperando que el contenedor web arranque"
    sleep 1
    CONTAINER=`docker-compose ps -q web`
done
echo ""

echo "Encriptamos los secretos en todos los entornos"
docker exec -ti $CONTAINER bash -c "rake chamber:secure_all"
echo ""
