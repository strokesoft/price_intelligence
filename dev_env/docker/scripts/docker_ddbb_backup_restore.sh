

echo "Descargando último backup de la bbdd en producción (puede usar la última versión descargada cancelando la descarga)"
echo ""

./download_production_db.sh

echo ""
echo ""

# Assign to DB_USER and DB_NAME values of user and database configured on database.yml
DB_USER=postgres
DB_NAME=myapp_development

echo "Paramos el contenedor con la aplicación"
docker-compose stop web
echo ""
echo "Borramos y recreamos la BBDD"
docker-compose up -d db
docker-compose run web bash -c "bundle install; rake db:drop; rake db:create"
echo ""
echo "Cargamos el backup de la DDBB (se necesita 7z para descomprimir dev_env/backups/latest.sql.gz)"
7z x -aoa -odev_env/backups dev_env/backups/latest.sql.gz
docker-compose exec db bash -c "psql -U "${DB_USER}" -d "${DB_NAME}" < /backups/latest.sql"
# rm backups/latest.sql
echo ""
echo "Volvemos a arrancar todos los contenedores."
docker-compose up &
