docker-compose stop web
docker-compose run web bash -c "bundle install; rake db:drop; rake db:create; rake db:migrate; rake db:fixtures:load; rake db:seed"
docker-compose up &