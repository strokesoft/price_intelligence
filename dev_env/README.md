./dev_env/backup:
=================

Use this directory to share backups with your dev environment.

Usefull for example to place production backups downloaded for test purpoueses or so.


./dev_env/postgres-data:
========================

Use this directory to store your dev environment database

Then it could be shared between different builds, even different dev environments (with same SGBD)

NOTE: this file is hidden to avoid issues on dabatase init (by no empty directory)
