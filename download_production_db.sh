mkdir -p dev_env/backups

read -p "Indique su usuario en priceintelligence.stramina.com (pulse intro para cancelar la descarga): " user

echo ""

if [ "$user" != "" ] ;then
    echo "Procediendo a descarga con usuario: $user"
    scp "$user"@priceintelligence.stramina.com:/var/opt/backups/projects/straminaprice/dbdumps/latest.sql.gz dev_env/backups
else
    echo "No se ha indicado usuario, cancelamos la descarga"
fi
