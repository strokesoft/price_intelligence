# == Schema Information
#
# Table name: product_prices
#
#  id              :integer          not null, primary key, indexed => [date]
#  product_page_id :integer          indexed
#  order           :integer
#  price           :float            not null
#  date            :date             indexed, indexed => [id], indexed => [out_of_stock]
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  out_of_stock    :boolean          indexed, indexed => [date]
#
# Indexes
#
#  index_product_prices_on_date                   (date)
#  index_product_prices_on_id_and_date            (id,date)
#  index_product_prices_on_out_of_stock           (out_of_stock)
#  index_product_prices_on_out_of_stock_and_date  (out_of_stock,date)
#  index_product_prices_on_product_page_id        (product_page_id)
#

require 'test_helper'

class ProductPriceTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
