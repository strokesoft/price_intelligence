# == Schema Information
#
# Table name: product_pages
#
#  id             :integer          not null, primary key
#  product_id     :integer          indexed
#  page           :string
#  image_url      :string
#  url            :string
#  shipment       :string
#  date           :date
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  ean            :string
#  category       :string           indexed
#  foul_name      :string
#  specs          :json
#  out_of_stock   :boolean
#  last_aparition :datetime
#  usable         :boolean          default(TRUE), indexed
#
# Indexes
#
#  index_product_pages_on_category    (category)
#  index_product_pages_on_product_id  (product_id)
#  index_product_pages_on_usable      (usable)
#

require 'test_helper'

class ProductPageTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
