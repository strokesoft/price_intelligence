require 'test_helper'

class CsvErrorsControllerTest < ActionController::TestCase
  setup do
    @csv_error = csv_errors(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:csv_errors)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create csv_error" do
    assert_difference('CsvError.count') do
      post :create, csv_error: { category: @csv_error.category, error: @csv_error.error, mode: @csv_error.mode, parser: @csv_error.parser, source_line: @csv_error.source_line, warn: @csv_error.warn }
    end

    assert_redirected_to csv_error_path(assigns(:csv_error))
  end

  test "should show csv_error" do
    get :show, id: @csv_error
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @csv_error
    assert_response :success
  end

  test "should update csv_error" do
    patch :update, id: @csv_error, csv_error: { category: @csv_error.category, error: @csv_error.error, mode: @csv_error.mode, parser: @csv_error.parser, source_line: @csv_error.source_line, warn: @csv_error.warn }
    assert_redirected_to csv_error_path(assigns(:csv_error))
  end

  test "should destroy csv_error" do
    assert_difference('CsvError.count', -1) do
      delete :destroy, id: @csv_error
    end

    assert_redirected_to csv_errors_path
  end
end
