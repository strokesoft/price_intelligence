# Preview all emails at http://localhost:3000/rails/mailers/daily_report
class DailyReportPreview < ActionMailer::Preview
  def daily
    today = Date.today
    DailyReport.daily('%02i' % today.day, '%02i' % today.month, '%i' % today.year)
  end

  def daily_dashboard
    user = User.first
    society = user.society
    start_date, end_date = DataGroupedCalculation.date_ranges.first
    markets = society.reference_ecommerces_slug_and_name.keys
    markets = nil if markets.blank?
    brands = Brand.reference_names(society)
    brands = nil if brands.blank?

    data = DataGroupedCalculation.new(start_date, end_date, society, {
      listed: 'available', page: markets, brand: brands
    }).fetch_dashboard
    dashboard = SendDailyReportsJob.new.send('basic_info', data)
    DailyReport.daily_dashboard(user: user, dashboard: dashboard,
                                start_date: start_date, end_date: end_date,
                                markets: markets, brands: brands)
  end
end
