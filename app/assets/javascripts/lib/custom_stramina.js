$(document).ready(function () {
  function after_load() {
    $('[data-url]').click(function (e) {
      var $this = $(this),
        loadurl = $this.attr('data-url'),
        target = $this.attr('data-target'),
        remote = $this.attr('data-remote');
      if (remote) {
        $.get(loadurl, function (data) {
          if(target) $(target).html(data);
        });
      } else {
        window.location.href = loadurl;
      }
    });

    $('.open-dialog').unbind('click').on('click', function (e) {
      e.preventDefault(e);
      var element = $(this);
      var url = element.data('remote-url');
      var buttons = [{
        label: 'Close',
        action: function(dialogRef){
          dialogRef.close();
        }
      }];
      if (element.data('edit-url')) {
        buttons.unshift({
          icon: 'fa fa-pencil',
          label: 'Edit',
          cssClass: 'btn-primary pull-left',
          action: function(dialogRef){
            dialogRef.close();
            window.location = element.data('edit-url');
          }
        });
      }
      BootstrapDialog.show({
        message: $('<div id="paginated-dialog"><div class="loader-wrapper"><div class="loader"></div></div></div>').load(url),
        size: BootstrapDialog.SIZE_WIDE,
        onshow: function (dialogRef) {
          dialogRef.getModalHeader().hide();
        },
        onshown: function (dialogRef) {
          dialogRef.getModalHeader().hide();
        },
        buttons: buttons
      });
    });

    //Control left menu
    setTimeout(hideShowOnLeftMenu, 250);

    $('.selectTwo').select2({
      closeOnSelect : false,
      multiple: true,
      include_blank: true,
      disabled: false,
      width: '100%',
      placeholder: 'Select value'
    });

    $('.reset-form').off('click').on('click', function() {
      $('#models').val('').trigger('change');
      $('#filters').trigger('reset');
    });

    $('.reset-brands').off('click').on('click', function() {
      var bck = $({});
      var brands = $('#collapseTwo').find('label:not(".hide") input[type="checkbox"]');

      var lastElement;
      brands.each(function() {
        // Backup and remove events associated with the element
        bck.off();
        copy_events($(this), bck);
        $(this).off();

        // Update the element and restore events
        $(this).prop('checked', true);
        copy_events(bck, $(this));
      })
      // Trigger change event on the first element (can be any) to reload the page
      $(brands[0]).trigger('change');
    });

    $('.uncheck-element').on('click', function() {
      window.uncheck($(this));
    });

    $('.show-filters').on('click', function() {
      window.showFilters($(this));
    });

    //TABS and AJAX
    $('[data-toggle="tabajax"]').click(function (e) {
      var $this = $(this),
        loadurl = $this.attr('href'),
        target = $this.attr('data-target');
      if ($(target).html() == '') {
        $.get(loadurl, function (data) {
          $(target).html(data);
        });
      }
      $this.tab('show');
      return false;
    });

    setTimeout(function(){
      $('.dialog-with-ajax, .tabs-with-ajax').each(function (index) {
        var element = $(this);
        var target = $("#"+element.data('target'));
        element.off('click', 'nav.pagination a, .i-link');
        element.on('click', 'nav.pagination a, .i-link', function (e) {
          e.preventDefault(e);
          if (element.hasClass('dialog-with-ajax')) {
            $(document).scrollTop(0);
            target.html('<div class="loader-wrapper"><div class="loader"></div></div>');
          }
          target.load($(this).attr("href"));
          return false;
        });
      });
      //Header fixed
      $('.fixed').stickySort({fixedOffset: 70});

      $(".missing_picture").on("error", function(){
        $(this).attr('src', gon.missing_picture);
      });
    }, 250);

    //Sort tables
    $('th.sort').click(function(){
      var iTag = $(this).find('i');
      var className = this.asc ? "la-chevron-down" : "la-chevron-up";
      $(this).parent().find('i').removeClass('la la-chevron-up la-chevron-down');
      iTag.addClass("la "+className);
      var table = $(this).parents('table').eq(0)
      var gt = table.data('gt') || 0;
      var rows = table.find('tr:gt('+gt+')').toArray().sort(comparer($(this).index()))
      this.asc = !this.asc
      if (!this.asc){rows = rows.reverse()}
      for (var i = 0; i < rows.length; i++){table.append(rows[i])}
    });
  }
  after_load();

  function comparer(index) {
    return function(a, b) {
      var valA = getCellValue(a, index), valB = getCellValue(b, index)
      return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.toString().localeCompare(valB)
    }
  }
  function getCellValue(row, index){
    return $(row).children('td').eq(index).text().replace(/[%\.€]/g, "");
  }

  $('.filters-type').on('click', function (e) {
    setTimeout(function(){window.hideShowBrands();}, 350);
  });

  $('#m_aside_left_minimize_toggle').click(function () {
    $('.menu-filter-check').toggleClass("hide");
  });

  $('.seebutton').click(function () {
    $(this).toggleClass('seebutton')
    if ($(this).hasClass('seebutton')) {
      $(this).text('See More');
    } else {
      $(this).text('See Less');
    }
  });

  $('.card-header').click(function () {
    $(this).toggleClass("card-header card-header-click")
  });

  // hide spinner
  $(".spinner").hide();

  // show spinner on AJAX start
  $(document).ajaxSend(function (event, xhr, settings) {
    if (!settings.url.match("^/public/products|^/products")) {
      $(".spinner").show();
    }
  });
  $(window).on('beforeunload', function (e) {
    if (e.target.activeElement.className.indexOf('action-download') == -1) {
      $(".spinner").show();
    }
  });
  // hide spinner on AJAX stop
  $(document).ajaxStop(function () {
    $(".spinner").hide();
    after_load();
  });

  var nameFunction, urlFunction;
  var pages = [
    'public', 'price_categories', 'benchmark', 'minimum_advertised_price',
    'price_evolution', 'product_evolution', 'product_evolution_detail'
  ];

  $.each(pages, function(index, value) {
    if ($('#view_'+value).length > 0) {
      switch(value) {
        case 'public':
          nameFunction = 'handleOverviewFilters', urlFunction = '/public.js';
          break;
        case 'price_categories':
          nameFunction = 'handleFilters', urlFunction = '/price_categories.js';
          break;
        case 'benchmark':
          nameFunction = 'handleFilters', urlFunction = '/benchmark.js';
          break;
        case 'minimum_advertised_price':
          nameFunction = 'handleMapFilters', urlFunction = '/map.js';
          break;
        case 'price_evolution':
          nameFunction = 'handleFilters', urlFunction = '/price_evolution.js';
          break;
        case 'product_evolution':
          nameFunction = 'handleFilters', urlFunction = '/product_evolution.js';
          break;
        case 'product_evolution_detail':
          nameFunction = 'handleFilters';
          urlFunction = '/product_evolution/' + $('#product_id').attr('class') + '.js';
          break;
      }
      $('#filters').on('reset', function() {
        setTimeout(function() {
          $('#listed').addClass('active');
          $('#available').removeClass('active');
          window[nameFunction](urlFunction);
        },1);
      });
      $('#m_dashboard_daterangepicker').on('apply.daterangepicker', function(ev, picker) {
        window[nameFunction](urlFunction);
      });
      $('#collapseOne input, #collapseTwo input, #collapseFive input, #collapseSix input, ' +
        '#advancedFilters input, #advancedFilters select').on('change', function () {
        window[nameFunction](urlFunction);
      });
      $('.search-button, .remove-all').on('click', function () {
        window[nameFunction](urlFunction);
      });
      $('#collapseThree input').on('change', function() {
        $(this).addClass("active").siblings('input:radio[name="options3"]').removeClass("active");
        window[nameFunction](urlFunction);
      });
      $('#start_date').on('apply.daterangepicker', function(ev, picker) {
        window[nameFunction](urlFunction);
      });
    }
  });

  $(".models-data-ajax").select2({
    ajax: {
      url: "/products" ,
      dataType: 'json',
      delay: 250,
      data: function (params) {
        var picker = $('#m_dashboard_daterangepicker').data('daterangepicker');
        var startDate = picker.startDate.format('YYYY-MM-DD');
        var endDate = picker.endDate.format('YYYY-MM-DD');
        var option = $("input[type='radio']:checked", '#collapseThree').val();
        var status = {};
        var keys = {pages: 'collapseOne', brands: 'collapseTwo', subcategories: 'collapseFive', specs: 'advancedFilters'};
        $.each(keys, function(index, value) { status[index] = []; });
        getStatus(status, keys);
        var data = {
          start_date: startDate,
          end_date: endDate,
          option: option,
          category: gon.category,
          subcategory: status['subcategories'],
          page: status['pages'],
          brand: status['brands'],
          min_price: $("#min_price").val(),
          max_price: $("#max_price").val(),
          search: params.term, // search term
          sheet: params.page   // request page
        };
        $.each(status['specs'], function(index, value) {
          data['specs['+value+']'] = status['specs['+value+']'];
        });
        return data;
      },
      processResults: function (data, params) {
        // parse the results into the format expected by Select2
        // since we are using custom formatting functions we do not need to
        // alter the remote JSON data, except to indicate that infinite
        // scrolling can be used
        params.page = params.page || 1;
        return {
          results: data.products,
          pagination: {
            more: (params.page * 10) < data.meta.total_count
          }
        };
      },
      cache: true
    },
    placeholder: 'Search model',
    escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
    //minimumInputLength: 1,
    templateResult: formatProduct,
    templateSelection: formatProductSelection
  });

  function formatProduct (product) {
    var image_url = (typeof product.image_url === "undefined") ? gon.missing_picture : product.image_url;
    var markup = "<div class='row mx-0'>" +
      "<div class='col-3 px-0'><img src='"+ image_url +"' onError='this.src = gon.missing_picture' class='frame-img'/></div>" +
      "<div class='col-9 px-0'><div class='row mx-0'>"+
      "<div class='col-12'>" + product.brand + "</div>" +
      "<div class='col-12 font-weight-bold'>" + product.name + "</div>"+
      "<div class='col-12'><small class='text-warning'>"+
      (product.ean || product.asin || product.vendor_part_number || product.mediamarkt_ref) +
      "</small></div></div></div>";
    return markup;
  }

  function formatProductSelection (product) {
    return product.name || product.ean;
  }

  function hideShowOnLeftMenu() {
    //Control brands with zero elements
    window.hideShowBrands();

    //Control categories with zero elements
    window.hideShowCategories();

    //Control categories with zero elements
    window.hideShowSubcategories();
  }

  $("input[type='radio']", '#collapseSix').change(function() {
    var category = this.value;
    $("label.label-left").each(function (i, element) {
      if ($.trim($(element).html()) == category) {
        $(element).parent().show();
      } else {
        $(element).parent().hide();
      }
    });
  });
});
