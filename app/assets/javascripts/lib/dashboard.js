var Dashboard = function() {
  var drp = function() {
    function e(e, a, r) {
      var o = "", l = "";
      a - e < 100 ? (o = "Today:", l = e.format("MMM D")) : "Yesterday" == r ? (o = "Yesterday:", l = e.format("MMM D")) : l = e.format("MMM D") + " - " + a.format("MMM D"), t.find(".m-subheader__daterange-date").html(l), t.find(".m-subheader__daterange-title").html(o)
    }
    if (0 != $("#m_dashboard_daterangepicker").length) {
      var t = $("#m_dashboard_daterangepicker");
      var a = t.data('start-date') ? moment(t.data('start-date'), 'YYYY-MM-DD') : moment();
      var r = t.data('end-date') ? moment(t.data('end-date'), 'YYYY-MM-DD') : moment();
      var m = t.data('min-date') ? moment(t.data('min-date'), 'YYYY-MM-DD') : null;
      t.daterangepicker({
        startDate: a,
        endDate: r,
        minDate: m,
        opens: "left",
        ranges: {
          Today: [moment(), moment()],
          Yesterday: [moment().subtract(1, "days"), moment().subtract(1, "days")],
          "Last 7 Days": [moment().subtract(6, "days"), moment()],
          "Last 30 Days": [moment().subtract(29, "days"), moment()],
          "This Month": [moment().startOf("month"), moment().endOf("month")],
          "Last Month": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
        }
      }, e), e(a, r, "")
    }

    if (0 != $("#start_date.m-a-p").length) {
      var t = $("#start_date");
      var a = t.val() ? moment(t.val(), 'YYYY-MM-DD') : moment();
      t.daterangepicker({
        start_date: a,
        singleDatePicker: true,
        showDropdowns: true,
        minYear: parseInt(moment().subtract(3, "year").format('YYYY'),10),
        maxYear: parseInt(moment().format('YYYY'),10),
        opens: "left",
        locale: {
          format: 'YYYY-MM-DD'
        }
      }, function(start, end, label) {
      });
    }  
  };
  return {
    init: function() {
      drp()
    }
  }
}();
jQuery(document).ready(function() {
  Dashboard.init()
});