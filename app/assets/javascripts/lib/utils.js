function showFlashMessage(message) {
  if (message) {
    $("#flash_message span").html(message);
    $("#flash_message").show();
  }
}

function hasOtherBrandsWithElements() {
  var flag = false;
  $('#accordion21').find("div[id^='brand-'].m-badge").each(function (i, element) {
    if ($.trim($(element).html()) != '0') {
      flag = true;
      return false;
    }
  });
  return flag;
}

function hideShowBrands() {
  var brands_with_values = 0;
  $("div[id^='brand-'].m-badge").each(function (i, element) {
    var label = $(element).closest("label");
    if ($.trim($(element).html())=='0') {
      label.hide();
    } else {
      label.show();
      brands_with_values ++;
    }
  });
  if (hasOtherBrandsWithElements()) {
    $('#accordion21').find('.seebutton').show();
  } else {
    $('#accordion21').find('.seebutton').hide();
  }
  if ((brands_with_values == 0) && ($('#collapseTwo *:not(:has(*)):visible').text()=="")) {
    $('#no-brands').show();
  } else {
    $('#no-brands').hide();
  }

  $("[data-filter-type='brand'].show-filters").toggle($('#collapseTwo input:checked').length != 0);
}

function hideShowCategories() {
  $("div[id^='category-'].m-badge").each(function (i, element) {
    var label = $(element).closest("label");
    if ($.trim($(element).html())=='0') {
      label.hide();
    } else {
      label.show();
    }
  });
  if ($('#collapseSix').length > 0)  {
    if (typeof($("input[type='radio']:checked", '#collapseSix').val()) === "undefined") {
      $("div#accordion5").hide();
    } else {
      $("div#accordion5").show();
    }
  }
}

function hideShowSubcategories() {
  $("div[id^='subcategory-'].m-badge").each(function (i, element) {
    var label = $(element).closest("label");
    if ($.trim($(element).html())=='0') {
      label.hide();
    } else {
      label.show();
    }
  });

  $("label.label-left").each(function (i, element) {
    if ($(element).parent().children("label.form-check-label:visible").length == 0) {
      $(element).hide();
    } else {
      $(element).show();
    }
  });

  var checkeds = $('#collapseFive input:checked').length;
  var toggle = (checkeds > 0) && (checkeds != $('#collapseFive input').length);
  $("[data-filter-type='subcategory'].show-filters").toggle(toggle);
}

function getStatus(status, keys) {
  var regExp = /\[([^\]]+)\]/;
  $.each(keys, function(index, value) {
    var data_elements = $('#'+value).find('select, input[type=checkbox]:checked');
    data_elements.each(function () {
      if (index == 'specs') {
        var fullName = $(this).attr('name');
        var name = regExp.exec(fullName)[1];

        if ($(this).is("select")) fullName = 'specs['+name+']';
        if (!status[fullName]) status[fullName] = [];

        if ($(this).is("select")) {
          $(this).find('option:selected').each(function() {
            status[fullName].push($(this).val());
          });
        } else {
          status[fullName].push($(this).val());
        }
        if($.inArray(name, status[index]) < 0) {
          status[index].push(name);
        }
      } else {
        if ($(this).is("select")) {
          $(this).find('option:selected').each(function() {
            status[index].push($(this).val());
          });
        } else {
          status[index].push($(this).val());
        }
      }
    });
  })
}

function handleOverviewFilters(url, message) {
  var picker = $('#m_dashboard_daterangepicker').data('daterangepicker');
  var startDate = picker.startDate.format('YYYY-MM-DD');
  var endDate = picker.endDate.format('YYYY-MM-DD');
  var listed = $("input[type='radio']:checked", '#collapseThree').val();
  // Current status of filters
  var status = {};
  var keys = {pages: 'collapseOne', brands: 'collapseTwo'};
  $.each(keys, function(index, value) { status[index] = []; });
  getStatus(status, keys);
  $.ajax({
    url: url,
    type: "GET",
    data: {start_date: startDate, end_date: endDate, page: status['pages'], brand: status['brands'], option: listed},
    success: function (data) {
      showFlashMessage(message);
    }
  });
}

function handleMapFilters(url, message) {
  var startDate = $('#start_date').val();
  var page = $("input[type='radio']:checked", '#collapseOne').val();
  var category = $("input[type='radio']:checked", '#collapseSix').val();
  var listed = $("input[type='radio']:checked", '#collapseThree').val();
    
  var status = {}
  var keys = {brands: 'collapseTwo', subcategories: 'collapseFive'};
  $.each(keys, function(index, value) { status[index] = []; });
  getStatus(status, keys);
  $.ajax({
    url: url,
    type: "GET",
    data: {start_date: startDate, page: page, brand: status['brands'],
           category: category, subcategory: status['subcategories'], option: listed},
    success: function (data) {
      showFlashMessage(message);
    }
  });
}

function handleFilters(url, message) {
  //Current tab
  var current_tab = $('a[data-toggle].active').attr('id');
  var picker = $('#m_dashboard_daterangepicker').data('daterangepicker');
  var startDate = picker.startDate.format('YYYY-MM-DD');
  var endDate = picker.endDate.format('YYYY-MM-DD');
  var listed = $("input[type='radio']:checked", '#collapseThree').val();
  var min_price = $("#min_price").val(), max_price = $("#max_price").val();
  // Current status of filters
  var status = {};
  var keys = {pages: 'collapseOne', brands: 'collapseTwo', subcategories: 'collapseFive',
              specs: 'advancedFilters', models: 'collapseModels'};
  $.each(keys, function(index, value) { status[index] = []; });
  getStatus(status, keys);

  var data = {
    start_date: startDate,
    end_date: endDate,
    page: status['pages'],
    brand: status['brands'],
    model: status['models'],
    option: listed,
    category: gon.category,
    subcategory: status['subcategories'],
    min_price: min_price,
    max_price: max_price
  };
  $.each(status['specs'], function(index, value) {
    data['specs['+value+']'] = status['specs['+value+']'];
  });
  $.ajax({
    url: url,
    type: "GET",
    data: data,
    success: function (data) {
      showFlashMessage(message);
      //keep current tab if there
      if (current_tab) setTimeout(function(){$('#'+current_tab).trigger('click');},1);
    }
  });
}

function setCookieEndOfDay(cname, cvalue) {
  var now = new Date();
  var expire = new Date();
  expire.setDate(now.getDate() + 1);
  expire.setHours(0);
  expire.setMinutes(0);
  expire.setSeconds(0);
  var expires = "expires=" + expire.toString();
  document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/";
}

function setBindings(data) {
  $.each(data, function(index, value) {
    $("#"+value[0]).html(value[1]);
  });
}

function showFilters(element) {
  $(element.data('ref')).show();
  if(element.data('filter-type')=='brand') {
    $('#accordion21').find('.seebutton').show();
  }
  element.hide();
}

function uncheck(element) {
  var bck = $({});
  var target = $(element.data('ref'));
  var apply_at_the_end = element.data('apply-at-the-end');

  target.each(function() {
    if (apply_at_the_end) {
      bck.off();
      copy_events($(this), bck);
      $(this).off('change');
    }
    if ($(this).is("select")) {
      var value = element.data('value');
      var select = $(this);
      var values = select.val();
      values.splice($.inArray(value, values), 1);
      select.val(values).trigger('change');
      if (!apply_at_the_end) $('.search-button').first().trigger('click');
    } else if ($(this).is("option")) {
      var value = $(this).val();
      var select = $(this).parent();
      var values = select.val();
      values.splice($.inArray(value, values), 1);
      select.val(values);
      if (!apply_at_the_end) select.trigger('change');
    } else if ($(this).is('input[type="number"]') || $(this).is('input[type="text"]')) {
      $(this).val(null);
      if (!apply_at_the_end) $('.search-button').first().trigger('click');
    } else {
      $(this).prop('checked', false).trigger('change');
    }
    if (apply_at_the_end) {
      copy_events(bck, $(this));
    }
  });
}

function onlySelectedBrand(brand) {
  var brands = $('#collapseTwo').find('input[type="checkbox"]');
  brands.each(function() {
    if ($(this).val() != brand) {
      $(this).prop('checked', false);
    } else {
      $(this).prop('checked', true);
    }
  });
  handleFilters('/benchmark.js');
}
// Charts
var chartData = null;
function downloadImg(data) {
  var a = $("<a>").attr("href", data).attr("download", "chart.png").appendTo("body");
  a[0].click(); a.remove();
  $('#content_img').attr('src', data);
  return true;
}
function drawChart(type, id, info, options) {
  switch(type) {
    case 'bubble':
      drawBubbleChart(id, info, options);
      break;
    case 'line':
      drawLineChart(id, info, options);
      break;
  }
}
function drawBubbleChart(id, info, options) {
  chartData = google.visualization.arrayToDataTable(info);
  var chart = new google.visualization.BubbleChart(document.getElementById(id));
  google.visualization.events.addListener(chart, 'click', clickHandler);

  chart.draw(chartData, options);
  $('#download_png').click(function() {
    $('#content_img').attr('src', chart.getImageURI());
  });
  $('#download_excel').click(function() {
    $('#data_url').val(chart.getImageURI());
  });
}
function drawLineChart(id, info, options) {
  chartData = google.visualization.arrayToDataTable(info);
  var chart = new google.visualization.LineChart(document.getElementById(id));
  google.visualization.events.addListener(chart, 'click', clickHandler);

  chart.draw(chartData, options);
  $('#download_png').click(function() {
    $('#content_img').attr('src', chart.getImageURI());
  });
  $('#download_excel').click(function() {
    $('#data_url').val(chart.getImageURI());
  });
}
function clickHandler(e) {
  var match = e.targetID.match(/bubble#(\d+)#(\d+)/);
  if (match && match.length) {
    var row = parseInt(match[2]);
    var brand = chartData.getValue(row, 3);
    onlySelectedBrand(brand);
  }
}
function copy_events(from, to) {
  $.each(from.getEvents(), function() {
    // iterate registered handler of original
    $.each(this, function() {
      to.bind(this.type, this.handler);
    });
  });
};

jQuery.fn.getEvents = function() {
  if (typeof(jQuery._data) == 'function') {
    return jQuery._data(this.get(0), 'events') || {};
  } else if (typeof(this.data) == 'function') { // jQuery version < 1.7.?
    return this.data('events') || {};
  }
  return {};
};

function doScrollTo(ref) {
  var etop = $(ref).offset().top - 75;
  $('html, body').animate({
    scrollTop: etop
  }, 1000);
}