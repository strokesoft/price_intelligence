# Picture Uploader
class PictureUploader < CarrierWave::Uploader::Base
  storage :file

  def extension_whitelist
    %w(jpg jpeg gif png)
  end

  def content_type_whitelist
    /image\//
  end

  def store_dir
    "uploads/#{get_class_name}/#{mounted_as}/#{model.id}"
  end

  def get_class_name
    model.class.to_s.underscore
  end

  def default_url
    ActionController::Base.helpers.asset_url("fallback/missing.png")
  end
end
