class SpreadsheetUploader < CarrierWave::Uploader::Base
  storage :file

  def store_dir
    Rails.configuration.route_to_xlsx_files
  end

  def extension_whitelist
    %w(xlsx xlsm)
  end
end
