class CsvUploader < CarrierWave::Uploader::Base
  storage :file

  def store_dir
    puts "******************************"
    puts "******************************"
    puts "******************************"
    puts "******* #{Rails.configuration.route_to_xlsx_files} ********"
    puts "******************************"
    puts "******************************"
    puts "******************************"
    Rails.configuration.route_to_xlsx_files
  end

  def extension_whitelist
    %w(csv txt)
  end
end
