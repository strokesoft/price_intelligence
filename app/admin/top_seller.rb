ActiveAdmin.register TopSeller do
  permit_params :product_page_id, :date, :topseller_order

  index do
    selectable_column
    column :page  do |product_page|
      Utils.page_name(product_page.page)
    end
    column :topseller_order
    column :date
    column :product_page, class: 'col-url'
    column :updated_at
    actions
  end

  # filter :product_name, :as => :string
  # filter :page
  filter :topseller_order
  filter :date
  filter :created_at
  filter :updated_at

  form do |f|
    f.inputs do
      f.input :product_page_id, as: :search_select, url: admin_product_pages_path,
              fields: ['product_name'], display_name: 'name'
      f.input :date
      f.input :topseller_order
    end
    f.actions
  end

  controller do
    include Caching
    def clear_caching(_resource)
      clear_caching_of('calculations')
    end
  end

  after_save    :clear_caching
  after_destroy :clear_caching
end
