ActiveAdmin.register Product do
  menu parent: I18n.t("active_admin.menu.production"), priority: 130
  skip_before_action :verify_authenticity_token, :only => [:update]

  index do
    selectable_column
    id_column
    column :brand
    column :name, class: 'col-width-200'
    column :image_url do |product|
      a(href: product.image_url, title: product.image_url, target: '_blank') do
        image_tag(product.image_url, class: 'col-img') rescue nil
      end
    end
    column :ean
    column :category
    column :subcategory
    column :specs
    toggle_bool_column :usable, success_message: I18n.t('active_admin.messages.updated_successfully')
    column :fixed_specs
    column :updated_at
    actions
  end

  permit_params Product::ATTRIBUTES

  filter :id
  filter :brand, as: :select
  filter :name
  filter :ean
  filter :asin
  filter :vendor_part_number
  filter :mediamarkt_ref
  filter :category, as: :select
  filter :subcategory, as: :select
  filter :description
  filter :msrp
  filter :specs
  filter :usable, as: :check_boxes
  filter :fixed_specs, as: :check_boxes
  filter :created_at
  filter :updated_at

  form do |f|
    f.inputs do
      f.input :brand
      f.input :name
      f.input :ean
      f.input :category
      f.input :subcategory
      f.input :description
      f.input :usable
      f.input :msrp
      f.input :asin
      f.input :vendor_part_number
      f.input :mediamarkt_ref
      f.input :specs, as: :text, input_html: { class: 'jsoneditor-target' }
    end
    f.actions
  end

  show do
    attributes_table do
      row :brand
      row :name
      row :image do |product|
        image_tag product.image_url, class: 'col-img medium no-center', title: product.image_url rescue nil
      end
      row :ean
      row :category
      row :subcategory
      row :description
      row :usable
      row :msrp
      row :asin
      row :vendor_part_number
      row :mediamarkt_ref
      row :specs
      row :fixed_specs
      row :slug
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

  controller do
    include Caching
    def clear_caching(_resource)
      clear_caching_of('calculations')
    end
  end

  after_save    :clear_caching
  after_destroy :clear_caching
end
