ActiveAdmin.register SocietyBrand do
  belongs_to :society
  permit_params :society_id, :brand_id, :is_reference
  skip_before_action :verify_authenticity_token, :only => [:update]

  config.batch_actions = false

  index do
    column :society
    column :brand
    toggle_bool_column :is_reference, success_message: I18n.t('active_admin.messages.updated_successfully')
    actions
  end

  filter :brand
  filter :is_reference, as: :check_boxes

  form(html: { autocomplete: :off }) do |f|
    f.inputs do
      f.input :society
      f.input :brand
      f.input :is_reference
    end
    f.actions
  end

  show do
    attributes_table do
      row :society
      row :brand
      row :is_reference
    end
  end

  controller do
    include Caching
    def clear_caching(_resource)
      clear_caching_of('calculations')
    end
  end

  after_save :clear_caching
end
