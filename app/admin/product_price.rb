ActiveAdmin.register ProductPrice do
  permit_params :product_page_id, :date, :price, :out_of_stock, :order

  index do
    selectable_column
    column :product_page, class: 'col-url'
    column :order
    number_column :price, as: :currency, unit: "€", separator: ",", delimiter: ".", format: "%n %u"
    column :date
    column :out_of_stock
    column :updated_at
    actions
  end

  filter :product_name, :as => :string
  filter :order
  filter :price, as: :numeric_range_filter
  filter :date
  filter :out_of_stock, as: :check_boxes
  filter :created_at
  filter :updated_at

#  filter :product, as: :string
#
  form do |f|
    f.inputs do
      f.input :product_page_id, as: :search_select, url: admin_product_pages_path,
              fields: ['product_name'], display_name: 'name'
      f.input :order
      f.input :price
      f.input :date
    end
    f.actions
  end

  controller do
    include Caching
    def clear_caching(_resource)
      clear_caching_of('calculations')
    end
  end

  after_save    :clear_caching
  after_destroy :clear_caching
end
