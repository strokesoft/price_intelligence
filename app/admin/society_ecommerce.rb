ActiveAdmin.register SocietyEcommerce do
  belongs_to :society
  permit_params :society_id, :ecommerce_id, :is_reference
  skip_before_action :verify_authenticity_token, :only => [:update]

  config.batch_actions = false

  index do
    column :society
    column :ecommerce
    toggle_bool_column :is_reference, success_message: I18n.t('active_admin.messages.updated_successfully')
    actions
  end

  filter :ecommerce
  filter :is_reference, as: :check_boxes

  form(html: { autocomplete: :off }) do |f|
    f.inputs do
      f.input :society_id
      f.input :ecommerce
      f.input :is_reference
    end
    f.actions
  end

  show do
    attributes_table do
      row :society
      row :ecommerce
      row :is_reference
    end
  end

  controller do
    include Caching
    def clear_caching(_resource)
      clear_caching_of('calculations')
    end
  end

  after_save :clear_caching
end
