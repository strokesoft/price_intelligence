ActiveAdmin.register ImportSpec do
  permit_params :category, :subcategory, :spec, :order, :page, :origin_spec
  skip_before_action :verify_authenticity_token, :only => [:change_subcategories, :change_specs, :change_origin_specs,
                                                           :change_spec_examples]

  config.sort_order = 'category_asc, subcategory_asc, spec_asc, order_asc'

#  filter :page, as: :select, collection: Utils.available_pages.map{|market| ["#{PageBase.get_class(market)::PAGE_NAME}", market]}

  form do |f|
    update_subcategories_list = remote_request(:post, :change_subcategories, {:category=>"$('#import_spec_category').val()"},
                                               :import_spec_subcategory)

    update_spec_list          = remote_request(:post, :change_specs,         {:category=>"$('#import_spec_category').val()",
                                                                              :subcategory=>"$('#import_spec_subcategory').val()"},
                                               :import_spec_spec)

    update_origin_spec_list   = remote_request(:post, :change_origin_specs,  {:category=>"$('#import_spec_category').val()",
                                                                              :subcategory=>"$('#import_spec_subcategory').val()",
                                                                              :page=>"$('#import_spec_page').val()"},
                                               :import_spec_origin_spec)

    update_spec_examples      = remote_request(:post, :change_spec_examples, {:category=>"$('#import_spec_category').val()",
                                                                              :subcategory=>"$('#import_spec_subcategory').val()",
                                                                              :page=>"$('#import_spec_page').val()",
                                                                              :spec=>"$('#import_spec_origin_spec').val()"},
                                               :change_spec_examples)

    f.inputs do
      f.input :category,    :label => 'Categoría de producto',    :as => :select, :collection => Product.order(:category).distinct.pluck(:category),
              :input_html => { :onchange => update_subcategories_list + update_spec_list + update_origin_spec_list}

      f.input :subcategory, :label => 'Subcategoría de producto', :as => :select, :collection => Product.where(category: f.object.category).order(:subcategory).distinct.pluck(:subcategory),
              :input_html => { :onchange => update_spec_list + update_origin_spec_list}

      f.input :spec,        :label => 'Spec de producto',         :as => :select, :collection => Product.product_specs(f.object.category, f.object.subcategory)

      f.input :order,       :label => 'Prioridad'

      f.input :page,        :label => 'Market',                   :as => :select, :collection => Utils.available_pages.map{|market| ["#{PageBase.get_class(market)::PAGE_NAME}", market]},
              :input_html => {:onchange => update_origin_spec_list}

      f.input :origin_spec, :label => 'Spec en el Market',        :as => :select, :collection => ProductPage.where(page: f.object.page).where(category: f.object.category).order('spec').distinct.pluck('json_object_keys(specs) as spec'),
              :input_html => {:onchange => update_spec_examples}
      f.li "<label class='label'>Valores de ejemplo:</label><span id='change_spec_examples'></span>".html_safe
    end
    f.actions
  end

  controller do
    def reder_array(options, id_method = :to_s, text_method = :to_s)
      render :json=>view_context.options_from_collection_for_select(options, id_method, text_method)
    end

    def change_subcategories
      reder_array [""] + Product
                      .where(category: params[:category])
                      .order(:subcategory)
                      .distinct.pluck(:subcategory)
    end

    def change_specs
      reder_array Product.product_specs(params[:category],
                                        params[:subcategory])
    end

    def change_origin_specs
      #TODO: Add subcategories:
      #                       .where(subcategory: params[:subcategory])
      reder_array ProductPage
                      .where(page: params[:page])
                      .where(category: params[:category])
                      .order('spec')
                      .distinct.pluck('json_object_keys(specs) as spec') # .map{ |spec| {id: spec, text: specs_values_examples(page, category, spec)} }, :id, :text
    end

    def change_spec_examples
      spec        = params[:spec]

      #TODO: Add subcategories:
      #                       .where(subcategory: params[:subcategory])
      render :json=> spec.blank? ? "" : "Ejemplos de '<i>#{spec}</i>': '<b>#{ProductPage
                                                    .where(page: params[:page])
                                                    .where(category: params[:category])
                                                    .where("(specs->>'#{spec}') IS NOT NULL")
                                                    .limit(5)
                                                    .distinct.pluck("specs->>'#{spec}'")
                                                    .join("</b>', '<b>")}</b>', ..."
    end
  end
end
