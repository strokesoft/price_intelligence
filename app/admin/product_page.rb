ActiveAdmin.register ProductPage do
  skip_before_action :verify_authenticity_token, :only => [:update]

  scope :all, :default => true do |product_pages|
    product_pages.joins("LEFT OUTER JOIN products AS p ON p.id = product_pages.product_id")
      .select("product_pages.*, p.name, p.name as product_name")
  end

  index do
    selectable_column
    id_column
    column :product, class: 'col-width-100'
    column :page
    column :image_url do |product_page|
      a(href: product_page.image_url, title: product_page.image_url, target: '_blank') do
        image_tag(product_page.image_url, class: 'col-img') rescue nil
      end
    end
    column :url, class: 'col-url', sortable: :url_template do |product_page|
      a(href: product_page.url, title: product_page.url, target: '_blank') {'link'}
    end
    column :images_counter
    column :ean
    column :category
    column :foul_brand
    column :foul_name, class: 'col-width-200'
    column :foul_description, class: 'col-width-200'
    toggle_bool_column :usable, success_message: I18n.t('active_admin.messages.updated_successfully')
    column :date
    column :notes
    actions
  end

  permit_params :product_id, :page, :category, :url, :date, :foul_brand, :foul_name, :foul_description, :shipment, :image_url,
                :ean, :asin, :vendor_part_number, :mediamarkt_ref, :out_of_stock, :last_aparition, :usable, :specs

  preserve_default_filters!
  remove_filter :product_prices
  remove_filter :product
  filter :id
  filter :product_name, :as => :string

  form do |f|
    f.inputs do
      f.input :product_id, as: :search_select, url: admin_products_path, fields: [:name]
      f.input :page
      f.input :category
      f.input :url
      f.input :date
      f.input :foul_brand
      f.input :foul_name
      f.input :foul_description
      f.input :shipment
      f.input :image_url
      f.input :images_counter
      f.input :ean
      f.input :out_of_stock
      f.input :last_aparition, as: :date_time_picker
      f.input :usable
      f.input :asin
      f.input :vendor_part_number
      f.input :mediamarkt_ref
      f.input :specs, as: :text, input_html: { class: 'jsoneditor-target' }
      f.input :notes
    end
    f.actions
  end

  show do
    attributes_table do
      row :product
      row :page
      row :image do |product_page|
        image_tag product_page.image_url, class: 'col-img medium no-center', title: product_page.image_url
      end
      row :url do |product_page|
        a(href: product_page.url, target: '_blank') {product_page.url}
      end
      row :images_counter
      row :ean
      row :category
      row :foul_brand
      row :foul_name
      row :foul_description
      row :asin
      row :vendor_part_number
      row :mediamarkt_ref
      row :specs
      row :out_of_stock
      row :usable
      row :variant
      row :shipment
      row :last_aparition
      row :date
      row :notes
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

  controller do
    include Caching
    def clear_caching(_resource)
      clear_caching_of('calculations')
    end
  end

  after_save    :clear_caching
  after_destroy :clear_caching
end
