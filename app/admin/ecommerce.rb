ActiveAdmin.register Ecommerce do
  config.sort_order = 'name_asc'
  config.batch_actions = false

  menu parent: I18n.t("active_admin.menu.production"), priority: 100
  permit_params :name, :picture, :is_reference, :has_proxy, :color, :proxy_address, :proxy_password, :selenium_proxy
  skip_before_action :verify_authenticity_token, :only => [:update]

  index do
    column :name
    column :picture do |ecommerce|
      image_tag ecommerce.picture.url, class: 'col-img micro'
    end
    toggle_bool_column :is_reference, success_message: I18n.t('active_admin.messages.updated_successfully')
    column :slug
    toggle_bool_column :has_proxy, success_message: I18n.t('active_admin.messages.updated_successfully')
    column :proxy_address
    toggle_bool_column :selenium_proxy, success_message: I18n.t('active_admin.messages.updated_successfully')
    column :color do |ecommerce|
      div(style: "background-color: #{ecommerce.color}") {'&nbsp;'.html_safe} if ecommerce.color
    end
    column :created_at
    column :updated_at
    actions
  end

  filter :name
  filter :slug
  filter :created_at
  filter :is_reference, as: :check_boxes
  filter :has_proxy, as: :check_boxes

  form do |f|
    f.inputs do
      f.input :name
      f.input :picture, as: :file, hint: image_tag(f.object.picture.url, class: 'col-img big no-center')
      f.input :is_reference
      f.input :has_proxy
      li(("<label class='label'><br/></label>" + "Default proxy Crawlera").html_safe)
      f.input :proxy_address
      f.input :proxy_user
      f.input :proxy_password
      f.input :selenium_proxy
      f.input :color, as: :color_picker, palette: Utils.colors
    end
    f.actions
  end

  show do
    attributes_table do
      row :name
      row :picture do |category|
        image_tag category.picture.url, class: 'col-img medium no-center'
      end
      row :slug
      row :is_reference
      row :has_proxy
      row :proxy_address
      row :color do |ecommerce|
        div(style: "background-color: #{ecommerce.color}; width: 50px") {'&nbsp;'.html_safe} if ecommerce.color
      end
      row :selenium_proxy
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end
end
