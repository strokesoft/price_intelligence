ActiveAdmin.register PageDepartment do
  permit_params :order, :page, :department, :category, :subcategory, :url_template, :pages_max, :products_listed_max

  #########################################################################################################

  batch_action :destroy, false

  batch_action :download do |ids|
    results = PageBase.scrapp_page_departments(batch_action_collection.find(ids), false)
    redirect_to collection_path, notice: "#{process_results(results)}"
  end

  batch_action :continue_downloading do |ids|
    results = PageBase.scrapp_page_departments(batch_action_collection.find(ids), true)
    redirect_to collection_path, notice: "#{process_results(results)}"
  end

  #########################################################################################################

  #collection_action es un botón que se muestra arriba a la derecha en index y en collection están los elementos mostrados en la página
  # collection_action :continue_downloading, method: :get do
  #   puts "************************"
  #   puts "************* #{collection} "
  #   puts "************* #{collection.count} "
  #   puts "************************"
  #
  #   # results = PageBase.scrapp_page_departments(collection, true)
  #   # redirect_to collection_path, notice: "#{results}"
  #   redirect_to collection_path, notice: "HOLAAAA index"
  # end

  # action_item :continue_downloading, only: :index  do
  #   link_to 'Continue Downloading', action: :continue_downloading
  # end

  #########################################################################################################

  member_action :download_item, method: :get do
    result = PageBase.scrapp_page_department(resource)
    redirect_to resource_path(resource), notice: "#{process_results(result)}"
  end

  # action_item :download, priority: 0  do
  action_item :download, only: :show   do
    link_to 'Download', action: :download_item
  end

  #########################################################################################################

  member_action :continue_downloading_item, method: :get do
    result = PageBase.scrapp_page_department(resource, resource.pages_last)
    redirect_to resource_path(resource), notice: "#{process_results(result)}"
  end

  # action_item :continue_downloading, priority: 0  do
  action_item :continue_downloading, only: :show   do
    link_to 'Continue Downloading', action: :continue_downloading_item
  end

  #########################################################################################################

  index do
    selectable_column
    column "Order", :order
    column "Page", :page
    column "Department", :department
    column "Category", :category
    column "Subcategory", :subcategory
    column "URL Template", :url_template, class: 'col-url', sortable: :url_template do |page_department|
      begin
        a(href: PageBase.get_class(page_department.page).build_url(page_department.url_template, 1),
          title: page_department.url_template, target: '_blank') do
          page_department.url_template
        end
      rescue SyntaxError
        page_department.url_template
      end
    end
    column "Pag. Max",     :pages_max
    column "Pag. Last",    :pages_last, sortable: :pages_last do |page_department|
      page_department.pages_last < page_department.pages_max ?
          div(style: "background-color: #FF9999") {"#{page_department.pages_last}"} :
          page_department.pages_last
    end
    column "Prod. Max",    :products_listed_max
    column "Prod. Last",   :products_listed_last
    column "Precios Last", :products_valid_last
    column "Updated At", :updated_at
    actions
  end

  form do |f|
    update_url_example = remote_request(:post, :url_example, {:page=>"$('#page_department_page').val()",
                                                              :url_template=>"$('#page_department_url_template').val()"},
                                               :url_example)
    f.inputs do
      f.input :order, :label => 'Orden de descarga'
      #f.input :page, :label => 'Market', :as => :select, :collection => User.all.map{|u| ["#{u.last_name}, #{u.first_name}", u.id]}
      f.input :page, :label => 'Market', :as => :select, :collection => Utils.available_pages.map{|market| ["#{PageBase.get_class(market)::PAGE_NAME}", market]},
              :input_html => {:onchange => update_url_example}
      f.input :department, :label => 'Departamento del market'
      f.input :category, :label => 'Categoría por defecto'
      f.input :subcategory, :label => 'Subcategoría por defecto'
      f.input :url_template, :label => 'Plantilla de URL, Parámetros: page_index y PRODUCTSxPAGE',
              :input_html => {:onchange => update_url_example}
      f.input :pages_max,           :label => 'Páginas max. (para resetear poner a 0)'
      f.input :products_listed_max, :label => 'Productos listados max. (para resetear poner a 0)'
      f.li "<label class='label'>URL de ejemplo:</label><span id='url_example'></span>".html_safe
    end
    f.actions
  end

  controller do
    def url_example
      page         = params[:page]
      url_template = params[:url_template]
      url          = PageBase.get_class(page).build_url(url_template, 1)

      render :json=> (page.blank? || url_template.blank?) ? "" : "<a href='#{url}' target='_blank'>#{url}</a>"
    end

    def process_results(results)
      results = [results] unless results.instance_of? Array

      html = ""
      results.each do |result|
        # html = "#{html} <br/> <a alt='View product list page 1' target='_blank'  href='#{PageBase.get_class(result[:page]).build_url(result[:url_template], 1)}'>"
        html = "#{html} --- "
        html = "#{html} [#{result[:page]}] [#{result[:category]}] [#{result[:subcategory]}]"
        html = "#{html} : Listados: [#{result[:listed_items]}] - Procesados: [#{result[:procesed_items]}]"
        # html = "#{html} </a>"
      end
      html
    end
  end
end
