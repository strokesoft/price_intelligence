ActiveAdmin.register Society do
  menu parent: I18n.t("active_admin.menu.access"), label: I18n.t("active_admin.menu.societies"), priority: 120
  permit_params :name, :acronym, :picture, :has_all_brands, :has_all_ecommerces, :has_all_categories,
                :has_filter_by_specs, :min_date,
                brand_ids: [], trademark_ids: [], category_ids: [], ecommerce_ids: []

  skip_before_action :verify_authenticity_token, :only => [:update]

  config.batch_actions = false

  sidebar I18n.t('activerecord.attributes.society.details'), only: [:show, :edit] do
    ul do
      li link_to I18n.t('activerecord.attributes.society.ecommerces'), admin_society_society_ecommerces_path(resource)
      li link_to I18n.t('activerecord.attributes.society.brands'), admin_society_society_brands_path(resource)
    end
  end

  index do
    id_column
    column :name
    column :acronym
    column :picture do |society|
      image_tag society.picture.url, class: 'col-img micro'
    end
    column :min_date
    toggle_bool_column :has_all_ecommerces, success_message: I18n.t('active_admin.messages.updated_successfully')
    toggle_bool_column :has_all_brands, success_message: I18n.t('active_admin.messages.updated_successfully')
    toggle_bool_column :has_all_categories, success_message: I18n.t('active_admin.messages.updated_successfully')
    toggle_bool_column :has_filter_by_specs, success_message: I18n.t('active_admin.messages.updated_successfully')
    #list_column :brand_names, list_type: :ol
    column :created_at
    actions
  end

  index as: :grid do |society|
    link_to image_tag(society.picture.url, class: 'col-img big'), admin_society_path(society)
  end

  filter :name
  filter :acronym
  filter :min_date
  filter :created_at
  filter :updated_at
  filter :has_all_ecommerces, as: :check_boxes
  filter :has_all_brands, as: :check_boxes
  filter :has_all_categories, as: :check_boxes
  filter :has_filter_by_specs, as: :check_boxes

  form(html: { autocomplete: :off }) do |f|
    f.inputs do
      f.input :name
      f.input :acronym
      f.input :picture, :as => :file, :hint => image_tag(f.object.picture.url, class: 'col-img big no-center')
      f.inputs I18n.t('activerecord.attributes.society.permissions') do
        f.input :min_date
        f.input :has_all_ecommerces
        f.input :has_all_brands
        f.input :has_all_categories
        f.input :has_filter_by_specs
        f.input :brand_ids, as: :select, multiple: true, collection: Brand.all.order(:name).map { |b| [b.name, b.id] }
        f.input :trademark_ids, as: :select, multiple: true, collection: Trademark.all.order(:name).map { |t| [t.name, t.id] }
        f.input :ecommerce_ids, as: :select, multiple: true, collection: Ecommerce.all.order(:name).map { |e| [e.name, e.id] }

        f.inputs class: 'tree' do
          Category.roots.order(:position).each_with_index do |root, idx|
            f.input :category_ids, label: ('' if idx > 0), as: :check_boxes, collection: [[root.name, root.id]], input_html: {class: 'parent'}
            f.input :category_ids, label: '', as: :check_boxes, collection: root.children.map { |n| [n.name, n.id] }, input_html: {class: 'child'}
          end
        end
      end
    end
    f.actions
  end

  show do
    attributes_table do
      row :name
      row :acronym
      row :picture do |society|
        image_tag society.picture.url, class: 'col-img medium no-center'
      end
      row :min_date
      row :has_all_ecommerces
      row :has_all_brands
      row :has_all_categories
      row :has_filter_by_specs
      #list_row :brand_names, list_type: :ol
      row :created_at
      row :updated_at

      columns do
        unless society.has_all_ecommerces?
          column do
            panel link_to(I18n.t('activerecord.attributes.society.ecommerces'),
                          admin_society_society_ecommerces_path(resource), class: 'no-decoration') do
              table_for society.society_ecommerces do
                column :name
                column :is_reference
              end
            end
          end
        end
        unless society.has_all_brands?
          column do
            panel link_to(I18n.t('activerecord.attributes.society.brands'),
                          admin_society_society_brands_path(resource), class: 'no-decoration') do
              table_for society.society_brands do
                column :name
                column :is_reference
              end
            end
          end
        end
        column do
          panel I18n.t('activerecord.attributes.society.trademarks') do
            table_for society.trademarks do
              column :name
            end
          end
        end
        unless society.has_all_categories?
          column do
            panel I18n.t('activerecord.attributes.society.categories') do
              table_for Category.sort_by_ancestry(society.categories) do
                column(:name) { |category| content_tag(:span, category.name, class: (category.has_children? ? 'tree-parent' : 'tree-child')) }
              end
            end
          end
        end
      end
    end
    active_admin_comments
  end

  controller do
    include Caching
    def clear_caching(_resource)
      clear_caching_of('calculations')
    end
  end

  after_save :clear_caching
end
