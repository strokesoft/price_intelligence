include AdminHashDataHelper
ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do
    columns do
      column do
        ['today', 'yesterday'].each do |day|
          panel "Numbers for #{day}" do
            range = Date.send(day).beginning_of_day..Date.send(day).end_of_day
            products_count = fetch_caching_of('end_of_day', "products_count_#{day}", type: 'Integer', expire_at_end_of_day: true) do
              Product.where(created_at: range).count
            end
            product_prices_count = fetch_caching_of('end_of_day', "product_prices_count_#{day}", type: 'Integer', expire_at_end_of_day: true) do
              ProductPrice.where(created_at: range).count
            end
            product_pages_count = fetch_caching_of('end_of_day', "product_pages_count_#{day}", type: 'Integer', expire_at_end_of_day: true) do
              ProductPage.where(created_at: range).count
            end
            info = [AdminHashData[name: 'Products', total: products_count],
                    AdminHashData[name: 'Product Prices', total: product_prices_count],
                    AdminHashData[name: 'Product Pages', total: product_pages_count]]

            table_for info do
              column("Name") do |entry|
                entry[:name]
              end
              column("Total") do |entry|
                entry[:total]
              end
            end
          end
        end
      end

      column do
        panel "New products" do
          new_products = Product.where(created_at: Date.today.beginning_of_day..Date.today.end_of_day)
          if new_products.size > 0
            table_for new_products.all do |p|
              p.column("name") { |product| product.name }
              p.column("category") { |product| product.category }
            end
          else
            div class: "blank_slate_container" do
              span class: "blank_slate" do
                span I18n.t("active_admin.today_empty")
              end
            end
          end
        end
      end
    end
  end # content
end
