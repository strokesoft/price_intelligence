ActiveAdmin.register User do
  config.batch_actions = false
  config.sort_order = 'id_asc'

  skip_before_action :verify_authenticity_token, :only => [:update]

  menu parent: I18n.t("active_admin.menu.access"), label: I18n.t("active_admin.menu.users"), priority: 130
  permit_params :email, :password, :password_confirmation, :first_name, :last_name, :society_id,
                :admin, :has_daily_email

  index do
    selectable_column
    column :society_id do |user|
      link_to user.society&.name, admin_society_path(user.society) if user.society
    end
    column :first_name
    column :last_name
    column :email
    toggle_bool_column :admin, success_message: I18n.t('active_admin.messages.updated_successfully')
    toggle_bool_column :has_daily_email, success_message: I18n.t('active_admin.messages.updated_successfully')
    column :current_sign_in_at
    column :created_at
    actions
  end

  filter :society, as: :select
  filter :first_name
  filter :last_name
  filter :email
  filter :admin, as: :check_boxes
  filter :has_daily_email, as: :check_boxes
  filter :current_sign_in_at
  filter :sign_in_count
  filter :created_at

  form(html: { autocomplete: :off }) do |f|
    f.inputs do
      f.input :society, as: :select, include_blank: true
      f.input :email, :input_html => { class: 'autocomplete-off' }
      f.input :password, :input_html => { class: 'autocomplete-off' }
      f.input :password_confirmation, :input_html => { class: 'autocomplete-off' }
      hr
      f.input :first_name, :input_html => { class: 'autocomplete-off' }
      f.input :last_name, :input_html => { class: 'autocomplete-off' }
      f.input :admin
      f.input :has_daily_email
    end
    f.actions
  end

  show do
    attributes_table do
      row :society
      row :first_name
      row :last_name
      row :email
      row :admin
      row :has_daily_email
      row :current_sign_in_at
      row :sign_in_count
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

end
