ActiveAdmin.register Brand do
  config.sort_order = 'name_asc'

  menu parent: I18n.t("active_admin.menu.production"), priority: 110
  permit_params :name, :picture, :is_reference, :color
  skip_before_action :verify_authenticity_token, :only => [:update]

  index do
    selectable_column
    column :name
    column :picture do |brand|
      image_tag brand.picture.url, class: 'col-img micro'
    end
    toggle_bool_column :is_reference, success_message: I18n.t('active_admin.messages.updated_successfully')
    column :color do |brand|
      div(style: "background-color: #{brand.color}") {'&nbsp;'.html_safe} if brand.color
    end
    column :created_at
    column :updated_at
    actions
  end

  filter :name
  filter :created_at
  filter :is_reference, as: :check_boxes

  form do |f|
    f.inputs do
      f.input :name
      f.input :picture, as: :file, hint: image_tag(f.object.picture.url, class: 'col-img big no-center')
      f.input :is_reference
      f.input :color, as: :color_picker, palette: Utils.colors
    end
    f.actions
  end

  show do
    attributes_table do
      row :name
      row :picture do |brand|
        image_tag brand.picture.url, class: 'col-img medium no-center'
      end
      row :is_reference
      row :color do |brand|
        div(style: "background-color: #{brand.color}; width: 50px") {'&nbsp;'.html_safe} if brand.color
      end
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

  controller do
    include Caching
    def clear_caching(_resource)
      clear_caching_of('calculations')
    end
  end

  after_save    :clear_caching
  after_destroy :clear_caching
end
