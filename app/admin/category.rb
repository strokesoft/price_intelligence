ActiveAdmin.register Category do
  config.sort_order = 'ancestry_desc, name_asc'
  config.batch_actions = false

  sortable tree: true,
           sorting_attribute: :position,
           parent_method: :parent,
           children_method: :children,
           roots_method: :roots,
           roots_collection: proc { Category.roots }

  menu parent: I18n.t("active_admin.menu.production"), priority: 120
  permit_params :name, :picture, :parent_id, :position, :usable, brand_ids: []

  skip_before_action :verify_authenticity_token, :only => [:sort, :update]

  filter :name
  filter :ancestry, :label => "Parent", :as => :select, collection: (Category.roots.map { |c| [c.name, c.id] } rescue [])

  index do
    column :ancestry do |category|
      category.parent
    end
    column :name
    # column :picture do |category|
    #   image_tag category.picture.url, class: 'col-img micro'
    # end
    list_column :reference_brands
    toggle_bool_column :usable, success_message: I18n.t('active_admin.messages.updated_successfully')
    column :created_at
    column :updated_at
    actions
  end

  index :as => :sortable do
    label :name # item content
    actions
  end

  form(html: { autocomplete: :off }) do |f|
    f.inputs do
      f.input :parent_id, as: :select, collection: Category.where.not(id: f.object.id).roots.map { |c| [c.name, c.id] }
      f.input :name
      f.input :usable
      f.input :brand_ids, as: :select, multiple: true, collection: Brand.all.map { |b| [b.name, b.id] }
      #f.input :picture, as: :file, hint: image_tag(f.object.picture.url, class: 'col-img big no-center')
    end
    f.actions
  end

  show do
    attributes_table do
      row :parent
      row :name
      # row :picture do |category|
      #   image_tag category.picture.url, class: 'col-img medium no-center'
      # end
      row :usable
      row :created_at
      row :updated_at

      if category.is_root?
        panel I18n.t('activerecord.models.subcategory.other') do
          table_for category.children.order(:position) do
            column :name
            column :usable
          end
        end
      end

      panel I18n.t('activerecord.attributes.category.brand_ids'), toggle: true do
        table_for category.brands do
          column :name
        end
      end
    end
    active_admin_comments
  end

  controller do
    include Caching
    def clear_caching(_resource)
      clear_caching_of('calculations')
    end
  end

  after_save    :clear_caching
  after_destroy :clear_caching
end
