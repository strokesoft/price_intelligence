class ProductSerializer < ActiveModel::Serializer
  attributes :id, :name, :brand, :description, :ean, :asin, :vendor_part_number, :mediamarkt_ref, :image_url
end
