class HashSerializer
  def self.dump(hash)
    hash.to_json
  end

  #Probar: ???
  # def self.dump(hash)
  #   while hash.instance_of? String
  #     hash = eval(hash)
  #   end
  # end

  def self.load(hash)
    while hash.instance_of? String
      hash = eval(hash)
    end
    hash.with_indifferent_access if hash.instance_of? Hash
  end
end
