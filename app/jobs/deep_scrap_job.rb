class DeepScrapJob < ActiveJob::Base
  queue_as :default

  def perform(*args)
    DeepScrapper.weekly_scrapp
  end
end
