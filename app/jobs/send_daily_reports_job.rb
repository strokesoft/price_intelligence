class SendDailyReportsJob < ActiveJob::Base
  queue_as :urgent

  def perform
    self.send
  end

  def self.send
    start_date, end_date = DataGroupedCalculation.date_ranges.first
    Society.all.each do |society|
      users = society.users.where("email is not null and has_daily_email = true")
      unless users.empty?
        markets = society.reference_ecommerces_slug_and_name.keys
        markets = nil if markets.blank?
        brands = Brand.reference_names(society)
        brands = nil if brands.blank?

        data = DataGroupedCalculation.new(start_date, end_date, society, {
            listed: 'available', page: markets, brand: brands
        }).fetch_dashboard
        info = basic_info(data)
        users.each do |user|
          DailyReport.daily_dashboard(user: user, dashboard: info,
                                      start_date: start_date, end_date: end_date,
                                      markets: markets, brands: brands).deliver_now
        end
      end
    end
  end

  private

  def self.basic_info(data)
    info = {}
    data['categories'].keys.each do |category|
      info[category] = {
        new: data['categories'][category]['new_products'],
        disappeared: data['categories'][category]['disappeared_products'],
        up: data['categories'][category]['products_prices_up'],
        up_percentage: data['categories'][category]['products_prices_up_percentage'],
        down: data['categories'][category]['products_prices_down'],
        down_percentage: data['categories'][category]['products_prices_down_percentage']
      }
    end
    info
  end
end