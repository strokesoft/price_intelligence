class WeeklyScrapSpecsJob < ActiveJob::Base
  include Caching
  queue_as :default

  def perform(*args)
    # We stop the caching of the calculations
    set_time_cached('stop_caching_of_calculations')

    start_at = Time.now
    Utils.nlogs("#{start_at}: Start Weekly Specs Scraping")
    PageBase.retrieve_info_when_lost_specs
    end_at = Time.now
    Utils.nlogs("#{end_at}: End Weekly Specs Scraping. Duration: #{end_at - start_at} seconds.")
  ensure
    # We active the caching of the calculations
    del_time_cached('stop_caching_of_calculations')
  end
end