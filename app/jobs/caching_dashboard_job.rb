class CachingDashboardJob < ActiveJob::Base
  include Caching
  queue_as :default

  def perform(*_args)
    caching_calculations
  end
end