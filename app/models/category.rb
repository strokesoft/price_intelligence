# == Schema Information
#
# Table name: categories
#
#  id         :integer          not null, primary key
#  name       :string           indexed
#  slug       :string           indexed
#  ancestry   :string           indexed
#  picture    :string
#  position   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_categories_on_ancestry  (ancestry)
#  index_categories_on_name      (name)
#  index_categories_on_slug      (slug)
#

class Category < ApplicationRecord
  extend FriendlyId
  mount_uploader :picture, PictureUploader
  has_ancestry orphan_strategy: :rootify
  friendly_id :name
  # Make this list act like a zero-indexed array to avoid off-by-one errors in your sorting
  acts_as_list top_of_list: 0, scope: [:ancestry]

  # Owning relationships
  has_many :society_categories, inverse_of: :category, dependent: :destroy
  has_many :societies, through: :society_categories
  has_many :category_brands, inverse_of: :category, dependent: :destroy
  has_many :brands, through: :category_brands

  # Validations
  validates :name, presence: true, uniqueness: { scope: :ancestry }

  # Callbacks
  before_save -> { Category.clear_caching_of('calculations') }

  # Scopes
  scope :subcategories, -> { where.not(ancestry: nil) }
  scope :by_society, ->(society) {
    joins(:society_categories).where(society_categories: {society: society}) unless society.has_all_categories?
  }
  scope :by_user, ->(user) {
    by_society(user.society) unless user.has_all_categories?
  }
  scope :usable, ->{ where(usable: true)}

  def reference_brands
    brands.order(:name).pluck(:name)
  end

  class << self
    def reference_brands(category_name)
      Category.roots.find_by(name: category_name).brands.pluck(:name)
    end

    # Maping {name => {id => #id, children => {name => #id, ...}, ...} of categories
    def map_category
      fetch_caching_of('calculations', 'map_category') do
        categories = {}
        Category.roots.each do |root|
          categories[root.name] = {'id' => root.id, 'children' => {}}
          root.children.each do |child|
            categories[root.name]['children'][child.name] = child.id
          end
        end
        categories
      end
    end

    def roots_total
      fetch_caching_of('calculations', 'root_category_count', type: 'Integer') do
        Category.roots.count
      end
    end
  end
end
