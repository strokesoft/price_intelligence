# == Schema Information
#
# Table name: product_offers
#
#  id              :integer          not null, primary key
#  product_id      :integer          indexed
#  product_page_id :integer          indexed
#  offer           :text
#  date            :date
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_product_offers_on_product_id       (product_id)
#  index_product_offers_on_product_page_id  (product_page_id)
#

class ProductOffer < ApplicationRecord
  belongs_to :product
  belongs_to :product_page
end
