module Page
  class Worten < PageBase
    include ActiveModel::Model
    extend ActiveModel::Translation

    COUNTRY_CODE = 'ES'.freeze

    # VALIDATIONS --------------------------------------------------------------------------------------------------------

    # METHODS ------------------------------------------------------------------------------------------------------------

    def self.get_items(doc)
      #doc.css('div.m_product_list_content div.m_product_list_member')
      doc.css('div#products-list-block div.w-product')
    end

    def self.get_current_items_page(doc)
      #Utils.clear_text(doc.css('div.m_paginator a.current')[0].try(:text)).to_i
      doc.css('ul.pagination li.current span').first.try(:[], :'data-page').to_i
    end

    # def self.get_item_ean(item)
    #   nil
    # end

    def self.get_item_price(item)
      #item.css('div.m_product_prices span.price_currency').remove
      #item.css('div.m_product_prices small').remove
      #item.css('div.m_product_prices p').remove
      #item.css('div.m_product_prices').text
      item.try(:[],:'data-price') # item.css('span.w-currentPrice').text
    end

    def self.get_item_url(item)
      #'https://www.worten.es' + item.css("div.m_product div.m_product_content div.m_product_info strong.m_product_name a").map{|w| w['href']}.first
      'https://www.worten.es' + item.css('a').first.try(:[], :href)
    end

    def self.get_item_name(item)
      #item.css("div.m_product div.m_product_content div.m_product_info strong.m_product_name a").map{|w| w['title']}.first
      item.css('h3').text
    end

    def self.get_item_brand(item)
      #item.css('span.m_product_brand').first.try(:text)
      item.try(:[], :'data-brand')
    end

    def self.get_item_out_of_stock(item)
      #item.at_css("div.m_stock_status.unavailable") ? true : false # Vale para 'FUERA DE STOCK', 'DISPONIBLE EN TIENDAS' o cualquier otra cosa

      # item.css('div.w-product__marketplace div.w-product__description-excerpt p').try(:text).try(:include?, 'Vendido por ') ?
      #     true :
      #     false

      # doc.text.split("\"dimension116\":\"#{item.try(:[],:'data-id')}\"").try(:[], 0).try(:split, '"dimension66":"').try(:[], -1).try(:split, '"').try(:[], 0)

      nil
    end

    def self.get_item_image_url(item)
      #"https://www.worten.es#{item.css('div.m_product_image_figure img @src').try(:text)}"
      "https://www.worten.es#{item.css('img').first.try(:[], :'data-src')}"
    end

    def self.get_item_description(item)
      #item.css('div.m_product_description').text
      item.css('div.w-product__description').text
    end

    PAGE_NAME = "Worten".freeze

    private

    MAX_RETRIES = 3

    PRODUCTSxPAGE = 48

    # DEPARTMENTS = {
    #     Hobs:          '"https://www.worten.es/productos/electrodomesticos/placas-y-vitroceramicas?p=#{page_index}"',
    #     Dishwashers:   '"https://www.worten.es/productos/electrodomesticos/lavavajillas/lavavajillas-60cm?p=#{page_index}"',
    #     Microwaves:    '"https://www.worten.es/productos/pequenos-electrodomesticos/microondas?p=#{page_index}"',
    #     Ovens:         '"https://www.worten.es/productos/electrodomesticos/hornos?p=#{page_index}"',
    #     Refrigerators: '"https://www.worten.es/productos/electrodomesticos/frigorificos/combis?p=#{page_index}"',
    #     Washers:       '"https://www.worten.es/productos/electrodomesticos/lavadoras/carga-frontal?p=#{page_index}"',
    #     Laptops:       '"https://www.worten.es/productos/informatica/portatiles?p=#{page_index}"',
    #     Integrables:   '"https://www.worten.es/productos/electrodomesticos/lavadoras/encastre?p=#{page_index}"'
    # }

    def self.get_price(doc)
      #Utils.clear_price doc.css('div.price span.price_current').first.text.split('€')[0] if doc.css('div.price span.price_current').first
      doc.text.split('"productPrice":').try(:[], 1).try(:split, ',"').try(:[], 0)
    end

    def self.get_ean(doc)
      #Utils.clear_text get_specs(doc).try(:[], "EAN")
      text = doc.text
      text.split('"ean":"').try(:[], 1).try(:split, '"').try(:[], 0) ||
          text.split('"gtin13":"').try(:[], 1).try(:split, '"').try(:[], 0) ||
          text.split('"dimension54":"').try(:[], 1).try(:split, '"').try(:[], 0)
    end

    def self.get_url(doc)
      "https://www.worten.es#{doc.css('section.w-compare').first.try(:[], 'data-compare-bar-url')}"
    end

    def self.get_brand(doc)
      #Utils.clear_text get_specs(doc).try(:[], "Marca")
      doc.text.split('"productBrand":"').try(:[], 1).try(:split, '"').try(:[], 0)
    end

    def self.get_name(doc)
      #Utils.clear_text doc.css('div.banner_sticky_toolbar_content div.banner_sticky_toolbar_nav span.product_name_sticky').text
      doc.text.split('"productName":"').try(:[], 1).try(:split, '"').try(:[], 0)
    end

    def self.get_specs(doc)
      #self.get_tabbed_specs(doc, 'div.tech_details_row div.wrapper div.m_tech_details div.cols div dl dt', 'div.tech_details_row div.wrapper div.m_tech_details div.cols div dl dd')

      groups = doc.css('div.w-product-details__wrapper p.w-product-details__subtitle')
      tables = doc.css('div.w-product-details__wrapper ul')
      specs = {}
      tables.each_with_index do |table, index|
        specs_to_add = get_tabbed_specs(table, 'span.details-label', 'span.details-value', groups.try(:[], index).try(:text).try(:strip))
        specs = specs.merge specs_to_add if specs_to_add
      end
      specs if specs.size > 0
    end

    def self.get_image_url(doc)
      #Utils.clear_text doc.css('div.m_show_case_images div.m_show_case_main_image span.m_show_case_main_image_content img').map{|q| q['src']}.first
      doc.css("meta[property='og:image'] @content").try(:text)
    end

    def self.get_out_of_stock(doc)
      return false if doc.css('div.w-product__availability-title').any?   # 'Disponible'
      return true  if doc.css('div.w-product__unavailability-title').any? # 'No disponible'

      #TODO: buscar/añadir más casos
      ScrapingLog.error "********************
      ********************
      Añadir caso a Page::Worten.get_out_of_stock - url [#{get_url(doc)}]
      ********************
      ********************"
      nil
    end

    def self.get_images_counter(doc)
      #TODO: ejemplo con dos imágener y un video (parece que el video lo añade por javascrit): 'https://www.worten.es/productos/electrodomesticos/lavadoras/encastre/lavadora-encastrable-8-kg-bosch-wiw28300es-6106167'
      #count = doc.css('div.m_show_case_thumbnails.hide_for_mobile img').try(:count)
      #count > 0 ? count : (get_image_url(doc).nil? ? nil : 1)
      count = doc.css('div.swiper-slide').try(:count)
      count > 0 ? count : (get_image_url(doc).nil? ? nil : 1)
    end
  end
end
