module Page
  class Pccomponentes < PageBase
    include ActiveModel::Model
    extend ActiveModel::Translation

    PAGE_NAME = 'PC Componentes'.freeze
    MAX_RETRIES = 3
    PRODUCTSxPAGE = 24
    URL_BASE = 'https://www.pccomponentes.com'.freeze
    CLASS_IN_STOCK = 'disponibilidad-inmediata'.freeze
    TEXT_IN_STOCK = 'En stock'.freeze
    
    def self.get_items(doc)
      doc.css('div.row article.tarjeta-articulo')
    end

    def self.main_article_number
      :vendor_part_number
    end

    def self.get_item_brand(item)
      item.css('a.enlace-disimulado').first['data-brand']
    end

    def self.get_item_name(item)
      item.css('a.enlace-disimulado').first['data-name']
    end

    def self.get_item_url(item)
      data = Utils.clear_text item.css('a.enlace-superpuesto').first.try(:[], 'href')
      URL_BASE + data if data
    end

    def self.get_item_image_url(item)
      data = item.css('img.img-fluid').first.try(:[], 'src')
      'https:' + data if data
    end

    def self.get_item_price(item)
      Utils.clear_price(item.css('a.enlace-disimulado').first['data-price'])
    end

    def self.get_item_out_of_stock(item)
      item.css('div.tarjeta-articulo__disponibilidad').first.values[0].include?(CLASS_IN_STOCK) ? false : true
    end

    def self.get_brand(doc)
      doc.css('div.ficha-producto__datos-de-compra a[itemprop="brand"]').first.try(:text)
    end

    def self.get_name(doc)
      doc.css('div.ficha-producto__encabezado h1.h4').first.try(:text)
    end

    def self.get_description(doc)
      doc.css('div#ficha-producto-caracteristicas p').first.try(:text)
    end

    def self.get_url(doc)
      doc.css('link[rel="canonical"]').first.try(:[], 'href')
    end

    def self.get_image_url(doc)
      data = doc.css('div.ficha-producto__imagen-producto img.img-fluid').first.try(:[], 'src')
      'https:' + data if data
    end

    def self.get_out_of_stock(doc)
      data = doc.css('div.ficha-producto__datos-de-compra a#GTM-desplegableFicha-disponibilidad').first.try(:text)
      return (data.include?(TEXT_IN_STOCK) ? false : true) if data
      true
    end

    def self.get_price(doc)
      doc.css('div.ficha-producto__encabezado div#precio-main').first['data-price']
    end

    def self.get_specs(doc)
      specs = {}
      doc.css('div#ficha-producto-caracteristicas ul>li').each do |key_val_spec|
        if key_val_spec.children.size == 1 && (spec_text = key_val_spec.try(:text)).include?(':')
          specs[Utils.clear_text spec_text[0..spec_text.index(':')-1]] = Utils.clear_text spec_text[spec_text.index(':')+1..-1]
        end
      end
      specs if specs.size > 0
    end

    def self.get_vendor_part_number(doc)
      doc.css('div.ficha-producto__datos-de-compra span[itemprop="productID"]').first.try(:text)
    end
  end
end
