module Page
  class Playstationstore < PageBase
    include ActiveModel::Model
    extend ActiveModel::Translation


    # VALIDATIONS --------------------------------------------------------------------------------------------------------

    # METHODS ------------------------------------------------------------------------------------------------------------

    def self.break_less_than_per_page?
      false
    end

    def self.get_items(doc)
      doc.css('div.ems-sdk-product-tile') # grid-cell grid-cell--game / grid-cell grid-cell--game-related
    end

    # def self.get_item_page(item)
    #  nil
    # end

    def self.finish_scrap?(doc, page_index)
      doc.attr_matches('data-qa', /^ems-sdk-grid-paginator-next-page-btn/).first.attribute("disabled").present?
      #page_index > 168
    end

    def self.valid_document(doc)
      get_current_items_page(doc)
    end

    def self.get_current_items_page(doc)
      res = doc.attr_matches('data-qa', /^ems-sdk-grid-paginator-btn/).css("button.psw-is-disabled").first.try(:text).to_i
      if res == 0
        ScrapingLog.error "Parece que la descarga de playstore no es valida"
        raise "Descarga no valida"
      else
        res
      end
    end

    # def self.get_item_ean(item)
    #   nil
    # end

    def self.get_item_price(item)
      Utils.clear_price(item.css("span.price").try(:text))
    end

    def self.get_item_url(item)
      data = item.css("a.ems-sdk-product-tile-link @href").first.try(:text)
      "https://store.playstation.com#{data}" if data
    end

    def self.get_item_name(item)
      Utils.clear_text(item.css("span.psw-body-2").first.try(:text))
    end

    # def self.get_item_brand(item)
    #   nil
    # end

    def self.get_item_out_of_stock(item)
      Utils.clear_price(item.css("span.price").try(:text)).blank? #? true : false
    end

    def self.get_item_image_url(item)
      (item.css("img.psw-l-fit-cover @src").last || item.css("img.psw-l-fit-cover @src").first).try(:text)
    end

    def self.get_item_description(item)
      Utils.clear_text item.css("span[data-qa='ems-sdk-product-tile-product-detail']").try(:text)
    end

    PAGE_NAME = "Play Station Store".freeze

    private

    ATTEMPTS = 5

    MAX_RETRIES = 5

    PRODUCTSxPAGE = 48

    # def self.get_page(doc)
    #   nil
    # end

    def self.get_price(doc)
      Utils.clear_price(doc.css("div.sku-info h3.price-display__price").try(:text))
    end

    def self.get_url(doc)
      "https://store.playstation.com#{doc.css('meta[property="og:url"]').first.try(:[], "content").split('3000')[1]}"
    end

    # def self.get_ean(doc)
    #   nil
    # end

    def self.get_brand(doc)
      doc.css('h5.provider-info__text').first.try(:text)
    end

    def self.get_name(doc)
      doc.css('h2.pdp__title').try(:text)
    end

    def self.get_description(doc)
      Utils.clear_text(doc.css("div.pdp__description").text)
    end

    def self.get_specs(doc)
      self.get_tabbed_specs(doc, 'div.tech-specs div.tech-specs__menu-header', 'div.tech-specs ul')
    end

    def self.get_image_url(doc)
      get_item_image_url(doc)
    end

    def self.get_out_of_stock(doc)
      doc.css("button.desktop-cta--add-to-cart").blank? #? true : false
    end
  end
end
