module Page
  class Clickelectrodomesticos < PageBase
    include ActiveModel::Model
    extend ActiveModel::Translation


    # VALIDATIONS --------------------------------------------------------------------------------------------------------

    # METHODS ------------------------------------------------------------------------------------------------------------

    def self.get_items(doc)
      doc.css('div.products ol.product-items li.product-item')
    end

    def self.get_current_items_page(doc)
      Utils.clear_text(doc.css('div.pages li.current')[0].text.split.last).to_i
    end

    # def self.get_item_ean(item)
    #   nil
    # end

    def self.get_item_price(item)
      Utils.clear_price(item.css('span.special-price span.price').try(:text).try(:gsub, '.', ''))
    end

    def self.get_item_url(item)
      # item.css('a.product-item-link @href').first.try(:text)
      item.css('a.product-item-link').first.try(:[], :href)
    end

    def self.get_item_name(item)
      Utils.clear_text(item.css('.product-item-name').try(:text))
    end

    def self.get_item_brand(item)
      item.css('div.manufacturer img').first.try(:[], :alt)
    end

    def self.get_item_out_of_stock(item)
      item.css('button.tocart').blank?
    end

    def self.get_item_image_url(item)
      item.css('img.product-image-photo').first.try(:[], :src)
    end

    def self.get_item_description(item)
      get_item_name(item)
    end

    PAGE_NAME = "Click electrodomesticos".freeze

    private

    MAX_RETRIES = 3

    PRODUCTSxPAGE = 30

    def self.get_price(doc)
      # get_item_price(doc)
      doc.css('meta[property="og:price:amount"]').first.try(:[], "content")
    end

    # def self.get_ean(doc)
    #   nil
    # end

    def self.get_url(doc)
      doc.css('meta[property="og:url"]').first.try(:[], "content")
    end

    def self.get_brand(doc)


      doc.css('div.manufacturer-image img').first.try(:[], :alt)
    end

    def self.get_name(doc)
      # Utils.clear_text item.css('span[itemprop="name"]').try(:text)
      doc.css('meta[property="og:title"]').first.try(:[], "content")
    end

    def self.get_description(doc)
      doc.css('meta[property="og:description"]').first.try(:[], "content")
    end

    def self.get_specs(doc)
      tables = doc.css('.product__specifications-group')
      specs = {}
      tables.each do |table|
        specs_to_add = get_tabbed_specs(table, '.product_spec_key', '.product_spec_value', table.css('.specs-group-title').text.strip)
        specs = specs.merge specs_to_add if specs_to_add
      end
      specs if specs.size > 0
    end

    def self.get_image_url(doc)
      doc.css('meta[property="og:image"]').first.try(:[], "content")
    end

    def self.get_out_of_stock(doc)
      text = doc.css('meta[property="og:availability"]').first.try(:[], "content")
      case text
        when 'in stock' then
          false
        # when "OutOfStock" then
        #   true
        else
          #TODO: buscar/añadir más casos
          ScrapingLog.error "********************
********************
Añadir a Page::ClickElectrodomesticos.out_of_stock_common este caso: [#{text}] url [#{get_url(doc)}]
********************
********************" unless text.nil?
          text.nil? ? nil : true
      end
    end

    def self.get_images_counter(doc)
      count = doc.text.split('"thumb"').count-1
      count > 0 ? count : (get_image_url(doc).nil? ? nil : 1)
    end
  end
end
