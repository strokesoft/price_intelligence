module Page
  class Tiendaazul < PageBase
    include ActiveModel::Model
    extend ActiveModel::Translation


    # VALIDATIONS --------------------------------------------------------------------------------------------------------

    # METHODS ------------------------------------------------------------------------------------------------------------

    def self.get_items(doc)
      doc.css('ul.product_list li')
    end


    def self.get_item_ean(item)
      # http://www.tiendaazul.com/placas-encimeras-baratas/12264-placa-edesa-urban-ci2gsa-negro-gas-natural-8435436100543.html
      # http://www.tiendaazul.com/placas-encimeras-baratas/12264-placa-edesa-urban-ci2gsa-negro-gas-natural-EAN13.html
      # puts "url [#{get_item_param(:url, item)}]"
      # puts "ean [#{get_item_param(:url, item).split('-').last.split('.').first}]"
      try :sprintf, '%013d', get_item_param(:url, item).split('-').last.split('.').first
    end

    def self.get_item_price(item)
      iva = item.css('div.price').css('span.tax').remove
      puts "*************** No dice IVA Incluido!!!!!! " if iva.text != "IVA Incluido"
      # price = Utils.clear_price item.css('div.price').text
      # iva ? price : Utils.add_iva price
      item.css('div.price').text.try(:gsub, '.', '')
    end

    def self.get_item_url(item)
      item.css('a[itemprop="url"]').first.try :[], 'href'
    end

    def self.get_item_name(item)
      item.css('a.product-name').first.try :[], 'title'
    end

    def self.get_item_brand(item)
      item.css('a.product_img_link img @alt').first.try(:text)
    end

    def self.get_item_out_of_stock(item)
      text = item.css('link[itemprop="availability"]').first.try :[], 'href'

      return false if text.try :include?, "://schema.org/InStock"
#      return false if text.include? "://schema.org/InStoreOnly"
      return true if text.try :include?, "://schema.org/OutOfStock"
      #TODO: buscar/añadir más casos
      ScrapingLog.error "********************
********************
Añadir a Page::Tiendaazul.get_out_of_stock este caso (buscar valor de \"availability\"): [#{text.inspect}] para [#{get_item_param(:url, item)}]
********************
********************" unless text.nil?
      nil
    end

    def self.get_item_image_url(item)
      item.css('a.product_img_link img[itemprop="image"]  @src').first.try(:text)
    end

    def self.get_item_description(item)
      item.css('p[itemprop="description"]').first.try(:text)
    end

    PAGE_NAME = "TiendaAzul".freeze

    private

    MAX_RETRIES = 3

    PRODUCTSxPAGE = 10000

    # DEPARTMENTS = {
    #     Hobs:          '"http://www.tiendaazul.com/63-placas-encimeras-baratas?n=#{self::PRODUCTSxPAGE}"',
    #     Dishwashers:   '"http://www.tiendaazul.com/23-lavavajillas-baratos?n=#{self::PRODUCTSxPAGE}"',
    #     Microwaves:    '"http://www.tiendaazul.com/24-microondas-baratos?n=#{self::PRODUCTSxPAGE}"',
    #     Ovens:         '"http://www.tiendaazul.com/21-hornos-baratos?n=#{self::PRODUCTSxPAGE}"',
    #     Refrigerators: '"http://www.tiendaazul.com/68-frigorificos-combi-baratos?n=#{self::PRODUCTSxPAGE}"',
    #     Washers:       '"http://www.tiendaazul.com/22-lavadoras-baratas?n=#{self::PRODUCTSxPAGE}"'
    #     #       Integrables:
    # }

    def self.get_price(doc)
      doc.css('span[itemprop="price"]').text.try(:gsub, '.', '')
    end

    def self.get_ean(doc)
      sprintf('%013d', get_param(:url, doc).split('-').last.split('.').first) rescue nil
    end

    def self.get_brand(doc)
      doc.css('p#manufacturer_name span.editable').text
    end

    def self.get_name(doc)
      doc.css('h1[itemprop="name"]').text
    end

    def self.get_description(doc)
      doc.css('div[itemprop="description"]').text
    end

    def self.get_specs(doc)
      # ver caso: https://www.tiendaazul.com/lavadoras-baratas/13835-lavadora-evvo-15-clase-a-5kg-800rpm-742832740197.html
      specs = {}

      specs_aux = self.get_tabbed_specs(doc, 'table.table-data-sheet tr td[1]', 'table.table-data-sheet tr td[2]')
      specs = specs.merge(specs_aux) if specs_aux

      tables = doc.css('div.rte table')
      tables.each do |table|
        group = table.css('tbody tr td[colspan="2"]').first.try(:text).try(:strip)
        table.css('tbody tr td[colspan="2"]').remove
        specs_aux = get_tabbed_specs(table, 'tbody tr td[1]', 'tbody tr td[2]', group)
        specs = specs.merge(specs_aux) if specs_aux
      end

      specs = specs.merge({info: (Utils.clear_text doc.css('div.rte').text)}) if tables.count == 0

      specs if specs.size > 0
    end

    def self.get_url(doc)
      doc.css('link[rel="canonical"]').first.try :[], 'href'
    end

    def self.get_image_url(doc)
      doc.css('img[itemprop="image"] @src').text
    end

    def self.get_out_of_stock(doc)
      get_item_out_of_stock doc
    end

    def self.get_images_counter(doc)
      count = doc.css('div#thumbs_list li img').try(:count)
      count > 0 ? count : (get_image_url(doc).nil? ? nil : 1)
    end
  end
end
