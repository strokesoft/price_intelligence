module Page
  class Amazonmarket < Amazon
    # VALIDATIONS --------------------------------------------------------------------------------------------------------

    # METHODS ------------------------------------------------------------------------------------------------------------

    def self.origin_page # needed for special cases as Amazon/Amazon Market (diferent markets on same URL)
      'amazon'
    end

    PAGE_NAME = "Amazon Market".freeze
  end
end
