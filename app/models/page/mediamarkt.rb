module Page
  class Mediamarkt < PageBase
    include ActiveModel::Model
    # include MechanizeDoc
    extend ActiveModel::Translation

    # def self.use_mechanize?
    #   true
    # end

    def self.get_items(doc)
      doc.search('div[data-test="mms-search-srp-productlist-item"]')
    end

    def self.get_current_items_page(doc)
      9999999
      #Utils.clear_text(doc.css('ul.pagination li.active')[0].try(:text)).to_i
    end

    def self.get_item_ean(item)
      nil
    end

    def self.get_item_price(item)

      #item.css('div.price').text.delete(',-')
      #item.css('script')[0].text.split('price":"')[1].split('"')[0]
      price = item.search('div[data-test="product-price"] span[color="primary"],sup[color="primary"]').try(:text)
      price = item.search('div[data-test="product-price"] span[font-size="lg"],sup[font-size="lg"]').try(:text) if price.blank?
      price.gsub(".–","")
    end

    def self.get_item_url(item)
      "https://www.mediamarkt.es#{item.css('a @href').try(:text)}"
    end

    def self.get_item_name(item)
      item.search('p[data-test="product-title"]').text
      # Utils.clear_text(item.css('h2 a').try(:text))
      #item.css('script')[0].text.split('name":"')[1].split('"')[0]
    end

    def self.get_item_brand(item)
      item.search('ul[data-test="feature-list"]').css('li').first.css('div').last.text
      #item.css('script')[0].text.split('brand":"')[1].split('"')[0]
    end

    def self.get_item_out_of_stock(item)
      if item.search('div[data-test="mms-delivery-online-availability_AVAILABLE"]').present? || item.search('div[data-test="mms-delivery-online-availability_PARTIAL_AVAILABLE"]').present?
        false
      else
        true
      end
    end

    def self.get_item_image_url(item)
      ""
    end

    def self.get_item_description(item)
      get_item_name(item)
    end

    def self.get_article_number(doc)
      doc.search('span[font-size="xl"]').first.text.gsub('(','').gsub(' artículos)','').gsub(' artículo)','').to_i
    end

    def self.scrapp_page_department(page_department, page_index = 1)
      scrapp_page_department_with_article_number(page_department, page_index)
    end

    def self.get_total_pages(doc) # it must be overwrited by each Page::Class
      nil
    end

    PAGE_NAME = "MediaMarkt".freeze

    private

    MAX_RETRIES = 3

    PRODUCTSxPAGE = 12

    def self.get_price(doc)
      doc.css("meta[property='product:price:amount'] @content").try(:text)
    end

    def self.get_ean(doc)
      doc.css('body script').text.split('ean":"')[1].try(:split, '"').try(:[], 0)
    end

    def self.get_brand(doc)
      doc.css("meta[property='product:brand'] @content").try(:text)
    end

    def self.get_name(doc)
      doc.search('h1[font-size="xxxl"]').try(:text)
    end

    def self.get_specs(doc)
      tables = doc.css('div#features section')
      specs = {}
      tables.each do |table|
        specs_to_add = get_tabbed_specs(table, 'dl dt', 'dl dd[class]', table.css('h2').try(:text).try(:strip))
        specs = specs.merge specs_to_add if specs_to_add
      end
      specs if specs.size > 0
      #doc.search('#features div[data-test="inner-foldable"] td').each{|x| specs += x.try(:text) + " "}
    end

    def self.get_image_url(doc)
      doc.search('source @srcset').try(:[],0).try(:text)
    end

    def self.get_description(doc)
      doc.search('#description div[data-test="inner-foldable"]').try(:text)
      description = get_name(doc) if description.blank?
      description
    end

    def self.get_out_of_stock(doc)
      nil
    end

    private

    def self.out_of_stock_common(text)
      nil
    end

    def self.get_images_counter(doc)
      #TODO: ejemplo con dos imágener y un video (parece que el video lo añade por javascrit): 'https://www.mediamarkt.es/es/product/_lavadora-integrable-bosch-wiw28300es-8kg-1400rpm-a-blanco-1361162.html'
      #      ejemplo con dos videos: 'https://www.mediamarkt.es/es/product/_mini-lavadora-lg-f70e1dn0-3-5-kg-700rpm-9-funciones-display-led-t%C3%A1ctil-blanco-1406474.html'
      count = doc.css('ul.thumbs li').try(:count)
      count > 0 ? count : (get_image_url(doc).nil? ? nil : 1)
    end
  end
end
