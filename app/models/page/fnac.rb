module Page
  class Fnac < PageBase
    include ActiveModel::Model
    # include MechanizeDoc
    extend ActiveModel::Translation

    PAGE_NAME = 'Fnac'.freeze
    MAX_RETRIES = 3
    PRODUCTSxPAGE = 20 #TODO: esto podría ser 150, pero por algún motivo, aunque se indique en la URL, el mecanize solo obtiene 20, aunque la paginación si es propia de 150!!! investigar
    TEXT_IN_STOCK = 'en stock'.freeze
    TEXT_IN_STOCK_DETAIL = 'stock'.freeze
    VENDOR_PART_NUMBER_REF = 'Ref:'.freeze

    # def self.use_mechanize?
    #   true
    # end

    def self.getDoc(url)
      # proxy = {
      #   http: '127.0.0.1:24000',
      #   ssl:  '127.0.0.1:24000'
      # }

      # browser = Watir::Browser.new :chrome, url: "http://stroke1.duckdns.org:4444/wd/hub", proxy: proxy, switches: ['--ignore-certificate-errors', '--headless', '--disable-gpu', '--no-sandbox', '--disable-web-security']
      # browser.goto(url)

      # res = Nokogiri::HTML(browser.html)
      # browser.close
      # res

      counter = 3
      begin
        get_doc_usual(url)
      rescue
        if counter > 0
          counter -= 1
          retry
        else
          get_doc_selenium(url)
        end
      end
    end

    def self.get_doc_usual(url)
      Nokogiri::HTML(open(url, getHeaders) {|url_stream|
        Encoding.default_external = url_stream.charset
        url_stream.read
      })
    end

    def self.get_current_items_page(doc)
      # (Utils.clear_text(doc.css('ul.bottom-toolbar li.current').first.try(:text)) || doc.css('li.pageView').try(:text).try(:split).try(:[], 1)).to_i
      page = doc.css('ul.toolbar-pager li.current').first.try(:text)
      unless page # Hacemos esto porque parece que las páginas multiplos de 10 no se muestran en el paginador como "current page"
        current_page_number = doc.text.split('"current_page_number":')[1].split(",")[0].to_i
        last_page_number    = doc.text.split('"last_page_number":'   )[1].split(",")[0].to_i
        page = current_page_number.to_s if current_page_number <= last_page_number
      end
      (Utils.clear_text(page)).to_i
    end

    def self.get_items(doc)
      items = doc.css('div.articleList div.Article-item')
      # items[0].try(:css, 'li.Article-item').try(:remove)
      items
    end

    def self.main_article_number
      :ean
    end

    def self.get_item_brand(item)
      item.css('p.Article-descSub span a').first.try(:text)
    end

    def self.get_item_name(item)
      item.css('p.Article-desc a.js-minifa-title').first.try(:text)
    end

    def self.get_item_url(item)
      item.css('p.Article-desc a.js-minifa-title').first.try(:[], 'href')
    end

    def self.get_item_image_url(item)
      item.css('div.Article-itemVisual img.Article-itemVisualImg').first.try(:[], 'src')
    end

    def self.get_item_price(item)
      price_text = item.css('div.Article-price a.userPrice').first.try(:text) ||
                   item.css('div.Article-price div.blocPriceBorder--bgGrey span.price').first.try(:text) ||
                   item.css('div.Article-price strong.userPrice').first.try(:text)
      # price_text = item.css('strong.stimuliOPC-flyer--AdhPrice').count > 0 ?
      #                   item.css('div.Article-price div.blocPriceBorder--bgGrey span.price').first.try(:text) :
      #                   item.css('div.Article-price span.red').first.try(:text)
      Utils.clear_price(price_text&.gsub('.',''))
    end

    def self.get_item_out_of_stock(item)
      text = item.css('div.shipping span.Dispo-txt').first.try(:text)
      text.nil? ?
          nil :
          text.downcase.include?(TEXT_IN_STOCK) ? false : true
    end

    def self.get_item_from_marketplace(item)
      item.css('div.Article-price').at_css('div.sellBy') ? true : false
    end

    def self.get_brand(doc)
      Utils.clear_text doc.css('div.f-productPage span.f-productHeader-subTitleLabel').first.try(:text)
    end

    def self.get_name(doc)
      Utils.clear_text doc.css('div.f-productPage h1.f-productHeader-Title').first.try(:text)
    end

    def self.get_url(doc)
      doc.css('link[rel="canonical"]').first.try(:[], 'href')
    end

    def self.get_image_url(doc)
      doc.css('div.f-productVisuals-main img').first.try(:[], 'src')
    end

    def self.get_out_of_stock(doc)
      #TODO: Ver: doc.css('span.f-buyBox-availabilityStatus-available')
      text = doc.css('div.f-buyBox-availability').first.try(:text)
      text.nil? ?
          nil :
          text.downcase.include?(TEXT_IN_STOCK_DETAIL) ? false : true
    end

    def self.get_price(doc)
      Utils.clear_price(doc.css('div.f-productOffers-content span.f-priceBox-price--reco').first.try(:text).try(:delete, '.'))
    end

    def self.get_specs(doc)
      self.get_tabbed_specs(doc, 'ul.Feature-list li.Feature-item span.Feature-label', 'ul.Feature-list li.Feature-item span.Feature-desc')
    end

    def self.get_ean(doc)
      doc.css('.f-stars').try(:attr, 'data-ean').try(:text)
    end

    def self.get_vendor_part_number(doc)
      data = doc.css('span.Feature-chapo').try(:text).delete(VENDOR_PART_NUMBER_REF)
      Utils.clear_text(data.split(' ').last) if data
    end
  end
end
