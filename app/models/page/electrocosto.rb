module Page
  class Electrocosto < PageBase
    include ActiveModel::Model
    extend ActiveModel::Translation


    # VALIDATIONS --------------------------------------------------------------------------------------------------------

    # METHODS ------------------------------------------------------------------------------------------------------------

    def self.get_items(doc)
      doc.css('ul.product_list.grid li')
    end

    def self.get_current_items_page(doc)
      Utils.clear_text(doc.css('ul.pagination li.current')[0].try(:text)).to_i
    end

    # item.css('@itemprop').each {|p| puts p.to_s}
    # item.css('[itemprop]').each {|p| puts p.to_s}

    # def self.get_item_ean(item)
    #   item
    # end

    def self.get_item_price(item)
      item.css("span.price.product-price").text.gsub('€','').strip
    end

    def self.get_item_url(item)
      item.at_xpath("div/div[2]/h5/a")['href']
    end

    def self.get_item_name(item)
      item.at_xpath("div/div[2]/h5/a")['title']
    end

    # def self.get_item_brand(item)
    #   nil
    # end

    def self.get_item_out_of_stock(item)
      item.css("span.availability").children.map{|ch| ch.text.strip}.join == 'Agotado'
    end

    def self.get_item_image_url(item)
      item.css('img @src').first.try(:text)
    end

    def self.get_item_description(item)
      item.css('p.product-desc').first.try(:text)
    end

    PAGE_NAME = "Electrocosto".freeze

    private

    MAX_RETRIES = 3

    PRODUCTSxPAGE = 100

    # DEPARTMENTS = {
    #     Hobs:          '"https://www.electrocosto.com/encimeras/?p=#{page_index}&n=#{self::PRODUCTSxPAGE}"',
    #     Dishwashers:   '"https://www.electrocosto.com/lavavajillas/?p=#{page_index}&n=#{self::PRODUCTSxPAGE}"',
    #     Microwaves:    '"https://www.electrocosto.com/microondas/?p=#{page_index}&n=#{self::PRODUCTSxPAGE}"',
    #     Ovens:         '"https://www.electrocosto.com/hornos/?p=#{page_index}&n=#{self::PRODUCTSxPAGE}"',
    #     Refrigerators: '"https://www.electrocosto.com/frigorificos-combis/?p=#{page_index}&n=#{self::PRODUCTSxPAGE}"',
    #     Washers:       '"https://www.electrocosto.com/lavadoras/?p=#{page_index}&n=#{self::PRODUCTSxPAGE}"'
    #    # Integrables:
    # }

    def self.get_price(doc)
      Utils.clear_price doc.css('span#our_price_display').text
    end

    def self.get_ean(doc)
      Utils.clear_text doc.css('p#product_reference span').text
    end

    def self.get_brand(doc)
      Utils.clear_text doc.css('div#center_column.center_column.col-xs-12.col-sm-9 div div.primary_block.row div.pb-left-column.col-xs-12.col-sm-4.col-md-5 div.content_manufacture div.manu a.image img').map{|q| q['alt']}.last
    end

    def self.get_name(doc)
      Utils.clear_text doc.css('div.pb-center-column h1').text
    end

    def self.get_specs(doc)
      tables = doc.css('div.ficha-producto table.tabla_')
      specs = {}
      #tables.each { |table| specs[table.css('thead').text.strip] = get_tabbed_specs(table, 'tbody tr td.tdi', 'tbody tr td.tdd')}
      tables.each do |table|
        specs_to_add = get_tabbed_specs(table, 'tbody tr td.tdi', 'tbody tr td.tdd', table.css('thead').text.strip)
        specs = specs.merge specs_to_add if specs_to_add
      end
      specs if specs.size > 0
    end

    def self.get_image_url(doc)
      Utils.clear_text doc.css('span#view_full_size a.jqzoom div.zoomPad img').map{|q| q['src']}.last
    end

    #    def self.get_out_of_stock(doc)
    #      doc.css('html').text.include? 'AGOTADO'
    #    end

    def self.get_images_counter(doc)
      #TODO: ejemplo con dos imágener y un video (parece que el video lo añade por javascrit): 'https://www.electrocosto.com/lavadoras/siemens-wm10t469es'
      count = doc.css('ul#thumbs_list_frame li').try(:count)
      count > 0 ? count : (get_image_url(doc).nil? ? nil : 1)
    end
  end
end
