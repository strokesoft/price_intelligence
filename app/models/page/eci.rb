module Page
  class Eci < PageBase
    include ActiveModel::Model
    extend ActiveModel::Translation


    # VALIDATIONS --------------------------------------------------------------------------------------------------------

    # METHODS ------------------------------------------------------------------------------------------------------------

    def self.get_items(doc)
      doc.css('li.products_list-item')
    end

    def self.get_item_page(item)
      get_item_url(item).try(:index, "://www.elcorteingles.es/electronica/MP_").nil? ? "eci" : "ecimarket"
    end

    def self.get_current_items_page(doc)
      Utils.clear_text(doc.css('div.pagination span.selected')[0].try(:text)).to_i
    end

    def self.get_item_ean(item)
      item.css('.product_preview-cart').first.try(:[], "data-product-gtin")
    end

    def self.get_item_price(item)
      if item.css(".price._big").try(:children).try(:[], 2)
        item.css(".price._big").try(:children).try(:[], 0).try(:text) +
        item.css(".price._big").try(:children).try(:[], 1).try(:text) +
        item.css(".price._big").try(:children).try(:[], 2).try(:text)
      else
        item.css(".price._big").try(:children).try(:[], 0).try(:text)
      end
    end

    def self.get_item_url(item)
      data = Utils.clear_text item.css('.product_preview-cart').first.try(:[], "data-product-url")
      'http://www.elcorteingles.es' + data if data
    end

    def self.get_item_name(item)
      item.css('.product_preview-desc').first.try(:text)
    end

    def self.get_item_brand(item)
      item.css('.product_preview-brand').first.try(:text)
    end

    def self.get_item_out_of_stock(item)
      item.at_css(".sold_out") ? true : false
    end

    def self.get_item_image_url(item)
      data = Utils.clear_text item.css('.js_preview_image').first.try(:[], "data-src")
      "http:" + data if data
    end

    def self.get_article_number(doc)
      doc.css('._count')[0].text.split(" ")[0].to_i
    end

    def self.scrapp_page_department(page_department, page_index = 1)
      scrapp_page_department_with_article_number(page_department, page_index)
    end

    # def self.get_item_description(item)
    #   nil
    # end

    PAGE_NAME = "El Corte Inglés".freeze

    private

    MAX_RETRIES = 3

    PRODUCTSxPAGE = 24

    # def self.break_less_than_per_page?
    #   false
    # end

    # DEPARTMENTS = {
    #     Hobs:          '"http://www.elcorteingles.es/electrodomesticos/hornos-placas-y-campanas/placas-de-cocina/#{page_index}?itemsPerPage=#{self::PRODUCTSxPAGE}"',
    #     Dishwashers:   '"http://www.elcorteingles.es/electrodomesticos/lavavajillas/lavavajillas-60-cm/#{page_index}?itemsPerPage=#{self::PRODUCTSxPAGE}"',
    #     Microwaves:    '"http://www.elcorteingles.es/electrodomesticos/hornos-placas-y-campanas/hornos-de-cocina/microondas/#{page_index}?itemsPerPage=#{self::PRODUCTSxPAGE}"',
    #     Ovens:         '"http://www.elcorteingles.es/electrodomesticos/hornos-placas-y-campanas/hornos-de-cocina/#{page_index}?itemsPerPage=#{self::PRODUCTSxPAGE}"',
    #     Refrigerators: '"http://www.elcorteingles.es/electrodomesticos/frigorificos-y-congeladores/frigorificos-combi/#{page_index}?itemsPerPage=#{self::PRODUCTSxPAGE}"',
    #     Washers:       '"http://www.elcorteingles.es/electrodomesticos/lavado-y-secado/lavadora-de-carga-frontal/#{page_index}?itemsPerPage=#{self::PRODUCTSxPAGE}"',
    #     Laptops:       '"https://www.elcorteingles.es/electronica/ordenadores/portatiles/#{page_index}?itemsPerPage=#{self::PRODUCTSxPAGE}"'
    #     #       Integrables:
    # }

    def self.get_page(doc)
      get_url(doc).index("://www.elcorteingles.es/electronica/MP_").nil? ? "eci" : "ecimarket"
    end

    def self.get_price(doc)
      doc.css("div#product-info div.product-price span.current").text.split('€')[0].try(:gsub, '.', '')
    end

    def self.get_url(doc)
      doc.css('meta[property="og:url"]').first.try(:[], "content")
    end

    def self.get_ean(doc)
      doc.css('div#product-info span#ean-ref').text
    end

    def self.get_brand(doc)
      doc.css('div#product-info h2.brand a').map{|q| q['title']}.first
    end

    def self.get_name(doc)
      doc.css('div#product-info h2.title').first.try(:text)
    end

    def self.get_specs(doc)
      self.get_tabbed_specs(doc, 'div#media-info div.product-features.c12 dl.cb dt', 'div#media-info div.product-features.c12 dl.cb dd')
    end

    def self.get_image_url(doc)
      data = Utils.clear_text doc.css('img#product-image-placer').map{|q| q['src']}.last
      "http:" + data if data
    end

#    def self.get_out_of_stock(doc)
#      doc.css('html').text.include? 'AGOTADO'
#    end

    def self.get_images_counter(doc)
      count = doc.css('ul.alternate-images li img').try(:count)
      count > 0 ? count : (get_image_url(doc).nil? ? nil : 1)
    end
  end
end
