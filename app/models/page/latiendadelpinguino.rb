module Page
  class Latiendadelpinguino < PageBase
    include ActiveModel::Model
    extend ActiveModel::Translation


    # VALIDATIONS --------------------------------------------------------------------------------------------------------

    # METHODS ------------------------------------------------------------------------------------------------------------

    def self.get_items(doc)
      doc.css('div.item-product-list')
    end

    def self.get_current_items_page(doc)
      Utils.clear_text(doc.css('ul.page-list li.current').first.try(:text)).to_i
    end


    # def self.get_item_ean(item)
    #   nil
    # end

    def self.get_item_price(item)
      item.css("span.price").try(:text).try(:gsub, '.', '')
    end

    def self.get_item_url(item)
      item.css("div.product_name a @href").first.try(:text)
    end

    def self.get_item_name(item)
      item.css("div.product_name a").first.try(:text)
    end

    # def self.get_item_brand(item)
    #   item.css("")
    # end

    def self.get_item_out_of_stock(item)
      (item.css("div.product-availability-list i.fa-check").count > 0 ? false : true)
    end

    def self.get_item_image_url(item)
      text = item.css('img @src').first.try(:text)
      text.try(:index, 'es-default-large_default.jpg').nil? ? text : nil
    end

    def self.get_item_description(item)
      item.css('div.decriptions-short').try(:text)
    end

    PAGE_NAME = "La tienda del pingüino".freeze

    private

    MAX_RETRIES = 3

    PRODUCTSxPAGE = 12

    # DEPARTMENTS = {
    #     Hobs:          '"http://www.latiendadelpinguino.com/placas?page=#{page_index}"',
    #     Dishwashers:   '"http://www.latiendadelpinguino.com/lavavajillas?page=#{page_index}"',
    #     Microwaves:    '"http://www.latiendadelpinguino.com/microondas?page=#{page_index}"',
    #     Ovens:         '"http://www.latiendadelpinguino.com/hornos?page=#{page_index}"',
    #     Refrigerators: '"http://www.latiendadelpinguino.com/233-frigorificos/s-2/clase-combinado?page=#{page_index}"',
    #     Washers:       '"http://www.latiendadelpinguino.com/136-lavadoras/s-4/tipo_de_carga-frontal?page=#{page_index}"'
    #     #       Integrables:
    # }

    def self.get_price(doc)
      Utils.clear_price doc.css('div.current-price span[itemprop="price"]').try(:text).try(:gsub, '.', '')
    end

#    def self.get_ean(doc)
#      .strip
#    end

    def self.get_brand(doc)
      aux = "http://www.latiendadelpinguino.com/manufacturer/"
      url = doc.css('div.product-manufacturer a @href').try(:text)

      url[aux.length, url.length-aux.length-1]&.capitalize
    end

    def self.get_name(doc)
      doc.css('div.product-info-main h1[itemprop="name"]').text
    end

    def self.get_specs(doc)
      self.get_tabbed_specs(doc, 'dl.data-sheet dt', 'dl.data-sheet dd')
    end

    def self.get_image_url(doc)
      text = doc.css('div.product-cover img @src').first.try(:text)
      text.try(:index, 'es-default-large_default.jpg').nil? ? text : nil
    end

    def self.get_out_of_stock(doc)
      form = doc.css('form#add-to-cart-or-refresh')
      form.blank? ? nil : form.text.include?('Agotado en este momento, consulte disponibilidad enviando un mail a')
    end

    def self.get_images_counter(doc)
      # count = doc.css('div.product-images img').try(:count)

      #TODO: Parece que esta tienda tiene listadas imágenes que en realidad no son descargables... Habría que probar a descargar y eliminar las que diesen 404
      images = doc.css('div.product-images img')
      count = images.blank? ?
                   0 :
                   images.map{|image| image["src"].try(:index, 'es-default-large_default.jpg').nil? ? image : nil}.compact.count
      count > 0 ? count : (get_image_url(doc).nil? ? nil : 1)
    end
  end
end
