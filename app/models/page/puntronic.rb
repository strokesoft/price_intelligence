module Page
  class Puntronic < PageBase
    include ActiveModel::Model
    extend ActiveModel::Translation

    MAX_RETRIES = 3
    PRODUCTSxPAGE = 10000

    PAGE_NAME = "Puntronic".freeze
    URL_BASE = 'https://www.puntronic.com'.freeze
    IN_STOCK = 'InStock'.freeze

    # METHODS ------------------------------------------------------------------------------------------------------------

    def self.get_items(doc)
      doc.css('div.productsContainer div.productColumn')
    end

    def self.get_current_items_page(doc)
      Utils.clear_text(doc.css('ul.pagination li.current')[0].try(:text)).to_i
    end

    # def self.get_item_ean(item)
    #   nil
    # end

    def self.get_item_price(item)
      #item.css('div.productListPrice span.product-price meta[itemprop="price"]').first.try(:[], "content")
      item.css('div.productListPrice span.product-price span.integerPrice').first.try(:[], "content")
    end

    def self.get_item_url(item)
      data = item.css('a.productListSmallImageLink').first.try(:[], 'href')
      URL_BASE + data if data
    end

    def self.get_item_name(item)
      item.css('meta[itemprop="name"]').first.try(:[], "content")
    end

    def self.get_item_brand(item)
      item.css('meta[itemprop="brand"]').first.try(:[], "content")
    end

    def self.get_item_out_of_stock(item)
      availability = item.css('link.availability').first.try(:[], 'href')
      (availability.include?(IN_STOCK) ? false : true) unless availability.nil?
    end

    def self.get_item_image_url(item)
      item.css('img.productListSmallImage').first.try(:[], 'src')
    end

    # def self.get_item_description(item)
    #   nil
    # end

    private

    # DEPARTMENTS = {
    #     Hobs:          '"http://www.puntronic.com/electrodomesticos-cocina/vitroceramicas?limit=#{self::PRODUCTSxPAGE}&mode=grid&p=#{page_index}"',
    #     Dishwashers:   '"http://www.puntronic.com/gran-electrodomestico/lavavajillas-online/lavavajillas-grandes-60cm?limit=#{self::PRODUCTSxPAGE}&mode=grid&p=#{page_index}"',
    #     Microwaves:    '"http://www.puntronic.com/electrodomesticos-cocina/hornos-microondas?limit=#{self::PRODUCTSxPAGE}&mode=grid&p=#{page_index}"',
    #     Ovens:         '"http://www.puntronic.com/electrodomesticos-cocina/hornos?limit=#{self::PRODUCTSxPAGE}&mode=grid&p=#{page_index}"',
    #     Refrigerators: '"http://www.puntronic.com/gran-electrodomestico/frigorificos-y-neveras/frigorificos-combis?limit=#{self::PRODUCTSxPAGE}&mode=grid&p=#{page_index}"',
    #     Washers:       '"http://www.puntronic.com/gran-electrodomestico/lavadoras/lavadoras-carga-frontal?limit=#{self::PRODUCTSxPAGE}&mode=grid&p=#{page_index}"'
    #     #       Integrables:
    # }

    def self.get_price(doc)
      doc.css('main#main-content span.product-price meta[itemprop="price"]').first.try(:[], "content")
    end

#    def self.get_ean(doc)
#      .strip
#    end

    def self.get_brand(doc)
      doc.css('main#main-content meta[itemprop="brand"]').first.try(:[], "content")
    end

    def self.get_name(doc)
      doc.css('main#main-content meta[itemprop="name"]').first.try(:[], "content")
    end

    def self.get_description(doc)
      doc.css('main#main-content div.product-shortDesc p').first.try(:text)
    end

    def self.get_specs(doc)
      self.get_tabbed_specs(doc, 'div.table-characteristics div.ct-cell-name', 'div.table-characteristics div.ct-cell-value')
    end

    def self.get_image_url(doc)
      doc.css('main#main-content img#main-image').first.try(:[], 'src')
    end

    def self.get_out_of_stock(doc)
      availability = doc.css('main#main-content link.availability').first.try(:[], 'href')
      availability.include?(IN_STOCK) ? false : true
    end

    def self.get_images_counter(doc)
      count = doc.css('div.gallery-thumbs img').try(:count)
      count > 0 ? count : (get_image_url(doc).nil? ? nil : 1)
    end
  end
end
