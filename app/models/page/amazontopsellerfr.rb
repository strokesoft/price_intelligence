module Page
  class Amazontopsellerfr < Amazonfr
    # include ActiveModel::Model
    # include MechanizeDoc
    # extend ActiveModel::Translation
    extend AmazonTopSelleresConcern

    PAGE_NAME = 'Amazon Top Sellers FR'.freeze
    # MAX_RETRIES = 3
    # PRODUCTSxPAGE = 50

    # TITLE_SEPARATOR = ' - '.freeze
    # OUT_OF_STOCK_TEXT = 'Temporalmente sin stock'.freeze
    # SECOND_HAND_OFFER = 'de 2ª mano'.freeze
    # SPONSORED_ITEM = '[Patrocinado]'.freeze
    # EXTRA_URL_INFO = '/ref='.freeze
    # VENDOR_PART_NUMBER_KEY = 'N&uacute;mero de modelo del producto'.freeze
    # OTHER_VARIANTS_TEXT = 'Ver otras variantes'.freeze
    # NEW_PRODUCT_ON_DETAIL = 'Nuevos:'.freeze
    # MORE_SALE_OPTIONS = 'Más opciones de compra'.freeze
    # HTTPS_AMAZON = "https://www.amazon.fr".freeze #doc.text.split("ue_sn='").try(:[], 1).try(:split, "'").try(:[], 0)
    # PAGE_NOT_VALID = 'Dinos cómo podemos mejorar'.freeze
    # NO_ELEMENTS = 'no ha coincidido con ning'.freeze # La búsqueda no ha coincidido con ningún producto
    # REVIEW_ORTHOGRAPHY = 'Revisa la ortograf'.freeze # "Revisa la ortografía o usa términos más generales."

  end
end
