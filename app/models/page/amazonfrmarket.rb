module Page
  class Amazonfrmarket < Amazonfr
    # VALIDATIONS --------------------------------------------------------------------------------------------------------

    # METHODS ------------------------------------------------------------------------------------------------------------

    def self.origin_page # needed for special cases as Amazon/Amazon Market (diferent markets on same URL)
      'amazonfr'
    end

    PAGE_NAME = 'Amazon FR Market'.freeze
  end
end
