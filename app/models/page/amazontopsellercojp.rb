module Page
  class Amazontopsellercojp < Amazon
    # include ActiveModel::Model
    # include MechanizeDoc
    # extend ActiveModel::Translation
    extend AmazonTopSelleresConcern

    PAGE_NAME = 'Amazon Top Sellers JP'.freeze
    # MAX_RETRIES = 3
    # PRODUCTSxPAGE = 20 / 50
    COUNTRY_CODE = 'JP'.freeze

    # TITLE_SEPARATOR = ' - '.freeze
    # OUT_OF_STOCK_TEXT = 'Temporalmente sin stock'.freeze
    # SECOND_HAND_OFFER = 'de 2ª mano'.freeze
    # SPONSORED_ITEM = '[Patrocinado]'.freeze
    # EXTRA_URL_INFO = '/ref='.freeze
    # VENDOR_PART_NUMBER_KEY = 'N&uacute;mero de modelo del producto'.freeze
    # OTHER_VARIANTS_TEXT = 'Ver otras variantes'.freeze
    # NEW_PRODUCT_ON_DETAIL = 'Nuevos:'.freeze
    # MORE_SALE_OPTIONS = 'Más opciones de compra'.freeze
    HTTPS_AMAZON = "https://www.amazon.co.jp".freeze #doc.text.split("ue_sn='").try(:[], 1).try(:split, "'").try(:[], 0)
    # PAGE_NOT_VALID = 'Dinos cómo podemos mejorar'.freeze
    # NO_ELEMENTS = 'no ha coincidido con ning'.freeze # La búsqueda no ha coincidido con ningún producto
    # REVIEW_ORTHOGRAPHY = 'Revisa la ortograf'.freeze # "Revisa la ortografía o usa términos más generales."

    def self.old_version_jp?(element)
      puts "********* old_version? [#{element.name}]"
      case element.name
        when 'div'
          return true
        when 'li'
          return false
        when 'document', 'html'
          # puts "********* PAGE_NOT_VALID [#{self::PAGE_NOT_VALID}]"
          # puts "********* NO_ELEMENTS [#{self::NO_ELEMENTS}]"
          if element.css('div.zg_itemRow').any?
            ScrapingLog.info "*********** VERSION: New (get_items)"
            return true
          else
            return false
          end
      end
      ScrapingLog.error "TopSeller JP: Esta traza no debería salir..."
    end

    def self.get_current_items_page(doc)
      if old_version_jp?(doc)
        doc.css('ol.zg_pagination li.zg_selected a @page').try(:text).to_i
      else
        doc.css('ul.a-pagination li.a-selected a').try(:text).to_i
      end
    end

    def self.get_items(doc)
      list = doc.css('div.zg_itemRow')
      # ScrapingLog.debug "list items count: [#{list.count}]"
      if list.blank?
        list = doc.css('ol#zg-ordered-list li.zg-item-immersion')
        # ScrapingLog.debug "list items seems blank, new list items count: [#{list.count}]"
      end
      list
    end

    def self.get_item_price(item)
      super.delete(',')
    end

    def self.get_item_customers_vote_average(item)
      item.css('div.a-icon-row a').first.try(:text).try(:split, ' ').try(:[], 1)
    end

    def self.get_item_topseller_order(item)
      if old_version_jp?(item)
        item.css('span.zg_rankNumber').try(:text).delete('.').to_i
      else
        super
      end
    end

  end
end
