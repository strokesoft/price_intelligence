module Page
  class Ecimarket < Eci
    # VALIDATIONS --------------------------------------------------------------------------------------------------------

    # METHODS ------------------------------------------------------------------------------------------------------------

    def self.origin_page # needed for special cases as Carrefour/Carrefourmarket, Eci/Ecimarket (diferent markets on same URL)
      'eci'
    end

    PAGE_NAME = "El Corte Inglés Market".freeze
  end
end
