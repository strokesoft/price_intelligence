module Page
  class Funnatic < PageBase
    include ActiveModel::Model
    extend ActiveModel::Translation


    # VALIDATIONS --------------------------------------------------------------------------------------------------------

    # METHODS ------------------------------------------------------------------------------------------------------------

    def self.get_items(doc)
      # doc.css('div.product.col-sm-12.col-md-6.col-lg-4')
      doc.css('div.products div.product')
    end

    def self.get_current_items_page(doc)
      Utils.clear_text(doc.css('ul.pagination li.active')[0].try(:text)).to_i
    end

    # def self.get_item_ean(item)
    #   nil
    # end

    def self.get_item_price(item)
      item.css("div.price_box div.final_price span.value").first.try(:text).try(:gsub, '.', '')
    end

    def self.get_item_url(item)
      # ('https://funnatic.es' + item.css("div.product_thumbnail.thumbnail div.caption a.show-loading").map{|w| w['href']}.first)
      "https://funnatic.es#{item.css('div.product_thumbnail.thumbnail div.caption a.show-loading @href').first.try(:text)}"
    end

    def self.get_item_name(item)
      item.css("div.product_thumbnail.thumbnail div.caption a.show-loading h4").text
    end

    def self.get_item_brand(item)
      get_item_name(item).split(' ')[0]
    end

    def self.get_item_out_of_stock(item)
      item.css('div.availability img @alt').first.try(:text) == "Consultar Entrega" ? true : false
    end

    def self.get_item_image_url(item)
      item.css('div.photo img @src').first.try(:text)
    end

    # def self.get_item_description(item)
    #   nil
    # end

    PAGE_NAME = "Funnatic".freeze

    private

    MAX_RETRIES = 3

    PRODUCTSxPAGE = 12

    # DEPARTMENTS = {
    #     Hobs:          '"https://funnatic.es/es/ct/induccion-gas-vitro-74?page=#{page_index}"',
    #     Dishwashers:   '"https://funnatic.es/es/ct/comprar-lavavajillas-inox-blanco-instalado-precios-73?page=#{page_index}"',
    #     Microwaves:    '"https://funnatic.es/es/ct/comprar-microondas-integrable-retro-diseno-madrid-barcelona-77?page=#{page_index}"',
    #     Ovens:         '"https://funnatic.es/es/ct/horno-standar-83?page=#{page_index}"',
    #     Refrigerators: '"https://funnatic.es/es/ct/combi-22?page=#{page_index}"',
    #     Washers:       '"https://funnatic.es/es/ct/lavadora-carga-frontal-34?page=#{page_index}"'
    #     #       Integrables:
    # }

    def self.get_price(doc)
      doc.css('div.final_price span.value').map{|q| q.text.gsub('.','')}.last
    end

    def self.get_ean(doc)
      doc.css('div.ean13 span.value').text
    end

    def self.get_brand(doc)
      doc.css('div.brand img.img-responsive').map{|q| q['alt'] }.last
    end

    def self.get_name(doc)
      doc.css('div.product_page_header div.page-header h1').text
    end

    def self.get_specs(doc)
      {info: (Utils.clear_text doc.css('div#long_desc').text)}
    end

    def self.get_image_url(doc)
      doc.css('div#product_detailed div.row div.photos.col-sm-4.col-md-5 div.main.thumbnail a img.img-responsive').map{|q| q['src'] }.last
    end

    def self.get_out_of_stock(doc)
      get_item_out_of_stock(doc)
    end

    def self.get_images_counter(doc)
      count = doc.css('div.thumbnails div.thumbnail').try(:count)
      count > 0 ? count : (get_image_url(doc).nil? ? nil : 1)
    end
  end
end
