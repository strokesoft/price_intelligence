module Page
  class Amazonfr < Amazon
    # VALIDATIONS --------------------------------------------------------------------------------------------------------

    # METHODS ------------------------------------------------------------------------------------------------------------

    PAGE_NAME = 'Amazon FR'.freeze
    COUNTRY_CODE = 'FR'.freeze

    OUT_OF_STOCK_TEXT = 'Temporairement en rupture de stock'.freeze ###########
    SECOND_HAND_OFFER = 'D\'occasion'.freeze
    SPONSORED_ITEM = 'Sponsorisé'.freeze
    EXTRA_URL_INFO = '/ref='.freeze
    VENDOR_PART_NUMBER_KEY = 'Num&eacute;o du mod&egrave;le de l\'article'.freeze # Numéro du modèle de l'article
    OTHER_VARIANTS_TEXT = 'Plusieurs choix disponibles'.freeze
    NEW_PRODUCT_ON_DETAIL = 'neuf'.freeze
    MORE_SALE_OPTIONS = 'Autres vendeurs sur Amazon'.freeze
    HTTPS_AMAZON = "https://www.amazon.fr".freeze
    PAGE_NOT_VALID = 'Dites-nous comment nous pouvons nous am'.freeze # 'Dites-nous comment nous pouvons nous am&eacute;liorer' # 'Dites-nous comment nous pouvons nous améliorer'
    NO_ELEMENTS = ' aucun article.'.freeze # ne correspond &agrave; aucun article # Votre recherche "xxxx" ne correspond à aucun article
    REVIEW_ORTHOGRAPHY = 'orthographe'.freeze ########### "Revisa la ortografía o usa términos más generales."
  end
end