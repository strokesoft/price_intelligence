module Page
  class Carrefourmarket < Carrefour
    # VALIDATIONS --------------------------------------------------------------------------------------------------------

    # METHODS ------------------------------------------------------------------------------------------------------------

    def self.origin_page # needed for special cases as Carrefour/Carrefourmarket, Eci/Ecimarket (diferent markets on same URL)
      'carrefour'
    end

    PAGE_NAME = "Carrefour Market".freeze
  end
end
