module Page
  class Game < PageBase
    include ActiveModel::Model
    extend ActiveModel::Translation


    # VALIDATIONS --------------------------------------------------------------------------------------------------------


    # Game: Por lo que veo no tiene paginación sino que a medida que haces scroll se van cargando mas resultados, lo malo es que los precios de las reservas lo muestran en la página interior, en el listado solo muestra el precio de reserva.
    #
    #     PS4:      https://www.game.es/buscar/juegos%20ps4%20nuevo/o=1&cf=000a+:GIDb+bS,aac_aa0032:2c0178:4c1263:4bS
    #     PC:       https://www.game.es/buscar/juegos-pc-nuevos
    #     Xbox one: https://www.game.es/buscar/juegos-xone/o=1&cf=000a_aa0195:4:GIDS
    #     Switch:   https://www.game.es/buscar/juego%20switch%20nuevo/o=1&cf=000a_aa0103:3:GIDS
    # 'https://www.game.es/buscar/juegos-pc-nuevos/0'
    #
    #
    # curl -X POST -H "Content-Type: application/json; charset=UTF-8" -d '{"Head":"xbox-live","Order":"2","TotalPages":2,"FirstSearch":false,"Page":1,"TotalResults":159,"CurrentProducts":70,"HotProducts":0}' https://www.game.es/api/search > test_3.json
    #
# curl -X POST -H "Content-Type: application/json; charset=UTF-8" -d '{"MinPrice":null,"MaxPrice":null,"Head":"xbox-live","SKU":"","Order":"1","CategoryFilter":[],"Category":null,"TotalPages":2,"FirstSearch":false,"Page":1,"TotalResults":159,"CurrentProducts":60,"HotProducts":0}' https://www.game.es/api/search > test.json
# curl -X POST -H "Content-Type: application/json; charset=UTF-8" -d '{"Head":"xbox-live","CategoryFilter":[],"Page":0}' https://www.game.es/api/search > test_0.json
    #
    # -H "Content-Type: application/json; charset=UTF-8"
    # -H ""
    # -H ""
    # -H ""
    # -H ""
    # -H ""
    #
    #
    #
    #
    # https://www.game.es/api/search
    #
    # POST /api/search HTTP/1.1
    # Host: www.game.es
    # Connection: keep-alive
    # Content-Length: 209
    # Accept: application/json, text/javascript, */*; q=0.01
    # Origin: https://www.game.es
    # X-Requested-With: XMLHttpRequest
    # User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36
    # Content-Type: application/json; charset=UTF-8
    # Referer: https://www.game.es/
    # Accept-Encoding: gzip, deflate, br
    # Accept-Language: en-US,en;q=0.9,es;q=0.8
    # Cookie: __RequestVerificationToken=4Xzf4V4wPK70tMAmmYHNPAYZdcrDvrrmpp9rSfb1z9l9khpIGIHmWAa0bp__kgb3K06VUkWCvvUP_gsJ197XYcds3a6Xm-rVgphO7zRFLhw1; __utma=163409322.1255056058.1542719536.1542719536.1542719536.1; __utmc=163409322; __utmz=163409322.1542719536.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); __utmt=1; __utmb=163409322.23.10.1542719536
    #
    #
    #
    # {"MinPrice":null,"MaxPrice":null,"Head":"xbox-live","SKU":"","Order":"2","CategoryFilter":[],"Category":null,"TotalPages":2,"FirstSearch":false,"Page":1,"TotalResults":159,"CurrentProducts":60,"HotProducts":0}
    # "MinPrice":null,
    # "MaxPrice":null,
    # "Head":"xbox-live",
    # "SKU":"",
    # "Order":"2",
    # "CategoryFilter":[],
    # "Category":null,
    # "TotalPages":2,
    # "FirstSearch":false,
    # "Page":1,
    # "TotalResults":159,
    # "CurrentProducts":60,
    # "HotProducts":0}
    #
    # {MinPrice: null, MaxPrice: null, Head: "xbox-live", SKU: "", Order: "2", CategoryFilter: [],…}
    # Category: null
    # CategoryFilter: []
    # CurrentProducts: 60
    # FirstSearch: false
    # Head: "xbox-live"
    # HotProducts: 0
    # MaxPrice: null
    # MinPrice: null
    # Order: "2"
    # Page: 1
    # SKU: ""
    # TotalPages: 2
    # TotalResults: 159

    # METHODS ------------------------------------------------------------------------------------------------------------

    def self.getPostHeaders()
      getHeaders().merge({'Content-Type': 'application/json; charset=UTF-8'})
    end

    def self.getDoc(url)
      url_items = url.split('https://www.game.es/buscar/')

      if url_items[1].nil?
        super(url)
      else
        url_items = url_items[1].split('/')

        # Basic REST.
        # Most REST APIs will set semantic values in response.body and response.code.
        require "net/http"

        # uri = URI.parse("https://www.game.es/api/search")
        # uri.host
        # uri.port
        # uri.request_uri

        if(proxy? && proxy_address.present?)
          proxy_port = (proxy_address.split(':')[-1]).delete('/').to_i
          proxy_host = (proxy_address.split(':')[-2]).delete('/')
          http = Net::HTTP.new("www.game.es", 443, proxy_host, proxy_port)
        else
          http = Net::HTTP.new("www.game.es", 443)
        end
        http.use_ssl = true
        # http.verify_mode = OpenSSL::SSL::VERIFY_NONE

        request = Net::HTTP::Post.new("/api/search")
        # request['Content-Type'] = 'application/json; charset=UTF-8'
        getPostHeaders().each{|header, value| request[header.to_s] = value}

        # Ejemplo de procesado de filtros:
        # /o=1&cf=000a+aa0b+:GIc+cDSd+d,b17e_b032:2e8:4d,aa1263:4e7:4d,Disponible:-5d
        #      cf=000a+aa0b+:GIc+cDSd+d,b17e_b032:2e8:4d,aa1263:4e7:4d,Disponible:-5d
        #             +0000000b+:GIc+cDSd+d,b17e_b032:2e8:4d,0000001263:4e7:4d,Disponible:-5d
        #             +       b+:GIc+cDSd+d,000000017e_0000000032:2e8:4d,0000001263:4e7:4d,Disponible:-5d
        #             +       b+   c+:GIDSd+d,000000017e_0000000032:2e8:4d,0000001263:4e7:4d,Disponible:-5d
        #             +       b+   c+     d+:GIDS,000000017e_0000000032:2e8:4:GIDS,0000001263:4e7:4:GIDS,Disponible:-5:GIDS
        #             +       b+   c+     d+               e_0000000032:2:GIDS,0000000178:4:GIDS,0000001263:4:GIDS,0000000177:4:GIDS,Disponible:-5:GIDS
        #             +       b+   c+     d+               e _ 0000000032:2:GIDS , 0000000178:4:GIDS , 0000001263:4:GIDS , 0000000177:4:GIDS , Disponible:-5:GIDS

        filters = ""
        if (url_items.count > 2)
          url_items[-2].split('&').each do |param|
            param_aux = param.split('=')
            if (param_aux[0] == 'cf')
              if (param_aux[1].include?('_'))
                filters = param_aux[1].gsub('Disponible', 'DISPONIBLE01234').split('_')

                codes = filters[0].split('+')
                codes.each_with_index do |aux, index|
                  code = aux.last
                  value = aux[0..-2]
                  codes.each_with_index { |aux_2, index_2| codes[index_2] = aux_2.gsub(code, value) if index_2 > index}
                  filters[1] = filters[1].gsub(code, value)
                end

                filters = filters[1].gsub('DISPONIBLE01234', 'Disponible')
              else
                filters = param_aux[1]
              end

              filters = filters.split(',').map do |filter|
                filter_params = filter.split(':')
                #category_filter<<{id: filter_params[0], type: filter_params[1], source: filter_params[2]}
                "{\"id\":\"#{filter_params[0]}\",\"type\":\"#{filter_params[1]}\",\"source\":\"#{filter_params[2]}\"}"
              end
              filters = filters.join(',')
            end
          end
        end

        # "CategoryFilter":[
        #       {"id":"0000000032","type":"2","source":"GIDS"},
        #       {"id":"0000000178","type":"4","source":"GIDS"},
        #       {"id":"0000001263","type":"4","source":"GIDS"}]

        # request.set_form_data('{"Head":"xbox-live","CategoryFilter":[],"Page":0}')
        request.body = "{\"Head\":\"#{url_items.first}\",\"CategoryFilter\":[#{filters}],\"Page\":#{url_items.last}}"

        # puts("******** request.body -> #{request.body}")

        response = http.request(request)

        # puts("******** response.inspect -> #{response.inspect}")
        # puts("******** response.body -> #{response.body}")

        # response.code             # => 301
        # response.body             # => The body (HTML, XML, blob, whatever)
        # # Headers are lowercased
        # response["cache-control"] # => public, max-age=2592000

        # File.open("#{DateTime.now}.json", 'w') { |file| file.write(response.body) }
        response.code == "200" ? response.body : '{"Products":[],"Filters":[],"TotalResults":0,"TotalPages":0,"HasPagination":null,"CacheID":null}'
      end
    end

    # def self.break_less_than_per_page?
    #   false
    # end

    def self.get_items(doc)
      json = eval doc.force_encoding('UTF-8').gsub(':null', ':nil')
      json[:Products]
    end

    # def self.get_current_items_page(doc)
    #   nil
    # end

    def self.get_total_pages(doc)
      json = eval doc.force_encoding('UTF-8').gsub(':null', ':nil')
      json[:TotalPages]
    end

    # def self.get_item_ean(item)
    #   nil
    # end

    def self.get_item_price(item)
      item[:Offers][0].try(:[], :SellPrice).to_s
    end

    def self.get_item_url(item)
      "https://www.game.es/#{item[:Navigation]}"
    end

    def self.get_item_name(item)
      item[:Name]
    end

    # def self.get_item_brand(item)
    #   nil
    # end

    def self.get_item_out_of_stock(item)
      # Próximamente "ButtonType":0
      # Comprar      "ButtonType":1
      # Pre-compra   "ButtonType":2
      # Reservar     "ButtonType":3
      # Agotado      "ButtonType":4
      # Comprar      "ButtonType":6
      text = item[:Offers][0].try(:[], :ButtonType)
      case text
        when 1,6 then
          false
        when 0,2,3,4 then
          true
        else
          #TODO: buscar/añadir más casos
          ScrapingLog.error "********************
********************
Añadir a Page::Game.get_item_out_of_stock este caso: [#{text}] juego listado [#{get_item_url(item)}]
********************
********************" unless text.nil?
          text.nil? ? nil : true
      end
    end

    def self.get_item_image_url(item)
      item[:ImageUrl]
    end

    def self.get_item_description(item)
      get_item_name(item)
    end

    PAGE_NAME = "Game".freeze

    private

    MAX_RETRIES = 3

    PRODUCTSxPAGE = 1

    def self.get_price(doc)
      Utils.clear_price(doc.css('script[type="application/ld+json"]').text.try(:split, '"price" : "')[1].try(:split, '"')[0])
    end

    # def self.get_ean(doc)
    #   nil
    # end

    def self.get_url(doc)
      "https://www.game.es/#{doc.css('a.active @href')[0].try(:text)}"
    end

    # def self.get_brand(doc)
    #   nil
    # end

    def self.get_name(doc)
      Utils.clear_text doc.css('h1.product-title').try(:text)
    end

    def self.get_description(doc)
      Utils.clear_text(doc.css('script[type="application/ld+json"]').text.try(:split, '"description" : "')[1].try(:split, '"')[0])
    end

    # def self.get_specs(doc)
    #   nil
    # end

    def self.get_image_url(doc)
      doc.css('figure.product-thumbnail div.u-100 img.stick').remove
      doc.css('figure.product-thumbnail div.u-100 img').first.try(:[], "src")
    end

    def self.get_out_of_stock(doc)
      text = Utils.clear_text(doc.css('script[type="application/ld+json"]').text.try(:split, '"availability" : "')[1].try(:split, '"')[0])
      case text
        when 'InStock' then
          false
        when 'PreOrder', 'OutOfStock' then
          true
        else
          #TODO: buscar/añadir más casos
          ScrapingLog.error "********************
********************
Añadir a Page::Game.get_out_of_stock este caso: [#{text}] url [#{get_url(doc)}]
********************
********************" unless text.nil?
          text.nil? ? nil : true
      end
    end
  end
end
