module Page
  class Xtralife < PageBase
    include ActiveModel::Model
    extend ActiveModel::Translation


    # VALIDATIONS --------------------------------------------------------------------------------------------------------


# Xtralife: por lo que me han dicho esta página va cambiar el diseño en octubre por tanto lo mejor es dejarlo para el final o hasta que veamos como es el nuevo porque si no nos tocará hacerlo 2 veces, lo he acordé así con el cliente.
#
#     PS4:      '"https://www.xtralife.es/playstation-4/juegos/todos/0/todos/cant/#{self::PRODUCTSxPAGE}"'
#     PC:       '"https://www.xtralife.es/pc-cd-rom/juegos/todos/0/todos/cant/#{self::PRODUCTSxPAGE}"'
#     Xbox One: '"https://www.xtralife.es/xbox-one/juegos/todos/0/todos/cant/#{self::PRODUCTSxPAGE}"'
#     Switch:   '"https://www.xtralife.es/switch/juegos/todos/0/todos/cant/#{self::PRODUCTSxPAGE}"'
               # 'https://www.xtralife.es/switch/juegos/todos/0/todos/cant/20'

#
#    https://www.xtralife.com/seleccion/switch-juegos/694
#    https://api.xtralife.com/public-api/v1/group?storefront_id=1&id=694&members%5Border%5D%5Bby%5D=&members%5Border%5D%5Btype%5D=DESC&members%5Bpage%5D=1&members%5Bhow_many%5D=24&members%5Bfilters%5D=%5B%5D
#
# curl -X GET -H "Content-Type: application/json" https://api.xtralife.com/public-api/v1/group?storefront_id=1&id=694&members%5Border%5D%5Bby%5D=&members%5Border%5D%5Btype%5D=DESC&members%5Bpage%5D=1&members%5Bhow_many%5D=24&members%5Bfilters%5D=%5B%5D > log/test.json
# curl -X GET -H "Content-Type: application/json" https://api.xtralife.com/public-api/v1/group\?storefront_id\=1\&id\=694\&members%5Border%5D%5Bby%5D\=\&members%5Border%5D%5Btype%5D\=DESC\&members%5Bpage%5D\=1\&members%5Bhow_many%5D\=24\&members%5Bfilters%5D\=%5B%5D > log/test.json
#
#   Hacer el PageBase.get_document poniendo en cabeceras el "Content-Type: application/json":
# PageBase.get_document 'https://api.xtralife.com/public-api/v1/group?storefront_id=1&id=694&members%5Border%5D%5Bby%5D=&members%5Border%5D%5Btype%5D=DESC&members%5Bpage%5D=1&members%5Bhow_many%5D=24&members%5Bfilters%5D=%5B%5D'
#
    #
    #
    # https://api.xtralife.com/public-api/v1/search?storefront_id=1&term=switch&type=group&page=1&how_many=3&order%5Bby%5D=relevancy&order%5Btype%5D=DESC&filters=%5B%7B%22category%22%3A%22Characteristic.tipo-producto-videojuegos%22%2C%22operator%22%3A%22%3D%3D%22%2C%22value%22%3A%22Videojuegos%22%2C%22condition_type%22%3A%22%22%7D%5D&includePosters=false
    # https://api.xtralife.com/public-api/v1/search?storefront_id=1&term=switch&type=sku  &page=1&how_many=24&order%5Bby%5D=relevancy&order%5Btype%5D=DESC&filters=%5B%7B%22category%22%3A%22Characteristic.tipo-producto-videojuegos%22%2C%22operator%22%3A%22%3D%3D%22%2C%22value%22%3A%22Videojuegos%22%2C%22condition_type%22%3A%22%22%7D%5D&includePosters=false
    #
#
    #
    #
    # https://www.xtralife.com/buscar/switch/%7B%22page%22%3A1%2C%22how_many%22%3A48%2C%22term%22%3A%22switch%22%2C%22filters%22%3A%5B%7B%22category%22%3A%22Characteristic.tipo-producto-videojuegos%22%2C%22operator%22%3A%22%3D%3D%22%2C%22value%22%3A%22Videojuegos%22%2C%22condition_type%22%3A%22%22%7D%5D%2C%22how_many_members%22%3A48%7D
    # https://api.xtralife.com/public-api/v1/search?storefront_id=1&term=switch&type=sku&page=1&how_many=48&order%5Bby%5D=relevancy&order%5Btype%5D=DESC&filters=%5B%7B%22category%22%3A%22Characteristic.tipo-producto-videojuegos%22%2C%22operator%22%3A%22%3D%3D%22%2C%22value%22%3A%22Videojuegos%22%2C%22condition_type%22%3A%22%22%7D%5D&includePosters=false
    #
    #
    ## https://www.xtralife.com/buscar/switch/%7B%22page%22%3A1%2C%22how_many%22%3A48%2C%22term%22%3A%22switch%22%2C%22filters%22%3A%5B%7B%22category%22%3A%22Characteristic.tipo-producto-videojuegos%22%2C%22operator%22%3A%22%3D%3D%22%2C%22value%22%3A%22Videojuegos%22%2C%22condition_type%22%3A%22%22%7D%5D%2C%22how_many_members%22%3A48%7D
    #  https://www.xtralife.com/buscar/switch/
    # {
    # :page=>1,
    # :how_many=>48,
    # :term=>"switch",
    # :filters=>[
    #   {:category=>"Characteristic.tipo-producto-videojuegos", :operator=>"==", :value=>"Videojuegos", :condition_type=>""}
    # ],
    # :how_many_members=>48
    # }
    #
    #
    ## https://api.xtralife.com/public-api/v1/search?storefront_id=1&term=switch&type=sku&page=1&how_many=48&order%5Bby%5D=relevancy&order%5Btype%5D=DESC&filters=%5B%7B%22category%22%3A%22Characteristic.tipo-producto-videojuegos%22%2C%22operator%22%3A%22%3D%3D%22%2C%22value%22%3A%22Videojuegos%22%2C%22condition_type%22%3A%22%22%7D%5D&includePosters=false
    #  https://api.xtralife.com/public-api/v1/search?
    # storefront_id=1&
    # term=switch&
    # type=sku&page=1&
    # how_many=48&
    # order[by]=relevancy&
    # order[type]=DESC&
    # filters=[{\"category\":\"Characteristic.tipo-producto-videojuegos\",\"operator\":\"==\",\"value\":\"Videojuegos\",\"condition_type\":\"\"}]&
    #   {:category=>"Characteristic.tipo-producto-videojuegos", :operator=>"==", :value=>"Videojuegos", :condition_type=>""}
    # includePosters=false
    #
    #
    #
    #
    #
    # curl -X GET -H "Content-Type: application/json" https://api.xtralife.com/public-api/v1/search\?storefront_id\=1\&term\=switch\&type\=sku\&page\=1\&how_many\=48\&order%5Bby%5D\=relevancy\&order%5Btype%5D\=DESC\&filters\=%5B%7B%22category%22%3A%22Characteristic.tipo-producto-videojuegos%22%2C%22operator%22%3A%22%3D%3D%22%2C%22value%22%3A%22Videojuegos%22%2C%22condition_type%22%3A%22%22%7D%5D\&includePosters\=false > test2.json
    # curl -X GET https://api.xtralife.com/public-api/v1/search\?storefront_id\=1\&term\=switch\&type\=sku\&page\=1\&how_many\=48\&order%5Bby%5D\=relevancy\&order%5Btype%5D\=DESC\&filters\=%5B%7B%22category%22%3A%22Characteristic.tipo-producto-videojuegos%22%2C%22operator%22%3A%22%3D%3D%22%2C%22value%22%3A%22Videojuegos%22%2C%22condition_type%22%3A%22%22%7D%5D\&includePosters\=false > test3.json
    # curl -X GET https://api.xtralife.com/public-api/v1/search\?storefront_id\=1\&term\=switch\&type\=sku\&page\=1\&how_many\=48\&order%5Bby%5D\=relevancy\&order%5Btype%5D\=DESC\&filters\=%5B%7B%22category%22%3A%22Characteristic.tipo-producto-videojuegos%22%2C%22operator%22%3A%22%3D%3D%22%2C%22value%22%3A%22Videojuegos%22%2C%22condition_type%22%3A%22%22%7D%5D\&includePosters\=false > test4.json
    #

# TIPO 1: seleccion:
# ==================
    # https://www.xtralife.com/seleccion/switch-juegos/743
    # curl -X GET https://api.xtralife.com/public-api/v1/group?storefront_id=1&id=743&members%5Border%5D%5Bby%5D=&members%5Border%5D%5Btype%5D=DESC&members%5Bpage%5D=2&members%5Bhow_many%5D=24&members%5Bfilters%5D=%5B%5D > 20190221_test1.json
    #
    ## https://api.xtralife.com/public-api/v1/group?storefront_id=1&id=743&members%5Border%5D%5Bby%5D=&members%5Border%5D%5Btype%5D=DESC&members%5Bpage%5D=2&members%5Bhow_many%5D=24&members%5Bfilters%5D=%5B%5D > 20190221_test1.json
    #  https://api.xtralife.com/public-api/v1/group?
    # storefront_id=1&
    # id=743&
    ## members%5Border%5D%5Bby%5D=&
    ## members%5Border%5D%5Btype%5D=DESC&
    # members%5Bpage%5D=2&
    # members%5Bhow_many%5D=24&
    ## members%5Bfilters%5D=%5B%5D
    #
    #

    # METHODS ------------------------------------------------------------------------------------------------------------


#     def self.getDoc(url)
#       if url.index("https://static.xtralife.com/storefronts/") == 0
#         Net::HTTP.get(URI(url)) # valorar si procesar el resultado por si indica algún error o success==false
#       else
#         new_url = nil
#         url_items = url.split('://www.xtralife.com/')

#         unless url_items[1].nil?
#           url_items = url_items[1].split('/')

#           case url_items[0]
#             when 'seleccion' then
#               new_url = "https://api.xtralife.com/public-api/v1/group?storefront_id=1"
#               new_url += "&id=#{url_items[2]}"
#               new_url += "&members[page]=1"
#               new_url += "&members[how_many]=#{PRODUCTSxPAGE}"
#             when 'buscar' then
#             when 'producto' then
#               # Caso de descarga de la página de un producto. Nada que cambiar
#             else
#               ScrapingLog.error "********************
# ********************
# Añadir a Page::Xtralife.getDoc este caso de tipo de url: [#{url_items[0]}] url [#{url}]
# ********************
# ********************"
#           end
#         end

#         if new_url
#           ScrapingLog.info "URL transformada, se va a escargar: [#{new_url}]"
#           Net::HTTP.get(URI(new_url)) # valorar si procesar el resultado por si indica algún error o success==false
#         else
#           super(url)
#         end
#       end
#     end

    # def self.break_less_than_per_page?
    #   false
    # end
    #
    #
    #
    # https://www.xtralife.com/producto/set-de-4-cables-hdmi-6-afterglow-verde-dorado-lila-azul-ps3-/15569
    # https://www.xtralife.com/producto/set-de-4-cables-hdmi-6-afterglow-verdedoradolilaazul-ps3-cables/15569

    def self.getDoc(url)
      JSON.parse(open(url, getHeaders).read)
    end

    # def self.get_json(doc)
    #         byebug
    #   eval(doc.gsub('\ud83d\udddd', '-ud83d-udddd').gsub(':null', ':nil')) # Los caracteres ud83d udddd son raros... los quitamos  #.force_encoding('UTF-8')
    # end

    def self.get_items(doc)
      doc.try(:[], 'body').try(:[], 'members').try(:[], 'results')
    end

    # def self.get_aditional_items_params(item_params, doc_item)
    #   []
    # end

    def self.get_current_items_page(doc)
      doc['body'].try(:[],'members').try(:[],'page')
    end

    def self.get_total_pages(doc)
      doc['body'].try(:[],'members').try(:[],'total_pages')
    end

    def self.get_item_category(item)
      types = get_item_characteristic_value(item, 'tipo-producto-videojuegos', true) ||
          get_item_characteristic_value(item, 'complemento-digital', true) ||
          get_item_characteristic_value(item, 'servicios', true) ||
          []
#       if types.size > 1
#         ScrapingLog.warn "********************
# ********************
# En Page::Xtralife hay un producto que indica que es de varios tipos: [#{types.inspect}] pero solo nos estamos quedando con el primero. Ver: [#{get_item_url(item)}]
# ********************
# ********************"
#       end

      type = nil
      case types[0]
        when 'Accesorios'
          type = 'Accesorios'
        when 'Consolas'
          type = 'Consolas'
        when 'Expansiones y DLCs'
          type = 'Expansiones y DLCs'
        when 'Guías'
          type = 'Guías'
        when 'Mapas'
          type = 'Mapas'
        when 'Merchandising'
          type = 'Merchandising'
        when 'Pases de Temporada'
          type = 'Pases de Temporada'
        when 'Suscripciones'
          type = 'Suscripciones'
        when 'Tarjetas Prepago'
          type = 'Tarjetas Prepago'
        when 'Videojuegos'
          type = 'Videogames'
        # when ''
        #   type = ''
        when nil then
          if !get_item_characteristic_value(item, 'merchandising', true).nil?
            type = 'Merchandising'
          elsif !get_item_characteristic_value(item, 'tipo-accesorio', true).nil?
            type = 'Accesorios'
          elsif !get_item_characteristic_value(item, 'desarrollador', true).nil? ||
                !get_item_characteristic_value(item, 'genre', true).nil?
             type = 'Videogames'
          else
            ScrapingLog.error "********************
********************
En Page::Xtralife hay un producto que no indica que sea de ningún tipo: [#{types.inspect}]. Ver: [#{get_item_url(item)}]
********************
********************"
          end
        else
          ScrapingLog.warn "********************
********************
En Page::Xtralife hay un producto para con un tipo no contemplado: [#{types[0]}]. Lo crearemos como '#{types[0]} PLATFORM' Ver: [#{get_item_url(item)}]
********************
********************"
          type = "#{types[0]}"
#           ScrapingLog.warn "********************
# ********************
# En Page::Xtralife hay un producto para con un tipo no contemplado: [#{types[0]}] Ver: [#{get_item_url(item)}]
# ********************
# ********************"
      end


      platforms = get_item_characteristic_value(item, 'platform', true) ||
                  get_item_characteristic_value(item, 'marketplace-digital', true) ||
                  []
#       if platforms.size > 1
#         ScrapingLog.warn "********************
# ********************
# En Page::Xtralife hay un producto para varias plataformas: [#{platforms.inspect}] pero solo nos estamos quedando con la primera. Ver: [#{get_item_url(item)}]
# ********************
# ********************"
#       end

      platform = nil
      case platforms[0]
        when '3DS'
          platform = '3DS'
        when 'PC'
          platform = 'PC'
        when 'PS3'
          platform = 'PS3'
        when 'PS4'
          platform = 'PS4'
        when 'Playstation Network'
          platform = 'Playstation Network'
        when 'PS Vita'
          platform = 'PS Vita'
        when 'Spotify'
          platform = 'Spotify'
        when 'Switch'
          platform = 'SWC'
        when 'Wii U'
          platform = 'Wii U'
        when 'WII'
          platform = 'WII'
        when 'Xbox 360'
          platform = 'Xbox 360'
        when 'Xbox Live'
          platform = 'Xbox Live'
        when 'Xbox One'
          platform = 'XBO'
        # when ''
        #   platform = ''
        when nil then
          if get_item_name(item).start_with?('Vinilo ')
            platform = 'Vinilo'
          else
            ScrapingLog.error "********************
********************
En Page::Xtralife hay un producto que no indica que sea para ninguna plataforma: [#{platforms.inspect}]. Ver: [#{get_item_url(item)}]
********************
********************"
          end
        else
          ScrapingLog.warn "********************
********************
En Page::Xtralife hay un producto para una plataforma no contemplada: [#{platforms[0]}]. Lo crearemos como '#{type} #{platforms[0]}' Ver: [#{get_item_url(item)}]
********************
********************"
          platform = "#{platforms[0]}"
#           ScrapingLog.warn "********************
# ********************
# En Page::Xtralife hay un producto para una plataforma no contemplada: [#{platforms[0]}]. Ver: [#{get_item_url(item)}]
# ********************
# ********************"
      end

      [type, platform].compact.join(" ") # unless type.blank? || platform.blank?
    end

    # def self.get_item_subcategory(item)
    #   nil
    # end

    # def self.get_item_ean(item)
    #   nil
    # end

    def self.get_item_price(item)
      item['currentPrice']['price'].to_s rescue nil
    end

    def self.get_item_url(item)
      product = "#{get_item_name(item).try(:parameterize).gsub('/','').gsub("'","")}-"
      product += "#{get_item_characteristic_value(item, 'platform')}-#{
                    get_item_characteristic_value(item, 'edition', true).try(:join,"-")}-#{
                    get_item_characteristic_value(item, 'tipo-accesorio', true).try(:join,"-")}-#{
                    get_item_characteristic_value(item, 'merchandising', true).try(:join,"-")}-#{
                    get_item_name(item).start_with?('Vinilo ') ? 'vinilo' : nil }".parameterize
      "https://www.xtralife.com/producto/#{product}/#{item['id']}"
    end

    def self.get_item_name(item)
      item['name']
    end

    def self.get_item_brand(item)
      get_item_characteristic_value(item, 'franchise') || get_item_characteristic_value(item, 'desarrollador') || get_item_characteristic_value(item, 'distribuidor')
    end

    def self.get_item_image_url(item)
      images = get_item_characteristic_value(item, 'cover').try(:[], 'conversions')
      images['fullhd'] || images['medium'] || images['thumb'] || images['xs'] || images['gallery'] unless images.blank?
    end

    def self.get_item_images_counter(item)
      count = get_item_characteristic_value(item, 'media', true).try(:count) || 0
      get_item_image_url(item).blank? ? count : (count+1)
    end

    def self.get_item_description(item)
      Nokogiri::HTML(get_item_characteristic_value(item, 'description')).text
    end

    def self.get_item_out_of_stock(item)
      text = item['disponibility']['disponibility']
      case text
        when 'sell' then
          false
        when 'out_of_stock', 'not_for_sale', 'reservation', 'reservation_out_of_stock', 'reservation_not_opened' then
          true
        else
          #TODO: buscar/añadir más casos
          ScrapingLog.error "********************
********************
Añadir a Page::Xtralife.get_item_out_of_stock este caso: [#{text}] juego listado [#{get_item_url(item)}]
********************
********************" unless text.nil?
          # text.nil? ? nil : true
          nil
      end
    end


    PAGE_NAME = "XtraLife".freeze

    private

    def self.get_item_characteristic_value(item, name, get_all = false)
      all_found = []
      item['characteristics'].each do |characteristic|
        if characteristic['name'] == name
          get_all ?
              (all_found << characteristic['value']) :
              (return characteristic['value'])
        end
      end
      all_found.blank? ? nil : all_found
    end

    MAX_RETRIES = 3

    PRODUCTSxPAGE = 200

    def self.get_price(doc)
      # Utils.clear_price(doc.css('p.precio_ficha span').text) # Precio falso modificado por js
      Utils.clear_price(doc.css('div.fichaCabeceraLeft img @alt').text.split(' ').last)
    end

    # def self.get_ean(doc)
    #   nil
    # end

    def self.get_url(doc)
      doc.css('meta[property="og:url"]').first.try(:[], "content")
    end

    # def self.get_brand(doc)
    #   nil
    # end

    def self.get_name(doc)
      # doc.css('meta[property="og:title"]').first.try(:[], "content")
      doc.css('h1.nombre_ficha').text
    end

    def self.get_description(doc)
      # doc.css('meta[property="og:description"]').first.try(:[], "content")
      doc.css('div.fichaDescripcion').text.gsub("\n", ' ').gsub("\r", ' ').gsub("\t" , ' ')
    end

    # def self.get_specs(doc)
    #   nil
    # end

    def self.get_image_url(doc)
      # doc.css('meta[property="og:image"]').first.try(:[], "content")
      doc.css('div.fichaCabeceraLeft img @src').text
    end

    def self.get_out_of_stock(doc)
      text = doc.css('div.popupFichaContent div.openPopupStock')[0].text.strip.split("\n")[0]
      case text
        when 'En Stock' then
          false
        # when "OutOfStock" then
        #   true
        else
          #TODO: buscar/añadir más casos
          ScrapingLog.error "********************
********************
Añadir a Page::Xtralife.get_out_of_stock este caso: [#{text}] url [#{get_url(doc)}]
********************
********************" unless text.nil?
          # true
         nil
      end
    end
  end
end
