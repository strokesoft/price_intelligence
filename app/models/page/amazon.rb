module Page
  class Amazon < PageBase
    include ActiveModel::Model
    # include MechanizeDoc
    extend ActiveModel::Translation

    PAGE_NAME = 'Amazon ES'.freeze
    COUNTRY_CODE = 'ES'.freeze
    MAX_RETRIES = 3
    PRODUCTSxPAGE = 16

    TITLE_SEPARATOR = ' - '.freeze
    OUT_OF_STOCK_TEXT = 'Temporalmente sin stock'.freeze
    SECOND_HAND_OFFER = 'de 2ª mano'.freeze
    SPONSORED_ITEM = '[Patrocinado]'.freeze
    EXTRA_URL_INFO = '/ref='.freeze
    VENDOR_PART_NUMBER_KEY = 'N&uacute;mero de modelo del producto'.freeze
    OTHER_VARIANTS_TEXT = 'Ver otras variantes'.freeze
    NEW_PRODUCT_ON_DETAIL = 'Nuevos:'.freeze
    MORE_SALE_OPTIONS = 'Más opciones de compra'.freeze
    HTTPS_AMAZON = "https://www.amazon.es".freeze
    PAGE_NOT_VALID = 'Dinos cómo podemos mejorar'.freeze
    NO_ELEMENTS = 'no ha coincidido con ning'.freeze # La búsqueda no ha coincidido con ningún producto
    REVIEW_ORTHOGRAPHY = 'Revisa la ortograf'.freeze # "Revisa la ortografía o usa términos más generales."

    # def self.use_mechanize?
    #   true
    # end

    def self.break_less_than_per_page?
      false
    end

    def self.get_article_number(doc)
      res = doc.css('div.a-section.a-spacing-small.a-spacing-top-small')[0].text.strip.split(" ")[2]
      if res == "más"
        1000
      else
        res.to_i
      end
    end

  def self.scrapp_page_department(page_department, page_index = 1)
    # ScrapingLog.info "\n\n\n\n************************ #{self} #{self.class} - [#{page_index}] - [#{get_class(page_department.page)}] [#{get_class(page_department.page).class}] ************************"
    return get_class(page_department.page).scrapp_page_department(page_department, page_index) if self != get_class(page_department.page)

    total_pages = -1
    items_counter = 0
    product_prices_counter = 0
    variants_array = []
    article_number = nil
    begin
      ScrapingLog.info "\n\n\n\n************************ #{self} #{self.class} SCRAPING: [#{page_department.page}] [#{page_department.category}] [#{page_department.subcategory}] [#{page_department.url_template}] start at page: [#{page_index}] ************************"

      # Instantiate new agent of Mechanize
      # mechanize_agent(page: page_department.page, force: true) if use_mechanize?

      loop do
        attempts = 8
        begin
          attempts -= 1
          doc = get_document(build_url(page_department.url_template, page_index), true)
          article_number = get_article_number(doc)

          if doc.nil?
            ScrapingLog.error "#{self.name}: ERROR No se ha podido descargar: [ #{eval(page_department.url_template)} ]"
            raise "not response"
          # else
          #   File.write("#{DateTime.now}_Downloaded.html", doc.to_html)
          end

          # ScrapingLog.info "DOCUMENTO: #{doc.to_s.truncate(100)}"

          # break if page_index > 1 && [nil, page_index].exclude?(get_current_items_page(doc)) # Break if we aren't on expected page number (not check on first page). IMPORTANT!!!! Don't add '0' do valid options!!! if you do it, some cases will fail: pages witch exactly PRODUCTSxPAGE products listed: then no pagination and no less products than expected, and then no break condition

          # items = get_products_info_from_list(doc)
          items = parse_products_list(doc)
          total_pages = get_total_pages(doc) || -1
        rescue SyntaxError, NameError => ex
          # Esto puede ocurrir en el 'eval' cuando el doc esperamos que sea un json a parsear y nos llega, por ejemplo, un html (como en Game)
          message = "#{self.name}: ERROR [ #{ex.class}: #{ex.message} ]"
          ScrapingLog.error message
          ScrapingLog.error "Backtrace:\n\t#{ex.backtrace.join("\n\t")}"
          CsvError.create({source_line: doc.to_s, error: message, mode: 'PageBase.scrapp_page_department', parser: "", category: "", date: Date.today, n: -1 }) if attempts == 0
          if attempts > 0
            ScrapingLog.info "Reintentamos la descarga tras un SyntaxError. Intentos restantes [#{attempts}]"
            sleep 5
            retry
          else
            if (total_pages > page_index)
              ScrapingLog.info "Aunque está habiendo algún tipo de problema, hemos encontrado que hay [#{total_pages}] pero vamos por la [#{page_index}]. Probamos con la siguiente página."
              page_index += 1
              retry
            else
              items = []
            end
          end
        end


        items.each do |item|
          item.store(:category,    page_department.category)    unless page_department.category.blank? || page_department.category == '*EXTRAER*'
          item.store(:subcategory, page_department.subcategory) unless page_department.subcategory.blank?
          item.store(:last_aparition, Date.today)
          item.store(:date, Date.today)
        end

        process_info = self.process_products_info(items, items_counter)
        product_prices_counter += process_info[0]
        variants_array += process_info[1]

        items_counter += items.count - 1
        # break if break_less_than_per_page? && (items.count < self::PRODUCTSxPAGE || page_department.url_template.exclude?("page_index"))
        break if (article_number/self::PRODUCTSxPAGE.to_f).ceil <= page_index
        page_index += 1
      end

      product_variants_counter = process_variants_array(page_department, variants_array.uniq, items_counter)

      ScrapingLog.info "Obtenidos total: #{items_counter} - válidos: [#{product_prices_counter}] --- [#{page_department.page}] [#{page_department.category}] [#{page_department.subcategory}] [#{page_department.url_template}]"
      ScrapingLog.info "Total variantes procesadas: #{product_variants_counter} --- [#{page_department.page}] [#{page_department.category}] [#{page_department.subcategory}]" if product_variants_counter > 0

      raise "WARN: Se han descargado [#{items_counter}] productos, el máximo por página. Podemos estar perdiendo productos en la plantilla sin paginación: [#{page_department.url_template}]" if items_counter == self::PRODUCTSxPAGE && page_department.url_template.exclude?("page_index")

    rescue Exception => ex
      ScrapingLog.error "#{self.name}: ERROR no controlado [ #{ex.class}: #{ex.message} ]"
      ScrapingLog.error "Backtrace:\n\t#{ex.backtrace.join("\n\t")}"
    end

    page_department.pages_max            = page_index    if page_index > page_department.pages_max
    page_department.pages_last           = page_index
    page_department.products_listed_max  = items_counter if items_counter > page_department.products_listed_max
    page_department.products_listed_last = items_counter
    page_department.products_valid_last  = product_prices_counter
    page_department.save!

    {page: page_department.page, category: page_department.category, subcategory: page_department.subcategory,
     url_template: page_department.url_template, listed_items: items_counter, procesed_items: product_prices_counter}
  end

  def self.getHeaders()
    headers = {allow_redirections: :all, # for 'open_uri_redirections': This gem applies a patch to OpenURI to optionally allow redirections from HTTP to HTTPS, or from HTTPS to HTTP.
               'User-Agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36',
               'Connection' => 'keep-alive'}

    if proxy?
      if proxy_address.present?
        ScrapingLog.info("*** OpenUri CONFIGURED WITH CUSTOM PROXY Header *** [#{proxy_address}]")
        headers = headers.merge({proxy_http_basic_authentication: [proxy_address, proxy_user, proxy_password]})
      else
        proxy_api_key = Settings.env.proxy.proxy_api_key
        ScrapingLog.info("*** OpenUri CONFIGURED WITH DEFAULT PROXY Header *** [#{proxy_api_key}]")
        headers = headers.merge({proxy_http_basic_authentication:
             ["http://#{Settings.env.proxy.proxy_host}:#{Settings.env.proxy.proxy_port}",
             proxy_api_key,
             '']})
      end
    end
    headers
  end

    def self.old_version?(element)
      # puts "********* old_version? [#{element.name}]"
      case element.name
        when 'div'
          # puts "*********** VERSION: New"
          return false
        when 'li'
          # puts "*********** VERSION: Old"
          return true
        when 'document', 'html'
          # puts "********* PAGE_NOT_VALID [#{self::PAGE_NOT_VALID}]"
          # puts "********* NO_ELEMENTS [#{self::NO_ELEMENTS}]"
          if element.css('div.s-result-item').any?
            ScrapingLog.info "*********** VERSION: New (get_items)"
            return false
          elsif element.css('li.s-result-item').any?
            ScrapingLog.info "*********** VERSION: Old (get_items)"
            return true
          elsif element.at("span:contains('#{self::PAGE_NOT_VALID}')")
            ScrapingLog.info "*********** Debe estar habiendo problemas de distintas paginaciones. Consideramos: VERSION: Old (get_items)"
            return true # No se reconoce un formato concreto
          elsif element.at("h1:contains('#{self::NO_ELEMENTS}')")
            return true # Se debe haber llegado al final de la paginación
          elsif element.at("span:contains('#{self::REVIEW_ORTHOGRAPHY}')")
            return true # Se debe haber llegado al final de la paginación
          end
      end
      file = "#{DateTime.now}_Amazon.html"
      begin
        File.write(file, element.to_html)
      rescue
        ScrapingLog.error "No se ha podido escribir el fichero: #{file})"
        file = "ERROR AL ESCRIBIR EL FICHERO"
      end
      ScrapingLog.error "Parece que #{origin_page.capitalize} ha cambiado... REVISAR EL SCRAPPER (Ver fichero: #{file})"
      raise "Parece que #{origin_page.capitalize} ha cambiado... REVISAR EL SCRAPPER (Ver fichero: #{file})"
    end

    def self.get_current_items_page(doc)
      if old_version?(doc)
        Utils.clear_text(doc.css('div.pagnHy span.pagnCur')[0].try(:text)).to_i
      else
        doc.css('ul.a-pagination li.a-selected').text.to_i
      end
    end

    def self.get_items(doc)
      puts "**************+ #{origin_page.capitalize}.get_items *************"
      if old_version?(doc)
        doc.css('ul.s-result-list li.s-result-item')
      else
        doc.css('div.s-result-list div.s-result-item')
      end
    end

    def self.main_article_number
      :asin
    end

    def self.get_item_brand(item)
      if old_version?(item)
        aux = item.css('div.a-spacing-small')
        aux = item.css('div.a-spacing-mini') if aux.blank?
        aux.first.css('span.a-color-secondary').last.try(:text)
      else
        item.css('div.a-color-secondary span').first.try(:text).try(:gsub, 'de', '')
      end
    end

    def self.get_item_name(item)
      if old_version?(item)
        get_item_title(item).try(:split, self::TITLE_SEPARATOR).try(:[], 0)
      else
        item.css('span.a-text-normal').first.try(:text)
      end
    end

    def self.get_item_description(item)
      if old_version?(item)
        get_item_title(item).try(:split, self::TITLE_SEPARATOR).try(:drop, 1).try(:join, self::TITLE_SEPARATOR)
      else
        get_item_name(item).try(:split, self::TITLE_SEPARATOR).try(:drop, 1).try(:join, self::TITLE_SEPARATOR)
      end
    end

    def self.get_item_url(item)
      if old_version?(item)
        url = item.css('div.s-item-container a.a-link-normal').first.try(:[], 'href')
        url.index(self::EXTRA_URL_INFO) ? url[0..(url.index(self::EXTRA_URL_INFO) - 1)] : url
      else
        url = item.css('a.a-link-normal').first.try(:[], 'href')
        unless url.nil?
          url = (url.index(self::EXTRA_URL_INFO) ? url[0..(url.index(self::EXTRA_URL_INFO) - 1)] : url)
          url.index(self::HTTPS_AMAZON) ? url : "#{self::HTTPS_AMAZON}#{url}"
        end
      end
    end

    def self.get_item_image_url(item)
      if old_version?(item)
        item.css('div.a-spacing-base img.s-access-image').first.try(:[], 'src')
      else
        item.css('img').first.try(:[], 'src')
      end
    end

    def self.get_item_price(item)
      if old_version?(item)
        # Utils.clear_price(get_item_prices(item).first&.try(:text)&.delete('.')&.delete('EUR'))
        Utils.clear_price(get_item_prices(item).first&.try(:text))
      else
        item.css('span.a-price[data-a-color=base] span.a-offscreen').text
      end
    end

    def self.get_item_page(item)
      if old_version?(item)
        get_item_prices(item).first&.parent&.parent&.at_css('i.a-icon-prime') ? origin_page : "#{origin_page}market"
      else
        item.css('i.a-icon-prime').blank? ? "#{origin_page}market" : origin_page
      end
    end

    def self.get_item_out_of_stock(item)
      item.at("span:contains('#{self::OUT_OF_STOCK_TEXT}')") ? true : false
    end

    def self.get_item_second_hand(item)
      get_item_prices(item).first&.parent&.try(:text)&.include?(self::SECOND_HAND_OFFER)
    end

    def self.get_item_sponsored(item)
      if old_version?(item)
        get_item_title(item).include?(self::SPONSORED_ITEM)
      else
        get_item_name(item).try(:include?, self::SPONSORED_ITEM)
      end
    end

    def self.get_item_asin(item)
      item['data-asin']
    end

    def self.get_item_valid_prices(item)
      valid_prices = []
      get_item_prices(item).each_with_index do |price, index|
        break if index > 1 ||
            (index == 1 &&
                (price.parent.try(:text).include?(self::SECOND_HAND_OFFER) ||
                 !price.try(:text).include?('EUR') ||
                 !item.css('div.s-item-container').text.include?(self::MORE_SALE_OPTIONS)))
        # valid_prices << Utils.clear_price(price.try(:text).delete('.').delete('EUR'))
        valid_prices << Utils.clear_price(price.try(:text))
        break if index == 0 && !price.parent.parent.at_css('i.a-icon-prime')
      end
      valid_prices
    end

    def self.get_item_with_variants(item)
      prices = item.css('div.a-spacing-base')
      if prices.blank?
        false
      else
        item.css('div.a-spacing-base').first.at("span.a-color-secondary:contains('#{self::OTHER_VARIANTS_TEXT}')") ? true : false
      end
    end

    def self.get_item_title(item)
      item.css('div.s-item-container h2.s-access-title').first.try(:text)
    end

    def self.get_title(doc)
      doc.css('span#productTitle').first.try(:text)
    end

    def self.get_item_prices(item)
      item.css('div.s-item-container span.a-color-price').css('span.a-text-bold')
    end

    def self.get_brand(doc)
      doc.css('a#bylineInfo').first.try(:text)
    end

    def self.get_name(doc)
      get_title(doc).split(self::TITLE_SEPARATOR)[0] if get_title(doc)
    end

    def self.get_description(doc)
      get_title(doc).split(self::TITLE_SEPARATOR).drop(1).join(self::TITLE_SEPARATOR) if get_title(doc)
    end

    def self.get_url(doc)
      doc.css('link[rel="canonical"]').first.try(:[], 'href')
    end

    def self.get_image_url(doc)
      doc.css('div#altImages li img').first.try(:[], 'src')
    end

    def self.get_page(doc)
      merchant_info = doc.css('div#merchant-info').first
      (merchant_info.try(:text).include?(self::PAGE_NAME) ? origin_page : "#{origin_page}market") if merchant_info
    end

    def self.get_out_of_stock(doc)
      get_item_out_of_stock(doc)
    end

    def self.get_price(doc)
      # doc.css('div#cerberus-data-metrics @data-asin-price').text
      price_element = doc.css('span#priceblock_ourprice').first || doc.css('span#priceblock_dealprice').first
      if price_element.nil? && get_page(doc) == "#{origin_page}market"
        price_element = doc.css('div#olp_feature_div span.a-color-price').first
        price_element = nil unless (price_element && price_element.parent.try(:text).include?(self::NEW_PRODUCT_ON_DETAIL))
      end
      # Utils.clear_price(price_element.try(:text).delete('.').delete('EUR')) if price_element
      Utils.clear_price(price_element.try(:text)) if price_element
    end

    def self.get_market_price(doc)
      price_element = doc.css("div#moreBuyingChoices_feature_div div.mbc-offer-row span.a-color-price").first
      # Utils.clear_price(price_element.try(:text).delete('.').delete('EUR')) if price_element
      Utils.clear_price(price_element.try(:text)) if price_element
    end

    def self.get_specs(doc)
      self.get_tabbed_specs(doc, 'div.col1 div.pdTab td.label', 'div.col1 div.pdTab td.value')
    end

    def self.get_vendor_part_number(doc)
      key = Nokogiri::HTML.parse self::VENDOR_PART_NUMBER_KEY
      get_specs(doc)[key.text] if get_specs(doc)
    end

    def self.get_variants(doc)
      variant_elements = doc.css('div#twisterContainer li.swatchAvailable') + doc.css('div#twisterContainer li.swatchUnavailable')
      variant_elements.map{ |variant| [(variant['data-defaultasin'].present? ? variant['data-defaultasin'] : variant['data-dp-url'].split('/')[2]),
                                       self::HTTPS_AMAZON+variant['data-dp-url']]}
    end

    def self.get_asin_variants(doc)
      get_variants(doc).map{ |variant| variant[0] }.uniq
    end
  end
end
