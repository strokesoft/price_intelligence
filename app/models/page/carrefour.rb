module Page
  class Carrefour < PageBase
    include ActiveModel::Model
    # include MechanizeDoc
    extend ActiveModel::Translation

    # VALIDATIONS --------------------------------------------------------------------------------------------------------

    # METHODS ------------------------------------------------------------------------------------------------------------
    # def self.getHeaders()
    #   {'Cookie' => 'JSESSIONID=....'}
    # end

    # def self.use_mechanize?
    #   true
    # end

    def self.get_items(doc)
      # doc.css('div.product-list div.item-container')
      # doc.css('div.product-card-list ul.product-card-list__list li.product-card-list__item')

      json = doc.to_html.try(:split, '"results":{"items":').try(:[], 1).try(:split, ',"offset"').try(:[], 0)
      (json.blank? || json == "null") ? [] : eval(json.gsub(':null', ':nil'))
    end

    def self.get_aditional_items_params(item_params, doc_item)
      return [] if doc_item[:seller_name] == 'Carrefour' # NOTE: 20191204: si el precio es de Carrefour, evitamos el scraper profundo porque si no es muy lento (https://gitlab.com/stramina/price_intelligence/issues/105)

      # if doc_item[:type] == 'Mix'
      #   new_item_params = item_params.clone
      #   new_item_params[:page]  = 'carrefourmarket'
      #   new_item_params[:price] = PageBase.check_param(:price, doc_item[:best_offer_price].gsub('.', ''))
      #   [new_item_params]
      if doc_item[:num_providers] && doc_item[:num_providers] > 1
        doc = get_document(get_item_url(doc_item))
        # seller_name = Utils.clear_text(doc.css('div.buybox__seller-name').text)
        #
        # doc.css('div.buybox__seller-name').map{ |div| Utils.clear_text(div.text) }
        # doc.css('span.buybox__price').map{ |div| Utils.clear_price(div.text) }
        # doc.css('span.buybox__price--current').map{ |div| Utils.clear_price(div.text) }
        #
        #
        # doc.css('div.mpoffer__seller-name').map{ |div| Utils.clear_text(div.text) }
        # doc.css('span.mpoffer__price').map{ |div| Utils.clear_price(div.text) }
        # doc.css('span.mpoffer__price').map{ |div| Utils.clear_price(div.text).to_f }.min
        #
        # doc.css('div.c4offer__seller-name').map{ |div| Utils.clear_text(div.text) }
        # doc.css('span.c4offer__price').map{ |div| Utils.clear_price(div.text) }

        unless doc.blank?
          new_item_params = item_params.clone
          case doc_item[:seller_name]
            when 'Carrefour'
              new_item_params[:page]  = 'carrefourmarket'
              new_item_params[:price] = doc.css('span.mpoffer__price').map{ |div| Utils.clear_price(div.text).to_f }.min.to_s
            else
              new_item_params[:price] = Utils.clear_price(doc.css('span.c4offer__price').text)
              unless new_item_params[:price].blank?
                new_item_params[:page]  = 'carrefour'
              else
                new_item_params = nil
              end
          end
          return [new_item_params] unless new_item_params.blank?
        end
      end
      []
    end

    def self.get_item_page(item)
      # case item[:type]
      case item[:seller_name]
        # when 'C4', 'Mix'
        when 'Carrefour'
          'carrefour'
        # when 'MP'
        #   'carrefourmarket'
        else
          # 'carrefour'
          ScrapingLog.error "********************
********************
En Page::Carrefour.get_item_page() hay un valor para seller_name distinto a Carrefour: [#{item[:type]}] pero best_offer_id indica ['c4'] y no debería ser. NOTA: hemos puesto 'carrefourmarket'. ********************
********************
********************" if item[:best_offer_id] == "c4"

          'carrefourmarket'
      end
    end

    def self.get_item_ean(item)
      # https://www.carrefour.es/placa-bosch-pkf631b17e/4242002726434/p
      # https://www.carrefour.es/PRODUCTO/REF(9) o EAN13/p
      # get_item_url(item).split('/')[-2]
      item[:ean]
    end

    def self.get_item_price(item)
      # text = Utils.clear_text item.css("span.new-price").first.try(:text).try(:delete, '€')
      # (text.split('.')[1] and text.split('.')[1].length > 2) ? text.try(:gsub, '.', '') : text #Carrefour have prices like: 1234,56, 1234.56, 1.234,56 ...
      item[:price].gsub('.', '')
    end

    def self.get_item_url(item)
      # data = Utils.clear_text item.css('a').first.try(:[], 'href')
      data = item[:url]
      "https://www.carrefour.es#{data}" if data
    end

    def self.get_item_name(item)
      # item.css('h2.product-title').text
      "#{item[:name]} (#{item[:product_id]})"
    end

    # def self.get_item_brand(item)
    #   nil
    # end

    def self.get_item_out_of_stock(item) # opción de usar: item['data-product-stock']: {stock-0: no stock} {}
      # (item.css("p.text-sold-out").count > 0 ? true : false)
      case item[:available]
        when true then
          false
        when nil then
          true
        else
          ScrapingLog.error "********************
********************
En Page::Carrefour.get_item_out_of_stock() hay un valor no contemplado: [#{item[:available]}] para [#{get_item_url(item)}]********************
********************
********************"
      end
    end

    def self.get_item_image_url(item)
      # data = Utils.clear_text item.css('img').first.try(:[], 'src')
      data = item[:images].try(:[], :desktop)
      "https:" + data if data
    end

    # def self.get_item_description(item)
    #   nil
    # end

    PAGE_NAME = "Carrefour".freeze

    private

    MAX_RETRIES = 3

    PRODUCTSxPAGE = 24

    # DEPARTMENTS = {
    #     Hobs:          '"https://www.carrefour.es/placas/cat5980052/c?No=#{(page_index-1)*self::PRODUCTSxPAGE}"',
    #     Dishwashers:   '"https://www.carrefour.es/lavavajillas/cat5980006/c?No=#{(page_index-1)*self::PRODUCTSxPAGE}"',
    #     Microwaves:    '"https://www.carrefour.es/microondas/cat5980056/c?No=#{(page_index-1)*self::PRODUCTSxPAGE}"',
    #     Ovens:         '"https://www.carrefour.es/hornos/cat5980054/c?No=#{(page_index-1)*self::PRODUCTSxPAGE}"',
    #     Refrigerators: '"https://www.carrefour.es/frigorificos/cat5980036/c?No=#{(page_index-1)*self::PRODUCTSxPAGE}"',
    #     Washers:       '"https://www.carrefour.es/lavadoras/cat5980022/c?No=#{(page_index-1)*self::PRODUCTSxPAGE}"',
    #     Laptops:       '"https://www.carrefour.es/portatiles/cat410364/c?No=#{(page_index-1)*self::PRODUCTSxPAGE}"'
    #     #       Integrables:
    # }

    def self.get_page(doc)
      case doc.text.split('productProvider').try(:[], 1).try(:split, '"').try(:[], 2)
        when 'carrefour', 'carrefour\\', nil
          return 'carrefour'
      end
      'carrefourmarket'
    end

    def self.get_price(doc)
      # #TODO: valorar coger otro precio en caso de out_of_stock como hace su propio listado (da el precio de otra tienda en lugar del propio)
      # ((doc.css('div#mainContent form#addToCart').text.include? 'AGOTADO' and doc.css('div#mainContent span.fuente-precio').count > 0) ?
      #    doc.css('div#mainContent span.fuente-precio').first.try(:text) :
      #    doc.css('div#mainContent span.new-price').first.try(:text)
      # ).try(:gsub, '.', '')
      doc.text.split('"price":"')[1].split('"')[0].try(:gsub, '.', '') if doc.text.split('"price":"')[1]
    end

    def self.get_ean_from_js(doc)
      begin
        cxt = V8::Context.new
        cxt.eval(doc.css('script')[1].content)
        JSON.parse(cxt.scope.pageModel.config.DataLayer)['productEAN']
      rescue => ex
        message = "#{self.name}: ERROR [ #{ex.class}: #{ex.message} ]"
        ScrapingLog.error message
        nil
      end
    end

    def self.get_ean(doc)
      doc.text.split('"productEAN":"')[1].split('"')[0] if doc.text.split('"productEAN":"')[1]
    end

    def self.get_brand(doc)
      get_specs(doc).try(:[], "MARCA") || doc.text.split('"productBrand":"')[1].split('"')[0] if doc.text.split('"productBrand":"')[1]
    end

    def self.get_name(doc)
      doc.text.split('"name":"')[1].split('"')[0] if doc.text.split('"name":"')[1] || doc.text.split('"productName":"')[1].split('"')[0] if doc.text.split('"productName":"')[1]
    end

    def self.get_specs(doc)
      groups = doc.css('div.product-details__feature-container p.product-details__section-title')
      tables = doc.css('div.product-details__feature-container dl')
      specs = {}
      tables.each_with_index do |table, index|
        specs_to_add = get_tabbed_specs(table, 'dt', 'dd', groups.try(:[], index).try(:text).try(:strip))
        specs = specs.merge specs_to_add if specs_to_add
      end
      specs if specs.size > 0
    end

    def self.get_image_url(doc)
      # data = Utils.clear_text doc.css('img#main-image__image').first.try(:[], 'src')
      # "https:" + data if data
      doc.css('img.main-image__image').first.try(:[], 'src')
    end

    def self.get_out_of_stock(doc)
      return false unless doc.css('div.buybox div.add-to-cart-button button.add-to-cart-button__button').blank?
      return true  unless doc.css('div.buybox div.add-to-cart-button p.add-to-cart-button__sold-out').blank?
    end

    def self.get_images_counter(doc)
      count = doc.css('ul.pics-slider__thumbnails li img').try(:count)
      count > 0 ? count : (get_image_url(doc).nil? ? nil : 1)
    end
  end
end
