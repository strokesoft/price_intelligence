module MechanizeDoc
  extend ActiveSupport::Concern

  class_methods do
    def getDoc(url)
      doc = nil
      ScrapingLog.info("*** USING: Mechanize - #{mechanize_agent.proxy_addr.blank? ? "without" : "with"} PROXY ***")
      attempts = 2
      force = false
      begin
        attempts -= 1
        mechanize_agent(page: self.name.split("::").last.downcase, force: force).get(url) do |page|
          page.encoding = 'utf-8'
          doc = Nokogiri::HTML::Document.parse(page.body)
        end
      rescue Mechanize::ResponseCodeError => ex
        raise ex
      rescue Mechanize::Error, Net::HTTP::Persistent::Error, Net::OpenTimeout, Net::HTTPFatalError => ex # Mechanize::ResponseCodeError
        ScrapingLog.error "ERROR en mechanize_doc: #{ex.class.name}: #{ex.message}. Intentos restantes [ #{attempts} ]"
        force  = true
        if attempts > 0
          sleep 1
          retry
        end
        raise ex
      end
      doc
    end
  end
end
