module AmazonTopSelleresConcern
  PRODUCTSxPAGE = 50

  # def self.use_mechanize?
  #   true
  # end
  #
  def get_article_number(doc)
    res = doc.css('.a-pagination li')[-2].text.to_i
    res.to_i*self::PRODUCTSxPAGE
  end

  def get_items(doc)
    doc.css('ol#zg-ordered-list li.zg-item-immersion')
  end

  def get_current_items_page(doc)
    Utils.clear_text(doc.css('ul.a-pagination li.a-selected')[0].try(:text)).to_i
  end

  def get_item_page(item)
    origin_page
  end

  # def get_item_ean(item)
  # end

  def get_item_asin(item)
    get_item_url(item).try(:split,'/').try(:[], -1)
  end

  def get_item_price(item)
    item.css('span.p13n-sc-price').try(:text)
  end

  def get_item_url(item)
    url = item.css('a.a-link-normal').first.try(:[], 'href')
    unless url.nil?
      url = (url.index(self::EXTRA_URL_INFO) ? url[0..(url.index(self::EXTRA_URL_INFO) - 1)] : url)
      url.index(self::HTTPS_AMAZON) ? url : "#{self::HTTPS_AMAZON}#{url}"
    end
  end

  def get_item_title(item)
    item.css('div.p13n-sc-truncate').try(:text)
  end

  def get_item_name(item)
    get_item_title(item).split(self::TITLE_SEPARATOR)[0]
  end

  def get_item_brand(item)
    # get_item_name(item).split(' ')[0]
    nil
  end

  def get_item_out_of_stock(item)
    nil
  end

  def get_item_image_url(item)
    item.css('img').first.try(:[], :src)
  end

  def get_item_customers_vote_average(item)
    item.css('div.a-icon-row a').first.try(:text).try(:split, ' ').try(:[], 0)
  end

  def get_item_customers_comments_count(item)
    item.css('div.a-icon-row a').last.try(:text).try(:split,' ').try(:[], 0).try(:delete, '.').try(:delete, ',')
  end

  def get_item_topseller_order(item)
    item.css('span.zg-badge-text').try(:text).delete('#').to_i
  end

  ####################


  def get_page(doc)
    origin_page
  end

  # def get_price(doc)
  #   Page::Amazon.get_price(doc)
  # end
  #
  # def get_url(doc)
  #   Page::Amazon.get_url(doc)
  # end
  #
  # def get_brand(doc)
  #   Page::Amazon.get_brand(doc)
  # end
  #
  # def get_name(doc)
  #   Page::Amazon.get_name(doc)
  # end
  #
  # def get_specs(doc)
  #   Page::Amazon.get_specs(doc)
  # end
  #
  # def get_image_url(doc)
  #   Page::Amazon.get_image_url(doc)
  # end
  #
  # def get_description(doc)
  #   Page::Amazon.get_description(doc)
  # end
  #
  # def get_image_url(doc)
  #   Page::Amazon.get_image_url(doc)
  # end
  #
  # def get_out_of_stock(doc)
  #   Page::Amazon.get_out_of_stock(doc)
  # end
  #
  # def get_market_price(doc)
  #   Page::Amazon.get_market_price(doc)
  # end
  #
  # def get_specs(doc)
  #   Page::Amazon.get_specs(doc)
  # end
  #
  # def get_vendor_part_number(doc)
  #   Page::Amazon.get_vendor_part_number(doc)
  # end
  #
  # def get_variants(doc)
  #   Page::Amazon.get_variants(doc)
  # end
  #
  # def get_asin_variants(doc)
  #   Page::Amazon.get_asin_variants(doc)
  # end
end
