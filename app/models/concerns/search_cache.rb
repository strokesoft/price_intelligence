module SearchCache
  extend ActiveSupport::Concern

  included do
    after_save :calculate_tsvector
  end

  def calculate_tsvector
    content_tsv = "#{searchable_values_sql}"
    unless content_tsv.blank?
      ActiveRecord::Base.connection.execute("UPDATE #{self.class.table_name} SET tsv = (#{content_tsv}) WHERE id = #{self.id}")
    end
  end

  private

  def searchable_values_sql
    searchable_values
      .select{ |k,_| k.present? }
      .collect{ |value, weight| set_tsvector(value, weight) }
      .join(" || ")
  end

  def set_tsvector(value, weight)
    "setweight(to_tsvector('english', unaccent(coalesce(#{quote(strip_html(value))}, ''))), #{quote(weight)})"
  end

  def quote(value)
    ActiveRecord::Base.connection.quote(value)
  end

  def strip_html(value)
    ActionController::Base.helpers.sanitize(value, tags: [])
  end
end
