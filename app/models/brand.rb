# == Schema Information
#
# Table name: brands
#
#  id           :integer          not null, primary key
#  name         :string           indexed
#  slug         :string           indexed
#  picture      :string
#  is_reference :boolean          default(FALSE)
#  color        :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_brands_on_name  (name)
#  index_brands_on_slug  (slug)
#

class Brand < ApplicationRecord
  extend FriendlyId
  mount_uploader :picture, PictureUploader
  friendly_id :name

  # Owning relationships
  has_many :society_brands, inverse_of: :brand, dependent: :destroy
  has_many :societies, through: :society_brands
  has_many :category_brands, inverse_of: :brand, dependent: :destroy
  has_many :categories, through: :category_brands

  # Validations
  validates :name, presence: true, uniqueness: true

  #Scopes
  scope :is_reference, ->(value = true)   { where(is_reference: value) }
  scope :is_reference_in, ->(society, value = true) {
    joins(:society_brands).where(society_brands: {society_id: society.id, is_reference: value}) unless society.has_all_brands?
  }
  scope :by_society, ->(society) {
    joins(:society_brands).where(society_brands: {society_id: society.id}) unless society.has_all_brands?
  }
  scope :by_user, ->(user) {
    by_society(user.society) unless user.has_all_brands?
  }

  # Callbacks
  after_save -> { Brand.clear_caching_of('calculations') }

  REFERENCE_BRANDS = {'AEG'     => 'brand1.jpg', 'Balay'      => 'brand2.jpg', 'Bosch'    => 'brand3.jpg',
                      'Candy'   => 'brand4.jpg', 'Electrolux' => 'brand5.jpg', 'Hotpoint' => 'brand6.jpg',
                      'Indesit' => 'brand7.jpg', 'Siemens'    => 'brand8.jpg', 'Teka'     => 'brand9.jpg'}.freeze
  class << self
    def images
      REFERENCE_BRANDS
    end

    # Brands reference by user or by default
    def references(source = nil)
      source = source.society if source.is_a?(User)
      if source.nil? || source.has_all_brands?
        Brand.is_reference.order(:name)
      else
        Brand.is_reference_in(source).order(:name)
      end
    end

    # Maping {name => image_url} of reference brands
    def references_with_images(source = nil)
      references(source).map{|b| [b.name, b.picture.url]}.to_h
    end

    # Maping {name => image_url} of available brands
    def availables_with_images(source = nil)
      source = source.society if source.is_a?(User)
      Brand.by_society(source).order(:name).map{|b| [b.name, b.picture.url]}.to_h
    end

    # Names of reference Brands
    def reference_names(source = nil)
      references(source).pluck(:name)
    end

    # Maping {slug => id} of brands
    def map_brand
      fetch_caching_of('calculations', 'map_brand') do
        Utils.scope_to_hash(Brand.all, :slug, :id, true)
      end
    end

    # Maping {names => color} of brands (it use setted colors to Brands, and set not used colors to not setted Brands)
    def map_brand_color
      fetch_caching_of('calculations', 'map_brand_color') do
        hash = Utils.scope_to_hash(Brand.all, :name, :color, true)
        available_colors = Utils.colors - hash.values.compact
        hash.keys.each{|key| hash[key] = available_colors.shift if hash[key].nil? }
        hash
      end
    end

    def total
      fetch_caching_of('calculations', 'brand_count', type: 'Integer') do
        Brand.count
      end
    end

  end
end