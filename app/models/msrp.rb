# == Schema Information
#
# Table name: msrps
#
#  id           :integer          not null, primary key
#  society_id   :integer          indexed, indexed => [trademark_id, product_id]
#  trademark_id :integer          indexed => [society_id, product_id], indexed
#  product_id   :integer          indexed, indexed => [society_id, trademark_id]
#  price        :decimal(, )
#  on_date      :date
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_msrps_on_product_id                                  (product_id)
#  index_msrps_on_society_id                                  (society_id)
#  index_msrps_on_society_id_and_trademark_id_and_product_id  (society_id,trademark_id,product_id) UNIQUE
#  index_msrps_on_trademark_id                                (trademark_id)
#

# Manufacturer's Suggested Retail Price
class Msrp < ApplicationRecord
  # Owning relationships
  belongs_to :society
  belongs_to :trademark
  belongs_to :product

  # Validations
  validates :on_date, presence: true
  validates :price, presence: true, uniqueness: { scope: [:society_id, :trademark_id, :product_id, :on_date]}
end
