class ContextFilter
  attr_reader :controller, :action, :filter

  def initialize(filter:, controller: nil, action: nil)
    @controller = controller
    @action = action
    @filter = filter
  end
end