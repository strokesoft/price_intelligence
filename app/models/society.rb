# == Schema Information
#
# Table name: societies
#
#  id                  :integer          not null, primary key
#  name                :string           indexed
#  slug                :string           indexed
#  picture             :string
#  has_all_ecommerces  :boolean          default(FALSE)
#  has_all_brands      :boolean          default(FALSE)
#  has_all_categories  :boolean          default(FALSE)
#  has_filter_by_specs :boolean          default(FALSE)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#
# Indexes
#
#  index_societies_on_name  (name)
#  index_societies_on_slug  (slug)
#

class Society < ApplicationRecord
  extend FriendlyId
  mount_uploader :picture, PictureUploader
  friendly_id :name

  # Owning relationships
  has_many :users, inverse_of: :society
  has_many :society_brands, inverse_of: :society, dependent: :destroy
  has_many :brands, through: :society_brands
  has_many :society_trademarks, inverse_of: :society, dependent: :destroy
  has_many :trademarks, through: :society_trademarks
  has_many :society_categories, inverse_of: :society, dependent: :destroy
  has_many :categories, through: :society_categories
  has_many :society_ecommerces, inverse_of: :society, dependent: :destroy
  has_many :ecommerces, through: :society_ecommerces

  # Validations
  validates :name, presence: true, uniqueness: true
  validates :acronym, presence: true, uniqueness: true
  validates :min_date, presence: true

  after_initialize do |society|
    society.min_date ||= Date.today if new_record?
  end

  def brand_names
    brands.pluck(:name)
  end

  def available(element_type)
    case element_type.to_sym
      when :ecommerces then
        Ecommerce.by_society(self)
      when :brands then
        Brand.by_society(self)
      when :categories then
        Category.usable.by_society(self)
      when :root_categories, :roots then
        Category.usable.roots.by_society(self)
      when :subcategories then
        Category.usable.subcategories.by_society(self)
    end
  end

  def available_elements_by(element_type, field:)
    Society.fetch_caching_of('calculations', "society-#{self.id}-available-elements-by-#{element_type}-#{field}") do
      available(element_type).order(:name).pluck(field)
    end
  end

  def available_names(element_type)
    available_elements_by(element_type, field: 'name')
  end

  def available_slugs(element_type)
    available_elements_by(element_type, field: 'slug')
  end

  def reference_ecommerces
    Society.fetch_caching_of('calculations', "society-#{self.id}-reference-ecommerces") do
      Ecommerce.references(self).order(:name).pluck(:slug)
    end
  end

  def reference_ecommerces_slug_and_name
    Society.fetch_caching_of('calculations', "society-#{self.id}-reference-ecommerces-slug-and-name") do
      Ecommerce.references(self).order(:name).pluck(:slug, :name).to_h
    end
  end

  def another_ecommerces
    Society.fetch_caching_of('calculations', "society-#{self.id}-another-ecommerces-false") do
      Ecommerce.is_reference_in(self, false).order(:name).pluck(:slug)
    end
  end

  def available_ecommerces
    Society.fetch_caching_of('calculations', "society-#{self.id}-available-ecommerces") do
      Ecommerce.by_society(self).order(:name).pluck(:slug, :name).to_h
    end
  end

  def available_root_categories
    Society.fetch_caching_of('calculations', "society-#{self.id}-available-root-categories") do
      available(:roots).order(:name).pluck(:name)
    end
  end

  def available_subcategories_for(category)
    ancestry = Category.map_category[category]['id']
    self.available(:categories).where(ancestry: ancestry).pluck(:name).sort
  end
end
