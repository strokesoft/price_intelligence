class SqlLog
  LOG_FILE = Rails.root.join('log', 'sql.log').freeze

  class << self
    cattr_accessor :logger
    delegate :debug, :info, :warn, :error, :fatal, :to => :logger
  end
end