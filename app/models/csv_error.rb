# == Schema Information
#
# Table name: csv_errors
#
#  id          :integer          not null, primary key
#  source_line :text
#  error       :string
#  mode        :string
#  parser      :string
#  category    :string
#  warn        :boolean
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  n           :integer
#  date        :date
#

class CsvError < ApplicationRecord

  scope :like_source_line,  -> (line)    { where('source_line LIKE ?', "%#{line}%") }
  scope :like_error,        -> (error)   { where('error LIKE ?', "%#{error}%") }
  scope :warn,              ->           { where('warn IS true' ) }
  scope :error,             ->           { where('warn IS false') }
  scope :by_category,       ->(category) { where(category: category)}
  scope :by_mode,           ->(mode)     { where(mode:     mode)}
  scope :by_parser,         ->(parser)   { where(parser:   parser)}

  def self.full_reprocess(error_class = nil)
    CSV.open("error_list.csv", "w", {headers: true, col_sep: ';', quote_char: '"'}) do |csv|
      csv << ['url', 'page', 'category']
      CsvError.all.each do |error|
        line     = error.source_line
        options  = Parsers::Fast.page_options(error.parser, nil)
        url      = eval(options[:url].strip)
        page     = error.parser
        category = error.category
        csv << [url, page, category]
      end
    end
  end

end
