include Bookland
require 'open-uri'

class PageBase
  extend Caching
  PAGES = Utils.available_pages
  SPECS_PAGES = PAGES - ["amazon", "amazonmarket", "carrefour", "carrefourmarket"]

  ATTEMPTS = 2
  MAX_RETRIES = 3
  PRODUCTSxPAGE = 1
  COUNTRY_CODE = 'XX'.freeze

  DEPARTMENTS = {}
  ALL_ATTRIBUTES = (Product::ATTRIBUTES + ProductPage::ATTRIBUTES + ProductPrice::ATTRIBUTES + TopSeller::ATTRIBUTES).uniq

  # def self.mechanize_agent(page: nil, force: false)
  #   if force && !@agent.nil?
  #     ScrapingLog.info("*** STOP MECHANIZE AGENT ***")
  #     @agent.shutdown
  #   end
  #
  #   if force || @agent.nil?
  #     ScrapingLog.info("*** NEW MECHANIZE AGENT (force: #{force}) ***")
  #
  #     @agent = Mechanize.new do |agent|
  #       page = page || self.name.split("::").last.downcase # revisar esto... debería venirnos siempre page, pero parece que a veces no viene, por eso lo buscamos.
  #       if Settings.env.proxy.enabled && Ecommerce.map_has_proxy[page]
  #         proxy_api_key = eval("Settings.env.proxy.#{Utils.country_code(page)}_proxy_api_key") || Settings.env.proxy.proxy_api_key
  #         ScrapingLog.info("*** AGENT CONFIGURED WITH PROXY *** [#{proxy_api_key}]")
  #         agent.set_proxy(Settings.env.proxy.proxy_host,
  #                         Settings.env.proxy.proxy_port,
  #                         proxy_api_key,
  #                         '')
  #       end
  #
  #       agent.log = Logger.new(Rails.root.join('log', 'mechanize.log'))
  #       agent.user_agent_alias = 'Linux Mozilla'
  #       agent.ignore_bad_chunking = true
  #     end
  #   else
  #     @agent
  #   end
  # end

  # Overwrite in each market that if you need to change it
  def self.main_article_number
    :ean
  end

  # Overwrite in each market that if you use it
  # def self.use_mechanize?
  #   false
  # end

  # Overwrite in each market when you need it
  def self.get_item_second_hand(item)
    false
  end

  # Overwrite in each market when you need it
  def self.get_item_from_marketplace(item)
    false
  end

  # Overwrite in each market that if you use it
  def self.get_item_valid_prices(item)
    []
  end

  # Overwrite in each market that if you use it
  def self.get_item_sponsored(item)
    false
  end

  # Overwrite in each market when you need it
  def self.get_item_with_variants(item)
    false
  end

  # Overwrite in each market
  def self.break_less_than_per_page?
    true
  end

  # Overwrite in each market
  def self.get_market_price(doc)
    nil
  end

  def self.scrapp(deep_scrapp = true)
    scrapp_pages(PAGES - ["fnac", "playstationstore"])

    thread = Thread.new do
      scrapp_pages(["fnac", "playstationstore"])
      ActiveRecord::Base.connection.close
    end

    attempts = 3
    while attempts > 0 && (page_departments = PageDepartment.where("pages_max > pages_last")).any? # where("pages_max = 0 OR pages_max > pages_last")
      attempts -= 1
      scrapp_page_departments(page_departments, true)
    end

    if (deep_scrapp)
      # retrieve_info_when_lost_specs
      retrieve_info_when_lost_out_of_stock
      Product.fill_specs(true)
    end
  end

  # Use Example: PageBase.deep_scrapp_by({page: [:eci, :worten], category: ["Dishwashers"]}, ["images_counter"])
  def self.deep_scrapp_by(params, updatable_params)
    start_at = Time.now
    ScrapingLog.info "************************ START DEEP SCRAPING AT #{start_at} - params: [#{params}] - updatable_params: [#{updatable_params}]"
    product_pages = ProductPage.usable
    params.each do |param, values|
      unless values.blank?
        values = Utils.to_array(values).map {|value| get_class(value).try(:origin_page) }.uniq if param.to_sym == :page
        product_pages = product_pages.by_param(param, values)
      end
    end
    ScrapingLog.info "******** deep_scrapp_by - product_pages to update: [#{product_pages.count}]"

    product_pages.each { |product_page| update_info_for(product_page, updatable_params)}
    Product.fill_specs(true) if updatable_params.nil? || params.member?(:specs)

    end_at = Time.now
    ScrapingLog.info("************************ END DEEP SCRAPING AT #{end_at}. Duration: #{end_at - start_at} seconds")
    product_pages.count
  end

  # def self.deep_scrapp(updatable_params = nil)
  #   ProductPage.usable.each { |product_page| update_info_for(product_page, updatable_params)}
  #   Product.fill_specs(true) if updatable_params.nil? || params.member?(:specs)
  # end

  # Use Example: PageBase.scrapp_by([[:page, [:eci, :worten]], [:subcategory, 'Top Loading, Front Loading']])
  def self.scrapp_by(from_last_page, params)
    ScrapingLog.debug "Comenzar en la última página descargada?: #{from_last_page}"
    page_departments = PageDepartment.order(order: :asc)
    params.each do |param, values|
      unless values.blank?
        values = Utils.to_array(values).map {|value| get_class(value).try(:origin_page) }.uniq if param.to_sym == :page
        page_departments = page_departments.by_param(param, values)
      end
    end
    ScrapingLog.debug "URLs encontradas: #{page_departments.size}"
    scrapp_page_departments(page_departments, from_last_page)
  end

  def self.scrapp_pages(values)
    values = Utils.to_array(values).map {|value| get_class(value).try(:origin_page) }.uniq
    scrapp_page_departments(PageDepartment.order(order: :asc).by_param(:page, values))
  end

  def self.scrapp_categories(values)
    scrapp_page_departments(PageDepartment.order(order: :asc).by_param(:category, values))
  end

  def self.scrapp_subcategories(values)
    scrapp_page_departments(PageDepartment.order(order: :asc).by_param(:subcategory, values))
  end

  def self.scrapp_page_departments(page_departments, from_last_page = false)
    start_at = Time.now
    ScrapingLog.info("************************ START SCRAPING AT #{start_at}")
    results = []
    page_departments.each { |page_department| results << scrapp_page_department(page_department, (from_last_page ? page_department.pages_last : 1))}
    end_at = Time.now
    ScrapingLog.info("************************ END SCRAPING AT #{end_at}. Duration: #{end_at - start_at} seconds")
    clear_caching_of('calculations')
    return results
  end

  def self.valid_document(doc)
    nil
  end

  def self.finish_scrap?(doc, page_index)
    page_index > 1 && [nil, page_index].exclude?(get_current_items_page(doc))
  end

  def self.scrapp_page_department(page_department, page_index = 1)
    # ScrapingLog.info "\n\n\n\n************************ #{self} #{self.class} - [#{page_index}] - [#{get_class(page_department.page)}] [#{get_class(page_department.page).class}] ************************"
    return get_class(page_department.page).scrapp_page_department(page_department, page_index) if self != get_class(page_department.page)

    total_pages = -1
    items_counter = 0
    product_prices_counter = 0
    variants_array = []
    begin
      ScrapingLog.info "\n\n\n\n************************ #{self} #{self.class} SCRAPING: [#{page_department.page}] [#{page_department.category}] [#{page_department.subcategory}] [#{page_department.url_template}] start at page: [#{page_index}] ************************"

      # Instantiate new agent of Mechanize
      # mechanize_agent(page: page_department.page, force: true) if use_mechanize?

      loop do
        attempts = self::ATTEMPTS
        begin
          attempts -= 1
          doc = get_document(build_url(page_department.url_template, page_index), true)

          if doc.nil?
            ScrapingLog.error "#{self.name}: ERROR No se ha podido descargar: [ #{eval(page_department.url_template)} ]"
            break
          # else
          #   File.write("#{DateTime.now}_Downloaded.html", doc.to_html)
          end
          # ScrapingLog.info "DOCUMENTO: #{doc.to_s.truncate(100)}"

          break if finish_scrap?(doc, page_index)# Break if we aren't on expected page number (not check on first page). IMPORTANT!!!! Don't add '0' do valid options!!! if you do it, some cases will fail: pages witch exactly PRODUCTSxPAGE products listed: then no pagination and no less products than expected, and then no break condition
          valid_document(doc)
          # items = get_products_info_from_list(doc)
          items = parse_products_list(doc)
          total_pages = get_total_pages(doc) || -1
        rescue SyntaxError, NameError, RuntimeError => ex
          # Esto puede ocurrir en el 'eval' cuando el doc esperamos que sea un json a parsear y nos llega, por ejemplo, un html (como en Game)
          message = "#{self.name}: ERROR [ #{ex.class}: #{ex.message} ]"
          ScrapingLog.error message
          unless ex.instance_of? RuntimeError
            ScrapingLog.error "Backtrace:\n\t#{ex.backtrace.join("\n\t")}"
            CsvError.create({source_line: doc.to_s, error: message, mode: 'PageBase.scrapp_page_department', parser: "", category: "", date: Date.today, n: -1 })
          end
          if attempts > 0
            ScrapingLog.info "Reintentamos la descarga tras un SyntaxError. Intentos restantes [#{attempts}]"
            sleep 2
            retry
          else
            if (total_pages > page_index)
              ScrapingLog.info "Aunque está habiendo algún tipo de problema, hemos encontrado que hay [#{total_pages}] pero vamos por la [#{page_index}]. Probamos con la siguiente página."
              page_index += 1
              retry
            else
              items = []
            end
          end
        end


        items.each do |item|
          item.store(:category,    page_department.category)    unless page_department.category.blank? || page_department.category == '*EXTRAER*'
          item.store(:subcategory, page_department.subcategory) unless page_department.subcategory.blank?
          item.store(:last_aparition, Date.today)
          item.store(:date, Date.today)
        end

        process_info = self.process_products_info(items, items_counter)
        product_prices_counter += process_info[0]
        variants_array += process_info[1]

        items_counter += items.count
        break if break_less_than_per_page? && (items.count < self::PRODUCTSxPAGE || page_department.url_template.exclude?("page_index"))
        page_index += 1
      end

      product_variants_counter = process_variants_array(page_department, variants_array.uniq, items_counter)

      ScrapingLog.info "Obtenidos total: #{items_counter} - válidos: [#{product_prices_counter}] --- [#{page_department.page}] [#{page_department.category}] [#{page_department.subcategory}] [#{page_department.url_template}]"
      ScrapingLog.info "Total variantes procesadas: #{product_variants_counter} --- [#{page_department.page}] [#{page_department.category}] [#{page_department.subcategory}]" if product_variants_counter > 0

      raise "WARN: Se han descargado [#{items_counter}] productos, el máximo por página. Podemos estar perdiendo productos en la plantilla sin paginación: [#{page_department.url_template}]" if items_counter == self::PRODUCTSxPAGE && page_department.url_template.exclude?("page_index")

    rescue Exception => ex
      ScrapingLog.error "#{self.name}: ERROR no controlado [ #{ex.class}: #{ex.message} ]"
      ScrapingLog.error "Backtrace:\n\t#{ex.backtrace.join("\n\t")}"
    end

    page_department.pages_max            = page_index    if page_index > page_department.pages_max
    page_department.pages_last           = page_index
    page_department.products_listed_max  = items_counter if items_counter > page_department.products_listed_max
    page_department.products_listed_last = items_counter
    page_department.products_valid_last  = product_prices_counter
    page_department.save!

    {page: page_department.page, category: page_department.category, subcategory: page_department.subcategory,
     url_template: page_department.url_template, listed_items: items_counter, procesed_items: product_prices_counter}
  end

  def self.get_article_number(doc)
    nil # Overwrite if is used scrapp_page_department_with_article_number
  end

  def self.scrapp_page_department_with_article_number(page_department, page_index = 1)
    # ScrapingLog.info "\n\n\n\n************************ #{self} #{self.class} - [#{page_index}] - [#{get_class(page_department.page)}] [#{get_class(page_department.page).class}] ************************"
    return get_class(page_department.page).scrapp_page_department(page_department, page_index) if self != get_class(page_department.page)

    total_pages = -1
    items_counter = 0
    product_prices_counter = 0
    variants_array = []
    article_number = nil
    begin
      ScrapingLog.info "\n\n\n\n************************ #{self} #{self.class} SCRAPING: [#{page_department.page}] [#{page_department.category}] [#{page_department.subcategory}] [#{page_department.url_template}] start at page: [#{page_index}] ************************"

      # Instantiate new agent of Mechanize
      # mechanize_agent(page: page_department.page, force: true) if use_mechanize?

      loop do
        attempts = self::ATTEMPTS
        begin
          attempts -= 1
          doc = get_document(build_url(page_department.url_template, page_index), true)

          article_number = get_article_number(doc)

          if doc.nil?
            ScrapingLog.error "#{self.name}: ERROR No se ha podido descargar: [ #{eval(page_department.url_template)} ]"
            break
          end

          #valid_document(doc)
          items = parse_products_list(doc)
          total_pages = get_total_pages(doc) || -1
        rescue SyntaxError, NameError => ex
          # Esto puede ocurrir en el 'eval' cuando el doc esperamos que sea un json a parsear y nos llega, por ejemplo, un html (como en Game)
          message = "#{self.name}: ERROR [ #{ex.class}: #{ex.message} ]"
          ScrapingLog.error message
          ScrapingLog.error "Backtrace:\n\t#{ex.backtrace.join("\n\t")}"
          CsvError.create({source_line: doc.to_s, error: message, mode: 'PageBase.scrapp_page_department', parser: "", category: "", date: Date.today, n: -1 })
          if attempts > 0
            ScrapingLog.info "Reintentamos la descarga tras un SyntaxError. Intentos restantes [#{attempts}]"
            sleep 5
            retry
          else
            if (total_pages > page_index)
              ScrapingLog.info "Aunque está habiendo algún tipo de problema, hemos encontrado que hay [#{total_pages}] pero vamos por la [#{page_index}]. Probamos con la siguiente página."
              page_index += 1
              retry
            else
              items = []
            end
          end
        end


        items.each do |item|
          item.store(:category,    page_department.category)    unless page_department.category.blank? || page_department.category == '*EXTRAER*'
          item.store(:subcategory, page_department.subcategory) unless page_department.subcategory.blank?
          item.store(:last_aparition, Date.today)
          item.store(:date, Date.today)
        end

        process_info = self.process_products_info(items, items_counter)
        product_prices_counter += process_info[0]
        variants_array += process_info[1]

        items_counter += items.count
        # break if break_less_than_per_page? && (items.count < self::PRODUCTSxPAGE || page_department.url_template.exclude?("page_index"))
        break if (article_number/self::PRODUCTSxPAGE.to_f).ceil <= page_index
        page_index += 1

      end

      product_variants_counter = process_variants_array(page_department, variants_array.uniq, items_counter)

      ScrapingLog.info "Obtenidos total: #{items_counter} - válidos: [#{product_prices_counter}] --- [#{page_department.page}] [#{page_department.category}] [#{page_department.subcategory}] [#{page_department.url_template}]"
      ScrapingLog.info "Total variantes procesadas: #{product_variants_counter} --- [#{page_department.page}] [#{page_department.category}] [#{page_department.subcategory}]" if product_variants_counter > 0

      raise "WARN: Se han descargado [#{items_counter}] productos, el máximo por página. Podemos estar perdiendo productos en la plantilla sin paginación: [#{page_department.url_template}]" if items_counter == self::PRODUCTSxPAGE && page_department.url_template.exclude?("page_index")

    rescue Exception => ex
      ScrapingLog.error "#{self.name}: ERROR no controlado [ #{ex.class}: #{ex.message} ]"
      ScrapingLog.error "Backtrace:\n\t#{ex.backtrace.join("\n\t")}"
    end

    page_department.pages_max            = page_index    if page_index > page_department.pages_max
    page_department.pages_last           = page_index
    page_department.products_listed_max  = items_counter if items_counter > page_department.products_listed_max
    page_department.products_listed_last = items_counter
    page_department.products_valid_last  = product_prices_counter
    page_department.save!

    {page: page_department.page, category: page_department.category, subcategory: page_department.subcategory,
     url_template: page_department.url_template, listed_items: items_counter, procesed_items: product_prices_counter}
  end

  def self.process_variants_array(page_department, variants_array, offset)
    product_variants_counter = 0
    if variants_array.size > 0
      ScrapingLog.info "Procesando variantes para: [#{page_department.page}] [#{page_department.category}] [#{page_department.subcategory}]"
      variants_array.each_with_index do |variant, i|
        unless ProductPage.by_page(page_department.page).by_asin(variant[0]).by_date(Date.today).no_variant.present?
          i += offset
          ScrapingLog.info "Procesando variante con ASIN: #{variant[0]} url: [#{variant[1]}]"
          variant_params = self.get_params(variant[1], with_variants: true).
            merge({ asin: variant[0], category: page_department.category, subcategory: page_department.subcategory,
                    order: (i + 1), last_aparition: Date.today, date: Date.today, variant: true })
          product_variants_counter +=1 if process_product_info(variant_params).instance_of? ProductPrice

          if variant_params[:page] == page_department.page && variant_params[:market_price].present?
            variant_params[:page] = "#{variant_params[:page]}market"
            variant_params[:price] = variant_params[:market_price]
            product_variants_counter +=1 if process_product_info(variant_params).instance_of? ProductPrice
          end
          if variant_params[:variants].present? && variant_params[:variants].size > 0
            variant_params[:variants].each do |new_variant|
              variants_array << new_variant unless variants_array.map{ |old_variant| old_variant[0] }.include? new_variant[0]
            end
          end
        else
          ScrapingLog.info "Variante con ASIN: #{variant[0]} procesada como producto principal"
        end
      end
    end
    product_variants_counter
  end

  def self.build_url(url_template, page_index)
    eval(url_template)
  end

  # def self.get_products_info_from_list(doc)
  #   items = nil
  #   downloads_count = 1
  #   loop do
  #     items = parse_products_list(doc)
  #     break if items.count > 0 || downloads_count >= self::MAX_RETRIES
  #     downloads_count += 1
  #     ScrapingLog.info "No se han obtenido items: reintentento #{downloads_count} de #{self::MAX_RETRIES} dentro de #{downloads_count} segundos."
  #     sleep downloads_count
  #   end
  #   ScrapingLog.info "Obtenidos: #{items.count} tras #{downloads_count} intentos"
  #   items
  # end

  def self.parse_products_list(doc)
    list    = []
    begin
      items = get_items(doc)
      # File.write("#{DateTime.now}_items_0.html", doc.to_html) if items.count == 0
      items.each do |item|
        params = get_item_params(item)
        unless params[:url].nil?
          list << params
          list << clone_item_params_to_page_market(list.last, item) if get_item_valid_prices(item).size > 1 # TODO: 20190306: Esto es solo para Amazon y es un poco raro... Diría que convendría revisarlo y en su caso unirlo con el método siguiente
          list += get_aditional_items_params(params, item)
        end
      end
    rescue => ex
      message = "#{self.name}: ERROR [ #{ex.class}: #{ex.message} ]"

      ScrapingLog.error message
      ScrapingLog.error "Backtrace:\n\t#{ex.backtrace.join("\n\t")}"

      CsvError.create({source_line: list.to_s, error: message, mode: 'PageBase.parse_products_list', parser: "", category: "", date: Date.today, n: -1 })
    end
    ScrapingLog.info "Obtenidos: #{list.count} items"
    list
  end

  def self.get_items(doc) # it must be overwrited by each Page::Class
    []
  end

  def self.get_current_items_page(doc) # it must be overwrited by each Page::Class
    nil
  end

  def self.get_total_pages(doc) # it must be overwrited by each Page::Class
    nil
  end

  def self.get_item_params(item)
    params = {}
    ALL_ATTRIBUTES.each do |param|
      value = get_item_param(param, item)
      params[param] = value if value != nil
    end
    params
  end

  def self.get_item_param(param, item)
    check_param(param, self.try("get_item_#{param}", item))
  end

  def self.get_aditional_items_params(item_params, doc_item)
    []
  end

  def self.clone_item_params_to_page_market(item_params, doc_item)
    new_item_params = item_params.clone
    new_item_params[:page] = "#{item_params[:page]}market"
    new_item_params[:price] = get_item_valid_prices(doc_item)[1]
    new_item_params
  end

  def self.process_products_info(list, offset = 0)
    product_prices_count = 0
    variants_array = []
    list.each_with_index do |params, i|
      i += offset
      params.store(:order, i + 1)
      product_article_number = ProductPage.by_url(params[:url]).last.try(:product).try(main_article_number)
      params.store(main_article_number, product_article_number) if params[main_article_number] == nil && product_article_number

      if params[main_article_number] == nil && self.respond_to?("get_#{main_article_number}") && processable_params?(params)
        params = params.merge(self.get_params(params[:url]))
      end

      if params[:with_variants] == true && processable_params?(params)
        page_variants = retrieve_variants(params[:url])
        variants_array += page_variants unless page_variants.nil?
      end

      product_prices_count +=1 if process_product_info(params).instance_of? ProductPrice
    end
    [product_prices_count, variants_array]
  end

  def self.process_product_info(params) # it will return less level object retrieve
    object_created = nil
    begin
      product = nil
      if params[main_article_number] && processable_params?(params)
        product = Product.try("by_#{main_article_number}", params[main_article_number]).last
        if product
          Product::POSIBLE_ATTRIBUTES_IN_PAGE.each { |attribute| eval("product.#{attribute} ||= params[:#{attribute}]") if params.include? attribute }
          product.save!
        elsif main_article_number.equal? :ean
          product = Product.create!(params.select { |key, value| Product::POSIBLE_ATTRIBUTES_IN_PAGE.include? key})
        end
        params.store(:product, product) if product
        object_created = product
      end

      if params[:url] && processable_params?(params)
        params[:foul_name] ||= params[:name] #TODO: Cambiar foul_name por name para Product Pages (necesario sobre todo para los casos de Lost Product)
        params[:foul_brand] ||= params[:brand]
        params[:foul_description] ||= params[:description]
        product_page = ProductPage.by_page(params[:page]).by_url(params[:url]).last
        if product_page
          product_page.url    = params[:url]    unless params[:url].blank?    # tras implementar el métoco ProductPage.equivalent_urls, la URL podría actualizarse y queremos forzarlo.
          product_page.page   = params[:page]   unless params[:page].blank?   # tras implementar la separación de Carrefour/CarrefourMarket, el page podría actualizarse y queremos forzarlo.
          ProductPage::ATTRIBUTES.each { |attribute| eval("product_page.#{attribute} = params[:#{attribute}]") if params.include? attribute }
          product_page.save!

          unless product
            if (product = product_page.product)
              Product::POSIBLE_ATTRIBUTES_IN_PAGE.each { |attribute| eval("product.#{attribute} ||= params[:#{attribute}]") if params.include? attribute }
              product.save!
            end
          end
        else
          product_page = ProductPage.create!(params.select { |key, value| ProductPage::ATTRIBUTES.include? key})
        end
        params.store(:product_page, product_page) if product_page
        object_created = product_page

        if product
          product.brand       ||= product_page.foul_brand
          product.name        ||= product_page.foul_name
          product.description ||= product_page.foul_description
          product.category    ||= product_page.category
          product.description ||= product_page.foul_name
        end

        unless params[:topseller_order].blank?
          top_seller = TopSeller.joins(:product_page).where(product_pages: {page: params[:page]}).where(product_pages: {category: params[:category]}).where(topseller_order: params[:topseller_order]).where(date: params[:date]).order(:updated_at).last
          if top_seller
            TopSeller::ATTRIBUTES.each { |attribute| eval("top_seller.#{attribute} = params[:#{attribute}]") if params.include? attribute }
            top_seller.save!
          else
            TopSeller.create!(params.select { |key, value| TopSeller::ATTRIBUTES.include? key})
          end
        end

        unless params[:price].blank?
          product_price = ProductPrice.where(product_page: product_page).where(date: params[:date]).order(:updated_at).last
          if product_price
            ProductPrice::ATTRIBUTES.each { |attribute| eval("product_price.#{attribute} = params[:#{attribute}]") if params.include? attribute }
            product_price.save!
          else
            product_price = ProductPrice.create!(params.select { |key, value| ProductPrice::ATTRIBUTES.include? key})
          end
          object_created = product_price
        end
      end
    rescue => ex
      message = "ERROR [ #{ex.class}: #{ex.message} ]"
      ScrapingLog.error message
      ScrapingLog.error "Backtrace:\n\t#{ex.backtrace.join("\n\t")}"
      CsvError.create({source_line: params.to_s, error: message, mode: 'PageBase.process_product_info', parser: "", category: "", date: Date.today, n: -1 })
    end
    object_created
  end

  def self.processable_params?(params)
    !params[:second_hand] && !params[:sponsored] && !params[:from_marketplace] && params[:price].present?
  end

  # doc is used in special cases as Carrefour/Carrefourmarket (diferent markets on same URL)
  def self.get_item_page(item)
    get_page(item)
  end

  # doc is used in special cases as Carrefour/Carrefourmarket (diferent markets on same URL)
  def self.get_page(doc)
    self.name.split("::")[1].try(:downcase)
  end

  # needed for special cases as Carrefour/Carrefourmarket or Amazon/Amazon Market (diferent markets on same URL)
  def self.origin_page
    self.name.split("::")[1].try(:downcase)
  end

  def self.get_class(page)
    page = page.page if page.instance_of? ProductPage
    "Page::#{page.capitalize}".safe_constantize
  end

  def self.clean_database
    duplicate_product_prices = ProductPrice.select(:product_page_id, :date).where.not(product_page_id: nil).where(date: Date.today).group(:product_page_id, :date).having("count(*) > 1")
    ScrapingLog.info "Encontrados [#{duplicate_product_prices.length} product_prices duplicados"
    duplicate_product_prices.each { |pp| remove_duplicate_prices(pp) }
  end

  def self.remove_duplicate_prices(product_price)
    ProductPrice.where(product_page_id: product_price.product_page_id, date: product_price.date)
  end

  def self.retrieve_lost_information
    retrieve_lost_products
    retrieve_info_when_lost_specs
    retrieve_info_when_lost_out_of_stock
  end

  def self.retrieve_info_when_lost_out_of_stock
    ProductPrice.where(out_of_stock: nil).where(date: Date.today).each { |product_price| update_info_for(product_price) if get_class(product_price.product_page).respond_to?("get_out_of_stock")}
  end

  def self.retrieve_info_when_lost_specs
    Ecommerce.find_by_slug('mediamarkt').update_attribute(:has_proxy, true)
    # mechanize_agent(page: 'mediamarkt', force: true)
    ProductPage.where(page: 'mediamarkt').where(usable: true).where(specs: nil).each { |product_page| update_info_for(product_page) if get_class(product_page).respond_to?("get_specs")}
    Ecommerce.find_by_slug('mediamarkt').update_attribute(:has_proxy, false)

    ProductPage.where(page: (SPECS_PAGES - ['mediamarkt'])).where(usable: true).where(specs: nil).each { |product_page| update_info_for(product_page) if get_class(product_page).respond_to?("get_specs")}
  end

  def self.update_info_for(object, updatable_params = nil)
    updatable_params = [updatable_params] unless updatable_params.nil? || updatable_params.instance_of?(Array)
    product_price = nil
    product_page  = nil
    case object
      when ProductPrice
        product_price = object
        product_page = product_price.product_page
      when ProductPage
        product_page = object
    end

    if product_page.try('usable')
      begin
        params = get_class(product_page.page).get_params(product_page)
        ScrapingLog.debug "Nuevos parámetros para [#{product_page.url}]: [#{params}]"
        if params.except(:page, :url).size == 0 # Siempre existe al menos :page y :url nos da igual porque no lo vamos a actualizar
          ScrapingLog.warn "WARN: ProductPage posiblemente no usable: [#{product_page.url}], comprobar"
          # product_page.usable = false
          # product_page.save!
          product_page.update notes: "WARNING: COMPROBAR SI NO-USABLE: No se han obtenido parámetros de esta URL."
        else
          if product_price
            (updatable_params || ProductPrice::ATTRIBUTES).each { |attribute| eval("product_price.#{attribute} = params[:#{attribute}]") if params[attribute] }
            product_price.save!
          end
          # No queremos actualizar la URL ya que es el identificador de este objeto y no tiene sentido actualizarla.
          (updatable_params || (ProductPage::ATTRIBUTES - [:url])).each { |attribute| eval("product_page.#{attribute} ||= params[:#{attribute}]")}
          product_page.save!
        end
      rescue => ex
        message = "ERROR [ #{ex.class}: #{ex.message} ]"
        ScrapingLog.error message
        ScrapingLog.error "Backtrace:\n\t#{ex.backtrace.join("\n\t")}"

        CsvError.create({source_line: product_page.try('url'), error: message, mode: 'PageBase.update_info_for', parser: "", category: "", date: Date.today, n: -1 })
      end
    end
  end

  def self.retrieve_lost_products
    ProductPage.where(usable: true).where(product_id: nil).each { |product_page| retrieve_lost_product(product_page) if get_class(product_page).respond_to?("get_ean") }
  end

  def self.retrieve_lost_product(product_page)
    begin
      params = PageBase.get_params(product_page)

      if params.size == 0
        ScrapingLog.debug "ProductPage no usable: [#{product_page.url}]"
        product_page.usable = false
        product_page.save!
        return
      end

      params.store(:url, product_page.url)
      params.store(:category, product_page.category)
      params.store(:last_aparition, Date.today)
      params.store(:usable, true)
      params.store(:date, Date.today)

      process_product_info(params)

    rescue => ex
      message = "ERROR [ #{ex.class}: #{ex.message} ]"
      ScrapingLog.error message
      ScrapingLog.error "Backtrace:\n\t#{ex.backtrace.join("\n\t")}"
      CsvError.create({source_line: product_page.url, error: message, mode: 'PageBase.retrieve_lost_product', parser: "", category: "", date: Date.today, n: -1 })
    end
  end

  def self.get_params(source, with_variants: false)
    params = {}
    case source
      when String
        source = get_document(source)
      when ProductPage
        page_class = get_class(source)
        source = get_document(source.url)
    end
    # if [Mechanize::Page, Nokogiri::HTML::Document].include? source.class
    if [Nokogiri::HTML::Document].include? source.class
      ALL_ATTRIBUTES.each do |param|
        value = (page_class ? page_class : self).try("get_param", param, source)
        params[param] = value if value != nil
      end
      if with_variants
        params[:variants] = get_variants(source)
      end
    end
    params
  end

  def self.get_param(param, source) # It must be rewritten for each page in witch we can find the ean
    case source
      when String
        source = get_document(source)
      when ProductPage
        page_class = get_class(source)
        source = get_document(source.url)
    end
    # if [Mechanize::Page, Nokogiri::HTML::Document].include? source.class
    if [Nokogiri::HTML::Document].include? source.class
      begin
        check_param(param, (page_class ? page_class : self).try("get_#{param}", source))
      rescue => e
        ScrapingLog.error "ERROR obteniendo parámetro [#{param}] - [ #{e.class}: #{e.message} ] - source:\n
----------------------------------------------\n\n#{source}\n\n----------------------------------------------"
        ScrapingLog.error "Backtrace:\n\t#{e.backtrace.join("\n\t")}"
        # raise e
      end
    end
  end

  def self.check_param(param, value)
    case param
      when  :ean
        value = Utils.clear_text value
        value = nil unless EAN.valid?(value)
      when :specs
        value = nil if value.try('size') == 0
      when :out_of_stock, :usable, :second_hand, :sponsored, :from_marketplace, :with_variants
        value = nil if !!value != value
      when :price, :customers_vote_average
        #TODO: Check Number/Float?
        value = Utils.clear_price value
        value = nil if value == ""
      when :images_counter, :comments_count
        value = nil unless value.instance_of?(Integer) and value >= 0
      when :topseller_order
        value = nil unless value.instance_of?(Integer) and value > 0
      else
        value = Utils.clear_text value
        value = nil if value == ""
    end
    value
  end

  def self.retrieve_variants(source_url)
    source = get_document(source_url)

    if source.nil?
      ScrapingLog.error "ERROR No se ha podido descargar: #{self.class}.retrieve_variants('#{source_url}')"
      return nil
    end

    get_variants(source)
  end

  def self.get_tabbed_specs(doc, names_css, values_css, group = nil)
    specs_names    = doc.css(names_css).map{|name| name.text.strip}
    specs_values   = doc.css(values_css).map{|value| value.text.strip}
    specs         = {}
    specs_names.each_with_index do |name, index|
      specs[(group ? "#{Utils.clear_text group} - " : "") + (Utils.clear_text name)] = Utils.clear_text specs_values[index]
    end
    specs if specs.size > 0
  end

  def self.page
    name.downcase.split('::').last
  end

  def self.proxy?
    Settings.env.proxy.enabled && Ecommerce.map_has_proxy[page]
  end

  def self.proxy_address
    Settings.env.proxy.enabled && Ecommerce.find_by_slug(page).proxy_address
  end

  def self.proxy_user
    Settings.env.proxy.enabled && Ecommerce.find_by_slug(page).proxy_user || ""
  end

  def self.proxy_password
    Settings.env.proxy.enabled && Ecommerce.find_by_slug(page).proxy_password || ""
  end

  def self.selenium_proxy?
    Ecommerce.find_by_slug(page).selenium_proxy
  end

  # Overwrite method in each page class that needs it
  def self.getHeaders()
    headers = {allow_redirections: :all, # for 'open_uri_redirections': This gem applies a patch to OpenURI to optionally allow redirections from HTTP to HTTPS, or from HTTPS to HTTP.
               'User-Agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36',
               'Connection' => 'keep-alive'}

    if proxy?
      if proxy_address.present?
        ScrapingLog.info("*** OpenUri CONFIGURED WITH CUSTOM PROXY Header *** [#{proxy_address}]")
        headers = headers.merge({proxy_http_basic_authentication: [proxy_address, proxy_user, proxy_password]})
      else
        proxy_api_key = eval("Settings.env.proxy.#{Utils.country_code(page)}_proxy_api_key") || Settings.env.proxy.proxy_api_key
        ScrapingLog.info("*** OpenUri CONFIGURED WITH DEFAULT PROXY Header *** [#{proxy_api_key}]")
        headers = headers.merge({proxy_http_basic_authentication:
             ["http://#{Settings.env.proxy.proxy_host}:#{Settings.env.proxy.proxy_port}",
             proxy_api_key,
             '']})
      end
    end
    headers
  end

  # Overwrite method in each page class that needs it
  def self.getDoc(url)
    if selenium_proxy?
      get_doc_selenium url
    else
      Nokogiri::HTML(open(url, getHeaders) {|url_stream|
        Encoding.default_external = url_stream.charset
        url_stream.read
      })
    end
  end

  def self.get_doc_selenium(url)
    urls = ["http://stroke1.duckdns.org:4444/wd/hub",
            "http://stroke1.duckdns.org:4445/wd/hub",
            "http://stroke1.duckdns.org:4446/wd/hub",
            "http://stroke4.duckdns.org:4444/wd/hub"]

    url_proxy = urls[(rand * urls.size).floor]
    browser = Watir::Browser.new :chrome, url: url_proxy#, proxy: proxy, switches: ['--ignore-certificate-errors', '--headless', '--disable-gpu', '--no-sandbox', '--disable-web-security']
    browser.goto(url)

    res = Nokogiri::HTML(browser.html)
    browser.close
    res
  end

  #TODO: Dar un repaso a todo esto y sobre todo al tema de reintentos ya sea aquí como más arriba en esta clase como en MechanizeDoc...
  def self.get_document(url, is_list = false)
    # ScrapingLog.debug -1
    doc = nil
    downloads_count = 0
    loop do
      # ScrapingLog.debug -2
      begin
        attempts = self::ATTEMPTS
        begin
          attempts -= 1
          downloads_count += 1
          ScrapingLog.info "Descargar: [#{url}]"
          doc = getDoc(url)

          if proxy? and doc.instance_of? Nokogiri::HTML::Document
            attempts_for_proxy = 10
            while doc.text.strip == 'No available proxies' and (attempts_for_proxy -= 1) > 0
              ScrapingLog.error "ERROR: 'No available proxies' de Crawlera. Esperamos cinco segundos y lo volvemos a intentar. Intentos restantes para obtener proxy[ #{ attempts_for_proxy } ]"
              sleep 5
              doc = getDoc(url)
            end
          end
          # ScrapingLog.debug -3
          # File.open("#{DateTime.now.to_i}_amazon.html", 'w') { |file| file.write(doc.to_html) }
        rescue URI::InvalidURIError, Net::ReadTimeout, EOFError => ex
          case ex
            when URI::InvalidURIError
              if (url_parts = url.split('#')).size > 1
                ScrapingLog.error "URI::InvalidURIError intentamos limpiar la URL de anclas y volvemos a descargar sin reducir intentos. Intentos restantes [ #{ attempts += 1 } ]"
                url = url_parts[0]
              else
                ScrapingLog.error "URI::InvalidURIError intentamos escapar la URL. Intentos restantes [ #{attempts} ]"
                url = URI.escape(url)
              end
            when Net::ReadTimeout
              ScrapingLog.error "Net::ReadTimeout intentos restantes [ #{attempts} ]"
            when EOFError
              ScrapingLog.error "EOFError intentos restantes [ #{attempts} ]"
          end
          # ScrapingLog.debug -4
          if attempts > 0
            sleep 2
            retry
          else
            # ScrapingLog.debug -5
            break
          end
        end
#      rescue Mechanize::Error, Mechanize::ResponseCodeError, Net::HTTP::Persistent::Error, Net::OpenTimeout, Net::HTTPFatalError => ex
#         begin
#           case e.try(:response_code)
#               when '404', '403', '410' ...
#
#             ScrapingLog.error "SCRAPING ERROR [ #{e.class}: #{e.message} ] - url [#{url}]"
#             ScrapingLog.error "SCRAPING ERROR e.inspect [#{e.inspect}]"
#             ScrapingLog.error "SCRAPING ERROR e.response_code [#{e.response_code}]"
#             ScrapingLog.error "SCRAPING ERROR e.page [#{e.page}]"
      rescue => e
        # ScrapingLog.debug 0
        ScrapingLog.error "ERROR [ #{e.class}: #{e.message} ] - url [#{url}]"
        product_page = ProductPage.by_url(url)
        # ScrapingLog.debug 1
        begin
          http_code = e.try(:io) ? e.io.status[0]           : e.try(:response_code)  # For OpenURL and Mechanize
          http_page = e.try(:io) ? e.io.readlines.join.to_s : e.try(:page).try(:body) # For OpenURL and Mechanize
#          ScrapingLog.info "page: #{http_page}"
#          File.open("20190623_borrar.html", 'w') { |file| file.write(http_page) } Falla!!!! --> Nokogiri::HTML::Document.parse(http_page)

          if http_code
            # ScrapingLog.debug 2
            ScrapingLog.info "HTTP Error Code::: #{http_code}"
            case http_code
              when '500','502', '503', '504'
                # ScrapingLog.debug 3
                ScrapingLog.error "ERROR [ #{e.class}: #{e.message} ] - url [#{url}] - Intentamos recuperar el cuerpo de la respuesta"
                doc = Nokogiri::HTML(http_page)
                if is_list && get_items(doc).blank?
                  File.new("Download_50X_#{DateTime.now}.html", "w").write(doc.to_html) if Rails.env.development?
                  ScrapingLog.error "ERROR [ #{e.class}: #{e.message} ] - url [#{url}] - La URL es un listado, pero la respuesta no contiene items... ignoramos el documento obtenido."
                  doc = nil
                end
              when '404', '403', '410'
                # ScrapingLog.debug 4
                product_page.update notes: "WARNING: COMPROBAR SI NO-USABLE: error al descargar: [#{http_code} - #{e.message}]" if product_page
                # ScrapingLog.debug "is list: [#{is_list}]"
                if is_list
                  doc = Nokogiri::HTML(http_page)
                  # ScrapingLog.debug "doc: [#{doc}]"
                  # File.write("#{DateTime.now}_HTML_ERROR_CODE_404.html", doc.to_html)
                  if get_current_items_page(doc).nil?
                    ScrapingLog.debug "El documento descargado no es un listado... lo desestimamos para intentar una nueva descarga si nos quedan intentos."
                    # File.write("#{DateTime.now}_HTML_ERROR_CODE_404.html", doc.to_html)
                    doc = nil
                  end
                end
              when '301', '302' # 301 Moved Permanently, 302 Found
                # ScrapingLog.debug 5
                protocol = url.split("://")[0]
                url = URI.escape(e.io.meta["location"].force_encoding("UTF-8"))
                unless url.start_with?("http")
                  url = "#{protocol}:#{url}"
                end
                ScrapingLog.info "INFO: la página redirige a #{url} (valorar qué hacer)"
                product_page.update notes: "INFO: la página redirige a #{url} (valorar qué hacer)" if product_page
              else
                # ScrapingLog.debug 6
                product_page.update notes: "WARNING: COMPROBAR SI NO-USABLE: error al descargar: [#{http_code} - #{e.message}]" if product_page
                raise e
            end
          else
            # ScrapingLog.debug 7
            Rollbar.error(e, "SCRAPING ERROR [ #{e.class}: #{e.message} ] - url [#{url}]")
            product_page.update notes: "WARNING: COMPROBAR SI NO-USABLE: error al descargar: [#{e.class}: #{e.message}]" if product_page
          end
        rescue => ex
          # ScrapingLog.debug 8
          ScrapingLog.error "ERROR [ #{ex.class}: #{ex.message} ] - url [#{url}]"
          ScrapingLog.error "Backtrace:\n\t#{ex.backtrace.join("\n\t")}"
          Rollbar.error(ex, "SCRAPING ERROR [ #{ex.class}: #{ex.message} ] - url [#{url}]")
          raise ex
        end
      end
      # ScrapingLog.debug 9
      # File.write("#{DateTime.now}_HTML_ERROR_CODE_404.html", doc.to_html) if !doc.blank? || downloads_count >= self::MAX_RETRIES
      break if !doc.blank? || downloads_count >= self::MAX_RETRIES
      # ScrapingLog.debug 10
      ScrapingLog.error "No se ha conseguido la descarga, intentos restantes [ #{self::MAX_RETRIES - downloads_count} ] - url [#{url}]"
      sleep downloads_count
    end
    doc
  end
end
