# == Schema Information
#
# Table name: ecommerces
#
#  id           :integer          not null, primary key
#  name         :string           indexed
#  slug         :string           indexed
#  picture      :string
#  is_reference :boolean          default(FALSE)
#  color        :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  has_proxy    :boolean          default(FALSE)
#
# Indexes
#
#  index_ecommerces_on_name  (name)
#  index_ecommerces_on_slug  (slug)
#

class Ecommerce < ApplicationRecord
  extend FriendlyId
  mount_uploader :picture, PictureUploader
  friendly_id :name

  # Owning relationships
  has_many :society_ecommerces, inverse_of: :ecommerce, dependent: :destroy
  has_many :societies, through: :society_ecommerces

  # Validations
  validates :name, presence: true, uniqueness: true

  # Scopes
  scope :is_reference, ->(value = true) { where(is_reference: value) }
  scope :is_reference_in, ->(society, value = true) {
    joins(:society_ecommerces).where(society_ecommerces: {society: society, is_reference: value}) unless society.has_all_ecommerces?
  }
  scope :by_society, ->(society) {
    joins(:society_ecommerces).where(society_ecommerces: {society: society}) unless society.has_all_ecommerces?
  }
  scope :by_user, ->(user) {
    by_society(user.society) unless user.has_all_ecommerces?
  }

  # Callbacks
  after_save -> { Ecommerce.clear_caching_of('calculations') }

  class << self
    # Reference E-commerces reference by user or by default
    def references(source = nil)
      source = source.society if source.is_a?(User)
      if source.nil? || source.has_all_ecommerces?
        Ecommerce.is_reference
      else
        Ecommerce.is_reference_in(source)
      end
    end

    # Maping {name => image_url} of reference E-commerces
    def references_with_images(source = nil)
      references(source).map{|e| [e.name, e.picture.url]}.to_h
    end

    # Names of reference E-commerces
    def reference_names(source = nil)
      references(source).pluck(:name)
    end

    # Maping {slug => id} of E-commerces
    def map_ecommerce
      fetch_caching_of('calculations', 'map_ecommerce') do
        Utils.scope_to_hash(Ecommerce.all, :slug, :id, true)
      end
    end

    def total
      fetch_caching_of('calculations', 'ecommerce_count', type: 'Integer') do
        Ecommerce.count
      end
    end

    # Maping {slug => name} of E-commerces
    def map_ecommerce_name
      fetch_caching_of('calculations', 'map_ecommerce_name') do
        Utils.scope_to_hash(Ecommerce.all, :slug, :name, true)
      end
    end

    # Maping {slug => has_proxy} of E-commerces
    def map_has_proxy
      fetch_caching_of('calculations', 'map_has_proxy') do
        Utils.scope_to_hash(Ecommerce.all, :slug, :has_proxy, true)
      end
    end

    def map_page_color
      fetch_caching_of('calculations', 'map_page_color', force: true) do
        pages_colors = Utils.scope_to_hash(all, :slug, :color, true)
        available_colors = Utils.colors - pages_colors.values.compact
        pages_colors.keys.each do |page|
          pages_colors[page] = available_colors.shift if pages_colors[page].nil?
        end
        pages_colors
      end
    end
  end
end
