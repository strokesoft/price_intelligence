class ApplicationRecord < ActiveRecord::Base
  extend Caching

  self.abstract_class = true
end
