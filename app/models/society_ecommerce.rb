# == Schema Information
#
# Table name: society_ecommerces
#
#  id           :integer          not null, primary key
#  society_id   :integer          indexed, indexed => [ecommerce_id]
#  ecommerce_id :integer          indexed, indexed => [society_id]
#  is_reference :boolean          default(FALSE)
#
# Indexes
#
#  index_society_ecommerces_on_ecommerce_id                 (ecommerce_id)
#  index_society_ecommerces_on_society_id                   (society_id)
#  index_society_ecommerces_on_society_id_and_ecommerce_id  (society_id,ecommerce_id) UNIQUE
#

class SocietyEcommerce < ApplicationRecord
  # Belonging relationships
  belongs_to :society, inverse_of: :society_ecommerces
  belongs_to :ecommerce, inverse_of: :society_ecommerces

  delegate :name, to: :ecommerce
end
