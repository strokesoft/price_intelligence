# == Schema Information
#
# Table name: products
#
#  id                 :integer          not null, primary key, indexed => [category]
#  brand              :string           indexed
#  name               :string
#  ean                :string
#  category           :string           indexed, indexed => [id]
#  subcategory        :string
#  description        :text
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  usable             :boolean          default(TRUE), indexed
#  msrp               :string
#  specs              :json
#  fixed_specs        :boolean          default(FALSE)
#  slug               :string
#  asin               :string
#  vendor_part_number :string
#  mediamarkt_ref     :integer
#
# Indexes
#
#  index_products_on_brand            (brand)
#  index_products_on_category         (category)
#  index_products_on_id_and_category  (id,category)
#  index_products_on_usable           (usable)
#

class Product < ApplicationRecord
  include Searchable
  extend FriendlyId
  friendly_id :ean

  has_many :product_pages
  has_many :product_prices, through: :product_pages

  before_validation :clean_specs

  validates :ean, uniqueness: true, allow_blank: true
  validates_presence_of :ean, unless: :using_asin?
  #validates :ean, format: {with: /[0-9]{13}/, message: "should contain thirteen numeric characters."}
  validates :asin, uniqueness: true, length: { is: 10 }, allow_blank: true
  validates_presence_of :asin, unless: :using_ean?
  validates :vendor_part_number, uniqueness: true, allow_blank: true
  validates :mediamarkt_ref, uniqueness: true, allow_blank: true

  scope :usable,         ->              { where(usable:       true)} # [true, nil])}
  scope :by_name,        ->(name)        { where(name:         name)}
  scope :by_ean,         ->(ean)         { where(ean:          formatted_ean(ean))}
  scope :by_asin,        ->(asin)        { where(asin:         asin) }
  scope :by_vendor_part_number, ->(vendor_part_number) { where(vendor_part_number: vendor_part_number) }
  scope :by_mediamarkt_ref,     ->(mediamarkt_ref)     { where(mediamarkt_ref:     mediamarkt_ref) }
  scope :by_page,        ->(page=nil, usable=true)     { joins(:product_pages).where(product_pages: {page: page, usable: usable}) unless page.blank?}
  scope :by_brand,       ->(brand)       { where(brand:        brand)}
  scope :by_price,       ->(min_price, max_price) {
    range = Product.default_price_range(min_price, max_price)
    range.blank? ? nil : joins(:product_prices).where(product_prices: {price: range})
  }
  scope :by_subcategory, ->(subcategory) { where(subcategory:  subcategory) unless subcategory.blank?}
  scope :by_category,    ->(category)    { where(category:     category) unless category.blank?}
  scope :by_specs,       ->(category, specs) { where(ProductPage.get_specs_conditions(category, specs)) if !category.blank? and !specs.blank? }
  scope :by_date,        ->(start_date, end_date) {joins(:product_prices).where(product_prices: {date: start_date..end_date})}
  scope :with_stock,     ->(with_stock=true)      {joins(:product_prices).where(product_prices: {out_of_stock: [false, nil]}) if with_stock}
  scope :like_name,      ->(name)        { where("products.name like ?", "%#{name}%")}
  scope :like_ean,       ->(ean)         { where("products.ean like ?", "%#{ean}%")}
  scope :like_asin,      ->(asin)        { where("products.asin like ?", "%#{asin}%") }
  scope :like_vendor_part_number, ->(vendor_part_number) { where("products.vendor_part_number like ?", "%#{vendor_part_number}%") }
  scope :like_mediamarkt_ref, ->(mediamarkt_ref) { where("products.mediamarkt_ref like ?", "%#{mediamarkt_ref}%") }

  before_validation :ensure_format_ean
  before_save :check_brand_and_categories

  ATTRIBUTES = self.column_names.map{|q| q.to_sym} - [:id, :created_at, :updated_at] rescue []
  POSIBLE_ATTRIBUTES_IN_PAGE = ATTRIBUTES - [:specs] if ATTRIBUTES.any?

  SPECS = {Ovens: {
               nil => [:"cleaning method", :"energy rating", :capacity, :width, :steam, :colour]},
           Refrigerators: {
               nil => [:colour, :"energy rating", :height, :width, :"refrigerator net capacity", :"freezer net capacity", :"net capacity", :display, :technology]},
           Microwaves: {
               nil => [:"form factor", :capacity, :colour,	:"cooking power (kw)",	:"grill power (w)"]},
           Hobs: {
               nil => [:capacity, :width, :"cooking zones", :"main burner size", :flexi, :wok]},
           Dishwashers: {
               nil => [:"energy rating", :"form factor", :"annual water consumption (l.)", :"noise level (wash | db)", :colour, :services, :"3rd rack"]},
           Washers: {
               nil => [:colour, :"form factor", :"energy rating", :"capacity (kg.)", :"spin speed (rpm)", :"noise level (wash | db)", :"annual water consumption (l.)", :"water cons. by cycle"]},
           External: {
               nil => [:capacity, :status, :family]},
           Internal: {
               nil => [:capacity, :status, :family]},
           "Memory Cards": {
               nil => [:capacity, :status, :family]},
           Pendrives: {
               nil => [:capacity, :status, :family]},
           SSD: {
               nil => [:capacity, :"form factor", :family, :interface, :serie]},
           DRAM: {
               nil => [:capacity, :"kit or single", :family, :module, :speed, :bandwidth, :"form factor", :"kit qty"]},
           nil => {
               nil => []}}

  def searchable_values
    {
      ean => 'A',
      asin => 'A',
      vendor_part_number => 'A',
      mediamarkt_ref&.to_s => 'A',
      name => 'B',
      brand => 'B',
      description => 'C'
    }
  end

  def self.product_specs(category = nil, subcategory = nil)
    ((SPECS[category.try(:to_sym)].try(:[], nil) || []) + (SPECS[category.try(:to_sym)].try(:[], subcategory.try(:to_sym)) || [])).uniq
  end

  def current_prices_info
    prices_info_by_date(Date.today)
  end

  def prices_info_by_date(date = Date.today)
    product_prices.where(date: date).joins(:product_page).includes(:product_page)
  end

  def asp_by_date(date = Date.today)
    product_prices.where(date: date).average(:price).to_f
  end

  def current_prices_info_by_filtering_unique_page(pages = PageBase::PAGES)
    prices_info_by_filtering_unique_page_by_date(pages, Date.today)
  end

  def prices_info_by_filtering_unique_page_by_date(pages = PageBase::PAGES, date = Date.today)
    prices_aux = prices_info_by_date(date)
    (prices_aux.size == 1 and Utils.to_strings_array(pages).include? prices_aux[0].page) ? nil : prices_aux
  end

  def using_ean?
    ean.present?
  end

  def using_asin?
    asin.present?
  end

  def image_url
    Product.map_product_image_url(self.id) || ActionController::Base.helpers.asset_url('fallback/missing_photo.jpg')
  end

  # Maping {id => image_url} of products
  def self.map_product_image_url(id)
    fetch_mcaching_of('map_product_image_url', id) do
      query = <<-SQL
	      select product_id, image_url from product_pages
        where (product_id, updated_at) in (select product_id, max(updated_at) from product_pages
        where image_url is not null and image_url ilike 'http%' group by product_id)
      SQL
      result = ActiveRecord::Base.connection.select_all(query).to_hash
      Utils.scope_to_hash(result, 'product_id', 'image_url', true)
    end
  end

  def self.categories
    SPECS.keys.compact.sort
  end

  def self.subcategories_by(category)
    fetch_caching_of('calculations', "subcategories_by_#{category}") do
      Product.where(category: category).pluck(:subcategory).uniq.compact
    end
  end

  def self.unique_brands
    fetch_caching_of('calculations', 'unique_brands') do
      Product.pluck(:brand).uniq.reject{|p| p.blank?}.sort
    end
  end

  def self.create_products_file
    workbook_name = "products_table_#{Date.today}.xlsx"
    products = Product.all
    sheets = {}
    Axlsx::Package.new do |p|
      products.each do |product|
        sheet_name = product.category ? product.category.to_sym : :"_"

        if (sheet = sheets[sheet_name]).nil?
          #puts "sheet_name: [#{sheet_name}]: [#{Product::SPECS[sheet_name].try(:[],nil)}]"
          sheet = p.workbook.add_worksheet(name: sheet_name.to_s)
          sheet.add_row (["id", "brand", "name", "ean", "category", "subcategory", "description", "usable", "msrp", "asin",
                          "vendor_part_number", "mediamarkt_ref", "specs"] + (Product::SPECS[sheet_name].try(:[],nil).nil? ? [] : Product::SPECS[sheet_name].try(:[],nil)))
          sheets[sheet_name] = sheet
        end

        specs = []
        # puts product.inspect
        (Product::SPECS[sheet_name].try(:[],nil).nil? ? [] : Product::SPECS[sheet_name].try(:[],nil)).each { |spec| specs << product.specs.try(:[], spec.to_s)}

        sheet.add_row [product.id, product.brand, product.name, Product.formatted_ean(product.ean), product.category,
                       product.subcategory, product.description, product.usable, product.msrp, product.asin,
                       product.vendor_part_number, product.mediamarkt_ref, product.specs.to_json] + specs,
                      :types => [:integer, :string, :string, :string, :string, :string, :string, :boolean, :string,
                                 :string, :string, :integer] # :date, time, :float, :integer, :string, :boolean
      end
      p.serialize(workbook_name)
    end
    system 'mv', workbook_name, Rails.configuration.route_to_xlsx_files
  end

  def self.import_products_file(products_file)
    errors = []
    integer_params = [:id, :usable, :mediamarkt_ref]
    string_params  = [:ean, :asin, :vendor_part_number]
    boolean_params = [:usable]
    json_params    = [:specs]

    if File.extname(products_file.path) == ".xlsx"
      spreadsheet = Roo::Excelx.new(products_file.path) #, packed: false, file_warning: :ignore)
    else
      spreadsheet = Roo::Excel.new(products_file.path) #, packed: false, file_warning: :ignore)
    end

    ActiveRecord::Base.transaction do
      spreadsheet.each_with_pagename do |category, sheet|
        category = category.try(:downcase).try(:titleize).to_sym

        head = sheet.row(1).map { |head| head.try(:downcase).try(:to_sym) }

        indexes = {}
        ([:id] + Product::ATTRIBUTES + product_specs(category)).uniq.each { |head_name| indexes[head_name] = head.index(head_name) if head.index(head_name)}

        (2..sheet.last_row).each do |index|
          row = sheet.row(index)

          params = {}
          indexes.keys.each do |head_name|
            valor = row[indexes[head_name]]

            # Check correction of data
            if valor.instance_of?(String)
              valor = valor.strip
              valor = nil if valor.length == 0
              if boolean_params.include? head_name
                case valor.try :upcase
                  when "FALSE", "FALSO"
                    valor = "0"
                  when "TRUE", "VERDADERO"
                    valor = "1"
                end
              end
              if integer_params.include? head_name
                valor = nil if valor != valor.to_i.to_s
              end
            end
            if valor != nil
              # set correct value
              if integer_params.include? head_name
                valor = valor.to_i
                row[indexes[head_name]] = valor
              end
              if string_params.include?(head_name) && valor.instance_of?(Float)
                valor = valor.to_s.gsub(/\.0$/, '')
                row[indexes[head_name]] = valor
              end
              if boolean_params.include? head_name
                valor = (valor.to_i == 0 ? false : true)
              end
              if json_params.include? head_name
                valor = Utils.clean_json(valor)
              end
            end
            params[head_name] = valor

            ([:specs] + product_specs(category)).each { |attribute| params[:fixed_specs] = true if params.include? attribute } # if we have set any specs (in group or one by one) we set them as fixed.
          end

          product_id = params[:id]
          ean = formatted_ean(params[:ean])

          begin
            raise "Valor [#{product_id}] no válido en la columna 'id'" if !product_id.blank? && product_id.to_s != product_id.to_i.to_s

            product = (Product.find_by_id(product_id) || (ean.present? ? Product.find_by_ean(ean) : nil))

            if product
              Product::ATTRIBUTES.each { |attribute| eval("product.#{attribute} = params[:#{attribute}]") if params.include? attribute }
              product.save!

              product.product_pages.each { |ppages| ppages.update!(usable: false) } if params[:usable] == false
            elsif ean
              product = Product.create!(params.select { |key, value| Product::ATTRIBUTES.include? key})
            else
              puts "ERROR: fichero con fila [#{index}] erronea: [#{row}]"
            end

            if product && Product::SPECS[category].try(:[],nil)
              Product::SPECS[category].try(:[],nil).each do |attribute|
                if params.include? attribute
                  eval("product.specs[:'#{attribute}'] = params[:'#{attribute}']")
                end
              end
              product.save!
            end
          rescue => ex
            message = "ERROR [ #{ex.class}: #{ex.message} ]"

            puts message
            puts "Backtrace:\n\t#{ex.backtrace.join("\n\t")}"

            errors << CsvError.create({source_line: row.to_s, error: message, mode: 'products_files',
                                       parser: row[1], n: index, category: row[7]})
          end
        end
      end

      raise ActiveRecord::Rollback unless errors.empty?
    end
    clear_caching_of('calculations') if errors.empty?
    return errors
  end


  def self.create_bad_ean_products_file()
    sheet_name = "Bad EANs #{Date.today}"
    workbook_name = "bad_eans_products_table_#{Date.today}.xlsx"
    Axlsx::Package.new do |p|
      p.workbook.add_worksheet(:name => sheet_name) do |sheet|
        sheet.add_row (["id", "brand", "name", "ean", "category", "subcategory", "description", "usable", "asin",
                        "vendor_part_number", "mediamarkt_ref"])

        Product.all.each do |product|
          unless EAN.valid? product.ean
            sheet.add_row [product.id, product.brand, product.name, product.ean, product.category, product.subcategory,
                           product.description, product.usable, product.asin, product.vendor_part_number, product.mediamarkt_ref],
                           :types => [:integer, :string, :string, :string, :string, :string, :string, :boolean,
                                      :string, :string, :integer] # :date, time, :float, :integer, :string, :boolean
          end
        end
      end
      p.serialize(workbook_name)
    end
    system 'mv', workbook_name, Rails.configuration.route_to_xlsx_files
  end

  def self.create_lost_products_file()
    sheet_name = "Products not found #{Date.today}"
    workbook_name = "lost_products_#{Date.today}.xlsx"
    product_pages = ProductPage.without_product.usable.all
    Axlsx::Package.new do |p|
      p.workbook.add_worksheet(:name => sheet_name) do |sheet|
        sheet.add_row (["id", "page", "image_url", "url", "shipment", "date", "ean", "product_id", "category", "subcategory", "name",
                        "asin", "vendor_part_number", "mediamarkt_ref", "specs", "out_of_stock", "last_aparition", "foul_name",
                        "brand", "description", "usable", "msrp"])
        product_pages.each do |product_page|
          sheet.add_row [product_page.id, product_page.page, product_page.image_url, product_page.url, product_page.shipment,
                         product_page.date, Product.formatted_ean(product_page.ean), product_page.product_id, product_page.category,
                         product_page.product.try(:subcategory), product_page.name, product_page.asin, product_page.vendor_part_number,
                         product_page.mediamarkt_ref, product_page.specs, product_page.out_of_stock, product_page.last_aparition,
                         product_page.foul_name, product_page.foul_brand, product_page.foul_description],
                        :types => [:integer, :string, :string, :string, :string, :date, :string, :integer, :string, :string,
                                   :string, :string, :string, :integer] # :date, time, :float, :integer, :string, :boolean
        end
        p.serialize(workbook_name)
      end
    end
    system 'mv', workbook_name, Rails.configuration.route_to_xlsx_files
  end

  def self.import_lost_products_file(lost_products_file)
    errors = []
    integer_params = [:id, :product_id, :mediamarkt_ref]
    boolean_params = [:usable]
    string_params  = [:ean, :asin, :vendor_part_number]

    if File.extname(lost_products_file.path) == ".xlsx"
      spreadsheet = Roo::Excelx.new(lost_products_file.path) #, packed: false, file_warning: :ignore)
    else
      spreadsheet = Roo::Excel.new(lost_products_file.path) #, packed: false, file_warning: :ignore)
    end

    head = spreadsheet.row(1).map { |head| head.try(:downcase) } # Problema con cabeceras en nill: head = spreadsheet.row(1).map(&:downcase)

    id_index          = head.index('id')
    ean_index         = head.index('ean')
    product_id_index  = head.index('product_id')
    brand_index       = head.index('brand')
    name_index        = head.index('name')
    description_index = head.index('description')
    category_index    = head.index('category')
    subcategory_index = head.index('subcategory')
    asin_index        = head.index('asin')
    vendor_part_number_index = head.index('vendor_part_number')
    mediamarkt_ref_index = head.index('mediamarkt_ref')
    usable_index      = head.index('usable')
    msrp_index        = head.index('msrp')

    indexes = %i(id ean product_id brand name description category subcategory asin vendor_part_number mediamarkt_ref usable msrp).inject({}) do |hash, ref|
      hash[ref] = eval("#{ref}_index")
      hash
    end

    ActiveRecord::Base.transaction do
      (2..spreadsheet.last_row).each do |index|
        row = spreadsheet.row(index)
        indexes.keys.each do |head_name|
          valor = row[indexes[head_name]] rescue nil
          if valor != nil
            # set correct value
            if integer_params.include? head_name
              valor = valor.to_i
              row[indexes[head_name]] = valor
            elsif boolean_params.include? head_name
              row[indexes[head_name]] = parse_boolean(row, indexes[head_name])
            elsif string_params.include?(head_name) && valor.instance_of?(Float)
              valor = valor.to_s.gsub(/\.0$/, '')
              row[indexes[head_name]] = valor
            end
          end
        end

        product_id = get_no_zero_number(row[product_id_index]) if product_id_index
        ean = row[ean_index] if ean_index
        asin = row[asin_index] if asin_index
        vendor_part_number = row[vendor_part_number_index] if vendor_part_number_index
        mediamarkt_ref = row[mediamarkt_ref_index] if mediamarkt_ref_index

        begin
          raise "Valor [#{product_id}] no válido en la columna 'product_id'" if !product_id.blank? && product_id != product_id.to_i.to_s

          prod = Product.find_by_id(product_id) || Product.find_by_refs(ean, asin, vendor_part_number, mediamarkt_ref)

          if prod
            prod.ean                = ean if ean
            prod.brand              = row[brand_index]              if check_not_empty(row, brand_index)
            prod.name               = row[name_index]               if check_not_empty(row, name_index)
            prod.description        = row[description_index]        if check_not_empty(row, description_index)
            prod.category           = row[category_index]           if check_not_empty(row, category_index)
            prod.subcategory        = row[subcategory_index]        if check_not_empty(row, subcategory_index)
            prod.usable             = row[usable_index]             if check_not_empty(row, usable_index)
            prod.asin               = row[asin_index]               if check_not_empty(row, asin_index)
            prod.vendor_part_number = row[vendor_part_number_index] if check_not_empty(row, vendor_part_number_index)
            prod.mediamarkt_ref     = row[mediamarkt_ref_index]     if check_not_empty(row, mediamarkt_ref_index)
            prod.msrp               = row[msrp_index]               if check_not_empty(row, msrp_index)
            prod.save!
          elsif ean.present?
            p = {ean:         ean}
            p[:brand]              = row[brand_index]              if check_not_empty(row, brand_index)
            p[:name]               = row[name_index]               if check_not_empty(row, name_index)
            p[:description]        = row[description_index]        if check_not_empty(row, description_index)
            p[:category]           = row[category_index]           if check_not_empty(row, category_index)
            p[:subcategory]        = row[subcategory_index]        if check_not_empty(row, subcategory_index)
            p[:usable]             = row[usable_index]             if check_not_empty(row, usable_index)
            p[:asin]               = row[asin_index]               if check_not_empty(row, asin_index)
            p[:vendor_part_number] = row[vendor_part_number_index] if check_not_empty(row, vendor_part_number_index)
            p[:mediamarkt_ref]     = row[mediamarkt_ref_index]     if check_not_empty(row, mediamarkt_ref_index)
            p[:msrp]               = row[msrp_index]               if check_not_empty(row, msrp_index)
            #TODO: hacer que si el ean está vacio, pero el product_id es 0 se pueda crear el producto (actualmente se valida que el ean exista y sea único)
            prod = Product.create!(p)
          end
        rescue => ex
          message = "ERROR [ #{ex.class}: #{ex.message} ]"

          Rails.logger.error message
          Rails.logger.error "Backtrace:\n\t#{ex.backtrace.join("\n\t")}"

          errors << CsvError.create({source_line: row.to_s, error: message, mode: 'lost_products_files',
                                     parser: row[1], n: index, category: row[7]})
        end

        begin
          if product_page = ProductPage.find(row[id_index])
            product_page.ean                = ean                           if ean
            product_page.category           = row[category_index]           if check_not_empty(row, category_index)
            product_page.product            = prod                          if prod
            product_page.asin               = row[asin_index]               if check_not_empty(row, asin_index)
            product_page.vendor_part_number = row[vendor_part_number_index] if check_not_empty(row, vendor_part_number_index)
            product_page.mediamarkt_ref     = row[mediamarkt_ref_index]     if check_not_empty(row, mediamarkt_ref_index)
            product_page.usable             = row[usable_index]             if check_not_empty(row, usable_index)
            product_page.save!
          end #TODO: implementar else... Si no se encuentra el product page, tal vez haya que cancelar el fichero.
        rescue => ex
          message = "ERROR [ #{ex.class}: #{ex.message} ]"

          puts message
          puts "Backtrace:\n\t#{ex.backtrace.join("\n\t")}"

          errors << CsvError.create({source_line: row.to_s, error: message, mode: 'lost_products_files',
                                     parser: row[1], n: index, category: row[7]})
        end
      end
      raise ActiveRecord::Rollback unless errors.empty?
    end
    clear_caching_of('calculations') if errors.empty?

    return errors
  end

  def self.find_by_refs(ean, asin, vendor_part_number, mediamarkt_ref)
    product = Product.by_ean(ean).last if ean.present?
    product = Product.find_by_asin(asin) if asin.present? && product.nil?
    product = Product.find_by_vendor_part_number(vendor_part_number) if vendor_part_number.present? && product.nil?
    product = Product.find_by_mediamarkt_ref(mediamarkt_ref) if mediamarkt_ref.present? && product.nil?
    product
  end

  def self.get_no_zero_number(data)
    if (data.instance_of?(String) && data.strip.length == 0) || 0 == (data = data.to_i)
      data = nil
    end
    data
  end

  def self.check_not_empty(row, index)
    index and !row[index].nil? and (!row[index].instance_of?(String) || row[index].length != 0)
  end

  def self.parse_boolean(row, index)
    value = row[index]

    case value
      when TrueClass, FalseClass
        return value
      when String
        (["1", "true", "yes", "si"].include? value.downcase.strip) ?
                                                (return true) :
                                                (return false)
    end
    (value.to_i.to_s.strip == "0") ? false : true
  end

  def clean_specs
    self.specs = Utils.clean_json(self.specs)
  end

  def extract_specs
    pages_specs = {}
    self.product_pages.each {|ppage| pages_specs[ppage.page] = ppage.specs unless ppage.specs.blank?}

    specs = {}
    import_specs = ImportSpec
    import_specs = import_specs.where(category:    [self.category, "", nil])    unless self.category.blank?
    import_specs = import_specs.where(subcategory: [self.subcategory, "", nil]) unless self.subcategory.blank?
    import_specs.order(:spec).order(category: :desc).order(subcategory: :desc).order(:order).each do |import_spec|
      value = pages_specs.try(:[], import_spec.page).try(:[], import_spec.origin_spec)
      specs[import_spec.spec] ||= value unless value.blank?
    end

    specs
  end

  def self.fill_specs(only_empty = true)
    products = Product.where(fixed_specs: false)
    products.where("specs::text = '{}' or specs IS NULL") if only_empty
    products.each do |product|
      product.update specs: product.extract_specs
    end

    clear_caching_of('calculations')
  end

  def check_brand_and_categories
    puts "Product: #{self.id} (check brand and categories)"
    unless self.brand.blank?
      ref_brand = Brand.map_brand[self.brand.parameterize]
      Brand.create(name: self.brand) unless ref_brand
    end

    categories = Category.map_category
    unless self.category.blank?
      ref_category = categories[self.category]['id'] rescue nil
      unless ref_category
        ref_category = Category.create(name: self.category).id
        categories = Category.map_category
      end
      if ref_category && !self.subcategory.blank?
        ref_subcategory = categories[self.category]['children'][self.subcategory]
        unless ref_subcategory
          root = Category.find(ref_category)
          root.children.create(name: self.subcategory) unless root.children.where(name: self.subcategory).first
        end
      end
    end
  end

  class << self
    alias_method :base_categories, :categories

    def available_filters_in_specs
      fetch_caching_of('calculations', 'filters_using_specs') do
        filters = {}
        info = Product.usable.select('DISTINCT(json_object_keys(specs)) AS field, category').
          where(fixed_specs: true).group('category', 'json_object_keys(specs)').order(:category)
        info.each do |row|
          filters[row['category']] = {} if filters[row['category']].nil?
          filters[row['category']][row['field']] = []
        end

        filters.keys.each do |category|
          str_select = filters[category].keys.map do |key|
            "STRING_AGG(DISTINCT(specs->>'#{key}'),'@') as field_#{key.parameterize.underscore}"
          end
          str_select = str_select.join(', ')

          data = Product.usable.select(str_select).where(fixed_specs: true, category: category)[0].as_json(except: :id)
          if data
            filters[category].keys.each do |key|
              values = data["field_#{key.parameterize.underscore}"]
              filters[category][key] = values.split('@').sort_by(&:to_i) if values.present?
            end
          end
        end
        filters
      end
    end

    def map_specs_keys
      fetch_caching_of('calculations', 'map_specs_keys') do
        map_keys = {}
        filters = available_filters_in_specs
        filters.keys.each do |category|
          map_keys[category] = {}
          filters[category].keys.each do |key|
            map_keys[category][key.parameterize.underscore] = key
          end
        end
        map_keys
      end
    end

    def formatted_ean(ean)
      ean.rjust(13, '0') if ean.present?
    end

    def default_price_range(min_price, max_price)
      return {} if min_price.blank? && max_price.blank?
      min = min_price.present? ? min_price.to_i : 0
      max = max_price.present? ? max_price.to_i : CalculationsHelper::MAX_PRICE
      return min..max
    end
  end

  private
  def ensure_format_ean
    self.ean = Product.formatted_ean(self.ean)
  end
end
