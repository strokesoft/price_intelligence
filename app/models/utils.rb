class Utils
  NBSP = Nokogiri::HTML("&nbsp;").text
  IVA = 1.21
  STRING_TO_SEARCH = "---STRING_TO_SEARCH---"

  class << self
    def clear_text(text)
      text.gsub(NBSP, ' ').strip if text
    end
  
    def clear_price(text)
      if text
        text = clear_text( text.delete('\n').delete('€').delete('EUR').delete('$').delete('£').delete('￥').delete(' ') )
        if text[-2] == "," || text[-3] == "," # decimal separator: ','
          text = text.delete('.').gsub(',','.')
        elsif text[-2] == "." || text[-3] == "." # decimal separator: '.'
          text = text.delete(',')
        else # no decimals
          text = text.delete(',').delete('.')
        end
      end
      text
    end
  
    def add_iva(price)
      (price.to_f * IVA).round(2).to_s if price && Float(price)
    end
  
    def to_strings_array(param)
      if param.instance_of? Array
        return param.map{ |item| item.to_s}
      else
        return [param.to_s]
      end
    end
  
    def page_name(page)
      page = page.page if page.instance_of? ProductPage
      "Page::#{page.capitalize}".safe_constantize::PAGE_NAME
    end

    def country_code(page)
      page = page.page if page.instance_of? ProductPage
      "Page::#{page.capitalize}".safe_constantize::COUNTRY_CODE
    end

    def page_names
      available_pages.map{ |page| page_name(page)}
    end
  
    def available_pages
      page_files = Rails.root.join('app', 'models', 'page', '*.rb').to_s
      Dir[page_files].sort_by{ |name| File.basename(name) }.inject([]) do |files, file_path|
        files << File.basename(file_path, '.rb')
      end
    end
  
    def populate_prices(date = Date.today)
      date = date.instance_of? Date ? date : Date.parse(date)
      ProductPrice.by_date(date-1).each {|price| price.dup.update_attributes date: date} if ProductPrice.by_date(date).first.nil? && !ProductPrice.by_date(date-1).first.nil?
    end

    def duplicate_empty_prices(date = Date.today, page = nil)
      date = date.instance_of? Date ? date : Date.parse(date)
      ProductPrice.by_date(date-1).by_page(page).each {|price| price.dup.update_attributes(date: date) if ProductPrice.by_product_page_id(price.product_page_id).by_date(date).first.nil?}
    end

    # def numeric_spec_to_string
    #   Product.where("specs::TEXT != '{}'").each do |product|
    #     changes = false
    #     product.specs.each do |key, value|
    #       case value.class
    #         when Float.class, Integer.class, Numeric.class
    #           product.specs[key] = value.to_s
    #           changes = true
    #         else
    #       end
    #     end
    #     product.save! if changes
    #   end
    # end
  
    def clean_json(value)
      while value.instance_of? String
        #puts "---------- #{value.class} #{value}"
        value = eval(value)
      end
      #puts "---------- #{value.class} #{value}"
      value
    end
  
    def clean_specs_for_object(object)
      #puts "Class: [#{object.class}] - id: [#{object.id}] - specs: [#{object.specs}]"
      object.update(specs: clean_json(object.specs)) if object.specs.instance_of? String
    end
  
    def clean_specs_for_class(object_class)
      object_class.all.each { |object| clean_specs_for_object(object)} # if object_class.method_defined? :specs
    end
  
    def clean_specs
      clean_specs_for_class(Product)
      clean_specs_for_class(ProductPage)
    end
  
    def reset_product_page_foul_names
      ["|#|", ""].each do |txt|
        ProductPage.by_foul_name(txt).each { |ppage| ppage.update foul_name: nil}
      end
    end
  
    def to_array(values)
      ((values.instance_of? Array) ?
           values :
           (values.instance_of? String) ?
               values.split(",").map { |value| value.strip } :
               [values]) # pending considere more cases
    end

    def nlogs(message, level = :info)
      Rails.logger.send(level, message)
      SqlLog.send(level, message)
      puts(message)
    end

    def hashcode(*args)
      args.join(',').hash.to_s
    end

    def scope_to_hash(relation, index_by, columns, is_direct = false)
      if is_direct
        relation.pluck(index_by, Array(columns).first).to_h
      else
        relation.inject({}) do |hash, row|
          hash[row[index_by]] = Array(columns).inject({}){|h, c| h[c] = row[c] ; h}
          hash
        end
      end
    end

    def mean(array)
      array.inject(0) { |sum, x| sum = sum + x; sum } / array.size.to_f
    end

    def variation(last_value, previous_value)
      (last_value - previous_value) / previous_value
    end

    def convert_data_url_to_image(data_url, file_path)
      split_data = splitBase64(data_url)
      file_path = "#{file_path}"
      imageDataString = split_data[:data]
      imageDataBinary = Base64.decode64(imageDataString)
      File.open("#{file_path}", "wb") { |f| f.write(imageDataBinary) }
      return true
    end

    def splitBase64(uri)
      if uri.match(%r{^data:(.*?);(.*?),(.*)$})
        return {
          :type => $1, # "image/png"
          :encoder => $2, # "base64"
          :data => $3, # data string
          :extension => $1.split('/')[1] # "png"
        }
      end
    end

    def colors
      %w( #000000  #003300  #006600  #009900  #00CC00  #00FF00  #000033  #003333  #006633  #009933
          #00CC33  #00FF33  #000066  #003366  #006666  #009966  #00CC66  #00FF66  #000099  #003399
          #006699  #009999  #00CC99  #00FF99  #0000CC  #0033CC  #0066CC  #0099CC  #00CCCC  #00FFCC
          #0000FF  #0033FF  #0066FF  #0099FF  #00CCFF  #00FFFF  #330000  #333300  #336600  #339900
          #33CC00  #33FF00  #330033  #333333  #336633  #339933  #33CC33  #33FF33  #330066  #333366
          #336666  #339966  #33CC66  #33FF66  #330099  #333399  #336699  #339999  #33CC99  #33FF99
          #3300CC  #3333CC  #3366CC  #3399CC  #33CCCC  #33FFCC  #3300FF  #3333FF  #3366FF  #3399FF
          #33CCFF  #33FFFF  #660000  #663300  #666600  #669900  #66CC00  #66FF00  #660033  #663333
          #666633  #669933  #66CC33  #66FF33  #660066  #663366  #666666  #669966  #66CC66  #66FF66
          #660099  #663399  #666699  #669999  #66CC99  #66FF99  #6600CC  #6633CC  #6666CC  #6699CC
          #66CCCC  #66FFCC  #6600FF  #6633FF  #6666FF  #6699FF  #66CCFF  #66FFFF  #990000  #993300
          #996600  #999900  #99CC00  #99FF00  #990033  #993333  #996633  #999933  #99CC33  #99FF33
          #990066  #993366  #996666  #999966  #99CC66  #99FF66  #990099  #993399  #996699  #999999
          #99CC99  #99FF99  #9900CC  #9933CC  #9966CC  #9999CC  #99CCCC  #99FFCC  #9900FF  #9933FF
          #9966FF  #9999FF  #99CCFF  #99FFFF  #CC0000  #CC3300  #CC6600  #CC9900  #CCCC00  #CCFF00
          #CC0033  #CC3333  #CC6633  #CC9933  #CCCC33  #CCFF33  #CC0066  #CC3366  #CC6666  #CC9966
          #CCCC66  #CCFF66  #CC0099  #CC3399  #CC6699  #CC9999  #CCCC99  #CCFF99  #CC00CC  #CC33CC
          #CC66CC  #CC99CC  #CCCCCC  #CCFFCC  #CC00FF  #CC33FF  #CC66FF  #CC99FF  #CCCCFF  #CCFFFF
          #FF0000  #FF3300  #FF6600  #FF9900  #FFCC00  #FFFF00  #FF0033  #FF3333  #FF6633  #FF9933
          #FFCC33  #FFFF33  #FF0066  #FF3366  #FF6666  #FF9966  #FFCC66  #FFFF66  #FF0099  #FF3399
          #FF6699  #FF9999  #FFCC99  #FFFF99  #FF00CC  #FF33CC  #FF66CC  #FF99CC  #FFCCCC  #FFFFCC
          #FF00FF  #FF33FF  #FF66FF  #FF99FF  #FFCCFF  #FFFFFF
          AliceBlue AntiqueWhite Aqua Aquamarine Azure Beige Bisque Black BlanchedAlmond Blue
          BlueViolet Brown BurlyWood CadetBlue Chartreuse 0Chocolate Coral CornflowerBlue Cornsilk
          Crimson Cyan DarkBlue DarkCyan DarkGoldenRod DarkGray DarkGrey DarkGreen DarkKhaki
          DarkMagenta DarkOliveGreen Darkorange DarkOrchid DarkRed DarkSalmon DarkSeaGreen
          DarkSlateBlue DarkSlateGray DarkSlateGrey DarkTurquoise DarkViolet DeepPink DeepSkyBlue
          DimGray DimGrey DodgerBlue FireBrick FloralWhite ForestGreen Fuchsia Gainsboro GhostWhite
          Gold GoldenRod Gray Grey Green GreenYellow HoneyDew HotPink IndianRed Indigo Ivory Khaki
          Lavender LavenderBlush LawnGreen LemonChiffon LightBlue LightCoral LightCyan
          LightGoldenRodYellow LightGray LightGrey LightGreen LightPink LightSalmon LightSeaGreen
          LightSkyBlue LightSlateGray LightSlateGrey LightSteelBlue LightYellow Lime LimeGreen Linen
          Magenta Maroon MediumAquaMarine MediumBlue MediumOrchid MediumPurple MediumSeaGreen
          MediumSlateBlue MediumSpringGreen MediumTurquoise MediumVioletRed MidnightBlue MintCream
          MistyRose Moccasin NavajoWhite Navy OldLace Olive OliveDrab Orange OrangeRed Orchid
          PaleGoldenRod PaleGreen PaleTurquoise PaleVioletRed PapayaWhip PeachPuff Peru Pink Plum
          PowderBlue Purple Red RosyBrown RoyalBlue SaddleBrown Salmon SandyBrown SeaGreen SeaShell
          Sienna Silver SkyBlue SlateBlue SlateGray SlateGrey Snow SpringGreen SteelBlue Tan Teal
          Thistle Tomato Turquoise Violet Wheat White WhiteSmoke Yellow YellowGreen).freeze
    end
  end
end



