# == Schema Information
#
# Table name: import_specs
#
#  id          :integer          not null, primary key
#  category    :string           indexed => [subcategory, spec, order]
#  subcategory :string           indexed => [category, spec, order]
#  spec        :string           indexed => [category, subcategory, order]
#  order       :integer          indexed => [category, subcategory, spec]
#  page        :string
#  origin_spec :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  import_specs_index  (category,subcategory,spec,order) UNIQUE
#

class ImportSpec < ApplicationRecord

 :string

  validates :category,    presence: true
  #validates :subcategory, presence: true
  validates :spec,        presence: true
  #validates :order,       presence: true
  validates :page,        presence: true
  validates :page,        inclusion: { :in => Utils.available_pages, message: "'%{value}' no es un market disponible" }
  validates :origin_spec, presence: true



  validates :category, uniqueness: { scope: [:subcategory, :spec, :order] }


  # scope :by_param, ->(param, values)  { where(param => ((values.instance_of? Array) ?
  #                                                           values :
  #                                                           (values.instance_of? String) ?
  #                                                               values.split(",").map { |value| value.strip } :
  #                                                               values)) unless values.blank?}
end
