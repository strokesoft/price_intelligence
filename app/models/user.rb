# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           indexed
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string           indexed
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  admin                  :boolean          not null
#  first_name             :string
#  last_name              :string
#  society_id             :integer          indexed
#
# Indexes
#
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#  index_users_on_society_id            (society_id)
#

class User < ApplicationRecord
  # Include default devise modules. Others available are:

  devise :database_authenticatable, :recoverable,
         :trackable, :validatable, :async, :timeoutable, :rememberable

  belongs_to :society, inverse_of: :users

  validates :email, presence: true, uniqueness: true
  validates :society, presence: true

  delegate :available, :available_names, :available_slugs,
           :reference_ecommerces, :reference_ecommerces_slug_and_name, :another_ecommerces,
           :available_ecommerces, :available_root_categories, :available_subcategories_for,
           :has_all_brands?, :has_all_ecommerces?, :has_all_categories?,
           :has_filter_by_specs?, :min_date, to: :society

  def name
    "#{self.first_name} #{self.last_name}"
  end

  def user_name
    self.first_name || self.email
  end

  def timeout_in
    2.hours
  end

  def send_on_create_confirmation_instructions
    Devise::Mailer.delay.confirmation_instructions(self)
  end

  def send_reset_password_instructions
    Devise::Mailer.delay.reset_password_instructions(self)
  end

  def send_unlock_instructions
    Devise::Mailer.delay.unlock_instructions(self)
  end

  def key
    "user-#{id}"
  end

  protected
  def password_required?
    !persisted? || !password.blank? || !password_confirmation.blank?
  end
end
