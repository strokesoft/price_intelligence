class DataGroupedCalculation
  include PriceCategoriesHelper
  include CalculationsHelper

  attr_accessor :start_date, :end_date, :society,
                :page, :brand, :category, :subcategory, :listed,
                :cost, :min_price, :max_price, :specs, :models

  delegate :available_root_categories, to: :society

  def initialize(start_date, end_date, society, options = {})
    # Date range & society
    @start_date  = start_date
    @end_date    = end_date
    @society     = society
    # Filters
    @page        = options[:page]
    @brand       = options[:brand]
    @category    = options[:category]
    @subcategory = options[:subcategory]
    @listed      = options[:listed]
    @cost        = options[:cost]
    @min_price   = options[:min_price]
    @max_price   = options[:max_price]
    @specs       = options[:specs]
    @models      = options[:models]
    @with_stock  = (@listed == :available)

    # Results, counters, totals in @data
    initialize_counters
  end

  def available_pages
    fetch_caching_of('calculations', "society-#{society.id}-available-pages") do
      society.available(:ecommerces).pluck(:slug).sort
    end
  end

  def available_brands
    fetch_caching_of('calculations', "society-#{society.id}-available-brands") do
      brand_names = society.available(:brands).pluck(:name).sort
      brand_names += [nil, ''] if society.has_all_brands?
      brand_names
    end
  end

  def root_ids
    society.available(:roots).pluck(:id)
  end

  def available_root_categories
    @available_categories ||= society.available_names(:root_categories)
  end

  def available_subcategories_for(category)
    ancestry = Category.usable.roots.find_by(name: category).id
    society.available(:categories).where(usable: true, ancestry: ancestry).pluck(:name).sort
  end

  def available_subcategories
    @available_subcategories ||= society.available(:categories).where(usable: true, ancestry: root_ids).pluck(:name).sort
  end

  # Initialize data
  def initialize_counters
    # benchamark and price_evolution share information
    @data = {markets: {}, brands: {}, categories: {}, averages: {}, benchmark: {}, minimum_advertised_price: {}}

    # Initialize counters in markets
    available_pages.each_with_object(@data[:markets]) {|market, node| node[market] = 0}
    # Initialize counters in brands
    available_brands.each_with_object(@data[:brands]) {|brand, node| node[brand] = 0}
    # Initialize counters in category
    available_root_categories.each_with_object(@data[:categories]) do |category, node|
      node[category] = {
        period_average: 0,
        previous_period_average: 0,
        average_percentage: 0,
        availables_filtered: 0,
        offered_filtered: 0,
        availables: 0,
        offered: 0,
        products_in_stock: 0,
        new_products: 0,
        new_products_ids: [],
        disappeared_products: 0,
        disappeared_products_ids: [],
        products_prices_up: 0,
        products_prices_up_ids: [],
        products_prices_up_percentage: 0,
        products_prices_down: 0,
        products_prices_down_ids: [],
        products_prices_down_percentage: 0,
        subcategories: initialize_subcategories(category)
      }
    end
  end

  def initialize_subcategories(category)
    fetch_caching_of('calculations', "initialize-subcategories-#{category}-#{society.id}") do
      root = society.available(:roots).find_by(name: category)
      all_subcategories = root.children.usable.by_society(society).pluck(:name) + ['']
      all_subcategories.inject({}) do |subcategories, subcategory|
        subcategories.merge(subcategory => {products_in_stock: 0})
      end
    end
  end

  def fetch_dashboard(force = false)
    key = field_key('dashboard')

    fetch_caching_of('calculations', key, force: force) do
      SqlLog.debug('::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::')
      SqlLog.debug("Fetch dashboard: #{key}")
      # Prepare totals in markets and brands
      prepare_totals_grouped(categories: available_root_categories)
      # Prepare offered and availables by category
      prepare_stock_status_grouped
      # Prepare offered_filtered and availables_filtered by category
      prepare_stock_status_filtered_grouped
      # Prepare period_average, previous_period_average y average_percentage by category
      prepare_period_average_grouped
      # Prepare products_in_stock by category
      prepare_products_in_stock_grouped
      # Prepare new_products by category
      prepare_new_products_grouped
      # Prepare disappeared_products by category
      prepare_disappeared_products_grouped
      # Prepare products_prices_up_down by category
      prepare_products_prices_up_down_grouped
      # Prepare products_prices_changed by category
      prepare_products_prices_changed_grouped
      #SqlLog.debug(@data)
      @data
    end
  end

  def fetch_price_categories(pages_to_show:, brands_to_show:)
    key = field_key('price_categories')

    fetch_caching_of('calculations', key) do
      SqlLog.debug("::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::")
      SqlLog.debug("Fetch price_categories: #{key}")
      # Prepare totals in markets and brands
      prepare_totals_grouped(categories: @category)
      # Prepare products_in_stock by category
      prepare_products_in_stock_grouped
      # Prepare prices table (averages)
      prepare_prices_table(pages_to_show: pages_to_show,
                           brands_to_show: brands_to_show,
                           subcategories: @subcategory || available_subcategories_for(@category))
      #SqlLog.debug(@data)
      @data
    end
  end

  def fetch_benchmark
    key = field_key('benchmark')
    fetch_caching_of('calculations', key) do
      SqlLog.debug("::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::")
      SqlLog.debug("Fetch benchmark: #{key}")
      # Prepare totals in markets and brands
      prepare_totals_grouped(categories: @category, subcategories: @subcategory)
      # Prepare products_in_stock by category
      prepare_products_in_stock_grouped(categories: @category, subcategories: @subcategory)
      # Prepare chart data
      prepare_chart_data(pages: page,
                         brands: brand,
                         subcategories: @subcategory || available_subcategories_for(@category))

      # Prepare price evolution data
      prepare_price_evolution_chart(pages: page,
                                    brands: brand,
                                    subcategories: @subcategory || available_subcategories_for(@category))
      @data
    end
  end

  alias_method :fetch_price_evolution, :fetch_benchmark

  def fetch_minimum_advertised_price(pages_to_show:, brands_to_show:)
    key = field_key('minimum_advertised_price')
    fetch_caching_of('calculations', key) do
      SqlLog.debug("::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::")
      SqlLog.debug("Fetch minimum advertised price: #{key}")
      prepare_totals_grouped(categories: @category, subcategories: @subcategory)
      # Prepare products_in_stock by category
      prepare_products_in_stock_grouped(categories: available_root_categories)
      prepare_minimum_advertised_price(brands: brands_to_show, categories: @category, subcategories: @subcategory)
      @data
    end
  end

  def self.date_ranges
    last_date = ProductPrice.get_last_date
    ranges = [[last_date, last_date]]

    today = Date.today
    ranges << [today.yesterday, today.yesterday]
    ranges << [today - 6.days, today]
    ranges << [today - 29.days, today]
    ranges << [today.beginning_of_month, today.end_of_month]
    ranges << [(today - 1.month).beginning_of_month, (today - 1.month).end_of_month]
  end

  private

  def filter_categories(categories = nil)
    if categories && !society.has_all_categories?
      categories
    else
      society.has_all_categories? ? nil : available_root_categories
    end
  end

  def filter_subcategories
    society.has_all_categories? ? nil: available_subcategories
  end

  def category_by_id
    Category.find_by(name: @category).id
  end

  def brand_key(value)
    # We avoid problems with brands
    # indesit -> Indesit, Indesit -> Indesit, BQ -> bq, Bq -> bq
    @brand_keys ||= @data[:brands].keys.compact.map{|k| [k.parameterize.underscore, k]}.to_h
    value.nil? ? nil : @brand_keys[value.parameterize.underscore]
  end

  def prepare_totals_grouped(categories:, subcategories: filter_subcategories)
    info = get_totals_grouped(available: @with_stock,
                              start_date: start_date,
                              end_date: end_date,
                              pages: page,
                              brands: brand,
                              categories: categories,
                              subcategories: subcategories,
                              min_price: min_price,
                              max_price: max_price,
                              specs: specs,
                              models: models)
    info.each do |row|
      defined_brand = brand_key(row['brand'])
      if ((@data[:brands][defined_brand]  || row['brand']  == Utils::STRING_TO_SEARCH) &&
          (@data[:markets][row['market']] || row['market'] == Utils::STRING_TO_SEARCH))
        @data[:brands][defined_brand]  += row['count'].to_i unless row['brand']  == Utils::STRING_TO_SEARCH
        @data[:markets][row['market']] += row['count'].to_i unless row['market'] == Utils::STRING_TO_SEARCH
      end
    end
  end

  def prepare_period_average_grouped
    info = get_period_average_grouped(available: @with_stock,
                                      start_date: start_date,
                                      end_date: end_date,
                                      previous_dates: previous_dates(start_date, end_date),
                                      pages: page || available_pages,
                                      brands: brand || available_brands,
                                      categories: available_root_categories,
                                      subcategories: filter_subcategories)
    # Initialize averages
    [:period_average, :previous_period_average].each do |range|
      available_root_categories.each_with_object(@data[:categories]) {|category, node| node[category][range] = 0}
    end

    # Current average and previous average
    info.each do |row|
      @data[:categories][row['category']][row['range'].to_sym] = row['average'].to_i
    end

    # Average percentage
    available_root_categories.each_with_object(@data[:categories]) do |category, node|
      node[category][:average_percentage] = calculate_avg_pct(node[category][:period_average],
                                                              node[category][:previous_period_average])
    end
  end

  def prepare_stock_status_grouped
    offered   = get_stock_status_grouped(pages: available_pages,
                                         brands: available_brands,
                                         categories: available_root_categories,
                                         subcategories: filter_subcategories)
    available = get_stock_status_grouped(available: true,
                                         pages: available_pages,
                                         brands: available_brands,
                                         categories: available_root_categories,
                                         subcategories: filter_subcategories)

    offered.each{|row| @data[:categories][row['category']][:offered] += row['count']}
    available.each{|row| @data[:categories][row['category']][:availables] += row['count']}
  end

  def copy_values_stock_status_in_filtered
    @data[:categories].keys.each do |category|
      @data[:categories][category][:availables_filtered] = @data[:categories][category][:availables]
      @data[:categories][category][:offered_filtered] = @data[:categories][category][:offered]
    end
  end

  # Prepare offered_filtered and availables_filtered by category (FILTERED)
  def prepare_stock_status_filtered_grouped
    if page.nil? && brand.nil?
      copy_values_stock_status_in_filtered
    else
      offered = get_stock_status_grouped(pages: page || available_pages,
                                         brands: brand || available_brands,
                                         categories: available_root_categories,
                                         subcategories: filter_subcategories)

      available = get_stock_status_grouped(available: true,
                                           pages: page || available_pages,
                                           brands: brand || available_brands,
                                           categories: available_root_categories,
                                           subcategories: filter_subcategories)

      offered.each {|row| @data[:categories][row['category']][:offered_filtered] += row['count']}
      available.each {|row| @data[:categories][row['category']][:availables_filtered] += row['count']}
    end
  end

  def prepare_products_in_stock_grouped(categories: available_root_categories, subcategories: filter_subcategories)
    info = get_products_in_stock_grouped(available: @with_stock,
                                         start_date: start_date,
                                         end_date: end_date,
                                         pages: page || available_pages,
                                         brands: brand || available_brands,
                                         categories: categories,
                                         subcategories: subcategories,
                                         min_price: min_price,
                                         max_price: max_price,
                                         specs: specs,
                                         models: models)
    info.each do |row|
      if @data[:categories][row['category']][:subcategories][row['subcategory']]
        @data[:categories][row['category']][:products_in_stock] += row['count']
        unless row['subcategory'].blank?
          @data[:categories][row['category']][:subcategories][row['subcategory']]['products_in_stock'] += row['count']
        end
      end
      @data[:categories][row['category']][:subcategories]['']['products_in_stock'] += row['count']
    end
  end

  def prepare_new_products_grouped
    info = get_new_products_grouped(available: true,
                                    start_date: start_date,
                                    end_date: end_date,
                                    pages: page || available_pages,
                                    brands: brand || available_brands,
                                    categories: available_root_categories,
                                    subcategories: filter_subcategories)
    info.each do |row|
      @data[:categories][row['category']][:new_products] += row['count']
      @data[:categories][row['category']][:new_products_ids] += row['product_ids'].split(',')
    end
  end

  def prepare_disappeared_products_grouped
    info = get_disappeared_products_grouped(available: @with_stock,
                                            start_date: start_date,
                                            end_date: end_date,
                                            pages: page || available_pages,
                                            brands: brand || available_brands,
                                            categories: available_root_categories,
                                            subcategories: filter_subcategories)
    info.each do |row|
      @data[:categories][row['category']][:disappeared_products] += row['count']
      @data[:categories][row['category']][:disappeared_products_ids] += row['product_ids'].split(',')
    end
  end

  def prepare_products_prices_changed_grouped
    data = get_products_prices_changed_grouped(
      available: @with_stock,
      start_date: start_date,
      end_date: end_date,
      pages: page || available_pages,
      brands: brand || available_brands,
      categories: available_root_categories,
      subcategories: filter_subcategories
    )

    data.each do |row|
      pointer = @data[:categories][row['category']]
      pointer[:products_prices_changed] = row['count']
      pointer[:products_prices_changed_ids] = row['product_ids']
    end
  end

  def prepare_products_prices_up_down_grouped
    info = get_products_prices_up_down_grouped(available: @with_stock,
                                               previous_dates: previous_dates(start_date, end_date),
                                               start_date: start_date,
                                               end_date: end_date,
                                               pages: page || available_pages,
                                               brands: brand || available_brands,
                                               categories: available_root_categories,
                                               subcategories: filter_subcategories)

    info.each do |row|
      pointer = @data[:categories][row['category']]
      avg_pct = calculate_avg_pct(row['avg_price_after'], row['avg_price_before'])

      if row['price_type'] == 'lower'
        pointer[:products_prices_down] = row['count']
        pointer[:products_prices_down_percentage] = avg_pct
        pointer[:products_prices_down_ids] = row['product_ids']
      else
        pointer[:products_prices_up] = row['count']
        pointer[:products_prices_up_percentage] = avg_pct
        pointer[:products_prices_up_ids] = row['product_ids']
      end
    end
  end

  def prepare_prices_table(pages_to_show:, brands_to_show:, subcategories:)
    pages = pages_to_show.keys
    brands = brands_to_show.map{|p| p[0]}

    if !pages.empty? && !brands.empty?
      subcategories = [subcategories].flatten if subcategories

      avgs_with_page    = get_only_period_average_grouped(available: @with_stock,
                                                          start_date: start_date, end_date: end_date,
                                                          pages: pages, brands: brands,
                                                          category: @category, subcategories: subcategories)

      avgs_without_page = get_only_period_average_grouped(available: @with_stock,
                                                          start_date: start_date, end_date: end_date,
                                                          pages: nil, brands: brands,
                                                          category: @category, subcategories: subcategories)
      tmp_averages = {}
      brands.each{ |brand| tmp_averages[brand] = {pages: {}, average: 0} }

      avgs_with_page.each{ |row| tmp_averages[row['brand']][:pages][row['page']] = row['average'].to_i }
      avgs_without_page.each{ |row| tmp_averages[row['brand']][:average] = row['average'].to_i }

      @data[:averages] = brands_to_show.keys.each.inject({}) do |avgs, brand|
        pages = pages_to_show.each.inject({}) do |markets, page_item|
          pa_with_page    = tmp_averages[brand][:pages][page_item[0]] || 0
          pa_without_page = tmp_averages[brand][:average]

          desviation = desviation(pa_with_page, pa_without_page)
          markets.merge(page_item[1] => [pa_with_page, level_by_desviation(desviation)])
        end
        avgs.merge(brand => [brands_to_show[brand], pages])
      end
    end
  end

  def prepare_chart_data(pages:, brands:, subcategories:)
    subcategories = [subcategories].flatten if subcategories
    @data[:benchmark] = {brands: {}, average: {}, summations: {}, minima: {}, maxima: {}}
    benchmark_info = @data[:benchmark][:brands]

    data = get_chart_data(available: @with_stock, start_date: start_date, end_date: end_date,
                          pages: pages, brands: brands,
                          category: @category, subcategories: subcategories,
                          min_price: min_price, max_price: max_price,
                          specs: specs,
                          models: models)

    # Initialize data
    unless brands.blank?
      brands.each_with_object(benchmark_info) do |brand, node|
        node[brand] = {'asp' => 0, 'min' => 0, 'max' => 0, 'skus' => 0, 'offers' => 0, 'pages' => [], 'weight' => 0}
      end
    end
    # Get average and totals
    data.each do |row|
      ref = benchmark_info[row['brand']]
      ref.merge!(row.as_json.except('brand')) unless ref.nil?
    end

    # Brands average
    [:skus, :asp, :offers, :min, :max].each do |key|
      values = benchmark_info.map{|_k, v| v[key.to_s]}.reject{|v| v == 0}
      @data[:benchmark][:average][key] = Utils.mean(values) if values.size > 0
      @data[:benchmark][:summations][key] = values.sum
      @data[:benchmark][:minima][key] = values.min
      @data[:benchmark][:maxima][key] = values.max
    end

    total_offers = general_query(groups: nil, select_string:  "count(*) as total_offers", available: @with_stock,
                                                                  start_date: start_date, end_date: end_date,
                                                                  pages: pages, brands: nil,
                                                                  category: @category, subcategories: nil,
                                                                  min_price: 0, max_price: CalculationsHelper::MAX_PRICE,
                                                                  specs: nil, models: nil)

    SqlLog.debug("total_offers ------------------------------------------------------------------>\n #{total_offers.to_sql}")

    @data[:benchmark][:summations][:total_offers] = total_offers.first.try(:[], "total_offers")

    # Set weight
    unless brands.blank?
      brands.each_with_object(benchmark_info) do |brand, node|
        node[brand]['pages'] = node[brand]['pages'].split(',')
        node[brand]['weight'] = (node[brand]['pages'].size * 100 / pages.size) rescue 0
        node[brand]['price_index'] = ((node[brand]['asp'] / @data[:benchmark][:average][:asp]) * 100).round(0) rescue 0
      end
    end


    @data[:product_ids] = get_product_ids(available: @with_stock,
                                          start_date: start_date, end_date: end_date,
                                          pages: pages, brands: brands,
                                          category: @category, subcategories: subcategories,
                                          min_price: min_price, max_price: max_price,
                                          specs: specs, models: models).pluck('products.id')
  end

  def prepare_price_evolution_chart(pages:, brands:, subcategories:)
    subcategories = [subcategories].flatten if subcategories
    groups = ['products.brand', 'product_prices.date']
    select_string = "AVG(product_prices.price) as asp,
                     product_prices.date,
                     COUNT(DISTINCT product_pages.product_id) as skus,
                     COUNT(DISTINCT product_prices.product_page_id) as offers,
                     products.brand"
    data = CalculationsHelper
             .general_query(groups: groups, select_string: select_string,
                            available: @with_stock, start_date: start_date, end_date: end_date,
                            pages: pages, brands: brands, category: @category, subcategories: subcategories,
                            min_price: min_price, max_price: max_price, specs: specs, models: models)
    SqlLog.debug("price_evolution_chart ----------------------------------------------------------->\n #{data.to_sql}")
    @data[:benchmark][:chart_data] = data.as_json(except: :id)
  end

  def prepare_minimum_advertised_price(brands:, categories:, subcategories:)
    products = Product.usable.where(brand: brands)
    products = products.by_category(categories) if categories.present?
    products = products.by_subcategory(subcategories) if subcategories.present?
    info = ProductPage.usable.by_page(page).includes(:product).references(:product).merge(products).
        includes(:product_prices).references(:product_prices).merge(ProductPrice.where(date: start_date))
    info.each do |product_page|
      prices_with_desviation = []
      product = product_page.product
      product_prices = product.product_prices.where(date: start_date).includes(:product_page)
      reference_price = product_prices.select{ |pprice| pprice.product_page.page == @page }.first.price
      product_prices.each do |product_price|
        desviation_hash = {}
        page_key = product_price.product_page.page
        if page_key == @page
          price_desviation = product.msrp.to_f != 0.0 ? desviation(product_price.price, product.msrp.to_f) : 0.0
        else
          price_desviation = desviation(product_price.price, reference_price)
        end
        level_desviation = (page_key == @page ? level_by_desviation_msrp(price_desviation) : level_by_desviation(price_desviation))
        desviation_hash[page_key] = [product_price.price, level_desviation]
        prices_with_desviation << desviation_hash
      end
      @data[:minimum_advertised_price][product.ean] = {
          product: product.attributes.slice('category', 'subcategory', 'brand', 'name', 'msrp'),
          prices: prices_with_desviation
      }
    end
  end

  def field_key(prefix)
    @dashboard_key ||= "#{prefix}-#{start_date}-#{end_date}-#{society.id}-#{page}-#{brand}-#{category}-#{subcategory}-#{listed}-#{cost}-#{min_price}-#{max_price}-#{specs}-#{models}"
  end

  # Averages
  def calculate_avg_pct(period_average, previous_period_average)
    return 0 if period_average.to_i == 0 || previous_period_average.to_i == 0
    percentage = ( (period_average - previous_period_average).to_f / previous_period_average ) * 100
    percentage.round(2)
  end
end
