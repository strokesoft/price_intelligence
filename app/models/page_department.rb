# == Schema Information
#
# Table name: page_departments
#
#  id           :integer          not null, primary key
#  page         :string           not null
#  department   :string
#  category     :string           not null
#  subcategory  :string
#  url_template :string           not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class PageDepartment < ApplicationRecord
  validates :page,                presence: true
  validates :page,                inclusion: { :in => Utils.available_pages, message: "'%{value}' no es un market disponible" }
  #validates :department,     presence: true
  validates :category,    presence: true
  #validates :subcategory, presence: true
  validates :url_template,        presence: true
  validates :url_template,        uniqueness: true # La URL debe ser única, si en algún caso se piensa que debiera ser la misma ver como se ha resuelto para Carrafour/Carrefourmarket
  validate  :format_url_template

  # default_scope { order(order: :asc) }

  scope :by_param, ->(param, values)  { where(param => Utils.to_array(values)) unless values.blank?}

#  scope :by_page, ->(pages)  { where(page: ((pages.instance_of? Array) ? pages : pages.split(",").map { |s| s.strip })) unless pages.blank?}
#  scope :by_category, ->(category)  { where(page: ((pages.instance_of? Array) ? pages : pages.split(",").map { |s| s.strip })) unless pages.blank?}

  def format_url_template
    begin
      PageBase.get_class(page).build_url(url_template, 1)
    rescue SyntaxError
      errors.add(:url_template, "has an invalid format. Check quotes and brackets")
    end
  end
end
