# == Schema Information
#
# Table name: product_pages
#
#  id                 :integer          not null, primary key
#  product_id         :integer          indexed
#  page               :string
#  image_url          :string
#  url                :string
#  shipment           :string
#  date               :date
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  ean                :string
#  category           :string           indexed
#  foul_name          :string
#  specs              :json
#  out_of_stock       :boolean
#  last_aparition     :datetime
#  usable             :boolean          default(TRUE), indexed
#  asin               :string
#  vendor_part_number :string
#  mediamarkt_ref     :integer
#  foul_brand         :string
#  variant:           :boolean          default(FALSE)
#  foul_description   :string
#
# Indexes
#
#  index_product_pages_on_category    (category)
#  index_product_pages_on_product_id  (product_id)
#  index_product_pages_on_usable      (usable)
#

class ProductPage < ApplicationRecord
  include PgSearch

  ## Query based on JSON document
  # The -> operator returns the original JSON type (which might be an object), whereas ->> returns text
  #ProductPage.where("specs->>'key' = ?", "value")
  #TODO: ver:
  # - https://nandovieira.com/using-postgresql-and-jsonb-with-ruby-on-rails
  # - https://blog.codeship.com/unleash-the-power-of-storing-json-in-postgres/
  # - https://stackoverflow.com/questions/28202158/postgresql-migrating-json-to-jsonb
  pg_search_scope :search_content_for, against: :specs, using: { tsearch: { any_word: true } }


  belongs_to :product
  has_many   :product_prices
  has_many   :top_sellers

#  serialize :specs, HashSerializer

  before_validation :check_or_set_relations
  before_validation :clean_specs
  #before_validation :ensure_format_ean

  validates :url, presence:   true
  validates_uniqueness_of :url, scope: :page
  #validates :ean, format: {with: /[0-9]{13}/, message: "should contain thirteen numeric characters."}, allow_blank: true
  validates :asin, length: { is: 10 }, allow_blank: true

  scope :usable,         ->            { where(usable:     true)} # [true, nil])}
  scope :by_product,     ->(id)        { where(product_id: id)}
  scope :by_page,        ->(page=nil)  { where(page:       page) unless page.blank?}
  scope :by_name,        ->(name)      { joins(:product).where(products: {name: name}) }
  scope :by_foul_brand,  ->(foul_brand) { where(foul_name: foul_brand)}
  scope :by_foul_name,   ->(foul_name) { where(foul_name:  foul_name)}
  scope :by_foul_description, ->(foul_description) { where(foul_description: foul_description)}
  scope :by_url,         ->(url)       { where(url:        equivalent_urls(url))}
  scope :by_date,        ->(date)      { where(date:       date)}
  scope :by_ean,         ->(ean)       { where(ean:        Product.formatted_ean(ean))}
  scope :by_asin,        ->(asin)      { where(asin:       asin)}
  scope :by_vendor_part_number, ->(vendor_part_number) { where(vendor_part_number: vendor_part_number) }
  scope :by_mediamarkt_ref,     ->(mediamarkt_ref)     { where(mediamarkt_ref:     mediamarkt_ref) }
  scope :like_url,       ->(url)       { where("product_pages.url like ?", "%#{url}%")}
  scope :like_name,      ->(name)      { joins(:product).where("products.name like ?", "%#{name}%") }
  scope :like_foul_brand, ->(foul_brand) { where("foul_brand like ?", "%#{foul_brand}%")}
  scope :like_foul_name, ->(foul_name) { where("foul_name like ?", "%#{foul_name}%")}
  scope :like_foul_description, ->(foul_description) { where("foul_description like ?", "%#{foul_description}%")}
  scope :stock,          ->(available) { where(out_of_stock: false) if available}
  scope :with_stock,     ->            { where(out_of_stock: false)}
  scope :no_variant,     ->            { where(variant: false)}
  scope :foul,           ->            { where("foul_name is not null") }
  scope :without_product,->            { where("product_id is null") }
  scope :only_usable,    ->            { usable.joins(:product).merge(Product.usable)}
  scope :by_brand,       ->(brand=nil)        { joins(:product).where(products: {brand: brand}) unless brand.blank?}
  scope :by_category,    ->(category=nil)     { joins(:product).where(products: {category: category}) unless category.blank?}
  scope :by_subcategory, ->(subcategory=nil)  { joins(:product).where(products: {subcategory: subcategory.uniq}) unless subcategory.blank?}
  scope :by_specs,       ->(category_name, specs) { joins(:product).where(get_specs_conditions(category_name, specs)) if !category_name.blank? and !specs.blank? }
  scope :by_model,       ->(model=nil)  { where(product_id: model) unless model.blank?}

  scope :unless_page,    ->(page=nil)  { where.not(page: page) unless page.blank?}
  scope :unless_brand,   ->(brand=nil) { joins(:product).where.not(products: {brand: brand}) unless brand.blank?}

  scope :by_param,       ->(param, values) { where(param => Utils.to_array(values)) unless values.blank?}

  # scope :by_page_or_brand, ->(page, brand) { by_page(page).union(ProductPage.by_brand(brand)) unless (page.blank? && brand.blank?)}

  attr_accessor :with_variants

  ATTRIBUTES = [:product, :page, :image_url, :url, :shipment, :date, :ean, :asin, :vendor_part_number, :mediamarkt_ref,
                :category, :foul_brand, :foul_name, :foul_description, :specs, :out_of_stock, :last_aparition, :usable,
                :with_variants, :variant, :images_counter, :customers_vote_average, :customers_comments_count]

  def name
    "#{(product.nil? ? foul_name : product.name)}"
  end

  def available_price_by_date(date = Date.today, same_day = true)
    price = (same_day ? product_prices.by_date(date) : product_prices.until_date(date)).last
    price.out_of_stock ? nil : price if price
  end

  def check_or_set_relations
    if self.product.nil? && self.ean
      self.product = Product.by_ean(self.ean).first
    end
    # if self.product_id.nil?
    #   product = Product.by_ean(self.ean).first
    #   self.last_aparition = self.date
    #   if product
    #     self.product = product
    #   else
    #     # if self.ean
    #     #   self.product = Product.create!({name:     self.name,
    #     #                                   ean:      self.ean,
    #     #                                   brand:    self.brand,
    #     #                                   category: self.category,
    #     #                                   usable:   false})
    #     # else
    #        self.foul_name = "#{(self.brand || '')}|#|#{self.name}" if self.id.nil?
    #     # end
    #   end
    # end
  end

  def clean_specs
    self.specs = Utils.clean_json(self.specs)
  end

  def current_product_price
    product_prices.where(date: Date.today)[0]
  end

  class << self
    def unique_pages
      fetch_caching_of('calculations', 'unique_pages') do
        ProductPage.pluck(:page).uniq.reject{|p| p.blank?}.sort
      end
    end

    def equivalent_urls(url)
      url_parts = url.split("://")
      case url_parts[0]
        when 'http', 'https'
          ["http","https"].map { |protocol| "#{protocol}://#{url_parts[1]}"}
        else
          url
      end
    end

    def get_specs_conditions(category_name, specs)
      specs.keys.map do |key|
        original_key = Product.map_specs_keys[category_name][key]
        "products.specs->>'#{original_key}' in ('#{specs[key].join("','")}')"
      end.join(' and ')
    end
  end

  private

  def ensure_format_ean
    self.ean = self.ean.rjust(13, '0')
  end
end
