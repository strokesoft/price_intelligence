# == Schema Information
#
# Table name: product_prices
#
#  id              :integer          not null, primary key, indexed => [date]
#  product_page_id :integer          indexed
#  order           :integer
#  price           :float            not null
#  date            :date             indexed, indexed => [id], indexed => [out_of_stock]
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  out_of_stock    :boolean          indexed, indexed => [date]
#
# Indexes
#
#  index_product_prices_on_date                   (date)
#  index_product_prices_on_id_and_date            (id,date)
#  index_product_prices_on_out_of_stock           (out_of_stock)
#  index_product_prices_on_out_of_stock_and_date  (out_of_stock,date)
#  index_product_prices_on_product_page_id        (product_page_id)
#

class ProductPrice < ApplicationRecord
  belongs_to :product_page
  has_one :product, through: :product_page

  validates  :price,      presence:     true
  # validates  :product_id, uniqueness: { scope: [:product_page_id, :date, :price] }

  default_scope { order(date: :asc) }

  scope :by_product_page_id,    ->(id)      { where(product_page_id: id) }
  #scope :by_page,      ->(id)    { where(page:            id)}
  scope :by_page,       ->(pages=nil)       { joins(:product_page).where(product_pages: {page:     Array(pages)})      unless pages.blank? }
  scope :by_category,   ->(categories=nil)  { joins(:product_page).where(product_pages: {category: Array(categories)}) unless categories.blank? }
  scope :by_date,       ->(date)            { where(date:        date).order(:id) }
  scope :until_date,    ->(date)            { where("date <= ?", date).order(:id) }
  scope :with_stock,    ->(with_stock=true) { where(out_of_stock: [false, nil]) if with_stock }
  # Using alias
  scope :stock_using_p, ->(with_stock=true) { where('p.out_of_stock' => [false, nil]) if with_stock }
  scope :like_price,    ->(price)           { where("product_prices.price like ?", "%#{price}%") }
  scope :without_stock, ->                  { where(out_of_stock: true)}

  attr_accessor :second_hand
  attr_accessor :sponsored
  attr_accessor :from_marketplace
  attr_accessor :market_price

  ATTRIBUTES = [:product_page, :order, :price, :date, :out_of_stock, :second_hand, :sponsored, :from_marketplace,
                :market_price]

  def page
    product_page.page
  end

  def url
    product_page.url
  end

  def usable
    product_page.usable
  end

  class << self
    alias_method :stock, :with_stock

    def get_last_date
      fetch_caching_of('calculations', 'last-date-in-product-prices', type: 'Date') do
        ProductPrice.order(:date).last.date rescue Date.today
      end
    end
  end

  DISCARD_UNIQUE_PRICE_ON_REPORT_FOR_PAGE = ["tiendaazul"]

  HEAD_LEFT = ['Brand', 'Name', 'EAN', 'Vendor Part Number', 'Category', 'Subcategory', 'Msrp', 'Specs']

  HEAD = HEAD_LEFT + Utils.page_names + ['Year', 'Month', 'Day']

  def self.daily_report(date = Date.today)
    report_fast(date, date, nil, true)
  end

  def self.report(first_date = '2017-07-03', last_date = Date.today)
    first_date = Date.parse(first_date) if first_date.instance_of? String
    last_date  = Date.parse(last_date)  if last_date.instance_of? String

    sheet_name = "#{first_date.strftime('%y%m%d')}" + (first_date < last_date ? "-#{last_date.strftime('%y%m%d')}" : "")
    workbook_name = "#{sheet_name}_price_benchmark_extended.xlsx"
    Axlsx::Package.new do |p|
      p.workbook.add_worksheet(:name => sheet_name) do |sheet|
        sheet.add_row (HEAD)
        first_date.upto(last_date) do |date|
          previous_product = nil
          pprices = {}
          Product.usable.order(:brand, :name).each do |product|
            #product.product_prices_by_filtering_unique_page_by_date('tiendaazul', date)  # use this when implement product_eans table.
            if (previous_product.try(:brand) == product.brand and previous_product.try(:name) == product.name)
              puts "Producto duplicado: [#{previous_product.id}]: [#{product.id}] - [#{product.brand}] - [#{product.name}]"
            else
              add_row(sheet, previous_product, pprices) if pprices.size > 0
              pprices = {}
              previous_product = product
            end
            product.prices_info_by_date(date).each do |pprice|
              page = pprice.page
              unless pprices[page].nil?
                puts "Varios precios #{pprices[page].price == pprice.price ? "iguales" : "DISTINTOS!!!!"} en una misma tienda: [#{page}] - [#{product.brand}] - [#{product.name}]:\n\t[#{pprices[page].id}] - [#{pprices[page].price}] - [#{pprices[page].url}]\n\t[#{pprice.id}] - [#{pprice.price}] - [#{pprice.url}]"
              end

              pprices[page] = pprice if (pprices[page].nil? || ((pprice.usable || !pprices[page].usable) and pprice.price.to_f < pprices[page].price.to_f))
            end
          end
          add_row(sheet, previous_product, pprices) if pprices.size > 0
        end
      end
      p.serialize(workbook_name)
      system 'mv', workbook_name, Rails.configuration.route_to_xlsx_files
    end
    "#{Rails.configuration.route_to_xlsx_files}/#{workbook_name}"
  end

  def self.add_row(sheet, product, pprices)
    return if pprices.size == 1 and DISCARD_UNIQUE_PRICE_ON_REPORT_FOR_PAGE.include? pprices.keys[0] # Evitamos generar lineas con solo precios existentes de paginas en: DISCARD_UNIQUE_PRICE_ON_REPORT_FOR_PAGE

    # Clean no usable and out_of_stock prices
    pprices.each{ |page, pprice| pprices.delete(page) if (!pprices[page].try(:usable) || pprices[page].try(:out_of_stock))}

    return if pprices.size == 0 # Evitamos generar lineas con solo precios de: ['tiendaazul']

    date = pprices.first[1].date

    prices = PageBase::PAGES.map{ |q| pprices[q].price if (pprices[q] and pprices[q].usable)}
    urls   = PageBase::PAGES.map{ |q| pprices[q].url   if (pprices[q] and pprices[q].usable)}

    row = sheet.add_row  [product.brand, product.name, product.ean, product.category, product.subcategory, product.msrp] +
                             prices +
                             [date.year, date.month, date.day],
                         :types => [:string, :string, :string, :string, :string, :integer] # :date, time, :float, :integer, :string, :boolean

    cell_offset = 'A'.ord + HEAD_LEFT.size
    (0..PageBase::PAGES.count-1).each { |index| sheet.add_hyperlink(:location => urls[index], :ref => "#{(cell_offset+index).chr}#{row.index + 1}") if urls[index] }
  end

  def self.report_fast(first_date = '2017-07-03', last_date = Date.today, categories = nil, add_links = false)
    first_date = Date.parse(first_date) if first_date.instance_of? String
    last_date  = Date.parse(last_date)  if last_date.instance_of? String

    sheet_name = "#{first_date.strftime('%y%m%d')}" + (first_date < last_date ? "-#{last_date.strftime('%y%m%d')}" : "")
    workbook_name = "#{sheet_name}_price_benchmark_extended.xlsx"
    Axlsx::Package.new do |p|
      # p.use_shared_strings = false
      p.workbook.add_worksheet(:name => sheet_name) do |sheet|
        sheet.add_row (HEAD)
        first_date.upto(last_date) do |date|
          sql = <<-SQL
  SELECT
    product_prices.price,

    product_pages.page,
    product_pages.url,
    product_pages.product_id,

    products.brand,
    products.name,
    products.ean,
    products.vendor_part_number,
    products.category,
    products.subcategory,
    NULLIF(products.msrp ,'') as msrp,
    products.specs

  FROM
    "product_prices"
    INNER JOIN "product_pages" ON "product_pages"."id" = "product_prices"."product_page_id"
    INNER JOIN "products" ON "products"."id" = "product_pages"."product_id"

  WHERE
    #{"products.category IN (#{categories.map(&:inspect).join(', ').gsub! '"', '\''}) AND" unless categories.blank?}
    products.usable = true AND
    product_pages.usable = true AND
    ("product_prices"."out_of_stock" = 'f' OR "product_prices"."out_of_stock" IS NULL) AND
    "product_prices"."date" = '#{date}'
  ORDER BY
    "products"."brand",
    "products"."name"
          SQL

          previous_product = {}
          pprices = {}
          ActiveRecord::Base.connection.select_all(sql).rows.map do |price, page, url, product_id, brand, name, ean,
              vendor_part_number, category, subcategory, msrp, specs|
            if (previous_product[:product_id] != product_id)
              if (previous_product[:brand] == brand and previous_product[:name] == name)
                Rails.logger.warn "Producto duplicado: [#{previous_product[:product_id]}]: [#{product_id}] - [#{brand}] - [#{name}]"
              else
                add_row_fast(sheet, date, previous_product, pprices, add_links)
                pprices = {}
                previous_product = {product_id: product_id, brand: brand, name: name, ean: ean,
                                    vendor_part_number: vendor_part_number, category: category, subcategory: subcategory,
                                    msrp: msrp, specs: specs}
              end
            end

            unless pprices[page].nil?
              Rails.logger.warn "Varios precios #{pprices[page][:price] == price ? "iguales" : "DISTINTOS!!!!"} en una misma tienda: [#{page}] - [#{brand}] - [#{name}]:
                    \t[#{pprices[page][:price]}] - [#{pprices[page][:url]}]
                    \t[#{price}] - [#{url}]"
            end

            pprices[page] = {price: price, url: url} if (pprices[page].nil? || price.to_f < pprices[page][:price].to_f)
          end
          add_row_fast(sheet, date, previous_product, pprices, add_links)
          add_links = false
        end
      end
      Rails.logger.info "Serializando INI"
      p.serialize(workbook_name)
      Rails.logger.info "Serializado FIN"
      system 'mv', workbook_name, Rails.configuration.route_to_xlsx_files
    end
    "#{Rails.configuration.route_to_xlsx_files}/#{workbook_name}"
  end

  def self.add_row_fast(sheet, date, product, pprices, add_links)
    return if pprices.size == 0
    return if pprices.size == 1 and DISCARD_UNIQUE_PRICE_ON_REPORT_FOR_PAGE.include? pprices.keys[0] # Evitamos generar lineas con solo precios existentes de paginas en: DISCARD_UNIQUE_PRICE_ON_REPORT_FOR_PAGE

    prices = PageBase::PAGES.map{ |q| pprices[q][:price] if (pprices[q])}
    urls   = PageBase::PAGES.map{ |q| pprices[q][:url]   if (pprices[q])} if add_links

    row = sheet.add_row  [product[:brand], product[:name], Product.formatted_ean(product[:ean]), product[:vendor_part_number],
                          product[:category], product[:subcategory], product[:msrp], product[:specs]] + prices +
                             [date.year, date.month, date.day],
                         :types => [:string, :string, :string, :string, :string, :string, :integer, :string] # :date, time, :float, :integer, :string, :boolean

    if add_links
      (0..PageBase::PAGES.count-1).each { |index| sheet.add_hyperlink(:location => urls[index], :ref => "#{calculate_excel_column(index)}#{row.index + 1}") if urls[index] }
    end
  end

  def self.calculate_excel_column(index)
    a = 'A'.ord
    z = 'Z'.ord
    az = z - a + 1
    position = HEAD_LEFT.size + index

    position >= az ?
        "#{(a + (position / az) - 1).chr}#{(a + (position % az)).chr}" :
        (a + position).chr
  end

  def self.price_over(low_price)
    "price >= #{low_price}" unless low_price.blank?
  end

  def self.price_under(high_price)
    "price <= #{high_price}" unless high_price.blank?
  end

  def self.create_bad_prices_file(low_price = 1000, high_price = nil)
    sheet_name = "Bad products #{Date.today}"
    workbook_name = "bad_prices_report_#{Date.today}.xlsx"
    Axlsx::Package.new do |p|
      p.workbook.add_worksheet(:name => sheet_name) do |sheet|
        head = ['id', 'price', 'date', 'page', 'Brand', 'Name', 'EAN', 'ASIN', 'Vendor Part Number', 'Mediamarkt Ref',
                'Category', 'Subcategory'] + Utils.page_names
        sheet.add_row (head)
#        ProductPrice.where("price SIMILAR TO '(%-%|%.%.%|%.___%|</div)'").order(:date).each do |pprice|
        ProductPrice.where(price_over(low_price)).where(price_under(high_price)).order(:price).each do |pprice|
          product = pprice.product

          available_product_prices = {}
          product.prices_info_by_date(pprice.date).each { |price| available_product_prices[price.page] = price } if product

          prices = PageBase::PAGES.map{ |q| available_product_prices[q].price if available_product_prices[q]}

          row = sheet.add_row [pprice.id, pprice.price, pprice.date, pprice.page, product.try(:brand), product.try(:name),
                               Product.formatted_ean(product.try(:ean)), product.try(:asin), product.try(:vendor_part_number),
                               product.try(:mediamarkt_ref), product.try(:category), product.try(:subcategory)] + prices
          sheet.add_hyperlink(:location => pprice.url, :ref => "C#{row.index + 1}")
        end
      end
      p.serialize(workbook_name)
      system 'mv', workbook_name, Rails.configuration.route_to_xlsx_files
    end
    "#{Rails.configuration.route_to_xlsx_files}/#{workbook_name}"
  end

  def self.import_bad_prices_file(bad_prices_file)
    errors = []

    if File.extname(bad_prices_file.path) == ".xlsx"
      spreadsheet = Roo::Excelx.new(bad_prices_file.path) #, packed: false, file_warning: :ignore)
    else
      spreadsheet = Roo::Excel.new(bad_prices_file.path) #, packed: false, file_warning: :ignore)
    end

    ActiveRecord::Base.transaction do
      spreadsheet.each_with_pagename do |category, sheet|
        begin
          head = sheet.row(1).map { |head| head.try(:downcase).try(:to_sym) }
          Rails.logger.info "head: #{head}"

          indexes = {}
          [:id, :price].each { |head_name| indexes[head_name] = head.index(head_name) if head.index(head_name)}

          (2..sheet.last_row).each do |index|
            row = sheet.row(index)

            Rails.logger.info "id   : #{row[indexes[:id]].to_i}"
            Rails.logger.info "price: #{row[indexes[:price]]}"
            begin
              ProductPrice.find(row[indexes[:id]].try(:to_i)).update_attributes price: row[indexes[:price]]
            rescue => ex
              message = "ERROR [ #{ex.class}: #{ex.message} ]"

              Rails.logger.error message
              Rails.logger.error "Backtrace:\n\t#{ex.backtrace.join("\n\t")}"

              errors << CsvError.create({source_line: row.to_s, error: message, mode: 'bad_prices_file'})
            end
          end
        rescue => ex
          message = "ERROR [ #{ex.class}: #{ex.message} ]"

          Rails.logger.error message
          Rails.logger.error "Backtrace:\n\t#{ex.backtrace.join("\n\t")}"

          errors << CsvError.create({error: message, mode: 'bad_prices_file'})
        end
      end
      raise ActiveRecord::Rollback unless errors.empty?
    end
    clear_caching_of('calculations') if errors.empty?

    return errors
  end

  def self.var_daily_report(day_2 = Date.today, day_1 = nil)
    day_1 = Date.parse(day_1) if day_1.instance_of? String
    day_2 = Date.parse(day_2) if day_2.instance_of? String

    day_1 = day_2 - 1 if day_1.blank?

    Rails.logger.info "********* #{day_1} - #{day_2} ********"

    sheet_name = "#{day_1.strftime('%y%m%d')}-#{day_2.strftime('%y%m%d')}"
    workbook_name = "#{sheet_name}_price_var_daily_report.xlsx"
    Axlsx::Package.new do |p|
      p.workbook.add_worksheet(:name => sheet_name) do |sheet|
        sheet.add_row (['URL', 'Brand', 'Name', 'EAN', 'ASIN', 'Vendor Part Number', 'Mediamarkt Ref', 'Category',
                        'Subcategory', 'Msrp', 'eCommerce', day_1, day_2, 'Var.', 'Var. %', 'Last ASP'])
        index = 1
        ProductPage.usable.includes(:product).references(:product).merge(Product.usable).includes(:product_prices).references(:product_prices).merge(ProductPrice.where(date: [day_1, day_2])).merge(Product.order(:category, :subcategory, :name, :ean)).each do |ppage|
          row = sheet.add_row  [ppage.url, ppage.product.try(:brand), ppage.product.try(:name), Product.formatted_ean(ppage.product.try(:ean)),
                                ppage.product.try(:asin), ppage.product.try(:vendor_part_number), ppage.product.try(:mediamarkt_ref),
                                ppage.product.try(:category), ppage.product.try(:subcategory), ppage.product.try(:msrp),
                                ppage.page, ppage.product_prices.by_date(day_1).first.try(:price), ppage.product_prices.by_date(day_2).first.try(:price),
                                0, 0]#, ppage.product.asp_by_date(day_2)]
          index += 1
          # ,
          #                      :types => [:string, :string, :string, :string, :string, :integer] # :date, time, :float, :integer, :string, :boolean

          cell = sheet["N#{index}"]
          cell.type = :string         # it is important to ensure the type of the cell is set before adding the formula
          cell.value = "=M#{index}-L#{index}"

          cell = sheet["O#{index}"]
          cell.type = :string         # it is important to ensure the type of the cell is set before adding the formula
          cell.value = "=100*N#{index}/L#{index}"
        end
        p.serialize(workbook_name)
        system 'mv', workbook_name, Rails.configuration.route_to_xlsx_files
      end
      "#{Rails.configuration.route_to_xlsx_files}/#{workbook_name}"
    end
  end

  def self.create_daily_offers_report(date = Date.today, categories = nil)
    date = Date.parse(date) if date.is_a? String

    sheet_name = date.strftime('%y%m%d')
    workbook_name = "#{sheet_name}_daily_offers_report.xlsx"

    p = Axlsx::Package.new
    p.workbook.add_worksheet(name: sheet_name) do |sheet|
      columns = %w(Brand Name EAN Category Subcategory eCommerce URL Price Date)
      sheet.add_row(columns)

      fields = 'products.brand, products.name, products.ean, products.category, products.subcategory, ' +
               'product_pages.page, product_pages.url, product_prices.price, product_prices.date'

      query = ProductPrice.joins(:product, :product_page)
        .select(fields)
        .where(date: date)

      query = query.where('products.category IN (?)', categories) if categories.present?

      query.each_with_index do |offer, i|
        types = [:string, :string, :string, :string, :string, :string, :string, :float, :date]
        row = [
          offer['brand'], offer['name'], offer['ean'], offer['category'], offer['subcategory'],
          offer['page'], offer['url'], offer.price, offer.date
        ]

        sheet.add_row row, types: types
      end

      sheet.add_table "A1:G#{query.count('*')}"
    end

    p.serialize("#{Rails.configuration.route_to_xlsx_files}/#{workbook_name}")
  end

  # Clone prices for the 'date' date and the indicated pages on the dates from start_date to end_date
  # Example uses from command line:
  # bundle exec bin/rails runner -e production "ProductPrice.clone_prices('2018-05-11', 'carrefour',  '2018-05-12')"
  # bundle exec bin/rails runner -e production "ProductPrice.clone_prices('2018-05-11', ['carrefour', 'carrefourmarket'],  '2018-05-12', '2018-05-13')"
  def self.clone_prices(from_date, pages, categories, start_date = Date.today, end_date = start_date)
    # prices = ProductPrice.joins(:product_page).where('product_prices.date' => date, 'product_pages.page': Array(pages))
    prices = ProductPrice.by_date(from_date).by_page(pages).by_category(categories)
    prices_selected = prices.count
    prices_created  = 0
    start_date = Date.parse(start_date) if start_date.instance_of? String
    end_date = Date.parse(end_date)     if   end_date.instance_of? String
    (start_date..end_date).each do |date|
      puts("Cloning from date: #{from_date}, to date: #{date}")
      cloned_prices = 0
      prices.each do |pp|
        if (ProductPrice.by_date(date).by_product_page_id(pp.product_page_id).count == 0)
          new_pp = pp.dup
          new_pp.date = date
          new_pp.save
          cloned_prices += 1
        end
      end
      puts("Cloned prices: #{cloned_prices}")
      prices_created += cloned_prices
    end
    clear_caching_of('calculations')
    "Precios encontrados para (#{from_date}, #{pages}, #{categories}): #{prices_selected}, precios clonados (#{start_date}#{"-#{end_date}" if end_date != start_date}): #{prices_created}#{prices_created < prices_selected ? ", el resto de precios ya existían y se han mantenido" : ""}."
  end
  # Ver:             Utils.populate_prices(date = Date.today)
  # Ver:             Utils.duplicate_empty_prices(date = Date.today, page = nil)
end
