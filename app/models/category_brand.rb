# == Schema Information
#
# Table name: category_brands
#
#  id          :integer          not null, primary key
#  category_id :integer          indexed, indexed => [brand_id]
#  brand_id    :integer          indexed, indexed => [category_id]
#
# Indexes
#
#  index_category_brands_on_brand_id                  (brand_id)
#  index_category_brands_on_category_id               (category_id)
#  index_category_brands_on_category_id_and_brand_id  (category_id,brand_id) UNIQUE
#

class CategoryBrand < ApplicationRecord
  # Belonging relationships
  belongs_to :category, inverse_of: :category_brands
  belongs_to :brand, inverse_of: :category_brands
end
