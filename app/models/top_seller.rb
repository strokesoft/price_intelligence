# == Schema Information
#
# Table name: top_sellers
#
#  id              :integer          not null, primary key, indexed => [date]
#  product_page_id :integer          indexed
#  date            :date             indexed, indexed => [id], indexed => [out_of_stock]
#  topseller_order :integer          not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_product_prices_on_date                   (date)
#  index_product_prices_on_id_and_date            (id,date)
#  index_product_prices_on_product_page_id        (product_page_id)
#

class TopSeller < ApplicationRecord
  belongs_to :product_page
  has_one :product, through: :product_page

  validates :date, presence: true
  validates :topseller_order, presence: true

  # validates  :page, uniqueness: { scope: [:page, :topseller_order] }

  default_scope { order(date: :asc) }

  scope :by_product_page_id, ->(id) { where(product_page_id: id)}
  scope :by_page,       ->(page=nil) { joins(:product_page).where(product_pages: {page: page}) unless page.blank?}
  scope :by_date,       ->(date)  { where(date:          date).order(:id)}
  #scope :until_date,    ->(date)  { where("date <= ?",   date).order(:id)}
  # Using alias

  ATTRIBUTES = [:product_page, :topseller_order, :date]


  def page
    product_page.page
  end




  def self.create_report(first_date = Date.today, reduce_url = false, last_date = nil)
    first_date = Date.parse(first_date) if first_date.instance_of? String
    last_date  = Date.parse(last_date)  if last_date.instance_of? String
    last_date  = first_date  unless last_date

    CSV.open("#{Rails.configuration.route_to_xlsx_files}/Stramina_#{first_date.strftime("%Y_%m_%d")}_MU.txt", "wb", col_sep: "^") do |csv|
#    CSV.generate(col_sep: "^") do |csv|
      csv << ["Country","Ranking","URL","ASIN","PartNumber","Brand","Model","Category","Capacity","FormFactor","Family","Speed","Kit","Qty","Serie","Price","Rating","Reviews","Date"]

      first_date.upto(last_date) do |date|
        sql = <<-SQL
SELECT
  product_pages.page,

  top_sellers.topseller_order,

  product_pages.url,

  products.asin,
  product_pages.asin,

  products.vendor_part_number,
  products.brand,
  products.name,

  product_pages.category,

  products.specs,

  product_prices.price,

  product_pages.customers_vote_average,
  product_pages.customers_comments_count
FROM
  "product_prices"
  INNER JOIN "product_pages" ON "product_pages"."id" = "product_prices"."product_page_id"
  INNER JOIN "top_sellers"   ON "product_pages"."id" = "top_sellers"."product_page_id"
  LEFT  JOIN "products" ON "products"."id" = "product_pages"."product_id"

WHERE
  "product_prices"."date" = '#{date}' AND
  "top_sellers"."date" = '#{date}'
ORDER BY
  "page",
  "category",
  "topseller_order"
        SQL

        ActiveRecord::Base.connection.select_all(sql).rows.map do |page, topseller_order, url, asin, pp_asin, vendor_part_number, brand, name, category, specs, price, customers_vote_average, customers_comments_count|
          asin = asin || pp_asin
          url = "#{"Page::#{page.capitalize}".safe_constantize::HTTPS_AMAZON}/dp/#{asin}" unless asin.blank? if reduce_url
          csv << [Utils.country_code(page), topseller_order, url, asin, vendor_part_number, brand, name, category,
                  specs.try(:[],"capacity"), specs.try(:[],"form factor"), specs.try(:[],"family"), specs.try(:[],"speed"), specs.try(:[],"kit or single"),
                  specs.try(:[],"kit qty"), specs.try(:[],"serie"), price, customers_vote_average, customers_comments_count, date.strftime("%d/%b/%Y")]
        end
      end
    end
  end
end
