# == Schema Information
#
# Table name: society_brands
#
#  id           :integer          not null, primary key
#  society_id   :integer          indexed, indexed => [brand_id]
#  brand_id     :integer          indexed, indexed => [society_id]
#  is_reference :boolean          default(FALSE)
#
# Indexes
#
#  index_society_brands_on_brand_id                 (brand_id)
#  index_society_brands_on_society_id               (society_id)
#  index_society_brands_on_society_id_and_brand_id  (society_id,brand_id) UNIQUE
#

class SocietyBrand < ApplicationRecord
  # Belonging relationships
  belongs_to :society, inverse_of: :society_brands
  belongs_to :brand, inverse_of: :society_brands

  delegate :name, to: :brand
end
