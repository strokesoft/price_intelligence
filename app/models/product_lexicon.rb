# == Schema Information
#
# Table name: product_lexicons
#
#  id          :integer          not null, primary key
#  name        :string
#  ean         :string
#  product_id  :integer          indexed
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  aproximated :boolean
#
# Indexes
#
#  index_product_lexicons_on_product_id  (product_id)
#

class ProductLexicon < ApplicationRecord
  include PgSearch

  pg_search_scope :search_for, against: %i(name)

  # belongs_to :product
  # has_many   :product_pages

  validates  :name,            presence: true
  validates  :name, uniqueness: { scope: :ean }

  scope :by_name,     ->(name) { where(name: name)}
  scope :like_name,   ->(name) { where("product_lexicons.name like ?", "%#{name}%")}
  scope :by_ean,      ->(ean)  { where(ean:  Product.formatted_ean(ean))}
  scope :without_ean, ->       { where("ean is null")}
  scope :with_ean,    ->       { where("ean is not null")}

  before_validation :check_ean

  def check_ean
    if self.ean
      self.ean = self.ean.delete("\\") if self.ean.include? "\\"
      self.ean = self.ean.delete("\[") if self.ean.include? "\["
      self.ean = self.ean.delete("\]") if self.ean.include? "\]"
      self.ean = self.ean.delete("\"") if self.ean.include? "\""
    end
  end

  def find_ean
    name_parts = self.name.split(' ')
    eans       = []
    name_parts.each do |part|
      eans << ProductLexicon.like_name(part).all.map(&:ean)
    end
    self.ean = eans.flatten.uniq
  end

  def self.fix_by_aproximation
    lexicons = []
    ProductLexicon.without_ean.all.each do |p|
      lexicons << [ProductLexicon.search_for(p.name).with_ean.all , p]
    end
    uniq_pages = lexicons.map{|q| q if q[0].size > 0}.compact.uniq
    uniq_pages.each do |page|
      begin
        page[1].ean = page[0].to_a.first.ean.delete("\\").delete("\[").delete("\]").delete("\"")
        page[1].save!
      rescue
        page[1].product_pages.each do |pr_page|
          pr_page.product_lexicon = page[0].to_a.first
          pr_page.save
        end
        page[1].destroy
      end
    end
    lost_pages = lexicons.map{|q| q[1].product_pages if q[0].size == 0}.compact.map{|q| q.map{|a| a }}.flatten.uniq
    filters    = ["q.product_lexicon.name.split(',')[0].split('-')[1]",
                  "q.product_lexicon.name.split(' ')[1]",
                  "q.product_lexicon.name.split(',')[0]",
                  "q.product_lexicon.name.split(' ')[0]",
                  "q.product_lexicon.name.split(' ')[2]",
                  "q.product_lexicon.name.split(' ')[3]",
                  "q.product_lexicon.name.split(' ')[4]",
                  "q.product_lexicon.name.split(' ')[5]",
                  "q.product_lexicon.name.split(' ')[6]",
                  "q.product_lexicon.name.split(' ')[7]",
                  "q.product_lexicon.name.split(' ')[1].to_s + ' ' + q.product_lexicon.name.split(' ')[2].to_s",
                  "q.product_lexicon.name.split(' ')[2].to_s + ' ' + q.product_lexicon.name.split(' ')[3].to_s",
                  "q.product_lexicon.name.split(' ')[3].to_s + ' ' + q.product_lexicon.name.split(' ')[4].to_s",
                  "q.product_lexicon.name.split(' ')[4].to_s + ' ' + q.product_lexicon.name.split(' ')[5].to_s",
                  "q.product_lexicon.name.split(' ')[3].to_s + ' ' + q.product_lexicon.name.split(' ')[4].to_s + ' ' + q.product_lexicon.name.split(' ')[5].to_s",
                  "q.product_lexicon.name.split(' ')[3].to_s + ' ' + q.product_lexicon.name.split(' ')[4].to_s + ' ' + q.product_lexicon.name.split(' ')[5].to_s + ' ' + q.product_lexicon.name.split(' ')[6].to_s"
                  ]
    mess         = []
    to_revision  = []
    filters.each do |filter|
      partial_names = lost_pages.map{|q| [ eval(filter), q]}
      found_eans    = []
      partial_names.each do |q|
        found_lexicons = ProductLexicon.search_for(q[0]).all
        found_eans     << [found_lexicons.map(&:ean).uniq.compact, q[1], found_lexicons.map(&:name).uniq.compact]
      end
      found_eans.select{|q| q[0].size == 1}.each do |lexicon|
        to_revision << [lexicon , filter].flatten
        lex         = lexicon[1].product_lexicon
        lex.ean     = lexicon[0].first
        begin
          lex.save!
        rescue => ex
          mess      << ex.message
          if ex.message.include? 'already been taken'
            mess    << lexicon
          end
        end
      end
    end
    unless to_revision.empty?
      CSV.open('uncertain_ean.csv', "w", {headers: true, col_sep: ';', quote_char: '"'}) do |csv|
        csv << ['name', 'ean']
        to_revision.each do |line|
          csv << [line[3], line[1].product_lexicon.name, line[0].first, line[2]].flatten
        end
      end
    end
    mess.each do |lex|
      #hacer algo con los dupes
    # lex.product_pages.each do |pr_page|
    #   pr_page.product_lexicon = lexicon[1].product_lexicon.delete("\\").delete("\[").delete("\]").delete("\"")
    #   pr_page.save
    # end
    # lex.save
    end
  end

  def self.load_ean_list
    files = ['/home/raul/price_intelligence_raw_data/teka_2017.csv',
             '/home/raul/price_intelligence_raw_data/teka_2017.csv']
             # '/home/raul/price_intelligence_raw_data/coverage_by_icecat.csv',]
    files.each do |file|
      CSV.foreach(file, {headers: true, header_converters: :symbol, col_sep: ';', quote_char: '"', encoding: "bom|utf-8"}) do |row|
        row             = row.to_hash
        ProductLexicon.create({name: row[:name], ean: row[:ean]})
      end
    end
  end
end



# CSV.open("product_con_marca.csv", "w", {headers: true, col_sep: ';', quote_char: '"'}) do |csv|
#   csv << ['id', 'name', 'brand', 'category', 'ean']
#   Product.all.each do |product|
#     csv << [product.id, product.name, product.brand, product.category, product.ean]
#   end
# end
