# == Schema Information
#
# Table name: society_trademarks
#
#  id           :integer          not null, primary key
#  society_id   :integer          indexed, indexed => [trademark_id]
#  trademark_id :integer          indexed => [society_id], indexed
#
# Indexes
#
#  index_society_trademarks_on_society_id                   (society_id)
#  index_society_trademarks_on_society_id_and_trademark_id  (society_id,trademark_id) UNIQUE
#  index_society_trademarks_on_trademark_id                 (trademark_id)
#

class SocietyTrademark < ApplicationRecord
  # Belonging relationships
  belongs_to :society, inverse_of: :society_trademarks
  belongs_to :trademark, inverse_of: :society_trademarks
end
