class ScrapingLog
  LOG_FILE = Rails.root.join('log', 'scraping.log').freeze

  class << self
    cattr_accessor :scrap_logger
    delegate :debug, :info, :warn, :error, :fatal, :to => :scrap_logger
  end
end