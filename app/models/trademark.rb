# == Schema Information
#
# Table name: brands
#
#  id           :integer          not null, primary key
#  name         :string           indexed
#  slug         :string           indexed
#  picture      :string
#  is_reference :boolean          default(FALSE)
#  color        :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_brands_on_name  (name)
#  index_brands_on_slug  (slug)
#

# Brand used to define MSRPs in a society
class Trademark < Brand
  # Owning relationships
  has_many :society_trademarks, inverse_of: :trademark, dependent: :destroy
  has_many :societies, through: :society_trademarks
end
