# == Schema Information
#
# Table name: society_categories
#
#  id          :integer          not null, primary key
#  society_id  :integer          indexed, indexed => [category_id]
#  category_id :integer          indexed, indexed => [society_id]
#
# Indexes
#
#  index_society_categories_on_category_id                 (category_id)
#  index_society_categories_on_society_id                  (society_id)
#  index_society_categories_on_society_id_and_category_id  (society_id,category_id) UNIQUE
#

class SocietyCategory < ApplicationRecord
  # Belonging relationships
  belongs_to :society, inverse_of: :society_categories
  belongs_to :category, inverse_of: :society_categories
end
