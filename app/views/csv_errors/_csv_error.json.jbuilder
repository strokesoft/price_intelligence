json.extract! csv_error, :id, :source_line, :error, :mode, :parser, :category, :warn, :created_at, :updated_at
json.url csv_error_url(csv_error, format: :json)
