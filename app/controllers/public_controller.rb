class PublicController < ApplicationController
  include ActionView::Helpers::NumberHelper
  include PriceCategoriesHelper
  include SearchableActions
  include ParamsUtils
  include ApplicationHelper

  has_orders %w{_name _brand _category _subcategory _page _price _first_day _last_day _last_price _previous_price}
  before_action :read_params

  POPUP = 'popup'.freeze

  def index
    prepare_data
    respond_to do |format|
      format.html
      format.js {
        render 'global/index', locals: {
          index_folder: 'public',
          templates: ['categories_content'],
          bind_elements: ['categories_content'],
          title: @title
        }
      }
    end
  end

  def products
    @mode = params[:mode] || POPUP
    @type = params[:type]
    @products = get_products_by_type(@type)
    search_resources(paginated: params[:format] != 'xlsx')

    respond_to do |format|
      format.html { render :products, layout: !helpers.is_popup_or_request_xhr? }
      format.xlsx { export_products }
    end
  end

  private

  def resource_model
    Product
  end

  def get_products_by_type(type)
    data = fetch_dashboard
    unless helpers.is_popup?
      filter_pages(data)
      filter_brands(data)
    end

    return new_or_disappeared(type, data) if %w(new disappeared).include? type

    up_or_down(type, data) if %w(up down).include? type
  end

  def new_or_disappeared(type, data)
    product_ids = data['categories'][@category]["#{type}_products_ids"]
    @num_products = data['categories'][@category]["#{type}_products"]

    return Product.none if product_ids.empty?

    select_string = "products.brand, products.name, products.category, products.subcategory, product_pages.page,
                     product_prices.price, product_prices.date,
                     (ARRAY_AGG(product_pages.image_url) OVER (PARTITION BY products.id, page))[1] AS image_url,
                     MIN(product_prices.date) OVER (PARTITION BY products.id, page) AS first_day,
                     MAX(product_prices.date) OVER (PARTITION BY products.id, page) AS LAST_DAY"

    products = CalculationsHelper.general_query(groups: [], select_string: select_string,
                                                available: get_available, start_date: current_user.min_date, end_date: Date.today,
                                                pages: @markets, brands: @brands, category: nil, subcategories: nil,
                                                min_price: 0, max_price: CalculationsHelper::MAX_PRICE, specs: nil, models: nil)
                 .where('products.id' => product_ids)

    Product.from("(#{products.to_sql}) as inner_query").select("inner_query.*").where("date = last_day")
  end

  def up_or_down(type, data)
    previous_interval = CalculationsHelper.previous_dates(@start_date, @end_date)
    product_ids = data['categories'][@category]["products_prices_#{type}_ids"]&.split(',')
    @num_products = data['categories'][@category]["products_prices_#{type}"]

    return Product.none if product_ids.blank?

    str_case = "(case when product_prices.date >= '#{previous_interval.first}' and product_prices.date <= '#{previous_interval.last}' then 'PREVIOUS' else 'CURRENT' end)"

    select_string = "products.id, products.brand, products.name, products.category,
                     products.subcategory, product_pages.page, product_prices.price,
                     (ARRAY_AGG(product_pages.image_url) OVER (PARTITION BY products.id, page))[1] AS image_url,
                     AVG(price) OVER (PARTITION BY products.id, page, #{str_case}) as avg_price"

    products = CalculationsHelper.general_query(groups: [], select_string: select_string,
                                                available: get_available, start_date: previous_interval.first, end_date: @end_date,
                                                pages: @markets, brands: @brands, category: nil, subcategories: nil,
                                                min_price: 0, max_price: CalculationsHelper::MAX_PRICE, specs: nil, models: nil)
                 .where('products.id' => product_ids)

    first_query = Product.from("(#{products.to_sql}) as first_query")
                 .select("id, image_url, brand, name, category, subcategory, #{type == 'up' ? 'max' : 'min'}(price) as last_price, #{type == 'up' ? 'min' : 'max'}(avg_price) as previous_price, page")
                 .group(%w(id image_url brand name category subcategory page))

    result = Product.select("second_query.*").from("(#{first_query.to_sql}) as second_query")
               .where("last_price <> previous_price")
    SqlLog.debug("PRODUCTS: #{product_ids}")
    SqlLog.debug("get_products_by_type (#{type}) ------------------------------------------->\n #{result.to_sql}")

    result
  end

  def export_products
    workbook_name = "#{I18n.t("public.products.title.#{@type}").parameterize.underscore}_#{@category.parameterize.underscore}.xlsx"
    case @type
      when 'new', 'disappeared' then
        Axlsx::Package.new do |p|
          p.workbook.add_worksheet(:name => I18n.t("public.products.title.#{@type}")) do |sheet|
            key = (@type == 'new') ? 'first_day' : 'last_day'
            sheet.add_row [I18n.t('activerecord.attributes.product.brand'),
                           I18n.t('activerecord.attributes.product.name'),
                           I18n.t('activerecord.attributes.product.category'),
                           I18n.t('activerecord.attributes.product.subcategory'),
                           I18n.t('activerecord.attributes.product.last_price'),
                           I18n.t("activerecord.attributes.product.#{key}"),
                           I18n.t('activerecord.attributes.product.page')], b: true
            @products.each do |product|

              sheet.add_row [product.brand, product.name, product.category, product.subcategory,
                             number_with_precision(product.price, precision: 0),
                             product.send(key), Ecommerce.map_ecommerce_name[product.page]],
                            types: [:string, :string, :string, :string, :float, :date, :string] # :date, :time, :float, :integer, :string, :boolean
            end
          end
          send_data p.to_stream.read,
                    type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                    filename: workbook_name
        end
      when 'up', 'down' then
        Axlsx::Package.new do |p|
          p.workbook.add_worksheet(:name => I18n.t("public.products.title.#{@type}")) do |sheet|
            sheet.add_row [I18n.t('activerecord.attributes.product.brand'),
                           I18n.t('activerecord.attributes.product.name'),
                           I18n.t('activerecord.attributes.product.category'),
                           I18n.t('activerecord.attributes.product.subcategory'),
                           I18n.t("activerecord.attributes.product.previous_asp"),
                           I18n.t('activerecord.attributes.product.last_price'),
                           I18n.t("activerecord.attributes.product.trend"),
                           I18n.t("activerecord.attributes.product.variance"),
                           I18n.t('activerecord.attributes.product.page')], b: true
            @products.each do |product|
              sheet.add_row [product.brand, product.name, product.category, product.subcategory,
                             number_with_precision(product.previous_price, precision: 2),
                             number_with_precision(product.last_price, precision: 2),
                             (product.last_price > product.previous_price) ? I18n.t('public.products.raise') : I18n.t('public.products.lower'),
                             number_to_percentage(Utils.variation(product.last_price, product.previous_price) * 100, precision: 2),
                             Ecommerce.map_ecommerce_name[product.page]],
                            types: [:string, :string, :string, :string, :float, :float, :string, :float, :string] # :date, :time, :float, :integer, :string, :boolean
            end
          end
          send_data p.to_stream.read,
                    type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                    filename: workbook_name
        end
    end
  end

  def fetch_dashboard
    @default_markets = pages_to_show.keys
    @default_brands = brands_to_show_filtered
    @markets = @default_markets.blank? ? nil : @default_markets
    @brands = @default_brands.blank? ? nil : @default_brands
    params.merge!({ page: @markets, brand: @brands })

    DataGroupedCalculation
      .new(@start_date, @end_date, current_user.society, {
       categories: @category,
       page: @markets,
       brand: @brands,
       listed: @filter
      }).fetch_dashboard
  end

  def prepare_data
    data = fetch_dashboard
    filter_pages(data)
    filter_brands(data)

    @values = current_user.available_root_categories.inject({}) do |values, category|
      results = []
      %w( period_average average_percentage previous_period_average
          products_prices_up products_prices_down
          availables offered new_products disappeared_products products_in_stock
          availables_filtered offered_filtered
          products_prices_up_percentage products_prices_down_percentage).each do |field|
        # Fit array with info
        if field != 'products_in_stock'
          results << data['categories'][category.to_s][field]
        else
          results << data['categories'][category.to_s]['subcategories']['']['products_in_stock']
        end
      end
      values.merge(category => results)
    end

    without_data = @values.collect do |_key, values|
      values.all?{|value| value == 0}
    end

    @warning_message = I18n.t('global.period_without_data') if without_data.all?
    @page_name = pages_names[@markets]

    SqlLog.debug("prepare_data: ---------------------------->\n#{@values}")
  end

  def products_params
    params.permit(:page, :order, :direction, :start_date, :end_date, :option, :category, :page, :brand, :type)
  end
  helper_method :products_params

  # Called by read_params
  def get_title
    @title = I18n.t('public.index.title')
  end
end
