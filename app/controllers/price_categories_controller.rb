class PriceCategoriesController < ApplicationController
  include PriceCategoriesHelper
  include ParamsUtils

  def index
    read_params
    prepare_data
    respond_to do |format|
      format.html
      format.js { render 'global/index', locals: { index_folder: 'price_categories',
                                                   templates: ['content'],
                                                   bind_elements: ['price_categories_content'],
                                                   title: @title}}
    end
  end

  private
  def prepare_data
    @default_markets = pages_to_show.keys
    @default_brands = brands_to_show.keys
    data = DataGroupedCalculation
             .new(@start_date, @end_date, current_user.society, {
               category: @category,
               subcategory: @subcategory,
               page: @markets,
               brand: @brands,
               listed: @filter
             }).fetch_price_categories(pages_to_show: pages_to_show, brands_to_show: brands_to_show)

    filter_pages(data)
    filter_brands(data, references: @default_brands)
    filter_subcategories(data, @category)

    @averages = data['averages']
  end

  def brands_to_show
    default_brands = brands_to_show_filtered
    @brands_images ||= Brand.availables_with_images(current_user).select {|key, _value| default_brands.include?(key)}
  end

  def get_title
    @title = I18n.t('price_categories.index.title', category: params[:category])
  end
end
