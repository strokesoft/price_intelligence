class ProductsController < ApplicationController
  include SearchableActions
  include ParamsUtils
  before_action :parse_search_terms, only: :index

  def index
    start_date, end_date, _last_date = get_dates
    with_stock = get_filter == :available
    @current_order = 'name'
    @current_direction = 'asc'
    @products = Product.usable
                  .by_category(params[:category])
                  .by_brand(params[:brand])
                  .by_subcategory(params[:subcategory])
                  .by_price(params[:min_price], params[:max_price])
                  .by_specs(params[:category], params[:specs]).group('products.id')
                  .by_date(start_date, end_date)
                  .by_page(params[:page])
                  .with_stock(with_stock)
    search_resources
    respond_to do |format|
      format.json { render json: @products, each_serializer: ProductSerializer,
                           meta: {total_count: @products.total_count}}
    end
  end

  private
  def resource_model
    Product
  end
end
