class CsvErrorsController < ApplicationController
  before_action :set_csv_error, only: [:show, :edit, :update, :destroy]

  # GET /csv_errors
  # GET /csv_errors.json
  def index
    @csv_errors = CsvError.all
  end

  # GET /csv_errors/1
  # GET /csv_errors/1.json
  def show
  end

  # GET /csv_errors/new
  def new
    @csv_error = CsvError.new
  end

  # GET /csv_errors/1/edit
  def edit
  end

  # POST /csv_errors
  # POST /csv_errors.json
  def create
    @csv_error = CsvError.new(csv_error_params)

    respond_to do |format|
      if @csv_error.save
        format.html { redirect_to @csv_error, notice: 'Csv error was successfully created.' }
        format.json { render :show, status: :created, location: @csv_error }
      else
        format.html { render :new }
        format.json { render json: @csv_error.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /csv_errors/1
  # PATCH/PUT /csv_errors/1.json
  def update
    respond_to do |format|
      if @csv_error.update(csv_error_params)
        format.html { redirect_to @csv_error, notice: 'Csv error was successfully updated.' }
        format.json { render :show, status: :ok, location: @csv_error }
      else
        format.html { render :edit }
        format.json { render json: @csv_error.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /csv_errors/1
  # DELETE /csv_errors/1.json
  def destroy
    @csv_error.destroy
    respond_to do |format|
      format.html { redirect_to csv_errors_url, notice: 'Csv error was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_csv_error
      @csv_error = CsvError.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def csv_error_params
      params.require(:csv_error).permit(:source_line, :error, :mode, :parser, :category, :warn)
    end
end
