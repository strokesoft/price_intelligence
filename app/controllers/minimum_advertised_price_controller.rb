class MinimumAdvertisedPriceController < ApplicationController
  include ActionView::Helpers::NumberHelper
  include SearchableActions
  include ParamsUtils
  include ApplicationHelper

  has_orders %w{category subcategory brand name msrp}
  before_action :read_params

  def index
    @end_date = @start_date
    prepare_data
    @map_page_result = Kaminari.paginate_array(sort_results).page(params[:sheet]).per(Settings.env.app.rows_by_page_large)
    respond_to do |format|
      format.html
      format.js { render 'global/index', locals: { index_folder: 'minimum_advertised_price',
                                                   n_templates: 1,
                                                   templates: ['content'],
                                                   bind_elements: ['minimum_advertised_price_content'],
                                                   title: @title}}
      format.xlsx { export_map }
    end
  end

  private

  def ecommerces_to_show
    @ecommerces_names ||= @markets.present? ? current_user.available_ecommerces.select { |key, _value| @markets.include?(key)} :
                            current_user.available_ecommerces.first(4).to_h
  end

  def fetch_minimum_advertised_price
    @default_markets = ecommerces_to_show.keys
    @default_brands = brands_to_show unless current_user.society.trademarks.empty?
    DataGroupedCalculation
      .new(@start_date, @start_date, current_user.society, {
        category: @category,
        subcategory: @subcategory,
        page: @markets,
        brand: @brands,
        listed: @filter,
        min_price: @min_price,
        max_price: @max_price,
        specs: @specs,
        models: @models
      }).fetch_minimum_advertised_price(pages_to_show: @default_markets, brands_to_show: brands_to_show)
  end

  def prepare_data
    data = fetch_minimum_advertised_price

    filter_pages(data)
    filter_brands(data)

    filter_categories(data, current_user.available_root_categories)

    @minimum_advertised_price = data['minimum_advertised_price']
    @other_markets = current_user.available_ecommerces.except(@markets)
  end

  def brands_to_show
    @brands.blank? ? brands_by_default : @brands
  end

  def brands_by_default
    available_brands = current_user.society.trademarks.map(&:name)
    available_brands.empty? ? current_user.available_names(:brands) : available_brands
  end

  # Called by read_params
  def get_title
    @title = I18n.t('minimum_advertised_price.title', ecommerce: Ecommerce.map_ecommerce_name[params[:page]])
  end

  def products_params
    params.permit(:page, :order, :direction, :start_date, :end_date, :option,
                  :min_price, :max_price, :markets, category: [], subcategory: [], brand: [],
                  specs: Product.map_specs_keys[@category]&.keys&.map{|key| [key, []]}.to_h)
  end
  helper_method :products_params

  def export_map
    workbook_name = "minimum_advertised_price_#{@markets}_#{@start_date}.xlsx"
    Axlsx::Package.new do |p|
      p.workbook.add_worksheet(:name => "MAP #{@markets} #{@start_date}") do |sheet|
        header_row = %w(Category Subcategory Brand Name Msrp)
        header_row << Ecommerce.map_ecommerce_name[@markets]
        header_row += @other_markets.values.sort
        sheet.add_row(header_row)

        @minimum_advertised_price.each_value do |product_detail|
          price_row = product_detail['product'].values
          price_row << price_for_market(product_detail['prices'], @markets)
          @other_markets.keys.sort.each do |market|
            price_row << price_for_market(product_detail['prices'], market)
          end
          sheet.add_row(price_row)
        end
      end
      send_data p.to_stream.read,
                type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                filename: workbook_name
    end
  end

  def sort_results
    map_values = @minimum_advertised_price.values
    if @current_order.present?
      map_values = map_values.sort_by { |element| [element['product'][@current_order.downcase] ? 0 : 1,
                                                   element['product'][@current_order.downcase] || 0] }
    end
    map_values = map_values.reverse if @current_direction.present? && @current_direction == 'desc'
    map_values
  end
end