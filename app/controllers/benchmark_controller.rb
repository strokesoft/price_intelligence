class BenchmarkController < ApplicationController
  include ActionView::Helpers::NumberHelper
  include SearchableActions
  include ParamsUtils
  include ApplicationHelper

  has_orders %w{name brand subcategory msrp }
  before_action :read_params

  def index
    prepare_data
    get_total_days
    respond_to do |format|
      format.html
      format.js { render 'global/index', locals: { index_folder: 'benchmark',
                                                   n_templates: 1,
                                                   templates: ['content'],
                                                   bind_elements: ['benchmark_content'],
                                                   title: @title}}
      format.xlsx { export_summary }
    end
  end

  def products
    @ecommerces = pages_to_show
    @product_ids = fetch_benchmark['product_ids']

    if get_total_days == 1
      products_one_day
    else
      products_range_days
    end
  end

  private
  def get_total_days
    @total_days ||= (@end_date == @start_date) ? 1 : (@end_date - @start_date).to_i
  end

  def resource_model
    Product
  end

  def products_one_day
    groups = ['products.id']
    select_string = "(ARRAY_AGG(product_pages.image_url))[1] AS image_url,
                     products.brand, products.name, products.subcategory, products.specs, products.msrp,
                     STRING_AGG(product_pages.page, ',') as pages,
                     STRING_AGG(product_prices.price::character varying, ',') as prices"

    @products = CalculationsHelper
                  .general_query(groups: groups, select_string: select_string,
                                 available: get_available, start_date: @start_date, end_date: @end_date,
                                 pages: @ecommerces.keys, brands: @default_brands, category: @category,
                                 subcategories: @subcategory || current_user.available_subcategories_for(@category),
                                 min_price: @min_price, max_price: @max_price, specs: @specs, models: @model)
                  .where('products.id' => @product_ids)
    SqlLog.debug("bechmark_products_one_day-------------------------------------------------------->\n")

    search_resources(paginated: params[:format] != 'xlsx')

    respond_to do |format|
      format.js { render :products_one_day, layout: false }
      format.xlsx { export_products_one_day }
    end
  end

  def products_range_days
    groups = ['products.id', 'product_pages.page']
    select_string = "products.brand, products.name, products.subcategory, products.specs, product_pages.page,
                     AVG(product_prices.price) as avg, MAX(product_prices.price) as max, MIN(product_prices.price) as min,
                     MIN(product_prices.date) as first_day, MAX(product_prices.date) as last_day"
    @products = CalculationsHelper
                  .general_query(groups: groups, select_string: select_string,
                                 available: get_available, start_date: @start_date, end_date: @end_date,
                                 pages: @ecommerces.keys, brands: @default_brands, category: @category,
                                 subcategories: @subcategory || current_user.available_subcategories_for(@category),
                                 min_price: @min_price, max_price: @max_price, specs: @specs, models: @model)
                  .where('products.id' => @product_ids)
    SqlLog.debug("bechmark_products_range_days ---------------------------------------------------->\n")

    search_resources(paginated: params[:format] != 'xlsx')

    respond_to do |format|
      format.js { render :products_range_days, layout: false }
      format.xlsx { export_products_range_days }
    end
  end

  def fetch_benchmark
    @default_markets = pages_to_show&.keys
    @default_brands = brands_to_show_filtered
    @markets = @default_markets.blank? ? nil: @default_markets
    @brands = @default_brands.blank? ? nil: @default_brands
    params.merge!({markets: @markets, brand: @brands})

    DataGroupedCalculation
      .new(@start_date, @end_date, current_user.society, {
        category: @category,
        subcategory: @subcategory,
        page: @default_markets,
        brand: @default_brands,
        listed: @filter,
        min_price: @min_price,
        max_price: @max_price,
        specs: @specs,
        models: @models
      }).fetch_benchmark
  end

  def prepare_data
    data = fetch_benchmark

    filter_pages(data)
    filter_brands(data, references: @default_brands)
    filter_subcategories(data, @category)

    get_total_days
    @benchmark = data['benchmark']
    @num_brands = @benchmark['brands'].keys.size
    @sort_class = @num_brands > 1 ? 'sort' : nil
  end

  # Called by read_params
  def get_title
    @title = "Benchmark Category Index: #{params[:category]}"
  end

  def products_params
    params.permit(:page, :order, :direction, :start_date, :end_date, :option, :category,
                  :min_price, :max_price, subcategory: [], markets: [], brand: [], model: [],
                  specs: Product.map_specs_keys[@category]&.keys&.map{|key| [key, []]}.to_h)
  end
  helper_method :products_params

  def available_days(total_days, first_day, last_day)
    interval = (last_day - first_day).to_i
    interval = 1 if interval == 0
    number_to_percentage((interval * 100)/total_days.to_f, precision: 0)
  end
  helper_method :available_days

  def export_summary
    workbook_name = "benchmark_#{@category.parameterize.underscore}_summary.xlsx"
    tmp = Tempfile.new(['chart', '.png'])
    Utils.convert_data_url_to_image(params[:data_url], tmp.path)
    Axlsx::Package.new do |p|
      p.workbook.add_worksheet(:name => 'Summary') do |sheet|
        sheet.add_row [@category,
                       I18n.t('benchmark.summary.skus'),
                       I18n.t('benchmark.summary.asp'),
                       I18n.t('benchmark.summary.offers'),
                       I18n.t('benchmark.summary.share'),
                       I18n.t('benchmark.summary.weight')], b: true
        if @benchmark['brands'].keys.size > 1
          sheet.add_row ['Average',
                         number_with_precision(@benchmark['average']['skus'], precision: 2),
                         number_with_precision(@benchmark['average']['asp'], precision: 0),
                         number_with_precision(@benchmark['average']['offers'], precision: 2), '---', '---'],
                        types: [:string, :float, :string, :float, :string] # :date, :time, :float, :integer, :string, :boolean
          sheet.rows.last.cells[0].b = true
        end
        @benchmark['brands'].keys.sort.each do |brand|
          sheet.add_row [brand,
                         @benchmark['brands'][brand]['skus'],
                         number_with_precision(@benchmark['brands'][brand]['asp'], precision: 0),
                         @benchmark['brands'][brand]['offers'],
                         (number_to_percentage(@benchmark['brands'][brand]['offers'] * 100.0 / @benchmark['summations']['total_offers'], precision: 2) rescue 0),
                         number_to_percentage(@benchmark['brands'][brand]['weight'], precision: 0)],
                        types: [:string, :integer, :string, :integer, :string] # :date, :time, :float, :integer, :string, :boolean
          sheet.rows.last.cells[0].b = true
        end
      end
      # Add chart
      p.workbook.add_worksheet(:name => I18n.t('benchmark.tabs.chart')) do |sheet|
        sheet.add_image(:image_src => tmp.path, :noSelect => true, :noMove => true) do |image|
          image.width = 1205
          image.height = 450
          image.start_at 0, 0
        end
        sheet.merge_cells("A1:Q19")
      end
      send_data p.to_stream.read,
                type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                filename: workbook_name
    end
  end

  def prices_one_day(product)
    @ecommerces.keys.map do |key|
      value = symmetrical_value(product.pages.split(','), product.prices.split(','), key)
      number_with_precision(value, precision: 0)
    end
  end

  def export_products_one_day
    workbook_name = "benchmark_#{@category.parameterize.underscore}_products_#{@start_date}.xlsx"
    head = [I18n.t('benchmark.products.brand'),
            I18n.t('benchmark.products.model'),
            I18n.t('benchmark.products.subcategory'),
            I18n.t('benchmark.products.specs'),
            I18n.t('benchmark.products.msrp')] + @ecommerces.keys.map{|key| @ecommerces[key]}
    Axlsx::Package.new do |p|
      p.use_shared_strings = true
      wrap = p.workbook.styles.add_style alignment: {wrap_text: true}
      p.workbook.add_worksheet(:name => 'Products (1 day)') do |sheet|
        sheet.add_row head, b: true
        @products.each do |product|
          sheet.add_row [product.brand, product.name, product.subcategory,
                         ActionView::Base.full_sanitizer.sanitize(pretty_print_json(product.specs, separator: "\r")),
                         number_with_precision(product.msrp, precision: 0), *prices_one_day(product)],
                        types: [:string] * head.size, # :date, :time, :float, :integer, :string, :boolean
                        style: wrap
          sheet.column_widths(*([nil] * 3 + [40] + [nil] * (@ecommerces.keys.size + 1)))
        end
      end

      send_data p.to_stream.read,
                type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                filename: workbook_name
    end
  end

  def export_products_range_days
    workbook_name = "benchmark_#{@category.parameterize.underscore}_products_#{@start_date}_#{@end_date}.xlsx"
    head = [I18n.t('benchmark.products.brand'),
            I18n.t('benchmark.products.model'),
            I18n.t('benchmark.products.subcategory'),
            I18n.t('benchmark.products.specs'),
            I18n.t('benchmark.products.ecommerce'),
            I18n.t('benchmark.products.asp'),
            I18n.t('benchmark.products.max_price'),
            I18n.t('benchmark.products.min_price'),
            I18n.t('benchmark.products.first_day'),
            I18n.t('benchmark.products.last_day'),
            I18n.t('benchmark.products.days_available')]
    Axlsx::Package.new do |p|
      p.use_shared_strings = true
      wrap = p.workbook.styles.add_style alignment: {wrap_text: true}
      p.workbook.add_worksheet(:name => 'Products (range of days)') do |sheet|
        sheet.add_row head, b: true
        @products.each do |product|
          sheet.add_row [product.brand, product.name, product.subcategory,
                         ActionView::Base.full_sanitizer.sanitize(pretty_print_json(product.specs, separator: "\r")),
                         @ecommerces[product['page']],
                         number_with_precision(product.avg, precision: 0),
                         number_with_precision(product.max, precision: 0),
                         number_with_precision(product.min, precision: 0),
                         format_date(product.first_day), format_date(product.last_day),
                         available_days(@total_days, product.first_day, product.last_day)],
                        types: [:string] * head.size, # :date, :time, :float, :integer, :string, :boolean
                        style: wrap
          sheet.column_widths(*([nil] * 3 + [40] + [nil] * 7))
        end
      end

      send_data p.to_stream.read,
                type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                filename: workbook_name
    end
  end
end