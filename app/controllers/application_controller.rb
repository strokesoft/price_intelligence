class ApplicationController < ActionController::Base
  include Caching
  include Pundit
  include HasOrders
  include BindingHelper

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception, unless: :remote_login?

  before_action :authenticate_user!
  before_action :set_cache_headers
  before_action :set_body_class

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  def authenticate_admin!
    redirect_to new_user_session_path unless current_user.admin?
  end

  def authenticate_user!
    if request.xhr? && !current_user
      render js: "window.location.pathname='#{new_user_session_path}'"
    else
      super
    end
  end

  protected

    def set_body_class
      # Default missing photo
      gon.missing_picture = ActionController::Base.helpers.asset_url('fallback/missing_photo.jpg')

      if current_user.nil?
        @body_class = 'm--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default'
      else
        @body_class = 'm-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default'
      end
    end

    def remote_login?
      (params[:controller] == 'devise/sessions') && (params[:action] == 'create')
    end

    def after_sign_out_path_for(resource_or_scope)
      # 'http://priceintelligence.stramina.com'
      # '/'
      new_user_session_path
    end

    def configure_permitted_parameters
      devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:email) }
    end

  private
    def user_not_authorized(exception)
      policy_name = exception.policy.class.to_s.underscore
      flash[:alert] = I18n.t('pundit.user_not_authorized')
      flash[:error] = t "#{policy_name}.#{exception.query}", scope: "pundit", default: :default

      if request.xhr?
        render js: "window.location.pathname='/403.html'"
      else
        redirect_to "/403.html"
      end
    end

    def set_cache_headers
      response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
      response.headers["Pragma"]        = "no-cache"
      response.headers["Expires"]       = "Fri, 01 Jan 1990 00:00:00 GMT"
    end
end
