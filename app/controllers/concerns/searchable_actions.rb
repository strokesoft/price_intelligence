module SearchableActions
  extend ActiveSupport::Concern
  include Polymorphic

  def search_resources(paginated: true, order_made: false, reorder_nil: false)
    @resources = get_resources_instance || resource_model.all
    @resources = @resources.pg_search(@search_terms) if @search_terms.present?
    @resources = @advanced_search_terms.present? ? @resources.filter(@advanced_search_terms) : @resources

    @resources = paginated ? @resources.page(params[:sheet]) : @resources.limit(Settings.env.app.max_rows_export)
    if reorder_nil
      @resources = @resources.reorder(nil)
    elsif !order_made && !@current_order.blank?
      @resources = @resources.reorder(generate_order)
    end

    SqlLog.debug(@resources.to_sql)
    set_resources_instance
  end

  private

  def generate_order
    table_name = resource_name.pluralize
    if @current_order.start_with?('polymorphic')
      column = @current_order.gsub('polymorphic_', '')
      ["#{table_name}.#{column}_type #{@current_direction}",
       "#{table_name}.#{column}_id #{@current_direction}",
       "#{table_name}.created_at #{@current_direction}"].join(", ")
    elsif @current_order.include?('#')
      ["#{table_name}.#{@current_order.split('#')[0]} #{@current_direction}",
       "#{table_name}.#{@current_order.split('#')[1]} #{@current_direction}"].join(", ")
    elsif @current_order.match(%r(\A@(.+)\.(.+)\z))
      parts = @current_order.scan %r(\A@(.+)\.(.+)\z)
      "#{parts[0][0]}.#{parts[0][1]} #{@current_direction}"
    elsif @current_order.start_with?('_')
      "#{@current_order[1, @current_order.length]} #{@current_direction}"
    else
      "#{table_name}.#{@current_order} #{@current_direction}"
    end
  end

  def parse_search_terms
    @search_terms = params[:search] unless params[:search].blank?
  end

  def parse_advanced_search_terms
    @advanced_search_terms = params[:advanced_search] if params[:advanced_search].present?
    parse_search_date
  end

  def parse_search_date
    return unless search_by_date?
    params[:advanced_search][:date_range] = search_date_range
  end

  def search_by_date?
    params[:advanced_search] && params[:advanced_search][:date_min].present?
  end

  def search_start_date
    case params[:advanced_search][:date_min]
    when '1'
      24.hours.ago
    when '2'
      1.week.ago
    when '3'
      1.month.ago
    when '4'
      1.year.ago
    else
      Date.parse(params[:advanced_search][:date_min]) rescue nil
    end
  end

  def search_finish_date
    params[:advanced_search][:date_max].try(:to_date) || Date.today
  end

  def search_date_range
    search_start_date.beginning_of_day..search_finish_date.end_of_day
  end

  def set_search_order
    if params[:order].blank?
      params.merge!({order: 'created_at', direction: 'desc'})
    end
  end
end