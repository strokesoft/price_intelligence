module ParamsUtils
  extend ActiveSupport::Concern

  def read_params
    @start_date, @end_date, last_date = get_dates
    @title       = get_title
    @filter      = get_filter
    @markets     = params[:markets] || params[:page]
    @brands      = params[:brand]
    @category    = params[:category]
    @subcategory = params[:subcategory]
    @with_cost   = params[:with_cost]
    @min_price   = params[:min_price]
    @max_price   = params[:max_price]
    @specs       = params[:specs]
    @models      = params[:model]
    @warning_message = I18n.t('global.data_not_available') if (Date.today > last_date) && cookies[:closed_warning].nil?

    gon.category = @category
    authorize getContextFilter, :filter?
  end

  def get_dates
    last_date = ProductPrice.get_last_date
    start_date = params[:start_date].nil? ? get_caching_of(current_user.key, 'start-date', type: 'Date') : params[:start_date].to_date
    end_date = params[:end_date].nil? ? get_caching_of(current_user.key, 'end-date', type: 'Date') : params[:end_date].to_date

    if start_date.nil? || (start_date == Date.today && Date.today > last_date)
      start_date = last_date
      end_date = last_date
    end

    # Check min date
    min_date = current_user.min_date
    start_date = min_date if start_date < min_date
    end_date = min_date if end_date < min_date

    fetch_caching_of(current_user.key, 'start-date', force: true, type: 'Date', expire_at_end_of_day: true) {start_date}
    fetch_caching_of(current_user.key, 'end-date', force: true, type: 'Date', expire_at_end_of_day: true) {end_date}

    return start_date, end_date, last_date
  end

  def get_available
    @filter == :available
  end

  def get_filter
    if params[:option].blank?
      @filter = :available
    else
      @filter = (params[:option] == 'Listed') ? :listed : :available
    end
    @filter
  end

  def pages_to_show
    @pages_names ||= @markets.blank? ? pages_by_default : current_user.available_ecommerces.select { |key, _value| @markets.include?(key)}
  end

  def ajax_or_xlsx?
    request.xhr? || (params[:format] == 'xlsx') || (params[:mode] == 'page')
  end

  def pages_by_default
    by_default = ajax_or_xlsx? ? {} : current_user.reference_ecommerces_slug_and_name
    @warnings = Array(@warnings) | [:ecommerces] if by_default.empty?
    by_default
  end

  def brands_to_show_filtered
    @brands.blank? ? brands_by_default : @brands
  end

  def brands_by_default
    by_default = []
    unless ajax_or_xlsx?
      available_brands = current_user.available_names(:brands)
      reference_brands = @category.blank? ? [] : available_brands & Category.reference_brands(@category)
      by_default = reference_brands.empty? ? Brand.reference_names(current_user) : reference_brands
    end
    @warnings = Array(@warnings) | [:brands] if by_default.empty?
    by_default
  end

  def filter_pages(data)
    reference_pages = current_user.reference_ecommerces
    available_pages = current_user.available_ecommerces
    @reference_pages, @another_pages = {}, {}

    available_pages.each do |key, name|
      if reference_pages.include?(key)
        @reference_pages[key] = [name, data['markets'][key]]
      else
        @another_pages[key] = [name, data['markets'][key]]
      end
    end

    if @reference_pages.empty?
      @reference_pages = @another_pages.extract!(*@another_pages.keys.sort[0..3])
    end

    add_bindings((@reference_pages.values + @another_pages.values).to_h, prefix: 'page-')
  end

  def filter_brands(data, references: nil)
    reference_brands = references || Brand.reference_names(current_user)
    @reference_brands = data['brands'].select { |key, _value| reference_brands.include?(key)}
    @another_brands   = data['brands'].reject { |key, _value| reference_brands.include?(key)}

    if @reference_brands.empty?
      @reference_brands = @another_brands.extract!(*@another_brands.keys.sort[0..3])
    end

    add_bindings(@reference_brands, prefix: 'brand-')
    add_bindings(@another_brands, prefix: 'brand-')
  end

  def filter_subcategories(data, category)
    @subcategories = {category => data['categories'][category]['subcategories'].inject({}) do |subcategories, node|
      subcategories[node[0]] = node[1]['products_in_stock']
      subcategories
    end}

    add_bindings(@subcategories[category], prefix: "subcategory-#{category}-")
  end

  def filter_categories(data, root_categories)
    @categories = {}
    @subcategories = {}
    root_categories.each do |category|
      category_data = data['categories'][category]['subcategories']['']['products_in_stock']
      @categories[category] = category_data
      @subcategories[category] = data['categories'][category]['subcategories'].inject({}) do |subcategories, node|
        subcategories[node[0]] = node[1]['products_in_stock']
        subcategories
      end
      add_bindings(@subcategories[category], prefix: "subcategory-#{category}-")
    end
    add_bindings(@categories, prefix: 'category-')
  end

  def getContextFilter
    ContextFilter.new(filter: {root_categories: @category,
                               subcategories: @subcategory,
                               ecommerces: @markets,
                               brands: @brands,
                               specs: @specs})
  end
end