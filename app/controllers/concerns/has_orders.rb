module HasOrders
  extend ActiveSupport::Concern

  class_methods do
    def has_orders(valid_orders, *args)

      before_action(*args) do
        unless params[:order].blank?
          @valid_orders = valid_orders
          @current_order = @valid_orders.include?(params[:order]) ? params[:order] : @valid_orders.first
          @current_direction = %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
        end
      end
    end
  end
end
