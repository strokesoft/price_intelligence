class ProductEvolutionController < ApplicationController
  include ActionView::Helpers::NumberHelper
  include SearchableActions
  include ParamsUtils
  include ApplicationHelper

  has_orders %w{_brand _name _category _subcategory _previous_price _price _variance _variance_percent _page _date}
  before_action :read_params

  def index
    @products = products_list

    search_resources(paginated: params[:format] != 'xlsx')

    respond_to do |format|
      format.html { render :index, layout: !helpers.is_popup_or_request_xhr? }
      format.js {
        render 'global/index', locals: {
          index_folder: 'product_evolution',
          templates: ['products_list'],
          bind_elements: ['paginated-dialog'],
          title: @title
        }
      }
      format.xlsx { export_products_index }
    end
  end

  def show
    data = fetch_dashboard
    prepare_data(data)
    start_date = @start_date.yesterday
    @title = 'Product Price Evolution'

    select1 = 'products.id AS product_id, product_pages.page, product_prices.product_page_id, ' +
              'product_prices.price, product_prices.date, LAG(price, 1, 0::double precision) ' +
              'OVER (PARTITION BY product_page_id ORDER BY product_page_id, product_prices.date ASC) AS previous_price'

    products = CalculationsHelper.general_query(
      groups: [], select_string: select1,
      available: get_available, start_date: start_date, end_date: @end_date,
      pages: @markets, brands: @brands, category: nil, subcategories: nil,
      min_price: 0, max_price: CalculationsHelper::MAX_PRICE, specs: nil, models: nil
    ).where('products.id' => params[:id])

    @prices = ProductPrice.select('DISTINCT ON (first_query.page, first_query.date) first_query.*')
      .from("(#{products.to_sql}) AS first_query")
      .reorder('page, date, price ASC')

    prepare_data_for_show

    respond_to do |format|
      format.html { render :show }
      format.js {
        render 'global/index', locals: {
          index_folder: 'product_evolution',
          templates: ['product_details'],
          bind_elements: ['product_evolution_detail'],
          title: @title
        }
      }
      format.xlsx { export_product }
    end
  end

  private

  def products_list
    data = fetch_dashboard
    prepare_data(data)
    product_ids = data['categories'].map(&:last)
      .reduce([]) { |ids, cat| ids += cat['products_prices_changed_ids'] || [] }

    return Product.none if product_ids.blank?

    start_date = @start_date.yesterday
    lag = 'LAG(price, 1, 0::double precision) OVER (PARTITION BY product_id, product_page_id ' +
          'ORDER BY product_id, product_page_id, product_prices.date asc)'
    select1 = 'products.id AS product_id, products.brand, products.name, products.category, products.subcategory, ' +
            'product_pages.page, product_prices.product_page_id, product_prices.price, product_prices.date, ' +
            "#{lag} AS previous_price, price - #{lag} AS variance, (price - #{lag}) / NULLIF(#{lag},0) * 100 AS variance_percent"

    products = CalculationsHelper.general_query(
      groups: [], select_string: select1,
      available: get_available, start_date: start_date, end_date: @end_date,
      pages: @markets, brands: @brands, category: nil, subcategories: nil,
      min_price: 0, max_price: CalculationsHelper::MAX_PRICE, specs: nil, models: params['model']
    ).where('products.id' => product_ids)

    subquery = ProductPrice.select('DISTINCT ON (first_query.page, first_query.date) first_query.*')
      .from("(#{products.to_sql}) AS first_query")
      .where('variance <> 0 AND previous_price <> 0')
      .reorder('page, date, price ASC')

    results = ProductPrice.select('subquery.*')
      .from("(#{subquery.to_sql}) AS subquery")
      .reorder('')

    @num_products = results.count

    SqlLog.debug("PRODUCTS: #{product_ids}")
    SqlLog.debug("get_products_prices_changed ------------------------------------->\n #{results.to_sql}")

    results
  end

  def export_products_index
    @products = @products.includes(:product_page, :product).to_a
    Axlsx::Package.new do |p|
      p.workbook.add_worksheet(name: I18n.t('product_evolution.index.title')) do |p|
        header = [
          I18n.t('activerecord.attributes.product.brand'),
          I18n.t('activerecord.attributes.product.name'),
          I18n.t('activerecord.attributes.product.category'),
          I18n.t('activerecord.attributes.product.subcategory'),
          I18n.t('activerecord.attributes.product.previous_price'),
          I18n.t('activerecord.attributes.product.last_price'),
          I18n.t('activerecord.attributes.product.trend'),
          I18n.t('activerecord.attributes.product.variance_price'),
          I18n.t('activerecord.attributes.product.variance'),
          I18n.t('activerecord.attributes.product.page'),
          I18n.t('activerecord.attributes.product.date')
        ]
        p.add_row header, b: true

        types = [:string, :string, :string, :string, :float, :float, :string, :float, :float, :string, :date]
        @products.each do |product_price|
          row = [
            product_price['brand'],
            product_price['name'],
            product_price['category'],
            product_price['subcategory'],
            basic_price(product_price['previous_price'], precision: 2),
            basic_price(product_price['price'], precision: 2),
            I18n.t("public.products.#{product_price['variance'] > 0 ? 'raise' : 'lower'}"),
            product_price['variance'],
            product_price['variance_percent'],
            Ecommerce.map_ecommerce_name[product_price.page],
            product_price.date
          ]
          p.add_row row, types: types
        end
      end
      send_data p.to_stream.read,
                type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                filename: "#{I18n.t('product_evolution.index.title').parameterize.underscore}.xlsx"
    end
  end

  def export_product
    pages = @prices.group_by(&:product_page_id).map { |e| [e.first, e.last.first.page] }

    Axlsx::Package.new do |p|
      p.workbook.add_worksheet(name: I18n.t('product_evolution.index.title')) do |p|
        header = pages.reduce([I18n.t('activerecord.attributes.product.date')]) do |header, page|
          header << Ecommerce.map_ecommerce_name[page.last]
        end
        p.add_row header, b: true

        types = Array.new(header.size, :float)
        types[0] = :date

        @prices.group_by(&:date).each do |date, prices|
          row = [date]
          row = pages.reduce([date]) do |row, page|
            row << prices.find { |e| e.product_page_id == page.first }&.price
          end
          p.add_row row, types: types
        end
      end

      send_data p.to_stream.read,
                type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                filename: "#{I18n.t('product_evolution.show.title', product: @product.name).parameterize.underscore}.xlsx"
    end
  end

  def prepare_data_for_show
    @product = @prices&.first&.product
    @default_markets = pages_to_show&.keys
    @markets = @default_markets.blank? ? nil : @default_markets

    data = { 'markets' => Hash.new(0) }
    data['markets'] = ProductPrice.select('page, COUNT(page)')
      .from("(#{@prices.to_sql}) AS counter GROUP BY page")
      .reorder('')
      .reduce({}) { |h,p| h.update(p['page'] => p['count']) }

    filter_pages(data)
  end

  def prepare_data(data)
    filter_pages(data)
    filter_brands(data)

    @values = current_user.available_root_categories.inject({}) do |values, category|
      results = []
      %w( period_average average_percentage previous_period_average
          products_prices_up products_prices_down
          availables offered new_products disappeared_products products_in_stock
          availables_filtered offered_filtered
          products_prices_up_percentage products_prices_down_percentage).each do |field|
        # Fit array with info
        if field != 'products_in_stock'
          results << data['categories'][category.to_s][field]
        else
          results << data['categories'][category.to_s]['subcategories']['']['products_in_stock']
        end
      end
      values.merge(category => results)
    end

    without_data = @values.collect do |_key, values|
      values.all?{|value| value == 0}
    end

    @warning_message = I18n.t('global.period_without_data') if without_data.all?
    @page_name = pages_names[@markets]

    SqlLog.debug("prepare_data: ---------------------------->\n#{@values}")
  end

  def fetch_dashboard(force: false)
    @default_markets = pages_to_show.keys
    @default_brands = brands_to_show_filtered
    @markets = @default_markets.presence
    @brands = @default_brands.presence
    params.merge!({ page: @markets, brand: @brands })

    DataGroupedCalculation.new(@start_date, @end_date, current_user.society, {
      categories: @category,
      page: @markets,
      brand: @brands,
      listed: @filter
    }).fetch_dashboard(force)
  end

  def resource_model
    Product
  end

  def get_title
    @title = I18n.t('product_evolution.index.title')
  end
end
