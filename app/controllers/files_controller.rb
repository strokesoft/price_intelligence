require 'net/sftp'

class FilesController < ApplicationController
  def index
    xls_route = Rails.configuration.route_to_xlsx_files

    if params[:file]
      file = "#{xls_route}/#{params[:file]}"
      return send_file file, type: 'image/xlsx', disposition: 'attachment'
    end

    if current_user.admin?
      @lost_products = Dir.glob("#{xls_route}/lost_products_*")
        .map{ |q| q.split('/').last }.sort

      @products_table = Dir.glob("#{xls_route}/products_table_*")
        .map{ |q| q.split('/').last }.sort

      @bad_eans = Dir.glob("#{xls_route}/bad_eans_products_table_*")
        .map{ |q| q.split('/').last }.sort

      @bad_prices = Dir.glob("#{xls_route}/bad_prices_report_*")
        .map{ |q| q.split('/').last }.sort

      acronym = "[^extended]*"
    else
      acronym = current_user.society.acronym&.upcase
    end

    @price_benchmark = Dir.glob("#{xls_route}/??????_price_benchmark_#{acronym}.xls?")
      .map{|q| q.split('/').last}.sort.reverse

    @price_benchmark_extended = Dir.glob("#{xls_route}/??????_price_benchmark_extended.xls?")
      .map{|q| q.split('/').last}.sort.reverse

    @price_benchmark_extended_multi = Dir.glob("#{xls_route}/??????-??????_price_benchmark_extended.xls?")
      .map{|q| q.split('/').last}.sort.reverse

    @price_var_report = Dir.glob("#{xls_route}/*_price_var_daily_report.xls?")
      .map{|q| q.split('/').last}.sort.reverse

    @top_sellers = Dir.glob("#{xls_route}/Stramina_????_??_??_MU.txt")
      .map{|q| q.split('/').last}.sort.reverse

    @daily_offers = Dir.glob("#{xls_route}/??????_daily_offers_report.xls?")
      .map{|q| q.split('/').last}.sort.reverse
  end

  def create
    if current_user.admin?
      Product.fill_specs(false) if params['fill_specs']

      Product.create_bad_ean_products_file if params['create_all'] || params['create_bad_ean_products_file']
      Product.create_lost_products_file    if params['create_all'] || params['create_lost_products_file']
      Product.create_products_file         if params['create_all'] || params['create_products_file']

      if params['create_all'] || params['create_bad_prices_file'] #params['low_price'], params['high_price']
        ProductPrice.create_bad_prices_file(params['low_price'] || 1900, params['high_price'] || 2000)
      end

      if params['create_daily_report']
        ProductPrice.report_fast(params['day_1'], params['day_2'], params['categories'], params['add_links'])
      end

      #ProductPrice.daily_report(params['date']) if params['create_all'] || params['create_daily_report']
      ProductPrice.var_daily_report(params['day_2'], params['day_1']) if params['create_var_prices_report']
      #ProductPrice.var_daily_report(params['date'], nil) if params['create_all'] || params['create_var_prices_report']

      TopSeller.create_report(params['day_1'], params['reduce_url']) if params['create_top_seller_report']

      ProductPrice.create_daily_offers_report(params['day_1'], params['categories']) if params['create_daily_offers_report']

      if params['scrapp'] && params['pages'] && params['categories']
        from_last_page = params['from_last_page'] ? true : false
        @scrapp_results = PageBase.scrapp_by(from_last_page, page: params['pages'], category: params['categories'])
      end

      if params['deep_scrapp'] && params['pages'] && params['categories'] && params['updatable_params']
        deep_scrapp_params = { page: params['pages'], category: params['categories'] }
        @deep_scrapp_info = PageBase.deep_scrapp_by(deep_scrapp_params, params['updatable_params'])
      end

      if params['clone_prices'] && params['pages'] && params['categories'] && params['day_1'] && params['day_2']
        if params['delete']
          ProductPrice.by_date(params['day_2'])
            .by_page(params['pages'])
            .by_category(params['categories'])
            .delete_all if params['delete']
        end
        @clone_prices_results = ProductPrice.clone_prices(params['day_1'], params['pages'], params['categories'], params['day_2'])
      end
    end

    index
    render 'index'
  end

  def upload
    if current_user.admin?
      if report = upload_params['report']
        begin
          SpreadsheetUploader.new.store!(report)
        rescue => e
          # Error storing file
          @errors = e.message
        end
      elsif top_sellers_report = upload_params['top_sellers_report']
        begin
          CsvUploader.new.store!(top_sellers_report)
        rescue => e
          # Error storing file
          @errors = e.message
        end
      else
        @errors = []
        if lost_products_files = upload_params['lost_products_files']
          lost_products_files.each  do |lost_products_file|
            @errors += Product.import_lost_products_file(lost_products_file)
          end
        end

        if products_files = upload_params['products_files']
          products_files.each  do |products_file|
            @errors += Product.import_products_file(products_file)
          end
        end

        if bad_prices_files = upload_params['bad_prices_files']
          bad_prices_files.each  do |bad_prices_file|
            @errors += ProductPrice.import_bad_prices_file(bad_prices_file)
          end
        end
      end

      @key = "errors_#{upload_params.keys.first}"
      respond_to do |format|
        format.js
      end
    end
  end

  def send_daily_reports
    if current_user.admin?
      begin
        # SendDailyReportsJob.perform_later
        # flash[:success] = I18n.t('messages.daily_reports.job_success')
        SendDailyReportsJob.send
        flash[:success] = I18n.t('messages.daily_reports.success')
      rescue => ex
        Rails.logger.error "ERROR [ #{ex.class}: #{ex.message} ]"
        flash[:error] = I18n.t('messages.daily_reports.error')
      end
    end
    redirect_to files_path
  end

  def send_top_seller_report
    if current_user.admin?
      begin
        ## TODO: Si problemas con Timeouts, ver: https://stackoverflow.com/questions/18718395/netsftp-connection-not-closing
        # begin
        #
        #   # Instance SSH/SFTP session :
        #   session = Net::SSH.start('host', 'user', password: 'pass', port: 22)
        #   sftp = Net::SFTP::Session.new(session)
        #
        #   # Always good to timeout :
        #   Timeout.timeout(10) do
        #     sftp.connect! # Establish connection
        #
        #     # Do stuff
        #
        #   end
        #
        # rescue Timeout::Error => e
        #
        #   # Do some custom logging
        #   puts e.message
        #
        # ensure
        #
        #   # Close SSH/SFTP session
        #   sftp.close_channel unless sftp.nil? # Close SFTP
        #   session.close unless session.nil? # Then SSH
        #
        #   # If you really really really wanna make sure it's closed,
        #   # and raise after 10 seconds delay
        #   Timeout.timeout(10) do
        #     sleep 1 until (sftp.nil? or sftp.closed?) and (session.nil? or session.closed?)
        #   end
        #
        # end
        Net::SFTP.start('myftp.crucial.com', 'CreativeSoapBox', :password => 'MicronBox!@#', port: 22, keys: "") do |sftp|
          file = "Stramina_#{Date.today.strftime("%Y_%m_%d")}_MU.txt"
          puts "***** #{file}"
          sftp.upload!("#{Rails.configuration.route_to_xlsx_files}/#{file}", "incoming/Stramina/#{file}")
        end
        flash[:success] = I18n.t('messages.top_sellers_report.success')
      rescue => ex
        Rails.logger.error "ERROR [ #{ex.class}: #{ex.message} ]"
        # Rails.logger.error "Backtrace:\n\t#{ex.backtrace.join("\n\t")}"
        flash[:error] = I18n.t('messages.top_sellers_report.error')
      end
    end
    redirect_to files_path
  end

  private

  def upload_params
    params.permit(
      :top_sellers_report, :report,
      :lost_products_files => [], :products_files => [], :bad_prices_files => []
    )
  end
end
