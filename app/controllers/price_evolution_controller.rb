class PriceEvolutionController < ApplicationController
  include ActionView::Helpers::NumberHelper
  include SearchableActions
  include ParamsUtils
  include ApplicationHelper

  before_action :read_params

  def index
    prepare_data
    respond_to do |format|
      format.html
      format.js { render 'global/index', locals: { index_folder: 'price_evolution',
                                                   templates: ['content'],
                                                   bind_elements: ['price_evolution_content'],
                                                   title: @title}}
      format.xlsx { export_summary }
    end
  end

  private

  def resource_model
    Product
  end

  def prepare_data
    @default_markets = pages_to_show&.keys
    @default_brands = brands_to_show_filtered
    @markets = @default_markets.blank? ? nil: @default_markets
    @brands = @default_brands.blank? ? nil: @default_brands

    data = DataGroupedCalculation
      .new(@start_date, @end_date, current_user.society, {
        category: @category,
        subcategory: @subcategory,
        page: @markets,
        brand: @brands,
        listed: @filter,
        min_price: @min_price,
        max_price: @max_price,
        specs: @specs,
        models: @models
      }).fetch_price_evolution

    filter_pages(data)
    filter_brands(data, references: @brands)
    filter_subcategories(data, @category)

    # benchamark and price_evolution share information
    @price_evolution = data['benchmark']
  end

  # Called by read_params
  def get_title
    @title = I18n.t('price_evolution.title', category: params[:category])
  end

  def export_summary
    workbook_name = "price_evolution_#{@category.parameterize.underscore}_summary.xlsx"
    tmp = Tempfile.new(['chart', '.png'])
    Utils.convert_data_url_to_image(params[:data_url], tmp.path)
    Axlsx::Package.new do |p|
      p.workbook.add_worksheet(:name => 'Summary') do |sheet|
        sheet.add_row [I18n.t('price_evolution.summary.brand'),
                       I18n.t('price_evolution.summary.asp'),
                       I18n.t('price_evolution.summary.price_index'),
                       I18n.t('price_evolution.summary.min'),
                       I18n.t('price_evolution.summary.max'),
                       I18n.t('price_evolution.summary.skus'),
                       I18n.t('price_evolution.summary.offers')], b: true
        if @price_evolution['brands'].keys.size > 1
          sheet.add_row [I18n.t('price_evolution.summary.market'),
                         number_with_precision(@price_evolution['average']['asp'], precision: 0),
                         100,
                         number_with_precision(@price_evolution['minima']['min'], precision: 0),
                         number_with_precision(@price_evolution['maxima']['max'], precision: 0),
                         @price_evolution['summations']['skus'],
                         @price_evolution['summations']['offers']],
                        types: [:string, :float, :integer, :float, :float, :integer, :integer] # :date, :time, :float, :integer, :string, :boolean
        end
        @price_evolution['brands'].keys.sort.each do |brand|
          sheet.add_row [brand,
                         number_with_precision(@price_evolution['brands'][brand]['asp'], precision: 0),
                         @price_evolution['brands'][brand]['price_index'],
                         number_with_precision(@price_evolution['brands'][brand]['min'], precision: 0),
                         number_with_precision(@price_evolution['brands'][brand]['max'], precision: 0),
                         @price_evolution['brands'][brand]['skus'],
                         @price_evolution['brands'][brand]['offers']],
                        types: [:string, :integer, :integer, :integer, :integer, :integer, :integer] # :date, :time, :float, :integer, :string, :boolean
        end
      end
      # Add chart
      p.workbook.add_worksheet(:name => I18n.t('benchmark.tabs.chart')) do |sheet|
        sheet.add_image(:image_src => tmp.path, :noSelect => true, :noMove => true) do |image|
          image.width = 1205
          image.height = 450
          image.start_at 0, 0
        end
        sheet.merge_cells("A1:Q19")
      end
      send_data p.to_stream.read,
                type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                filename: workbook_name
    end
  end
end