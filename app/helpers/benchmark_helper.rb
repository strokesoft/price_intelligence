module BenchmarkHelper
  def bubble_chart(data_source, options = {})
    chartkick_chart "BubbleChart", data_source, options
  end
end
