module CalculationsHelper
  include Caching
  MAX_PRICE = 60000

  def price_range(min_price, max_price)
    range = Product.default_price_range(min_price, max_price)
    range.blank? ? range : {'product_prices.price' => range}
  end

  # Get totals in markets and brands
  def get_totals_grouped(available:, start_date:, end_date:, pages:, brands:,
                         categories:, subcategories:, min_price:, max_price:,
                         specs:, models:)

    key = "totals-grouped-#{Utils.hashcode(available, start_date, end_date, pages, brands,
                                           categories, subcategories, min_price, max_price,
                                           specs, models)}"

    # Used in fetch_dashboard and fetch_price_categories
    fetch_caching_of('calculations', key) do
      where = {date: start_date..end_date}
      where.merge!(price_range(min_price, max_price))
      info = ProductPrice.stock(available).joins(:product_page)
               .merge(product_page_scope(available: available, pages: pages, brands: brands,
                                         categories: categories, subcategories: subcategories,
                                         specs: specs, models: models))
               .where(where)
      if (pages != nil || brands != nil)
        info = info.select('product_prices.product_page_id as product_page_id, product_pages.page as market, products.brand as brand')
        info = info.union(ProductPrice.stock(available).joins(:product_page)
                          .merge(product_page_scope(available: available, pages: nil, brands: brands,
                                                    categories: categories, subcategories: subcategories,
                                                    specs: specs, models: models).unless_page(pages))
                          .where(where)
                          .select("product_prices.product_page_id as product_page_id, product_pages.page as market, '#{Utils::STRING_TO_SEARCH}' as brand")
               ) if pages != nil
        info = info.union(ProductPrice.stock(available).joins(:product_page)
                          .merge(product_page_scope(available: available, pages: pages, brands: nil,
                                                    categories: categories, subcategories: subcategories,
                                                    specs: specs, models: models).unless_brand(brands))
                          .where(where)
                          .select("product_prices.product_page_id as product_page_id, '#{Utils::STRING_TO_SEARCH}' as market, products.brand as brand")
               ) if brands != nil
        info = info.select('COUNT(DISTINCT product_page_id) as count,
                        market, brand').group('market', 'brand').reorder(:count)
      else
        info = info.select('COUNT(DISTINCT product_prices.product_page_id) as count,
                        product_pages.page as market, products.brand').group('product_pages.page', 'products.brand').reorder(:count)
      end


      SqlLog.debug("get_totals_grouped ------------------------------------------------------------>\n #{info.to_sql}")
      info
    end
  end

  # Get period_average, previous_period_average y average_percentage by category
  def get_period_average_grouped(available:, start_date:, end_date:, previous_dates:,
                                 pages:, brands:, categories:, subcategories:)
    info = ProductPrice
             .stock_using_p(available)
             .from(ProductPrice.select("id, price, date, product_page_id, out_of_stock, case
                  when date >= '#{start_date.strftime("%Y-%m-%d")}' and date <= '#{end_date.strftime("%Y-%m-%d")}' then 'period_average'
                  when date >= '#{previous_dates.first.strftime("%Y-%m-%d")}' and date <= '#{previous_dates.last.strftime("%Y-%m-%d")}' then 'previous_period_average'
                  else 'other'
                  end as range").where('date' => previous_dates.first..end_date),
                   "p INNER JOIN product_pages ON product_pages.id = p.product_page_id")
             .merge(product_page_scope(available: available, pages: pages, brands: brands,
                                       categories: categories, subcategories: subcategories))
             .select('AVG(p.price) as average, products.category, p.range')
             .group('products.category', 'p.range').reorder(nil)

    SqlLog.debug("get_period_average_grouped ------------------------------------------------------>\n #{info.to_sql}")
    info
  end

  # Get period_average by category, pages and brands
  def get_only_period_average_grouped(available:, start_date:, end_date:, pages:, brands:, category:, subcategories:)
    groups = ['products.brand']
    select_string = 'AVG(price) as average, products.brand'
    if pages.present?
      groups << 'product_pages.page'
      select_string += ', product_pages.page'
    end

    info = ProductPrice.stock(available).joins(:product_page)
             .where(date: start_date..end_date)
             .merge(product_page_scope(available: available, pages: pages, brands: brands,
                                       categories: category, subcategories: subcategories))
             .select(select_string).group(*groups).reorder(nil)

    SqlLog.debug("get_only_period_average_grouped ------------------------------------------------->\n #{info.to_sql}")
    info
  end

  # Reusable query
  def general_query(groups:, select_string:, available:, start_date:, end_date:, pages:, brands:,
                    category:, subcategories:, min_price:, max_price:, specs:, models:)
    query = ProductPrice.stock(available).joins(:product_page)
              .merge(product_page_scope(available: available, pages: pages, brands: brands,
                                        categories: category, subcategories: subcategories,
                                        specs: specs, models: models))
              .select(select_string).reorder(nil)
    query = start_date.year == 1970 ? query.where("product_prices.date <= ?", end_date) : query.where(date: start_date..end_date)
    query = query.where(price_range(min_price, max_price)) if (min_price.to_i != 0 || max_price != CalculationsHelper::MAX_PRICE)
    query = query.group(*groups) unless groups.blank?
    query
  end

  def previous_dates(start_date, end_date)
    n_days = (end_date - start_date).to_i + 1

    # Yesterday or today
    if n_days == 1
      previous_start_date = start_date.yesterday
      previous_end_date   = end_date.yesterday
      #One week
    elsif n_days == 7
      previous_start_date = start_date - 7.days
      previous_end_date   = end_date - 7.days
    else
      # Month previous to current
      past_month = Date.today.month - 1
      if start_date.month == past_month
        days_in_month = Time.days_in_month(start_date.month, start_date.year)
        if days_in_month == n_days
          days_in_past_month = start_date.month == 1 ?
                                   31 : #si estamos en Enero devolvemos 31 que son los días de Diciembre.
                                   Time.days_in_month(start_date.month-1, start_date.year)
          previous_start_date = start_date - days_in_past_month.days
          previous_end_date   = end_date - days_in_month.days
        else
          previous_start_date = start_date - n_days.days
          previous_end_date   = end_date - n_days.days
        end
        # Last 30 days or custom range
      else
        previous_start_date = start_date - n_days.days
        previous_end_date   = end_date - n_days.days
      end
    end
    [previous_start_date, previous_end_date]
  end
  module_function :general_query, :price_range, :previous_dates

  # Get chart data
  def get_chart_data(available:, start_date:, end_date:, pages:, brands:, category:, subcategories:,
                     min_price:, max_price:, specs:, models:)
    groups = ['products.brand']
    select_string = "AVG(product_prices.price) as asp,
                     MIN(product_prices.price) as min,
                     MAX(product_prices.price) as max,
                     COUNT(DISTINCT product_pages.product_id) as skus,
                     COUNT(DISTINCT product_prices.product_page_id) as offers,
                     STRING_AGG(DISTINCT(product_pages.page), ',') as pages,
                     products.brand"

    info = general_query(groups: groups, select_string: select_string,
                         available: available, start_date: start_date, end_date: end_date,
                         pages: pages, brands: brands, category: category, subcategories: subcategories,
                         min_price: min_price, max_price: max_price, specs: specs, models: models)

    SqlLog.debug("get_chart_data ------------------------------------------------------------------>\n #{info.to_sql}")
    info
  end

  def get_product_ids(available:, start_date:, end_date:, pages:, brands:, category:, subcategories:, min_price:, max_price:, specs:, models:)
    groups = ['products.id']
    select_string = "products.id"

    info = general_query(groups: groups, select_string: select_string,
                         available: available, start_date: start_date, end_date: end_date,
                         pages: pages, brands: brands, category: category, subcategories: subcategories,
                         min_price: min_price, max_price: max_price, specs: specs, models: models)

    SqlLog.debug("get_product_ids ----------------------------------------------------------------->\n #{info.to_sql}")
    info
  end

  def get_stock_status_grouped(available: nil, pages:, brands:, categories:, subcategories:)
    #TODO: KKK ver si aquí queremos incluir productos con subtategoría nil o ''

    info = ProductPrice.joins(:product_page).stock(available)
             .merge(product_page_scope(available: available, pages: pages, brands: brands,
                                       categories: categories, subcategories: subcategories))
             .where('product_prices.date' => start_date..end_date)
             .select('COUNT(DISTINCT product_prices.product_page_id) as count, products.category')
             .group('products.category').reorder(:count)

    SqlLog.debug("get_stock_status_grouped (#{available ? 'available':'offered'}) ----------------->\n #{info.to_sql}")
    info
  end

  # Get products_in_stock by category
  def get_products_in_stock_grouped(available:, start_date:, end_date:, pages:, brands:,
                                    categories:, subcategories:, min_price:, max_price:, specs:, models:)

    key = "products-in-stock-grouped-#{Utils.hashcode(available, start_date, end_date, pages, brands,
                                                      categories, subcategories, min_price, max_price, specs, models)}"

    # Used in fetch_dashboard and fetch_price_categories
    fetch_caching_of('calculations', key) do
      info = ProductPrice.joins(:product_page).stock(available)
               .merge(product_page_scope(available: available, pages: pages, brands: brands,
                                         categories: categories, subcategories: subcategories,
                                         specs: specs, models: models)
                        .select('COUNT(product_pages.product_id) as count, products.category, products.subcategory')
                        .group('products.category', 'products.subcategory').reorder(:count))
               .where('product_prices.date' => start_date..end_date)
               .where(price_range(min_price, max_price))

      SqlLog.debug("get_products_in_stock_grouped ------------------------------------------------->\n #{info.to_sql}")
      info
    end
  end

  # Get new_products by category
  def get_new_products_grouped(available:, start_date:, end_date:, pages:, brands:, categories:, subcategories:)
    info = product_page_scope(available: available, pages: pages, brands: brands,
                              categories: categories, subcategories: subcategories)
             .where("DATE(products.created_at) BETWEEN ? AND ?", start_date, end_date)
             .select("COUNT(DISTINCT products.id) as count, STRING_AGG(DISTINCT products.id::character varying, ',') as product_ids, products.category")
             .group('products.category').reorder(nil)
    SqlLog.debug("get_new_products_grouped -------------------------------------------------------->\n #{info.to_sql}")
    info
  end

  def get_disappeared_products_grouped(available:, start_date:, end_date:, pages:, brands:, categories:, subcategories:)
    current_products_ids = product_page_scope(available: available, pages: pages, brands: brands,
                                              categories: categories, subcategories: subcategories)
                             .select('DISTINCT product_pages.product_id')
                             .where(date: start_date..end_date)
                             .to_sql
    disappeared_products = product_page_scope(available: available, pages: pages, brands: brands,
                                              categories: categories, subcategories: subcategories)
                             .select('DISTINCT product_pages.product_id, products.category')
                             .where(date: end_date - 1.day).where("product_id not in (#{current_products_ids})")
                             .to_sql
    info = ProductPage.select("count(A.product_id), STRING_AGG(DISTINCT A.product_id::character varying, ',') as product_ids, A.category").from("(#{disappeared_products}) A").group('A.category')

    SqlLog.debug("get_disappeared_products_grouped ------------------------------------------------>\n #{info.to_sql}")
    info
  end

  def get_products_prices_up_down_grouped(available: , start_date:, end_date:, previous_dates:,
                                          pages:, brands:, categories:, subcategories:)
    base = ProductPrice.stock(available).joins(:product_page)
             .where(date: previous_dates.first..end_date)
             .where("product_id is not null")
             .merge(product_page_scope(available: available, pages: pages, brands: brands,
                                       categories: categories, subcategories: subcategories))
             .select("AVG(price) AS avg_price_after, product_pages.product_id, product_pages.page, products.category,
                              case when product_prices.date >= '#{previous_dates.first}' and
                              product_prices.date <= '#{previous_dates.last}' then 'before'
                              else 'after' end as period")
             .group('product_pages.product_id, product_pages.page, products.category, period')
             .reorder('product_id, page, period desc')

    intermediate = Product.from("(#{base.to_sql}) as base").select("base.*,
                    avg_price_after - lag(avg_price_after) over (partition by product_id, page order by product_id, page, period desc) as increase,
                    count(product_id) over (partition by product_id, page order by product_id, page, period desc) as num")

    subquery = Product.from("(#{intermediate.to_sql}) as intermediate")
                 .select("CASE WHEN (increase > 0) THEN 'upper' else 'lower' END AS price_type,
	                       (avg_price_after - increase) as avg_price_before, *")
                 .where("intermediate.increase <> 0")
                 .order("product_id, page, period desc")

    results = Product.from("(#{subquery.to_sql}) as subquery")
                .select("subquery.category, subquery.price_type, count(distinct product_id) as count,
                         STRING_AGG(DISTINCT subquery.product_id::character varying, ',') AS product_ids,
                         AVG(avg_price_before) as avg_price_before, AVG(avg_price_after) as avg_price_after")
                .group("subquery.category, subquery.price_type")

    SqlLog.debug("get_products_prices_up_down_grouped --------------------------------------------->\n #{results.to_sql}")
    results
  end

  def get_products_prices_changed_grouped(available: , start_date:, end_date:, pages:, brands:, categories:, subcategories:)
    base = ProductPrice.stock(available).joins(:product_page)
      .where(date: start_date..end_date)
      .where('product_id is not null')
      .merge(product_page_scope(
        available: available, pages: pages, brands: brands,
        categories: categories, subcategories: subcategories)
      )
      .select('product_pages.product_id, product_pages.page, products.category, product_prices.price')
      .reorder('product_pages.product_id, product_pages.page, product_prices.price')

    intermediate = Product.from("(#{base.to_sql}) as base")
      .select('base.*, lag(price) over (partition by product_id, page order by product_id, page desc) as increase,
              count(product_id) over (partition by product_id, page order by product_id, page desc) as num')

    subquery = Product.select('*')
      .from("(#{intermediate.to_sql}) as intermediate")
      .where('intermediate.increase <> 0')
      .order('product_id, page')

    results = Product.from("(#{subquery.to_sql}) as subquery")
      .select("subquery.category, count(distinct product_id) as count,
               ARRAY_AGG(DISTINCT subquery.product_id) AS product_ids")
      .group("subquery.category")

    SqlLog.debug("get_products_prices_changed_grouped --------------------------------------------->\n #{results.to_sql}")
    results
  end

  def product_page_scope(available:, pages:, brands:, categories:, subcategories:, specs: nil, models: nil)
    pages = nil if Array(pages).size == Ecommerce.total
    brands = nil if Array(brands).size >= Brand.total
    ProductPage
      .only_usable.stock(available)
      .by_page(pages)
      .by_brand(brands)
      .by_category(categories)
      .by_subcategory(subcategories)
      .by_specs(categories, specs)
      .by_model(models)
  end
  module_function :product_page_scope
end
