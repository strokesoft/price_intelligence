module StatisticsHelper
  def deviation(a, b)
    ((a - b).to_f / b) * 100.round(2) rescue 0
  end

  alias_method :desviation, :deviation

  def level_by_deviation(deviation, suffix = nil)
    return '' if deviation == 0

    if deviation < -20
      level = "level3-#{suffix}down"
    elsif deviation < -10
      level = "level2-#{suffix}down"
    elsif deviation < 0
      level = "level1-#{suffix}down"
    elsif deviation <= 10
      level = "level1-#{suffix}up"
    elsif deviation <= 20
      level = "level2-#{suffix}up"
    elsif deviation > 20
      level = "level3-#{suffix}up"
    end
  end

  def level_by_variance(previous, current, suffix = nil)
    return '' if previous == 0
    return '' if (variance = (current - previous) / previous.to_f * 100.0) == 0

    if variance < -20
      level = "level3-#{suffix}up"
    elsif variance < -10
      level = "level2-#{suffix}up"
    elsif variance < 0
      level = "level1-#{suffix}up"
    elsif variance <= 10
      level = "level1-#{suffix}down"
    elsif variance <= 20
      level = "level2-#{suffix}down"
    elsif variance > 20
      level = "level3-#{suffix}down"
    end
  end
end
