module PublicHelper
  def is_popup?
    @mode == PublicController::POPUP
  end

  def is_popup_or_request_xhr?
    is_popup? || request.xhr?
  end

  def selected_brands(brands)
    brands.size > 1 ? t('global.selected_brands').upcase : brands.join(', ').upcase
  end
end
