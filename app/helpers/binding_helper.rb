module BindingHelper
  def add_bindings(data, prefix: nil)
    cdata = data.clone
    cdata.transform_keys!{ |key| "#{prefix}#{key}".parameterize }
    get_bindings.merge!(cdata)
  end

  def get_bindings
    @bindings ||= {}
  end
end
