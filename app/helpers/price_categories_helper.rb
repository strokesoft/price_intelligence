module PriceCategoriesHelper
  include StatisticsHelper

  def level_by_desviation(deviation)
    level_by_deviation(deviation)
  end

  def level_by_desviation_msrp(deviation)
    level_by_deviation(deviation, 'msrp-')
  end

  def pages_names
    Utils.available_pages.map{|market| [market, "#{PageBase.get_class(market)::PAGE_NAME}"]}.to_h
  end

  def price_for_market(prices_array, market)
    market_hash = prices_array.detect{|p| p[market]}
    market_hash[market][0] if market_hash
  end

  def desviation_for_market(prices_array, market)
    market_hash = prices_array.detect{|p| p[market]}
    market_hash[market][1] if market_hash
  end
end
