module FilesHelper
  def show_errors(errors)
    if errors.is_a?(Array)
      render partial: "global/errors_table", locals: { errors: errors }
    else
      errors
    end
  end
end
