module ChartHelper
  def bubble_data(raw_data)
    data = [[I18n.t('benchmark.summary.id'),
             I18n.t('benchmark.summary.skus'),
             I18n.t('benchmark.summary.asp'),
             I18n.t('benchmark.summary.brand'),
             I18n.t('benchmark.summary.offers')]]
    raw_data['brands'].each do |key, value|
      if value['skus'] > 0
        data << [key[0..2].upcase, value['skus'], number_with_precision(value['asp'], precision: 0).to_i, key, value['offers']]
      end
    end
    data
  end

  def bubble_options(raw_data)
    elements = raw_data['brands'].select{|_key, value| value['skus'] > 0}
    xmin, xmax = elements.map{|_k,v| v['skus']}.minmax
    ymin, ymax = elements.map{|_k,v| v['asp']}.minmax
    # Default values
    xmin ||= 0; xmax ||= 0;
    ymin ||= 0; ymax ||= 0;
    options = {
      is3D: true,
      explorer: {},
      series: {},
      bubble: {
        textStyle: {
          fontSize: 11
        }
      },
      sizeAxis: {minSize: 15, maxSize: 40},
      hAxis: {
        viewWindow: {min: xmin - 50, max: xmax + 50}
      },
      vAxis: {
        viewWindow: {min: ymin - 50, max: ymax + 50},
        format:'# €'
      }
    }
    raw_data['brands'].each do |key, _value|
      options[:series][key] = {color: Brand.map_brand_color[key]}
    end
    options
  end

  def price_evolution_options(raw_data)
    options = chart_options
    brands = raw_data['brands'].keys
    brands.each do |brand|
      options[:series][brand] = {pointShape: 'circle'}
      options[:colors] << Brand.map_brand_color[brand]
    end
    options
  end

  def price_evolution_data(raw_data)
    data = []
    brands = raw_data['brands'].keys
    unless brands.empty?
      labels = [{label: 'Date', type: 'date'}]
      brands.each do |brand|
        labels << {label: brand, id: brand, type:'number'}
        labels << {type: 'string', role: 'tooltip', 'p': {'html': true}}
      end
      data = [labels] #Description of columns
      dates = raw_data['chart_data'].pluck('date').uniq
      # Data
      dates.each do |date|
        records_by_date = raw_data['chart_data'].select {|v| v['date'] == date}
        asps = Utils.scope_to_hash(records_by_date, 'brand', 'asp', true)
        skus = Utils.scope_to_hash(records_by_date, 'brand', 'skus', true)
        offers = Utils.scope_to_hash(records_by_date, 'brand', 'offers', true)
        date_obj = Time.strptime(date, "%Y-%m-%d")
        row = ["script::new Date(#{date_obj.year}, #{date_obj.month - 1}, #{date_obj.day})::script"]
        brands.each do |brand|
          row << asps[brand]
          row << price_evolution_tooltip(brand, date, asps[brand], skus[brand], offers[brand])
        end
        data << row
      end
    end
    data
  end

  def product_evolution_data(raw_data)
    data = []
    pages = @prices.group_by(&:page).keys
    return data if pages.empty?

    labels = [{ label: 'Date', type: 'date' }]
    pages.each do |page|
      label = Ecommerce.map_ecommerce_name[page]
      labels << { label: label, id: page, type: 'number' }
      labels << { type: 'string', role: 'tooltip', 'p' => { 'html' => true } }
    end

    data = [labels] # Description of columns
    dates = raw_data.map { |e| e.date }.uniq

    # Data
    dates.each do |date|
      row = ["script::new Date(#{date.year}, #{date.month - 1}, #{date.day})::script"]
      prices = raw_data.select { |e| e.date == date }
        .reduce({}) { |prices, e| prices.update(e.page => e.price) }

      pages.each do |page|
        date_prices = prices.to_a.map { |e| [Ecommerce.map_ecommerce_name[e[0]], e[1]] }.sort_by { |e| e[0] }
        row << prices[page]
        row << product_evolution_tooltip(date, date_prices)
      end
      data << row
    end

    data
  end

  def product_evolution_options(raw_data)
    options = chart_options
    pages = @prices.group_by(&:page).keys
    pages.each do |page|
      options[:series][page] = { pointShape: 'circle' }
      options[:colors] << Ecommerce.map_page_color[page]
    end
    options
  end

  def chart_options
    {
      is3D: true,
      explorer: {},
      series: {},
      vAxis: { format:'# €' },
      hAxis: { format: 'd, MMM' },
      tooltip: { isHtml: true },
      pointSize: 8,
      colors: [],
      table: {
        sortAscending: true,
        sortColumn: 1
      }
    }
  end

  def render_chart(data, type:, partial:, options: {}, id: 'chart-1', width: '100%',  height: '450px',
                   title: nil, h_axis_title: nil, v_axis_title: nil)
    options[:title] = title unless title.blank?
    options[:chartArea] = {width: '78%', height: '80%'}

    unless h_axis_title.blank?
      options[:hAxis] = {} if options[:hAxis].nil?
      options[:hAxis].merge!({title: h_axis_title})
    end
    unless v_axis_title.blank?
      options[:vAxis] = {} if options[:vAxis].nil?
      options[:vAxis].merge!({title: v_axis_title})
    end
    render partial: partial, locals: {
      type: type,
      data: data,
      options: options,
      id: id, width: width, height: height
    }
  end

  private

  def price_evolution_tooltip(brand, date, asp, skus, offers)
    "script::pet('#{brand}', '#{format_sdate(date, format: '%-d %b %y')}', '#{basic_price(asp)}', '#{skus}', '#{offers}')::script"
  end

  def product_evolution_tooltip(date, prices)
    unscaped_prices = prices.to_s.gsub('"', "'")
    min = prices.map { |e| e[1] }.min
    max = prices.map { |e| e[1] }.max
    "script::product_et('#{date.strftime('%-d %b')}', #{unscaped_prices}, #{min}, #{max})::script"
  end
end
