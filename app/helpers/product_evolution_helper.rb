module ProductEvolutionHelper
  def is_popup?
    @mode == PublicController::POPUP
  end

  def is_popup_or_request_xhr?
    is_popup? || request.xhr?
  end
end
