module ApplicationHelper
  include Caching
  include AlertifyjsHelper
  include ChartHelper
  include BindingHelper
  include PriceCategoriesHelper

  def logged_in?
    !current_user.nil?
  end

  def remote_request(type, path, params={}, target_tag_id)
  #  "$.#{type}('#{path}',
  #           {#{params.collect { |p| "#{p[0]}: #{p[1]}" }.join(", ")}},
  #           function(data) {alert(data); $('##{target_tag_id}').html(data);}
  # );"
    "$.ajax({
        async: true,
        type: '#{type}',
        url:  '#{path}',
        data: {#{params.collect { |p| "#{p[0]}: #{p[1]}" }.join(", ")}},
        dataType: 'html',
        success: function(data, textStatus, jqXHR) {
          $('##{target_tag_id}').html(data);
          $('##{target_tag_id}').trigger('change');
        }
      });
    "
  end

  def brand_logo(brand, logo, fallback = true)
    if !fallback && logo.include?('missing')
      content_tag(:span, brand, class: 'brand-text')
    else
      image_tag(logo, alt: brand, title: brand, class: 'logo')
    end
  end

  def default_if_zero(value, check: value, default: nil)
    (check == 0 || check.nil?) ? default : value
  end

  def current_filters(options = {})
    { start_date: @start_date,
      end_date: @end_date,
      page: @markets,
      brand: @brands,
      category: @category,
      subcategory: @subcategory,
      min_price: @min_price,
      max_price: @max_price,
      specs: @specs,
      model: @models,
      type: @type,
      option: params[:option] }.merge(options)
  end

  def current_params(options = {})
    keys = request.params.keys
    if keys.include?('page') && !keys.include?('markets')
      request.params.merge!(markets: request.params['page']).delete(:page)
    end
    request.params.merge!(options)
  end

  def ensure_image_url(url)
    url&.start_with?('http') ? url : asset_url('fallback/missing_photo.jpg')
  end

  def symmetrical_value(keys, values, key)
    idx = keys.index(key)
    idx.nil? ? nil : values[idx]
  end

  def basic_price(price, precision: 0)
    meaningless_zero = "#{I18n.t('number.format.separator')}00"
    number_to_currency(price, unit: "€", precision: precision, format: "%n %u").remove(meaningless_zero) unless price.blank?
  end

  def remove_zero_decimals(number)
    number.to_s.gsub(/\.0{1,2}$/, '')
  end

  def remove_zeros_options(options, category)
    Array(options).map{|option| [remove_zero_decimals(option), option, {id: Utils.hashcode("spec-#{category}", option)}]}
  end

  def format_sdate(sdate, format: "%d/%m/%Y")
    Time.strptime(sdate, "%Y-%m-%d").strftime(format) unless sdate.blank?
  end

  def format_date(date, format: "%d/%m/%Y")
    date.strftime(format) if date.present?
  end

  def params_search
    params.permit(:page, :search, :order, :direction).reject{|_, v| v.blank?}
  end

  def pretty_print_json(object, highlight_keys: true, separator: '<br/>')
    object = JSON.parse(object) if object.is_a?(String)
    start_tag, end_tag = highlight_keys ? ['<span class="weight-600">', '</span>'] : nil
    object.inject([]) do |array, (k, v)|
      array << "#{k}: #{start_tag}#{v.to_s.gsub(/\.0$/, '')}#{end_tag}"
    end.sort.join(separator).html_safe
  end

  def sort_link(column, title: nil, remote: false, target: nil)
    title ||= @resource_name.classify.constantize.human_attribute_name(column)
    direction = column == @current_order && @current_direction == 'asc' ? 'desc' : 'asc'
    icon = @current_direction == 'asc' ? 'la la-chevron-up' : 'la la-chevron-down'
    icon = column == @current_order ? icon : ''
    link_to("#{title} <i class='#{icon}'></i>".html_safe,
            url_for(current_params({ order: column, direction: direction, format: :html })),
            title: I18n.t('messages.sort_by', column: title.downcase, direction: I18n.t("messages.directions.#{direction}").downcase),
            remote: remote,
            data: {target: target},
            class: "i-link")
  end

  def available_elements_by(element_type, field:)
    variable_name = "@#{element_type}_#{field}".to_sym
    value = instance_variable_get(variable_name)
    return value if value.present?

    value = available(element_type).order(:name).pluck(field)
    instance_variable_set(variable_name, value)
  end

  def show_current_filters
    filters = %w(date_range markets brands category subcategory price_range models availability specs)
    data = []
    filters.each do |filter|
      legend = I18n.t(filter, scope: 'global.filters').capitalize
      case filter
        when 'date_range'
          data << {legend: legend, values: "#{format_date(@start_date)} - #{format_date(@end_date)}"}
        when 'brands'
          brands = @brands || @default_brands
          is_default = false
          if @brands.blank? && !@default_brands.blank?
            is_default = true
            legend = I18n.t('default_brands', scope: 'global.filters').capitalize
          end
          if brands.present?
            info = {legend: legend, values: Array(brands)}
            info[:uncheck_prefix] = 'brand' unless is_default
            data << info
          end
        when 'markets'
          markets = @markets || @default_markets
          is_default = false
          if @markets.blank? && !@default_markets.blank?
            is_default = true
            legend = I18n.t('default_markets', scope: 'global.filters').capitalize
          end
          if markets.present?
            values = Array(markets).map{|market| Ecommerce.map_ecommerce_name[market] }
            info = {legend: legend, values: values}
            info[:uncheck_prefix] = 'market' unless is_default
            data << info
          end
        when 'price_range'
          range = Product.default_price_range(@min_price, @max_price)
          data << {legend: legend, ref: '.price-range',
                   values: "#{basic_price(range.begin)} - #{basic_price(range.last)}"} unless range.blank?
        when 'availability'
          data << {legend: legend, values: I18n.t(@filter, scope: 'global.filters')}
        when 'specs'
          @specs.keys.each do |key|
            data << {legend: Product.map_specs_keys[@category][key].capitalize,
                      values: @specs[key],
                      uncheck_prefix: "spec-#{@category}"}
          end if @specs
        when 'models'
          data << {legend: legend,
                   values: Product.where(id: @models).pluck(:name),
                   data: @models,
                   ref: "#models"} if @models
        else
          values = instance_variable_get("@#{filter}")
          info = {legend: legend, values: values}
          info[:uncheck_prefix] = filter.singularize if filter != 'category'
          data << info unless values.blank?
      end
    end
    html_current_filters(data) unless data.empty?
  end

  def print_checked?(type, value)
    default = type.to_s == 'page' ? 'market' : type
    default_values = instance_variable_get("@default_#{default.to_s.pluralize}") || []
    'checked' if value == params[type] || default_values.include?(value)
  end

  private
  def html_current_filters(data)
    removables = 0
    elements = data.map do |element|
      element_values = if element[:uncheck_prefix] || element[:ref]
                         idx = -1
                         Array(element[:values]).map do |e|
                           idx += 1
                           removables += 1
                           data_value = element[:data] ? element[:data][idx] : nil
                           content_tag(:span, e, class: 'uncheck-element',
                                       title: I18n.t('global.remove_filter'),
                                       data: {value: data_value, ref: element[:ref] || "##{Utils.hashcode(element[:uncheck_prefix], e)}"})
                         end
                       else
                         Array(element[:values])
                       end
      "<span class='bold'>#{element[:legend]}</span>: #{element_values.join(', ')}"
    end.join("</li><li>")

    remove_filter = "<i class='ml-1 fa fa-remove reset-form cursor-pointer text-muted' title='#{I18n.t('global.reset_filters')}'></i>"
    output = "<div class='mb-2'><mark class='d-inline-block px-2 py-0'>#{I18n.t('global.current_filters').upcase}</mark>"
    output += remove_filter if removables > 0
    output += "<ul class='d-inline current-filters'><li>"
    output += elements
    output += "</li></ul></div>"
    output.html_safe
  end

end
