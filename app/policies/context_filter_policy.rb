class ContextFilterPolicy < ApplicationPolicy
  def filter?
    user.admin? || %i(root_categories subcategories ecommerces brands specs).none? do |key|
      !record.filter[key].blank? && not_available_key?(key)
    end
  end

  def not_available_key?(key)
    case key
      when :specs
        !user.has_filter_by_specs?
      else
        not_in_availables?(key)
    end
  end

  def not_in_availables?(key)
    method = (key != :ecommerces) ? 'available_names' : 'available_slugs'
    words = Array(record.filter[key])
    (user.send(method, key) & words).size != words.size
  end
end