class ApplicationMailer < ActionMailer::Base
  include Roadie::Rails::Automatic
  add_template_helper(ApplicationHelper)
  add_template_helper(Rails.application.routes.url_helpers)

  # Roadie issue https://github.com/Mange/roadie-rails#known-issues
  self.asset_host = nil

  default from: Settings.env.action_mailer.default_from
  layout 'mailer'

  alias_method :original_mail, :mail

  protected

  def roadie_options
    super.merge(url_options: {
      protocol: Settings.env.app.protocol,
      host: Settings.env.app.domain,
      port: Settings.env.app.port
    }, asset_providers: Roadie::FilesystemProvider.new(Rails.root.join("app", "assets", "stylesheets")) )
  end

  def mail(params)
    I18n.locale = :en
    attachments.inline["logo.png"] = File.read("#{Rails.root}/app/assets/images/logo_priceintelligence.png")
    original_mail params
  end
end

