class DailyReport < ApplicationMailer
  def daily(day, month, year)
    I18n.locale = :es
    file_date = [year[2..3], month, day].join()
    attachments["#{file_date}_price_benchmark.xlsx"] = File.read("#{Rails.configuration.route_to_xlsx_files}/#{file_date}_price_benchmark.xlsx")
    to  = Settings.env.mailing.daily_report.daily.to
    bcc = Settings.env.mailing.daily_report.daily.bcc
    mail to:            to,
         bcc:           bcc,
         subject:       "Price Benchmark #{day} #{I18n.t("date.month_names")[month.to_i].camelcase}",
         template_path: 'mails/daily_report'
  end

  def daily_dashboard(user:, dashboard:, start_date:, end_date:, markets:, brands:)
    @user = user
    @dashboard = dashboard
    @start_date = start_date
    @end_date = end_date
    @markets = markets
    @brands = brands
    params = {
      to: @user.email,
      subject: I18n.t('mailer.daily_report.daily_dashboard.subject', date: Date.today)
    }
    mail params
  end
end
