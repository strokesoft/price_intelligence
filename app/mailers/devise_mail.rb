class DeviseMail < Devise::Mailer

  helper :application
  include Devise::Controllers::UrlHelpers
  default template_path: 'devise/mailer'
  include Devise::Mailers::Helpers

  def confirmation_instructions(record, opts={})
    devise_mail(record, :confirmation_instructions, opts={})
  end

  def reset_password_instructions(record)
    devise_mail(record, :reset_password_instructions, opts={})
  end

  def unlock_instructions(record)
    devise_mail(record, :unlock_instructions, opts={})
  end

  def headers_for(actions, opts={})
    # see http://stackoverflow.com/a/14698599/18706
  end

end