cp dev_env/docker/docker-compose.yml docker-compose.yml
cp dev_env/docker/Dockerfile Dockerfile
cp dev_env/docker/config/database.yml config/database.yml
cp dev_env/docker/scripts/*.sh .

#cp .env.local.sample .env

cp config/settings/app.yml.sample config/settings/app.yml

mkdir -p dev_env/backups
mkdir -p dev_env/postgres-data
mkdir -p dev_env/redis-data


docker-compose build --no-cache
# See: https://github.com/vishnubob/wait-for-it
docker-compose run web bash -c "./dev_env/wait-for-it.sh db:5432 -t 0 -- rake db:create; rake db:migrate; rake db:fixtures:load; rake db:seed"
docker-compose up &
